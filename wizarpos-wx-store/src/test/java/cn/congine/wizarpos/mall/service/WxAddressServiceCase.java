package cn.congine.wizarpos.mall.service;

import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import cn.congine.wizarpos.mall.model.WxAddress;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/applicationContext-test.xml" })
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
public class WxAddressServiceCase {

	@Autowired
	private WxAddressService wxAddressService;
	
	
	@Test
	public void testCase(){
		WxAddress wa = new WxAddress();
		wa.setId(UUID.randomUUID().toString());
		wa.setCardId("00001");
		wa.setDefaultState("0");
		wa.setAddress("中国上海北京东路666号科技京城B区707室22");
		wa.setPhone("13987654321");
		wa.setUsername("李洲1");
		
		 wxAddressService.save(wa);
		
		//WxAddress wa3 = wxAddressService.getDefaultWxAddress("00001");
		//System.out.println(wa3.getId()+"|"+wa3.getAddress());
		
		//wa3.setUserName("李洲2");
		//wxAddressService.update(wa3);
		
		
	}
	
}
