package cn.congine.wizarpos.mall.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cn.congine.wizarpos.mall.model.ErpProduct;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/applicationContext-test.xml" })
public class ErpProductServiceCase {
	
	@Autowired
	private ErpProductService erpProductService;
	
	@Test
	public void testCase(){
		ErpProduct ep = erpProductService.getErpProductById("999888");
		System.out.println(ep.getName());
	}

}
