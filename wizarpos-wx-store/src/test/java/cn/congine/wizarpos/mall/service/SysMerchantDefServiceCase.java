package cn.congine.wizarpos.mall.service;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cn.congine.wizarpos.mall.model.SysMerchantDef;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/applicationContext-test.xml" })
public class SysMerchantDefServiceCase {

	@Resource(type = SysMerchantDefService.class)
	private SysMerchantDefService sysMerchantDefService;
	
	@Test
	public void testCase(){
		SysMerchantDef smd = sysMerchantDefService.getSysMerchantDefByMid("100005100000001");
		System.out.println(smd.getMerchantName());
	}
/*	@Test
	public void testCase1(){
		SysMerchantDef smd1= sysMerchantDefService.getSysMerchantDefByMid("100005100000002");
		System.out.println(smd1.getMerchantName());
	}*/
}
