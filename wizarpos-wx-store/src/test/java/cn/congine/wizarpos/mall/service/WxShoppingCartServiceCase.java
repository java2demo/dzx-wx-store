
//package cn.congine.wizarpos.mall.service;
//
//import java.util.Date;
//import java.util.UUID;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.test.context.transaction.TransactionConfiguration;
//
//import cn.congine.wizarpos.mall.model.WxShoppingCart;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations = { "classpath:/applicationContext-test.xml" })
//@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
//public class WxShoppingCartServiceCase {
//
//	@Autowired
//	private WxShoppingCartService wxShoppingCartService;
//	
//	//@Test
//	public void testCase(){
//		WxShoppingCart wx = wxShoppingCartService.getWxShoppingCartByOpenId("0001");
//		System.out.println(wx.getmId());
//		
//	}
//	@Test
//	public void testCase1(){
//		WxShoppingCart obj = new WxShoppingCart();
//		obj.setOpenId("0003");
//		obj.setCartType("A");
//		obj.setCreateTime(System.currentTimeMillis());
//		obj.setId(UUID.randomUUID().toString());
//		obj.setmId("3333");
//		wxShoppingCartService.save(obj);
//	}
//}
package cn.congine.wizarpos.mall.service;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/applicationContext-test.xml" })
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
public class WxShoppingCartServiceCase {

//	@Autowired
//	private WxShoppingCartService wxShoppingCartService;
//	
//	//@Test
//	public void testCase(){
////		WxShoppingCart wx = wxShoppingCartService.getWxShoppingCartByOpenId("0001");
////		System.out.println(wx.getMId());
//		
//	}
//	@Test
//	public void testCase1(){
//		WxShoppingCart obj = new WxShoppingCart();
//		obj.setOpenId("0003");
//		obj.setCartType("A");
//		obj.setCreateTime(System.currentTimeMillis());
//		obj.setId(UUID.randomUUID().toString());
//		obj.setMId("3333");
//		wxShoppingCartService.save(obj);
//	}
}
