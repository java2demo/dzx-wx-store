package cn.congine.wizarpos.mall.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cn.congine.wizarpos.mall.model.ErpProductCategory;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/applicationContext-test.xml" })
public class ErpProductCategoryServiceCase {

	@Autowired
	private ErpProductCategoryService erpProductCategoryService;
	
	@Test
	public void testCase(){
		ErpProductCategory ep = erpProductCategoryService.getErpProductCategoryByCode("100105100000001","0102");
		System.out.println(ep.getName());
		System.out.println(ep.getMid());
	}
	
}
