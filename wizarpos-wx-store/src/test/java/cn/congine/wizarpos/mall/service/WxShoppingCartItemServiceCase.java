package cn.congine.wizarpos.mall.service;

import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import cn.congine.wizarpos.mall.model.WxShoppingCartItem;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/applicationContext-test.xml" })
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
public class WxShoppingCartItemServiceCase {
	@Autowired
	private WxShoppingCartItemService wxShoppingCartItemService;
	
	@Test
	public void test(){
		WxShoppingCartItem obj = testGet();
		System.out.println(obj.getProductNum());
		obj.setProductNum(5);
		wxShoppingCartItemService.update(obj);
	}
	
	//@Test
	public void testCaseA(){
		WxShoppingCartItem obj = new WxShoppingCartItem();
		obj.setId(UUID.randomUUID().toString());
		obj.setCartId("0001");
		obj.setProductId(UUID.randomUUID().toString());
		obj.setProductNum(3);
		obj.setProductPrice(4000);
		
		wxShoppingCartItemService.save(obj);
		
	}
	
	//@Test
//	public void testCase2(){
//		List<WxShoppingCartItem> list = wxShoppingCartItemService.getWxShoppingCartItemListByCartId("0001");
//		System.out.println(list.size());
//		for (WxShoppingCartItem wst : list) {
//			System.out.println(wst.getProductNum());
//		}
//	}
	
	//@Test
	public void testCaseB(){
		wxShoppingCartItemService.removeByCartId("100105100000001","0001");
	}
	
	public WxShoppingCartItem testGet(){
		return wxShoppingCartItemService.getWxShoppingCartItemById("1e9ea8b0-4d1b-4044-b3ab-96c51f89683d");
	}
}
