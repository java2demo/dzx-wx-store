package cn.congine.wizarpos.mall.service;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cn.congine.wizarpos.mall.model.WxOrder;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/applicationContext-test.xml" })
public class WxOrderServiceCase {

	@Autowired
	private WxOrderService wxOrderService;
	
	@Test
	public void testCase(){
		List<WxOrder> list =  wxOrderService.getWxOrderListByMid("000001");
		System.out.println(list.get(0).getAddressId());
		List<WxOrder> list2 =  wxOrderService.getWxOrderListByOpenId("00001");
		System.out.println(list2.get(0).getAddressId());
	}
}
