/**
* 详情说明显示与隐藏
*/
function togg(id1,id2) {//id1<a>id,用来切换图标，id2表示显示与隐藏内容的id
	if($("#"+id2).css('display') == 'none') {
		$(".vdi").css('display','none');
		$(".adi").addClass("close");
		$("#"+id2).css('display','block');
		$("#"+id1).removeClass("close");
	}else {
		$("#"+id2).css('display','none');
		$("#"+id1).addClass("close");
	}
}

var  y1,y2,ny=0,rotYINT;

function rotateYDIV() {
	y1=document.getElementById("rotatey1");
	y2=document.getElementById("rotatey2");
	clearInterval(rotYINT) ;
	rotYINT=setInterval("startYRotate()",0);
	
}

function startYRotate() {
	ny += 1;
	if(ny == 90 && $(y1).css('display') =='block') {
		$(y1).css('display','none');
		$(y2).css('display','block');
		ny += 180;		
	}
	if(ny == 90 && $(y2).css('display') =='block') {
		$(y2).css('display','none');
		$(y1).css('display','block');
		ny += 180;		
	}
	if($(y1).css('display') == 'block') {
		y1.style.transform="rotateY(" + ny + "deg)";		
		y1.style.webkitTransform="rotateY(" + ny + "deg)";
		y1.style.OTransform="rotateY(" + ny + "deg)";
		y1.style.MozTransform="rotateY(" + ny + "deg)";
	}else {
		y2.style.transform="rotateY(" + ny+ "deg)";		
		y2.style.webkitTransform="rotateY(" + ny + "deg)";
		y2.style.OTransform="rotateY(" + ny + "deg)";
		y2.style.MozTransform="rotateY(" + ny +  "deg)";
	}
	if (ny==180 || ny>=360) {
		clearInterval(rotYINT);
		if (ny>=360) {
			ny=0;
		}
	}
}

function toggleSearchPanel(){
	if($('.search_panel').css('display') =='block') {
	    $('.search_panel').css('left',$(window).width()+'px').hide();
	}
	else{
		$('.search_panel').show().css('left',0);
	}
	
}

