var Order = {
		    noWeixinMsg:'请到微信客户端完成支付',
			payOrder : function(oid){
	    	if(isWeixn()){
	    		if(!WeixinJSBridge) return;
	    		ajaxGet(basePath+'/tenpay/messageNative.ajax?orderId='+oid,function(data){
	    			if(data.code == 0){
	    				$("#code_url").attr("src",data.obj);
	    			}else{
	    				Dialog.alert(data.message); 
	    			}
				});
	    	}else{
	    		Dialog.alert(Order.noWeixinMsg);
	    	}
	    },
	    checkOrderPayStatus : function(oid){
	    	ajaxGet(basePath+'/tenpay/status.ajax?orderId='+oid,function(payStatus){
	    		if(payStatus == "0"){
	    			setTimeout(function(){Order.checkOrderPayStatus(oid);},500);
	    		}
	    		else if(payStatus == "1"){
	    			//跳转支付成功页面
	    			window.location.replace(basePath+"/wx_order_detail/get_order_detail?orderId="+oid);
	    			}
	    		else{
	    			Dialog.alert(payStatus); 
	    		}
	    	});
	    }
	};

function isWeixn(){
	var ua = navigator.userAgent.toLowerCase();
	if(ua.match(/MicroMessenger/i)=="micromessenger") {
	    return true;
	} else {
	    return false;
	}
}

function ajaxGet(url,callback){
	$.ajax({
		type : "GET",
		url : url,
		dataType : 'json',
		cache : false,
		success : function(data) {
			callback(data);
		},
		error : function(xhr, status) {
			//_ajaxErrorHandler(xhr);
		}
	});
}
function ajaxPost(url,data,callback){
	$.ajax({
		type : "POST",
		url : url,
		data : JSON.stringify(data),
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		success : function(data) {
			callback(data);
		},
		error : function(xhr, status) {
			//_ajaxErrorHandler(xhr);
		}
	});
}
