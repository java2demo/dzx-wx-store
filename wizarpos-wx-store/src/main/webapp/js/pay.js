var Order = {
		    noWeixinMsg:'请到微信客户端完成支付',
			payOrder : function(oid, mid){
	    	if(isWeixn()){
	    		if(!WeixinJSBridge) return;
	    		ajaxGet(basePath+'/tenpay/message.ajax?mid='+ mid + '&orderId='+oid,function(data){
	    			if(data.code == 0){
	    				WeixinJSBridge.invoke('getBrandWCPayRequest',{
	                        "appId" : data.obj.appId, //公众号名称，由商户传入
	                        "timeStamp" : data.obj.timeStamp, //时间戳
	                        "nonceStr" : data.obj.nonceStr, //随机串
	                        "package" : data.obj.paymentPackage,//v2扩展包,v3微信生成的预支付ID
	                        "signType" : data.obj.signType, //微信签名方式:1.sha1
	                        "paySign" : data.obj.paySign //微信签名
	                        },function(res){
	                        	if(res.err_msg == "get_brand_wcpay_request:ok" ) {
		                        	Order.checkOrderPayStatus(mid, oid);
		                        }
		                        else if(res.err_msg == "get_brand_wcpay_request:fail"){
		                        	Dialog.alert("支付发送失败");
		                        	window.location.replace(basePath+"/wx_order_detail/get_order_detail?mid="+mid+"&orderId="+oid);
		                        }
		                        else if(res.err_msg == "get_brand_wcpay_request:cancel"){
		                        	Dialog.alert("已取消支付发送");
		                        	window.location.replace(basePath+"/wx_order_detail/get_order_detail?mid="+mid+"&orderId="+oid);
		                        }
		                        else{
		                        	Dialog.alert("支付访问被拒绝:商户设置错误");
		                        	window.location.replace(basePath+"/wx_order_detail/get_order_detail?mid="+mid+"&orderId="+oid);
		                        }
		                }); 
	    			}else{
	    				Dialog.alert(data.message); 
	    			}
				});
	    	}else{
	    		Dialog.alert(Order.noWeixinMsg);
	    	}
	    },
	    checkOrderPayStatus : function(mid, oid){
	    	ajaxGet(basePath+'/tenpay/status.ajax?mid='+mid+'&orderId='+oid,function(payStatus){
	    		if(payStatus == "0"){
	    			setTimeout(function(){Order.checkOrderPayStatus(mid, oid);},500);
	    		}
	    		else if(payStatus == "1"){
	    			//跳转支付成功页面
	    			window.location.replace(basePath+"/wx_order_detail/get_order_detail?mid="+mid+"&orderId="+oid);
	    			}
	    		else{
	    			Dialog.alert(payStatus); 
	    		}
	    	});
	    }
	};

function isWeixn(){
	var ua = navigator.userAgent.toLowerCase();
	if(ua.match(/MicroMessenger/i)=="micromessenger") {
	    return true;
	} else {
	    return false;
	}
}

function ajaxGet(url,callback){
	$.ajax({
		type : "GET",
		url : url,
		dataType : 'json',
		cache : false,
		success : function(data) {
			callback(data);
		},
		error : function(xhr, status) {
			//_ajaxErrorHandler(xhr);
		}
	});
}
function ajaxPost(url,data,callback){
	$.ajax({
		type : "POST",
		url : url,
		data : JSON.stringify(data),
		dataType : "json",
		contentType : "application/json; charset=utf-8",
		success : function(data) {
			callback(data);
		},
		error : function(xhr, status) {
			//_ajaxErrorHandler(xhr);
		}
	});
}
