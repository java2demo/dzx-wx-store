/**
 * Property Editor - jQuery EasyUI Extension
 * 
 * Copyright (c) 2009-2013 Rime Lee. All rights reserved.
 */
(function($) {
	$.fn.joyeditor = function(options, param) {
		if (typeof options == 'string') {
			var method = $.fn.joyeditor.methods[options];
			if (method) {
				return method(this, param);
			} else {
				return this.propertygrid(options, param);
			}
		}

		options = options || {};
		return this.each(function() {
			var state = $.data(this, 'joyeditor');
			if (state) {
				$.extend(state.options, options);
			} else {
				var opts = $.extend({}, $.fn.joyeditor.defaults, options);
				$.data(this, 'joyeditor', {
					options : opts
				});
			}
			buildForm(this);
		});
	};

	function buildForm(target) {
		var opts = $.data(target, 'joyeditor').options;
		$(target).propertygrid($.extend({}, opts, {
			loader : function(param, success, error) {
				$.ajax({
					type: "GET",
					url: opts.url,
					data: opts.queryParams,
					dataType: 'json',
					success: function(data){
						success(data);
					},
					error: function(){
						error.apply(this, arguments);
					}
				});
			},
			onAfterEdit : function(rowIndex, rowData, changes) {
				if (changes.value != undefined) {
					var btns = getToolbarBtns(target);
					$(btns[1]).linkbutton('enable');
				}
			},
			onBeforeEdit : function(rowIndex, rowData) {
				return opts.editable;
			}
		}));
		var btns = getToolbarBtns(target);
		$(btns[0]).bind('click', function() {
			if (opts.editable) {
				opts.editable = false;
				applyChanges(target, false);
				$(this).linkbutton({
					iconCls : 'icon-edit',
					text : '编辑'
				});
				$(btns[1]).linkbutton('disable');
			} else {
				opts.editable = true;
				$(this).linkbutton({
					iconCls : 'icon-cancel',
					text : '取消'
				});
			}
		});
		$(btns[1]).bind('click', function() {
			var disabled = $(this).linkbutton('options').disabled;
			if (!disabled && opts.onSave) {
				applyChanges(target, true);
				var data = $(target).propertygrid('getData');
				if (opts.onSave.call(this, data)) {
					$(this).linkbutton('disable');
					opts.editable = false;
					$(btns[0]).linkbutton({
						iconCls : 'icon-edit',
						text : '编辑'
					});
				}
			}
		});
	}

	function applyChanges(target, accept) {
		if (accept) {
			$(target).propertygrid('acceptChanges');
		} else {
			$(target).propertygrid('rejectChanges');
		}
		var gridopts = $.data(target, 'propertygrid').options;
		if (gridopts) {
			gridopts.editIndex = undefined;
		}
	}

	function getToolbarBtns(target) {
		return $(target).datagrid('getPanel').find('div.datagrid-toolbar a');
	}

	function formatter(value, row, index) {
		if (row.editor.type == 'combobox') {
			var items = row.editor.options['data'];
			for ( var i = 0; i < items.length; i++) {
				if (value == items[i].value) {
					return items[i].text;
				}
			}
			return '';
		} else if (row.editor.type == 'checkbox') {
			if (value == row.editor.options['on']) {
				return (row.editor.options['yes'] ? row.editor.options['yes']
						: "<div class='icon-ok' style='height:16px;width:16px;display:inline-block'></div>");
			} else {
				return (row.editor.options['no'] ? row.editor.options['no']
						: "");
			}
		} else {
			return value;
		}
	}

	$.fn.joyeditor.methods = {
		options : function(jq) {
			return $.data(jq[0], 'joyeditor').options;
		},
		load : function(jq, param) {
			return jq.each(function() {
				if (param) {
					opts = $.data(this, 'joyeditor').options;
					opts.queryParams = param;
				}
				$(this).propertygrid('load');
			});
		}
	};

	$.fn.joyeditor.defaults = $.extend({}, $.fn.propertygrid.defaults, {
		title : null,
		columns : [ [ {
			field : 'label',
			title : '名称',
			width : 120,
			sortable : false
		}, {
			field : 'value',
			title : '内容',
			width : 380,
			formatter : formatter
		} ] ],
		toolbar : [ {
			text : '编辑',
			iconCls : 'icon-edit'
		}, {
			text : '保存',
			iconCls : 'icon-save',
			disabled : true
		} ],
		editable : false,
		showGroup : true,
		showHeader : false,
		scrollbarSize : 0,
		onSave : function(data) {
			return true;
		}
	});
})(jQuery);