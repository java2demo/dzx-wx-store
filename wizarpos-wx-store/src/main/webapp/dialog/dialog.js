﻿var Dialog = {
		loading:function(){
			var tpl = '<div class="modal modal_loading hide">';
		    tpl += '<div class="modal-body">'; 
		    tpl += '<div class="spinner">';
		    tpl += '<div class="spinner-container container1">';
		    tpl += '<div class="circle1"></div><div class="circle2"></div><div class="circle3"></div><div class="circle4"></div>';
		    tpl += '</div>';
		    tpl += '<div class="spinner-container container2">';
		    tpl += '<div class="circle1"></div><div class="circle2"></div><div class="circle3"></div><div class="circle4"></div>';
		    tpl += '</div>';
		    tpl += '<div class="spinner-container container3">';
		    tpl += '<div class="circle1"></div><div class="circle2"></div><div class="circle3"></div><div class="circle4"></div>';
		    tpl += '</div>';
		    tpl += '</div>';
		    tpl += '</div>';
		    tpl += '</div>';
		    var $messager = $(tpl).appendTo("body");
		    $messager.on('show.bs.modal',function(e){
		    }).on('hide.bs.modal', function () {
		    	$messager.remove();
			}).modal({backdrop:false});
		    return $messager;
		},
		alert:function(message,type){
			var tpl = '<div class="modal alert '+ (type == 'success' ? 'alert-success' : 'alert-danger') +' hide">';
			    tpl += '<div class="modal-body">'; 
			    tpl += '<p>'+message+'</p>';
			    tpl += '</div>';
			    tpl += '</div>';
			    $(tpl).appendTo("body").on('show.bs.modal',function(e){
			    	var $alert = $(this);
			    	setTimeout(function(){
			    		$alert.modal('hide').remove();
			    	},3000);
			    }).modal({backdrop:false});
		},
		/*确认框，点击确定后不立马关闭，直接调用回调函数*/
		confirmMsg : function(message,callback){
			var tpl = '<div class="modal confirm hide">';
		    tpl += '<div class="modal-header">';
		    tpl += '</div>';
		    tpl += '<div class="modal-body">';
		    tpl += message;
		    tpl += '</div>';
		    tpl += '<div class="modal-footer">';
		    tpl += '<a class="btn btn-default" data-dismiss="modal">取消</a>';
		    tpl += '<a class="btn btn-primary" data-confirmform="true">确定</a>';
		    tpl += '</div>';
		    tpl += '</div>';
		    var $messager = $(tpl).appendTo("body");
		    $messager.find(".modal-footer .btn-primary").bind('click',function(){
		    	$messager.trigger('mango.dialog.confirmform');
		    });
		    $messager.on('mango.dialog.confirmform',function(){
		    	callback($messager);
		    }).on('show.bs.modal',function(e){
		    	$(this).css("margin-top",0 - $(this).height() / 2);
		    }).on('hide.bs.modal', function () {
		    	$messager.remove();
			}).modal();
		},
		/*确认框，点击确定后立马关闭，同时调用回调函数*/
		confirm : function(message,callback){
			var tpl = '<div class="modal confirm hide">';
		    tpl += '<div class="modal-header">';
		    tpl += '</div>';
		    tpl += '<div class="modal-body">';
		    tpl += message;
		    tpl += '</div>';
		    tpl += '<div class="modal-footer">';
		    tpl += '<a class="btn btn-default" data-dismiss="modal">取消</a>';
		    tpl += '<a class="btn btn-primary" data-dismiss="modal" data-confirmform="true">确定</a>';
		    tpl += '</div>';
		    tpl += '</div>';
		    var $messager = $(tpl).appendTo("body");
		    $messager.find(".modal-footer .btn-primary").bind('click',function(){
		    	$messager.trigger('mango.dialog.confirmform');
		    });
		    $messager.on('mango.dialog.confirmform',function(){
		    	callback();
		    }).on('show.bs.modal',function(e){
		    	$(this).css("margin-top",0 - $(this).height() / 2);
		    }).on('hide.bs.modal', function () {
		    	$messager.remove();
			}).modal();
		}
		
};
