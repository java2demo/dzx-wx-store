package cn.congine.wizarpos.mall.model;

import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("request")
public class MangoRequest implements Serializable {
	private static final long serialVersionUID = 1586984850175778024L;

	public enum Type {
		Text, Voice, Click;
	}

	/**
	 * 事件类型
	 */
	@XStreamAsAttribute
	private Type type = Type.Text;

	/**
	 * 事件内容
	 */
	private String content;

	/**
	 * 请求来源，即关注者
	 */
	@XStreamAlias("source")
	private Follower source;

	/**
	 * 公众号ID
	 */
	@XStreamAlias("pid")
	private String publicID;

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Follower getSource() {
		return source;
	}

	public void setSource(Follower source) {
		this.source = source;
	}

	public String getPublicID() {
		return publicID;
	}

	public void setPublicID(String publicID) {
		this.publicID = publicID;
	}
}
