package cn.congine.wizarpos.mall.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "sys_merchant_def")
public class SysMerchantDef extends BaseEntity {
	private static final long serialVersionUID = 7471548394644675615L;

	// 慧商户号
	// 唯一标识
	@Id
	@Column(name = "mid")
	private String mid;

	// 代理商编号(慧商户号)
	@Column(name = "agent_id")
	private String agentId;
	
	// 收单商户号
	@Column(name = "merchant_id")
	private String merchantId;

	// 收单商户号
	@Column(name = "mgt_loct")
	private String mgtLoct;

	// 收单商户号
	@Column(name = "city_loct")
	private String cityLoct;

	// 支付通道
	@Column(name = "pay_id")
	private Integer payId;

	// 商户名称
	@Column(name = "merchant_name")
	private String merchantName;

	// 微信会员卡开始编号
	@Column(name = "start_no")
	private Integer startNo;
	
	// 商户是否启用
	@Column(name = "valid_flag")
	private Integer validFlag;
	
	// 使用慧银的微信配置参数
	@Column(name = "use_wizarpos_weixin_pay_config")
	private String useWizarposWeixinPayConfig;

	// 使用慧银的支付宝配置参数
	@Column(name = "use_wizarpos_alipay_config")
	private String useWizarposAlipayConfig;

	// 收款通知开关
	@Column(name = "collect_notify_mark")
	private String collectNotifyMark;

	// 销售通知开关
	@Column(name = "sale_notify_mark")
	private String saleNotifyMark;

	// 商户编号，从1开始顺序增加,每次加1。与自发行卡的卡号前6位对应
	@Column(name = "sequence_no")
	private Integer sequenceNo;

	// 入驻时间
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_time")
	private Date createTime;

	// 商户LOGO
	@Column(name = "logo_image")
	private String logoImage;

	// 商铺模板:1货架式模板 2全部商铺式模板
	@Column(name = "stores_templates")
	private Integer storesTemplates;

	// 门店相关说明
	@Column(name = "shop_desc")
	private String shopDesc;

	// vip图片
	@Column(name = "vip_image")
	private String vipImage;

	// vip正面图片
	@Column(name = "vip_front_image")
	private String vipFrontImage;

	// 商户公众号openid
	@Column(name = "p_open_id")
	private String pOpenId;

	// 慧商户公众号openid
	@Column(name = "m_open_id")
	private String mOpenId;

	// 经度
	@Column(name = "lon")
	private String lon;

	// 纬度
	@Column(name = "lat")
	private String lat;

	// 支付宝账户号
	@Column(name = "alipay_account")
	private String alipayAccount;

	// 商户地址
	@Column(name = "merchant_addr")
	private String merchantAddr;

	// 商户联系方式(多个联系方式以逗号分隔)
	@Column(name = "merchant_tel")
	private String merchantTel;

	// 商户简介
	@Column(name = "merchant_summary")
	private String merchantSummary;

	// 距离
	@Transient
	private Double distance;

	// 页面显示距离
	@Transient
	private String showDistance;

	// 会员数
	@Transient
	private Integer memberNum;

	// 商户背景图
	@Column(name = "banner_image")
	private String bannerImage;

	// appid
	@Column(name = "weixin_app_id")
	private String appid;

	// appsecret
	@Column(name = "weixin_app_secret")
	private String appsecret;

	// 微信商户平台ID(partner)
	@Column(name = "weixin_mch_id")
	private String weixinMchId;

	// 微信商户平台支付密钥
	@Column(name = "weixin_partner_key")
	private String weixinPartnerKey;

	// pay_sign_key
	@Column(name = "weixin_app_key")
	private String weixinAppKey;

	// 公众号的全局唯一票据，调用各接口时都需使用
	@Column(name = "wx_access_token")
	private String wxAccessToken;

	// 唯一票据的申请时间戳
	@Column(name = "wx_access_token_timestamp")
	private Long wxAccessTokenTimestamp;

	// 商户类型 1零售2餐饮3服务
	@Column(name = "merchant_type")
	private String merchantType;//add xudongdong
	
	public Integer getValidFlag() {
		return validFlag;
	}

	public void setValidFlag(Integer validFlag) {
		this.validFlag = validFlag;
	}

	public String getWeixinMchId() {
		return weixinMchId;
	}

	public void setWeixinMchId(String weixinMchId) {
		this.weixinMchId = weixinMchId;
	}

	public String getWeixinPartnerKey() {
		return weixinPartnerKey;
	}

	public void setWeixinPartnerKey(String weixinPartnerKey) {
		this.weixinPartnerKey = weixinPartnerKey;
	}

	public String getWeixinAppKey() {
		return weixinAppKey;
	}

	public void setWeixinAppKey(String weixinAppKey) {
		this.weixinAppKey = weixinAppKey;
	}

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getAppsecret() {
		return appsecret;
	}

	public void setAppsecret(String appsecret) {
		this.appsecret = appsecret;
	}

	public String getBannerImage() {
		return bannerImage;
	}

	public void setBannerImage(String bannerImage) {
		this.bannerImage = bannerImage;
	}

	public Integer getMemberNum() {
		return memberNum;
	}

	public void setMemberNum(Integer memberNum) {
		this.memberNum = memberNum;
	}

	public String getShowDistance() {
		return showDistance;
	}

	public void setShowDistance(String showDistance) {
		this.showDistance = showDistance;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public Integer getPayId() {
		return payId;
	}

	public void setPayId(Integer payId) {
		this.payId = payId;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public Integer getStartNo() {
		return startNo;
	}

	public void setStartNo(Integer startNo) {
		this.startNo = startNo;
	}

	public String getUseWizarposWeixinPayConfig() {
		return useWizarposWeixinPayConfig;
	}

	public void setUseWizarposWeixinPayConfig(String useWizarposWeixinPayConfig) {
		this.useWizarposWeixinPayConfig = useWizarposWeixinPayConfig;
	}

	public String getUseWizarposAlipayConfig() {
		return useWizarposAlipayConfig;
	}

	public void setUseWizarposAlipayConfig(String useWizarposAlipayConfig) {
		this.useWizarposAlipayConfig = useWizarposAlipayConfig;
	}

	public String getCollectNotifyMark() {
		return collectNotifyMark;
	}

	public void setCollectNotifyMark(String collectNotifyMark) {
		this.collectNotifyMark = collectNotifyMark;
	}

	public String getSaleNotifyMark() {
		return saleNotifyMark;
	}

	public void setSaleNotifyMark(String saleNotifyMark) {
		this.saleNotifyMark = saleNotifyMark;
	}

	public Integer getSequenceNo() {
		return sequenceNo;
	}

	public void setSequenceNo(Integer sequenceNo) {
		this.sequenceNo = sequenceNo;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getLogoImage() {
		return logoImage;
	}

	public void setLogoImage(String logoImage) {
		this.logoImage = logoImage;
	}

	public Integer getStoresTemplates() {
		return storesTemplates;
	}

	public void setStoresTemplates(Integer storesTemplates) {
		this.storesTemplates = storesTemplates;
	}

	public String getShopDesc() {
		return shopDesc;
	}

	public void setShopDesc(String shopDesc) {
		this.shopDesc = shopDesc;
	}

	public String getVipImage() {
		return vipImage;
	}

	public void setVipImage(String vipImage) {
		this.vipImage = vipImage;
	}

	public String getVipFrontImage() {
		return vipFrontImage;
	}

	public void setVipFrontImage(String vipFrontImage) {
		this.vipFrontImage = vipFrontImage;
	}

	public String getpOpenId() {
		return pOpenId;
	}

	public void setpOpenId(String pOpenId) {
		this.pOpenId = pOpenId;
	}

	public String getmOpenId() {
		return mOpenId;
	}

	public void setmOpenId(String mOpenId) {
		this.mOpenId = mOpenId;
	}

	public String getLon() {
		return lon;
	}

	public void setLon(String lon) {
		this.lon = lon;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getAlipayAccount() {
		return alipayAccount;
	}

	public void setAlipayAccount(String alipayAccount) {
		this.alipayAccount = alipayAccount;
	}

	public String getMerchantAddr() {
		return merchantAddr;
	}

	public void setMerchantAddr(String merchantAddr) {
		this.merchantAddr = merchantAddr;
	}

	public String getMerchantTel() {
		return merchantTel;
	}

	public void setMerchantTel(String merchantTel) {
		this.merchantTel = merchantTel;
	}

	public String getMerchantSummary() {
		return merchantSummary;
	}

	public void setMerchantSummary(String merchantSummary) {
		this.merchantSummary = merchantSummary;
	}

	public String getMgtLoct() {
		return mgtLoct;
	}

	public void setMgtLoct(String mgtLoct) {
		this.mgtLoct = mgtLoct;
	}

	public String getCityLoct() {
		return cityLoct;
	}

	public void setCityLoct(String cityLoct) {
		this.cityLoct = cityLoct;
	}

	public String getWxAccessToken() {
		return wxAccessToken;
	}

	public void setWxAccessToken(String wxAccessToken) {
		this.wxAccessToken = wxAccessToken;
	}

	public Long getWxAccessTokenTimestamp() {
		return wxAccessTokenTimestamp;
	}

	public void setWxAccessTokenTimestamp(Long wxAccessTokenTimestamp) {
		this.wxAccessTokenTimestamp = wxAccessTokenTimestamp;
	}

	public String getMerchantType() {
		return merchantType;
	}

	public void setMerchantType(String merchantType) {
		this.merchantType = merchantType;
	}

}
