package cn.congine.wizarpos.mall.service;

import cn.congine.wizarpos.mall.model.SysTableKey;

public interface SysTableKeyService {

	void updateSysTableKey(SysTableKey obj);

	SysTableKey getSysTableKey(String keyName);

	String getTranLogId();

	String getWxOrderId();
}
