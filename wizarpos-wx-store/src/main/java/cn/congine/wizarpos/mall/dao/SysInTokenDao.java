package cn.congine.wizarpos.mall.dao;

import cn.congine.wizarpos.mall.entity.SysInToken;

public interface SysInTokenDao extends GenericDao<SysInToken> {
	SysInToken getSysInTokenByTokenId(String tokenId);

	void delete(SysInToken token);
}
