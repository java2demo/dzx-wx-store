package cn.congine.wizarpos.mall.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sys_wx_child_merchant_token")
public class SysWxChildMerchantToken extends BaseEntity {
	private static final long serialVersionUID = -4309994218963920437L;
	// 唯一标识
	@Id
	@Column(name = "id")
	private String id;
	// 母商户id
	@Column(name = "primary_id")
	private String primaryId;
	// 慧商户号
	@Column(name = "mid")
	private String mid;
	// 公众号的gh号
	@Column(name = "gh_code")
	private String ghCode;
	// 授权商户app_id
	@Column(name = "wx_app_id")
	private String wxAppId;
	// 授权商户app_secret
	@Column(name = "wx_app_secret")
	private String wxAppSecret;
	// 授权商户mchid
	@Column(name = "wx_mchid")
	private String wxMchid;
	// 授权商户支付key
	@Column(name = "wx_app_key")
	private String wxAppKey;
	// 授权商户权限集
	@Column(name = "func_info")
	private String funcInfo;
	// 授权码
	@Column(name = "auth_code")
	private String authCode;
	// 授权码有效期
	@Column(name = "auth_code_expires_in")
	private Long authCodeExpiresIn;
	// 授权商户access_token
	@Column(name = "authorizer_access_token")
	private String authorizerAccessToken;
	// 授权商户刷新access_token
	@Column(name = "authorizer_refresh_token")
	private String authorizerRefreshToken;
	// 授权商户access_token有效时间
	@Column(name = "authorizer_access_token_expiry")
	private Long authorizerAccessTokenExpiry;
	// 公众号的全局唯一票据，调用各接口时都需使用
	@Column(name = "merchant_access_token")
	private String merchantAccessToken;
	// 公众号的全局唯一票据有效时间
	@Column(name = "merchant_access_token_expiry")
	private Long merchantAccessTokenExpiry;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getWxAppId() {
		return wxAppId;
	}

	public void setWxAppId(String wxAppId) {
		this.wxAppId = wxAppId;
	}

	public String getAuthorizerAccessToken() {
		return authorizerAccessToken;
	}

	public void setAuthorizerAccessToken(String authorizerAccessToken) {
		this.authorizerAccessToken = authorizerAccessToken;
	}

	public String getAuthorizerRefreshToken() {
		return authorizerRefreshToken;
	}

	public void setAuthorizerRefreshToken(String authorizerRefreshToken) {
		this.authorizerRefreshToken = authorizerRefreshToken;
	}

	public Long getAuthorizerAccessTokenExpiry() {
		return authorizerAccessTokenExpiry;
	}

	public void setAuthorizerAccessTokenExpiry(Long authorizerAccessTokenExpiry) {
		this.authorizerAccessTokenExpiry = authorizerAccessTokenExpiry;
	}

	public String getFuncInfo() {
		return funcInfo;
	}

	public void setFuncInfo(String funcInfo) {
		this.funcInfo = funcInfo;
	}

	public Long getAuthCodeExpiresIn() {
		return authCodeExpiresIn;
	}

	public void setAuthCodeExpiresIn(Long authCodeExpiresIn) {
		this.authCodeExpiresIn = authCodeExpiresIn;
	}

	public String getAuthCode() {
		return authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	public String getPrimaryId() {
		return primaryId;
	}

	public void setPrimaryId(String primaryId) {
		this.primaryId = primaryId;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getGhCode() {
		return ghCode;
	}

	public void setGhCode(String ghCode) {
		this.ghCode = ghCode;
	}

	public String getWxAppSecret() {
		return wxAppSecret;
	}

	public void setWxAppSecret(String wxAppSecret) {
		this.wxAppSecret = wxAppSecret;
	}

	public String getWxMchid() {
		return wxMchid;
	}

	public void setWxMchid(String wxMchid) {
		this.wxMchid = wxMchid;
	}

	public String getWxAppKey() {
		return wxAppKey;
	}

	public void setWxAppKey(String wxAppKey) {
		this.wxAppKey = wxAppKey;
	}

	public String getMerchantAccessToken() {
		return merchantAccessToken;
	}

	public void setMerchantAccessToken(String merchantAccessToken) {
		this.merchantAccessToken = merchantAccessToken;
	}

	public Long getMerchantAccessTokenExpiry() {
		return merchantAccessTokenExpiry;
	}

	public void setMerchantAccessTokenExpiry(Long merchantAccessTokenExpiry) {
		this.merchantAccessTokenExpiry = merchantAccessTokenExpiry;
	}

}
