package cn.congine.wizarpos.mall.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "mrt_wechart_user_info")
public class MrtWechartUserInfo extends BaseEntity {
	private static final long serialVersionUID = 1L;

	// 唯一标识
	@Id
	@GeneratedValue(generator = "uuid")
	// 指定生成器名称
	@GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
	private String id;

	// 慧商户号
	@Column(name = "mid")
	private String mid;

	//
	@Column(name = "open_id")
	private String openId;

	// 0：取消关注，1：关注
	@Column(name = "subscribe")
	private String subscribe;

	// 昵称
	@Column(name = "nickname")
	private String nickname;

	// 0：男，1女
	@Column(name = "sex")
	private String sex;

	// 城市
	@Column(name = "city")
	private String city;

	// 国家
	@Column(name = "country")
	private String country;

	// 省份
	@Column(name = "province")
	private String province;

	// 语言：zh_CH
	@Column(name = "language")
	private String language;

	// 头像地址
	@Column(name = "headimgurl")
	private String headimgurl;

	// 关注时间
	@Column(name = "subscribe_time")
	private String subscribeTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getSubscribe() {
		return subscribe;
	}

	public void setSubscribe(String subscribe) {
		this.subscribe = subscribe;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getHeadimgurl() {
		return headimgurl;
	}

	public void setHeadimgurl(String headimgurl) {
		this.headimgurl = headimgurl;
	}

	public String getSubscribeTime() {
		return subscribeTime;
	}

	public void setSubscribeTime(String subscribeTime) {
		this.subscribeTime = subscribeTime;
	}

}
