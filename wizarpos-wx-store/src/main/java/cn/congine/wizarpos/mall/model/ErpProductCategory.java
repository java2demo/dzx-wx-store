package cn.congine.wizarpos.mall.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "erp_product_category")
public class ErpProductCategory extends BaseEntity {
	private static final long serialVersionUID = 3071165802824917076L;

	// 唯一标识
	@Id
	@Column(name = "id")
	private String id;

	// 品类代码
	@Column(name = "code")
	private String code;

	// 品类名称
	@Column(name = "name")
	private String name;

	// 上级品类
	@Column(name = "parent_id")
	private String parentId;

	// 慧商户号
	@Column(name = "mid")
	private String mid;

	// 最后修改时间
	@Column(name = "last_time")
	private Long lastTime;

	// 上架标识
	@Column(name = "storage_rack_flag")
	private String storageRackFlag;

	@Transient
	@Column(name = "flag")
	private boolean flag;

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public Long getLastTime() {
		return lastTime;
	}

	public void setLastTime(Long lastTime) {
		this.lastTime = lastTime;
	}

	public String getStorageRackFlag() {
		return storageRackFlag;
	}

	public void setStorageRackFlag(String storageRackFlag) {
		this.storageRackFlag = storageRackFlag;
	}

}
