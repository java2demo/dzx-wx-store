package cn.congine.wizarpos.mall.dao.hibernate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.FlushMode;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import cn.congine.wizarpos.mall.common.Page;
import cn.congine.wizarpos.mall.dao.WxUserCaterBindDao;
import cn.congine.wizarpos.mall.model.WxUserCaterBind;
import cn.congine.wizarpos.mall.utils.Function;

@Repository("wxUserCaterBindDao")
public class WxUserCaterBindDaoHibernate extends
		GenericDaoHibernate<WxUserCaterBind> implements WxUserCaterBindDao {

	public WxUserCaterBindDaoHibernate() {
		super(WxUserCaterBind.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<WxUserCaterBind> getByOpenId(String openId) {
		Query query = getSession()
				.createQuery(
						"from WxUserCaterBind where openId = :openId order by lastTime  desc")
				.setParameter("openId", openId);
		return (List<WxUserCaterBind>) query.list();
	}

	@Override
	public List<WxUserCaterBind> getByOpenId(String openId, int pageNo) {
		String hql = "from WxUserCaterBind where openId = :openId ";
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("openId", openId);

		Page<WxUserCaterBind> page = new Page<WxUserCaterBind>();
		page.setPageNo(pageNo);

		page = this.readAll4Page(hql.toString(), "", param, page);
		return page.getList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> getByOpenIdMid(String openId, String mid) {
		Query query = getSession()
				.createSQLQuery(
						"select o.order_id, t.table_name, t.table_no, s.stage_range, o.create_time, o.order_amount, o.status, o.audit_flag,o.order_date,o.id "
								+ "from wx_user_carter_bind b, erp_carter_order o, erp_table t, erp_table_order_stage s "
								+ "where b.carter_order_id=o.id and o.table_id=t.table_id and o.stage_id=s.id "
								+ "and b.mid=:mid and b.open_id=:openId order by b.create_time desc")
				.setParameter("mid", mid).setParameter("openId", openId);
		return (List<Object[]>) query.list();
	}

	@Override
	public List<WxUserCaterBind> getByOpenIdMid(String openId, String mid,
			int pageNo) {
		String hql = "from WxUserCaterBind where mid=:mid and openId=:openId ";
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("openId", openId);
		param.put("mid", mid);
		Page<WxUserCaterBind> page = new Page<WxUserCaterBind>();
		page.setPageNo(pageNo);

		page = this.readAll4Page(hql.toString(), "", param, page);
		return page.getList();
	}

	@Override
	public WxUserCaterBind save(WxUserCaterBind obj) {
		String uuid = Function.getUid();
		obj.setId(uuid);
		String sql = "INSERT INTO wx_user_carter_bind (id, mid, open_id, carter_order_id, create_time) VALUES (?,?,?,?,?)";
		this.getSession().setFlushMode(FlushMode.MANUAL);
		Query query = this.getSession().createSQLQuery(sql);
		query.setString(0, uuid);
		query.setString(1, obj.getMid());
		query.setString(2, obj.getOpenId());
		query.setString(3, obj.getCarterOrderId());
		query.setString(4,
				Function.formatDate(obj.getCreateTime(), "yyyy-MM-dd HH:mm:ss"));
		query.executeUpdate();
		return obj;
	}
}
