package cn.congine.wizarpos.mall.model.open;

public class WxOrderSubmitInput extends CommonInput {

	private static final long serialVersionUID = 1L;
	private String openId = null;
	private String mid = null;
	// 购物车结算
	private String cartId = null;
	// 立即购买
	private String productId = null;
	// 原始订单修改操作
	private String orderId = null;
	// 地址
	private String addressId = null;
	// 配送方式1 货到付款 2 微信支付送货上门 3 微信支付到店提货 4 会员卡支付送货上门 5 会员卡支付到店提货
	private String dispatchType = null;
	// pos机是否要打印小票
	private boolean print = false;
	// 微信消息模板跳转链接
	private String url = null;
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getOpenId() {
		return openId;
	}
	public void setOpenId(String openId) {
		this.openId = openId;
	}
	public String getMid() {
		return mid;
	}
	public void setMid(String mid) {
		this.mid = mid;
	}
	public String getCartId() {
		return cartId;
	}
	public void setCartId(String cartId) {
		this.cartId = cartId;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getAddressId() {
		return addressId;
	}
	public void setAddressId(String addressId) {
		this.addressId = addressId;
	}
	public String getDispatchType() {
		return dispatchType;
	}
	public void setDispatchType(String dispatchType) {
		this.dispatchType = dispatchType;
	}
	public boolean isPrint() {
		return print;
	}
	public void setPrint(boolean print) {
		this.print = print;
	}
}
