package cn.congine.wizarpos.mall.common;

import java.util.List;

public class Page<T> {

	public static final int DEFAULT_PAGE_SIZE = 10;

	public static final int DEFAULT_PAGE_NO = 1;

	// 每页记录数
	private int pageSize;

	// 当前页
	private int pageNo;

	// 总页数
	private int totalPage;

	// 总记录数
	private int totalCount;

	// 返回记录
	private List<T> list;

	// 上一页
	private int prevPageNo;

	// 下一页
	private int nextPageNo;

	// 首页
	private int startPageNo;

	// 尾页
	private int endPageNo;

	public Page() {
		this.pageSize = DEFAULT_PAGE_SIZE;
		this.pageNo = DEFAULT_PAGE_NO;
	}

	public Page(int pageSize) {
		this.pageSize = pageSize;
		this.pageNo = DEFAULT_PAGE_NO;
	}

	public Page(int pageSize, int pageNo) {
		this.pageSize = pageSize;
		this.pageNo = pageNo;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}

	public int getPrevPageNo() {
		if (pageNo > 1) {
			prevPageNo = pageNo - 1;
		} else {
			prevPageNo = 0;
		}
		return prevPageNo;
	}

	public int getNextPageNo() {
		if (pageNo < totalPage) {
			nextPageNo = pageNo + 1;
		} else {
			nextPageNo = 0;
		}
		return nextPageNo;
	}

	public int getStartPageNo() {
		if (pageNo == totalPage && totalPage > 2) {
			startPageNo = 1;
		} else {
			startPageNo = 0;
		}
		return startPageNo;
	}

	public int getEndPageNo() {
		if (pageNo == 1 && totalPage > 2) {
			endPageNo = totalPage;
		} else {
			endPageNo = 0;
		}
		return endPageNo;
	}

}
