package cn.congine.wizarpos.mall.dao.hibernate;

import org.springframework.stereotype.Repository;

import cn.congine.wizarpos.mall.dao.YudingTableDao;
import cn.congine.wizarpos.mall.model.ErpCarterOrder;

@Repository("yudingTableDao")
public class YudingTableDaoHibernate extends
		GenericDaoHibernate<ErpCarterOrder> implements YudingTableDao {

	public YudingTableDaoHibernate() {
		super(ErpCarterOrder.class);
	}

}
