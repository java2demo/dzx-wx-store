package cn.congine.wizarpos.mall.model;

import java.io.Serializable;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

/**
 * 公众号订阅者
 * 
 * @author Rime
 */
@XStreamAlias("Follower")
public class Follower implements Serializable {
	private static final long serialVersionUID = 6156270555615998L;

	@XStreamAsAttribute
	private String id;

	/**
	 * 微信Open Id
	 */
	@XStreamAlias("openid")
	private String openId;

	/**
	 * 微信昵称
	 */
	@XStreamAlias("nickName")
	private String nickName;

	/**
	 * 微信号
	 */
	@XStreamAlias("username")
	private String username;

	/**
	 * 真实姓名
	 */
	@XStreamAlias("realName")
	private String realName;

	/**
	 * 国家
	 */
	@XStreamAlias("country")
	private String country;

	/**
	 * 省份
	 */
	@XStreamAlias("province")
	private String province;

	/**
	 * 城市
	 */
	@XStreamAlias("city")
	private String city;

	/**
	 * 性别： 1-男；2-女；0-未知
	 */
	@XStreamAlias("sex")
	private String sex;

	/**
	 * 移动电话
	 */
	@XStreamAlias("phone")
	private String phone;

	/**
	 * 电子邮箱
	 */
	@XStreamAlias("email")
	private String email;

	/**
	 * 身份证号码
	 */
	@XStreamAlias("idNo")
	private String idNo;

	@XStreamAlias("followWay")
	private String followWay;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getIdNo() {
		return idNo;
	}

	public void setIdNo(String idNo) {
		this.idNo = idNo;
	}

	public String getFollowWay() {
		return followWay;
	}

	public void setFollowWay(String followWay) {
		this.followWay = followWay;
	}

	@Override
	public String toString() {
		return "Follower " + nickName + " [" + openId + "]";
	}
}
