package cn.congine.wizarpos.mall.model.open;

public class CateringTableReservedQueryInput extends CommonInput {

	private static final long serialVersionUID = 1L;
	private String mid = null;
	private String date = null;
	private String timeId = null;
	
	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTimeId() {
		return timeId;
	}

	public void setTimeId(String timeId) {
		this.timeId = timeId;
	}
}
