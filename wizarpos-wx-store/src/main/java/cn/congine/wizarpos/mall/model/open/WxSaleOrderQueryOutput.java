package cn.congine.wizarpos.mall.model.open;

/**
 * 
 * @author xudongdong
 * Date: 2015.7.14
 */


public class WxSaleOrderQueryOutput extends CommonOutput {

	private static final long serialVersionUID = 1L;

	private SaleOrderView result = null;

	public SaleOrderView getResult() {
		return result;
	}

	public void setResult(SaleOrderView result) {
		this.result = result;
	}

}


