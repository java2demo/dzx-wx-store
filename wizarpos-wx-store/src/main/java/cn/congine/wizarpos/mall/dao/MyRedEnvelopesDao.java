package cn.congine.wizarpos.mall.dao;

import java.util.List;

import cn.congine.wizarpos.mall.entity.MyRedEnvelopes;

public interface MyRedEnvelopesDao extends GenericDao<MyRedEnvelopes> {

      List<MyRedEnvelopes> getMyRedEnvelopesList(String mid,String onlineActivityId);
	
	  MyRedEnvelopes getMyRedEnvelopes(String mid,String onlineActivityId);

	  void delete(MyRedEnvelopes object);

	MyRedEnvelopes getByMidAndMyShakeId(String mid, String myShakeId);
}
