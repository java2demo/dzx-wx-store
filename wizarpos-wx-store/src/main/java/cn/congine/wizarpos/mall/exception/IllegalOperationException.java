/**
 * The Software is owned by Congine and is protected by copyright laws and other national laws.
 * You agree that you have no right, title or interest in the Software.
 * This code cannot simply be copied and put under another distribution license.
 * Copyright remains Congine's, and as such any Copyright notices in the code are not to be removed.
 */
package cn.congine.wizarpos.mall.exception;

public class IllegalOperationException extends RuntimeException {
	private static final long serialVersionUID = -8357813659957704858L;

	public IllegalOperationException(String message) {
		super(message);
	}

}