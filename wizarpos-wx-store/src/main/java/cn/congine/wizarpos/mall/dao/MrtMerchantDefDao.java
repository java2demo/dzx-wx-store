package cn.congine.wizarpos.mall.dao;

import cn.congine.wizarpos.mall.entity.MrtMerchantDef;

public interface MrtMerchantDefDao extends GenericDao<MrtMerchantDef> {

	public MrtMerchantDef getSysMerchantDefByMid(String mid);

}
