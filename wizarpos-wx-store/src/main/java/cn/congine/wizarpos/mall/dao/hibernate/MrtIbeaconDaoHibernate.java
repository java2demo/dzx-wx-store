package cn.congine.wizarpos.mall.dao.hibernate;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import cn.congine.wizarpos.mall.dao.MrtIbeaconDao;
import cn.congine.wizarpos.mall.model.MrtIbeacon;

@Repository("mrtIbeaconDao")
public class MrtIbeaconDaoHibernate extends GenericDaoHibernate<MrtIbeacon>
		implements MrtIbeaconDao {

	public MrtIbeaconDaoHibernate() {
		super(MrtIbeacon.class);
	}

	@Override
	public MrtIbeacon getActive(String mid) {
		Query query = getSession().createQuery(
				"from MrtIbeacon where mid=:mid and enabled='0'").setParameter(
				"mid", mid);
		return (MrtIbeacon) query.uniqueResult();
	}

}
