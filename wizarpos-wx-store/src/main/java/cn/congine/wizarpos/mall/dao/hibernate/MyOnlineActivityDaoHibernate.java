package cn.congine.wizarpos.mall.dao.hibernate;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import cn.congine.wizarpos.mall.dao.MyOnlineActivityDao;
import cn.congine.wizarpos.mall.entity.MyOnlineActivity;

@Repository("myOnlineActivityDao")
public class MyOnlineActivityDaoHibernate extends GenericDaoHibernate<MyOnlineActivity>
		implements MyOnlineActivityDao {

	public MyOnlineActivityDaoHibernate() {
		super(MyOnlineActivity.class);
	}

	@Override
	public MyOnlineActivity getMyOnlineActivityByMid(String mid) {
		Query query = getSession().createQuery(
						"from MyOnlineActivity where status='0' and mid=:mid").setParameter("mid", mid);
		return (MyOnlineActivity) query.uniqueResult();
	}

}
