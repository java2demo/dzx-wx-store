package cn.congine.wizarpos.mall.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.congine.wizarpos.mall.dao.SysInTicketDao;
import cn.congine.wizarpos.mall.model.SysInTicket;
import cn.congine.wizarpos.mall.service.SysInTicketService;

@Service("wxApiTicketService")
public class SysInTicketServiceImpl implements SysInTicketService {

	@Autowired
	private SysInTicketDao sysInTicketDao;

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void updateSysInTicket(SysInTicket obj) {
		sysInTicketDao.update(obj);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public SysInTicket saveSysInTicket(SysInTicket sysInTicket) {
		return sysInTicketDao.save(sysInTicket);
	}

	@Override
	public SysInTicket findByApiSymbolAndMid(String apiSymbol, String mid) {
		return sysInTicketDao.getSysInTicket(apiSymbol, mid);
	}

	@Override
	public void del(SysInTicket obj) {
		sysInTicketDao.del(obj);
	}

}
