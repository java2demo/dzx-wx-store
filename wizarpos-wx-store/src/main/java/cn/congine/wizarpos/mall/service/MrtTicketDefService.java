package cn.congine.wizarpos.mall.service;

import cn.congine.wizarpos.mall.model.MrtTicketDef;

public interface MrtTicketDefService {

	MrtTicketDef getByWxCardId(String wxCardId);

	MrtTicketDef get(String id);

	void update(MrtTicketDef object);
	
	public void ticketPass(String cardid, String mid);
	
	public void ticketUnpass(String cardid, String mid);	
}
