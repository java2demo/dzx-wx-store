package cn.congine.wizarpos.mall.service;

import java.util.List;

import cn.congine.wizarpos.mall.entity.MyRedEnvelopes;


public interface MyRedEnvelopesService {
	
	public MyRedEnvelopes get(String id);

	List<MyRedEnvelopes> getMyRedEnvelopesList(String mid,String onlineActivityId);
	
	MyRedEnvelopes getMyRedEnvelopes(String mid,String onlineActivityId);
	
	void update(MyRedEnvelopes object);
	
	void delete (MyRedEnvelopes object) ;

	public MyRedEnvelopes getByMidAndMyShakeId(String mid, String id);
}
