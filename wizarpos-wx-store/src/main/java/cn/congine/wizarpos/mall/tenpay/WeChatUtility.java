package cn.congine.wizarpos.mall.tenpay;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

public class WeChatUtility {
	public static long toWechatTime(Date time) {
		return (long) (time.getTime() / 1000);
	}

	public static Date getWechatTime(long time) {
		return new Date(time * 1000);
	}

	public static long toWechatPrice(BigDecimal price) {
		return price.multiply(new BigDecimal(100)).longValue();
	}

	public static BigDecimal getWechatPrice(long price) {
		return BigDecimal.valueOf(price).divide(BigDecimal.valueOf(100L), 2,
				RoundingMode.HALF_DOWN);
	}
}
