package cn.congine.wizarpos.mall.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.congine.wizarpos.mall.dao.SaleOrderDao;
import cn.congine.wizarpos.mall.model.SaleOrder;
import cn.congine.wizarpos.mall.service.SaleOrderService;

@Service("saleOrderService")
public class SaleOrderServiceImpl implements SaleOrderService {

	@Autowired
	private SaleOrderDao saleOrderDao;

	@Override
	public SaleOrder getById(String id) {
		return saleOrderDao.getById(id);
	}

	@Override
	public SaleOrder getByNo(String orderNo) {
		return saleOrderDao.getByNo(orderNo);
	}

}
