package cn.congine.wizarpos.mall.service;

import java.util.List;

import cn.congine.wizarpos.mall.model.BankCardBind;

public interface BankCardBindService {
	BankCardBind get(String id);

	BankCardBind save(BankCardBind bind);

	BankCardBind get(String cardNo, String mid);

	BankCardBind get(Integer cid);

	List<BankCardBind> getList(String mid);

	List<BankCardBind> getList(String mid, String cardId);

	BankCardBind find(String id, String mid);

	void update(BankCardBind obj);
}
