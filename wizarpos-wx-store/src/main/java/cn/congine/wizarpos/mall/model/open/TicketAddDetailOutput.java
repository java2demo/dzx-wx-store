package cn.congine.wizarpos.mall.model.open;

/**
 * 
 * @author xudongdong
 * Date: 2015.7.3
 */

import java.util.List;

public class TicketAddDetailOutput extends CommonOutput {

	private static final long serialVersionUID = 1L;

	private List<Object> ticketInfoList = null;

	public List<Object> getTicketInfoList() {
		return ticketInfoList;
	}

	public void setTicketInfoList(List<Object> ticketInfoList) {
		this.ticketInfoList = ticketInfoList;
	}
	
/*	private List<TicketRecord> result = null;
	public List<TicketRecord> getResult() {
		return result;
	}
	public void setResult(List<TicketRecord> result) {
		this.result = result;
	}	*/
	
}


