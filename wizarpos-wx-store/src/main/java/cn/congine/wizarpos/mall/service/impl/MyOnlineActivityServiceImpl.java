package cn.congine.wizarpos.mall.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.congine.wizarpos.mall.dao.MyOnlineActivityDao;
import cn.congine.wizarpos.mall.entity.MyOnlineActivity;
import cn.congine.wizarpos.mall.service.MyOnlineActivityService;

@Service("myOnlineActivityService")
public class MyOnlineActivityServiceImpl implements MyOnlineActivityService {

	@Autowired
	private MyOnlineActivityDao myOnlineActivityDao;

	@Override
	public MyOnlineActivity getMyOnlineActivityByMid(String mid) {
		// TODO Auto-generated method stub
		return myOnlineActivityDao.getMyOnlineActivityByMid(mid);
	}



}
