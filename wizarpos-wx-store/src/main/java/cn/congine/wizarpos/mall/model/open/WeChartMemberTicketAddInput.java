package cn.congine.wizarpos.mall.model.open;

import java.util.List;
import java.util.Map;

public class WeChartMemberTicketAddInput extends CommonInput {

	private static final long serialVersionUID = 1L;

	private String mid = null;	
	private String cardNo = null;
	private List<Map<String, Object>> ticketIds = null;
	
	//领取红包
	private String hongbao = null;
	private String openId = null;
	//是否添加到了卡包（0未添加，1已添加）
	private char wxAdded = '0';
	// 红包是否分享（1分享，0未分享）
	private String hbShared = null;
	private String remark = null;
	
	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public List<Map<String, Object>> getTicketIds()
	{
		return ticketIds;
	}

	public void setTicketIds(List<Map<String, Object>> ticketIds)
	{
		this.ticketIds = ticketIds;
	}

	public String getHongbao() {
		return hongbao;
	}

	public void setHongbao(String hongbao) {
		this.hongbao = hongbao;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public char getWxAdded() {
		return wxAdded;
	}

	public void setWxAdded(char wxAdded) {
		this.wxAdded = wxAdded;
	}

	public String getHbShared() {
		return hbShared;
	}

	public void setHbShared(String hbShared) {
		this.hbShared = hbShared;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	
}
