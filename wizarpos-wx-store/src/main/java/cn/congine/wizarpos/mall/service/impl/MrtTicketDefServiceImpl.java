package cn.congine.wizarpos.mall.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.congine.wizarpos.mall.dao.MrtTicketDefDao;
import cn.congine.wizarpos.mall.model.MrtTicketDef;
import cn.congine.wizarpos.mall.service.MrtTicketDefService;

@Service("mrtTicketDefService")
public class MrtTicketDefServiceImpl implements MrtTicketDefService {
	@Autowired
	private MrtTicketDefDao mrtTicketDefDao;

	@Override
	public MrtTicketDef getByWxCardId(String wxCardId) {
		return mrtTicketDefDao.getByWxCardId(wxCardId);
	}

	@Override
	public MrtTicketDef get(String id) {
		return mrtTicketDefDao.get(id);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void update(MrtTicketDef object) {
		mrtTicketDefDao.update(object);
	}
	
	@Override
	public void ticketPass(String cardid, String mid) {
		mrtTicketDefDao.ticketPass(cardid, mid);
	}

	@Override
	public void ticketUnpass(String cardid, String mid) {
		mrtTicketDefDao.ticketUnpass(cardid, mid);
	}
}
