package cn.congine.wizarpos.mall.exception;

public class MangoException extends Exception {

	private static final long serialVersionUID = 2341404154773355142L;

	private final String detail;

	public MangoException(String message) {
		super(message);
		this.detail = "";
	}

	public MangoException(String message, String detail) {
		super(message);
		this.detail = detail;
	}

	public String getDetail() {
		return detail;
	}

	@Override
	public String toString() {
		return getMessage() + ": " + detail;
	}
}
