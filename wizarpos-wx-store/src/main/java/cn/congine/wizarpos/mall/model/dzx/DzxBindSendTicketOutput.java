package cn.congine.wizarpos.mall.model.dzx;

import cn.congine.wizarpos.mall.model.open.CommonOutput;

/**
 * 东志信联盟商户二维码摇一摇关注送券事件, 送券output
 * 
 * @author xudongdong
 *         2015年8月24日 上午9:33:45
 */
public class DzxBindSendTicketOutput extends CommonOutput {

	private static final long serialVersionUID = 1L;

}
