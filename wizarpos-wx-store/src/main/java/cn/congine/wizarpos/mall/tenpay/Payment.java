package cn.congine.wizarpos.mall.tenpay;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.commons.codec.binary.Hex;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Payment {
	// 公众号ID
	@JsonProperty("appid")
	private String appid;
	// 商品描述
	@JsonProperty("body")
	private String body;
	// 商户号
	@JsonProperty("mch_id")
	private String mch_id;
	// 随机字符串
	@JsonProperty("nonce_str")
	private String nonce_str;
	// 商户订单号
	@JsonProperty("out_trade_no")
	private String out_trade_no;
	// 终端IP
	@JsonProperty("spbill_create_ip")
	private String spbill_create_ip;
	// 总金额
	@JsonProperty("total_fee")
	private Integer total_fee;

	// 通知地址
	@JsonProperty("notify_url")
	private String notify_url;

	// 交易类型
	@JsonProperty("trade_type")
	private String trade_type;

	// 返回状态码
	@JsonProperty("return_code")
	private String return_code;

	@JsonProperty("sign")
	private String sign;

	@JsonIgnore
	private String payKey;

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getMch_id() {
		return mch_id;
	}

	public void setMch_id(String mch_id) {
		this.mch_id = mch_id;
	}

	public String getNonce_str() {
		return nonce_str;
	}

	public void setNonce_str(String nonce_str) {
		this.nonce_str = nonce_str;
	}

	public String getOut_trade_no() {
		return out_trade_no;
	}

	public void setOut_trade_no(String out_trade_no) {
		this.out_trade_no = out_trade_no;
	}

	public String getSpbill_create_ip() {
		return spbill_create_ip;
	}

	public void setSpbill_create_ip(String spbill_create_ip) {
		this.spbill_create_ip = spbill_create_ip;
	}

	public Integer getTotal_fee() {
		return total_fee;
	}

	public void setTotal_fee(Integer total_fee) {
		this.total_fee = total_fee;
	}

	public String getNotify_url() {
		return notify_url;
	}

	public void setNotify_url(String notify_url) {
		this.notify_url = notify_url;
	}

	public String getTrade_type() {
		return trade_type;
	}

	public void setTrade_type(String trade_type) {
		this.trade_type = trade_type;
	}

	public String getReturn_code() {
		return return_code;
	}

	public void setReturn_code(String return_code) {
		this.return_code = return_code;
	}

	public String getPayKey() {
		return payKey;
	}

	public void setPayKey(String payKey) {
		this.payKey = payKey;
	}

	public void sign(String appId, String key) {
		setAppid(appId);
		setPayKey(key);

		setNonce_str(getRadom());

		Map<String, String> map = new HashMap<String, String>();
		map.put("appid", getAppid());
		map.put("nonce_st", getNonce_str());
		map.put("body", getBody());
		map.put("mch_id", getMch_id());
		map.put("out_trade_no", getOut_trade_no());
		map.put("spbill_create_ip", getSpbill_create_ip());
		map.put("notify_url", getNotify_url());
		map.put("total_fee", getTotal_fee().toString());
		map.put("trade_type", getTrade_type());
		map.put("return_code", getReturn_code());
		StringBuffer sb = new StringBuffer();
		SortedSet<String> keys = new TreeSet<String>(map.keySet());
		Iterator<String> it = keys.iterator();
		while (it.hasNext()) {
			String signKey = it.next();
			String value = map.get(signKey);
			sb.append(signKey).append("=").append(value);
			if (it.hasNext()) {
				sb.append("&");
			}
		}
	}

	public String signMessage(String signMessage, String payKey) {
		String sign = null;
		try {
			String msg = signMessage + "&key=" + payKey;
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] array = md.digest(msg.getBytes("UTF-8"));
			sign = new String(Hex.encodeHex(array));
			sign = sign.toUpperCase();
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			return null;
		}
		return "sign=" + sign;
	}

	private String getRadom() {
		Random random = new Random();
		String _rand = String.valueOf(random.nextInt(10000));
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");
			byte[] array = md.digest(_rand.getBytes());
			return new String(Hex.encodeHex(array));
		} catch (NoSuchAlgorithmException e) {
			return null;
		}
	}

	public static boolean isEmpty(String str) {
		return ((str == null) || (str.length() == 0) || (str.trim().length() == 0));
	}
}
