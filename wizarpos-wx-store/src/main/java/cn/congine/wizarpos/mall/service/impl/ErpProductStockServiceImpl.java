package cn.congine.wizarpos.mall.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.congine.wizarpos.mall.dao.ErpProductStockDao;
import cn.congine.wizarpos.mall.service.ErpProductStockService;

@Service("erpProductStockService")
public class ErpProductStockServiceImpl implements ErpProductStockService {

	@Autowired
	private ErpProductStockDao erpProductStockDao;

	@Override
	public Integer getTotalStock(String mid, String productId) {
		return erpProductStockDao.getTotalStock(mid, productId);
	}

}
