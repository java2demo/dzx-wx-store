package cn.congine.wizarpos.mall.model.open;

import java.io.Serializable;

public class Stage implements Serializable {

	private static final long serialVersionUID = 1L;
	private String timeId = null;
	private String timeCode = null;
	private String timeName = null;
	private String timeRange = null;

	public String getTimeId() {
		return timeId;
	}

	public void setTimeId(String timeId) {
		this.timeId = timeId;
	}

	public String getTimeCode() {
		return timeCode;
	}

	public void setTimeCode(String timeCode) {
		this.timeCode = timeCode;
	}

	public String getTimeName() {
		return timeName;
	}

	public void setTimeName(String timeName) {
		this.timeName = timeName;
	}

	public String getTimeRange() {
		return timeRange;
	}

	public void setTimeRange(String timeRange) {
		this.timeRange = timeRange;
	}
}
