package cn.congine.wizarpos.mall.weChat.bussiness;

import java.lang.reflect.Method;

import org.apache.log4j.Logger;

import cn.congine.wizarpos.mall.utils.Constants;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * @author 史正烨
 * @date 2011-6-12
 */
public class ServiceController {

	/**
	 * actionName命名规则：类名$动作名称。动作名称：数据对象名称#布局名称#详细的动作名称。
	 * 
	 * @throws ServiceException
	 */
	public static ActionResponse doAction(String actionName, Object parameters)
			throws ServiceException {
		Object actionInstance = null;

		int dindex = actionName.indexOf("$");
		String className = actionName.substring(0, dindex);
		String methodName = actionName.substring(dindex + 1);
		logger.debug("className:" + className);
		logger.debug("methodName:" + methodName);
		try {
			actionInstance = Class.forName(className).newInstance();
		} catch (InstantiationException e) {
			throw new ServiceException(100, "类不能实例化:" + className, e);
		} catch (IllegalAccessException e) {
			throw new ServiceException(100, "类不能被访问:" + className, e);
		} catch (ClassNotFoundException e) {
			throw new ServiceException(100, "没有找到模块名称对应的类:" + className, e);
		} catch (Exception e) {
			throw new ServiceException(100, "系统发生异常:" + className, e);
		}

		ActionResponse response = new ActionResponse();
		try {
			Class<?>[] params = null;
			if (parameters != null) {
				params = new Class[] { Object.class };
			}
			Method method = actionInstance.getClass().getMethod(methodName,
					params);
			Object result = method.invoke(actionInstance, parameters);
			if (result instanceof String) {
				response.setData(result);

			} else if (result instanceof JSONObject) {
				response.setData(((JSONObject) result).toString());

			} else if (result instanceof JSONArray) {
				response.setData(((JSONArray) result).toString());

			} else if (result instanceof ActionResponse) {
				return (ActionResponse) result;

			} else {
				throw new ServiceException(100,
						"传入参数类型只能为字符串，JSONObject或JSONArray");
			}
		} catch (NoSuchMethodException e) {
			throw new ServiceException(Constants.NO_METCH_ACTION_NAME,
					"没有找到与action name匹配的方法名称。", e);
		} catch (IllegalAccessException e) {
			throw new ServiceException(Constants.ILLEGAL_ACCESS,
					"与action name匹配的方法不能被访问。", e);
		} catch (Exception e) {
			logger.error("服务端处理业务逻辑错误:", e.getCause());
			if (e instanceof ActionException) {
				ActionException ae = (ActionException) e;
				throw new ServiceException(ae.getCode(), ae.getMessage(), ae);
			}
			throw new ServiceException(Constants.USER_PROCESS_ERROR,
					"服务端处理业务逻辑错误。", e);
		}
		return response;
	}

	/** js或文件下载的前缀，工程内部 */
	private static final String URL_PRE_LOCAL = "local://";
	/** js或文件下载的前缀，网络 */
	private static final String URL_PRE_HTTP = "http://";
	/** js或文件下载的前缀，通过类寻找 */
	private static final String URL_PRE_CLASS = "class://";

	private static final Logger logger = Logger
			.getLogger(ServiceController.class);

}
