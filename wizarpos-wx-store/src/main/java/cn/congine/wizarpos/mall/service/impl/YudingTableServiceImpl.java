package cn.congine.wizarpos.mall.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.congine.wizarpos.mall.model.ErpTable;
import cn.congine.wizarpos.mall.service.ErpCarterOrderService;
import cn.congine.wizarpos.mall.service.ErpTableService;
import cn.congine.wizarpos.mall.service.YudingTableService;

@Service("yudingTableService")
public class YudingTableServiceImpl implements YudingTableService {

	@Autowired
	private ErpCarterOrderService erpCarterOrderService;

	@Autowired
	private ErpTableService erpTableService;

	/**
	 * 获取空闲的table
	 */
	@Override
	public List<ErpTable> getYudingTableIdByDate(String mid, Date date) {
		String tableNos = "";
		// 获取已经被预定的table_id
		List<String> tableNoList = erpCarterOrderService
				.getYudingTableIdByDate(mid, date);
		tableNos = YudingTableServiceImpl.processTableNoStr(tableNoList);

		// 获取空闲table
		List<ErpTable> tables = erpTableService.getIdleTables(mid, tableNos);

		return tables;
	}

	/**
	 * 获取空闲的table
	 */
	@Override
	public List<ErpTable> getYudingTableIdByDateStage(String mid, Date date,
			String stageId) {
		String tableIds = "";
		// 获取已经被预定的table_id
		List<String> tableIdList = erpCarterOrderService
				.getYudingTableIdByDateStage(mid, date, stageId);
		tableIds = YudingTableServiceImpl.processTableNoStr(tableIdList);

		// 获取空闲table
		List<ErpTable> tables = erpTableService.getIdleTables(mid, tableIds);

		return tables;
	}

	/**
	 * 返回的数据格式是：'1','2','3'
	 */
	protected static String processTableNoStr(List<String> tableNoList) {
		String tableNos = "";
		for (int i = 0; i < tableNoList.size(); i++) {
			String tableNo = tableNoList.get(i);
			if (tableNo != null && !tableNo.trim().isEmpty()) {
				String[] arrTableIDs = tableNo.split(",");
				for (int j = 0; j < arrTableIDs.length; j++) {
					tableNos += "'" + arrTableIDs[j] + "',";
				}
			}
		}
		if (tableNos.length() > 0) {
			tableNos = tableNos.substring(0, tableNos.length() - 1);
		}
		return tableNos;
	}

}
