package cn.congine.wizarpos.mall.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.congine.wizarpos.mall.dao.MyShakeDao;
import cn.congine.wizarpos.mall.entity.MyShake;
import cn.congine.wizarpos.mall.service.MyShakeService;

@Service("myShakeService")
public class MyShakeServiceImpl implements MyShakeService {

	@Autowired
	private MyShakeDao myShakeDao;

	@Override
	public List<MyShake> getMyShakeList(String mid, String onlineActivityId,List<String> ticketMids) {
		if(ticketMids != null && ticketMids.size() > 0) {
			return myShakeDao.getMyShakeList(mid, onlineActivityId,ticketMids);
		} else {
			return myShakeDao.getMyShakeListExitHongBao(mid, onlineActivityId);
		}
	}
	
	@Override
	public List<MyShake> getMyShakeList(String mid, String onlineActivityId) {
		return myShakeDao.getMyShakeList(mid, onlineActivityId);
	}

	@Override
	public MyShake getMyShakeByid(String ticketId, String mid, String onlineActivityId) {
		return myShakeDao.getMyShakeByid(ticketId,mid, onlineActivityId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void update(MyShake object) {
		myShakeDao.update(object);
	}

	@Override
	public MyShake get(String id) {
		return myShakeDao.get(id);
	}
}
