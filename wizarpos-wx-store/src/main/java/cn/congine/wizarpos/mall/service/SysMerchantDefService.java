package cn.congine.wizarpos.mall.service;

import cn.congine.wizarpos.mall.model.SysMerchantDef;

public interface SysMerchantDefService {

	SysMerchantDef getSysMerchantDefByMid(String mid);

	SysMerchantDef getSysMerchantDefByPid(String pid);

	void updateAccess(SysMerchantDef obj);

	void updateStartNo(String mid, int startNo);
}
