package cn.congine.wizarpos.mall.model.open;

import cn.congine.wizarpos.mall.model.MrtWechartUserInfo;

public class MrtWechartUserInfoOutput extends CommonOutput {

	private static final long serialVersionUID = 1L;

	private MrtWechartUserInfo result = null;

	public MrtWechartUserInfo getResult() {
		return result;
	}

	public void setResult(MrtWechartUserInfo userInfo) {
		this.result = userInfo;
	}
	
}
