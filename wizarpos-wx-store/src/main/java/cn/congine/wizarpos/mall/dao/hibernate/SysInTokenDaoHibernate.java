package cn.congine.wizarpos.mall.dao.hibernate;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import cn.congine.wizarpos.mall.dao.SysInTokenDao;
import cn.congine.wizarpos.mall.entity.SysInToken;

@Repository("sysInTokenDao")
public class SysInTokenDaoHibernate extends GenericDaoHibernate<SysInToken>
		implements SysInTokenDao {

	public SysInTokenDaoHibernate() {
		super(SysInToken.class);
	}
	
	
	@Override
	public SysInToken getSysInTokenByTokenId(String tokenId) {
		Query query = getSession()
				.createQuery(
						"from SysInToken where tokenId = :tokenId")
				.setParameter("tokenId", tokenId);
		return (SysInToken) query.uniqueResult();
	}


	@Override
	public void delete(SysInToken object) {
		Query query = getSession().createSQLQuery(
						"delete from sys_in_token where token_id=:tokenId")
				.setParameter("tokenId", object.getTokenId());
		query.executeUpdate();
	}
}
