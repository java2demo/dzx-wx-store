package cn.congine.wizarpos.mall.dao;

import cn.congine.wizarpos.mall.model.MrtIbeacon;

public interface MrtIbeaconDao extends GenericDao<MrtIbeacon> {

	public MrtIbeacon getActive(String mid);

}
