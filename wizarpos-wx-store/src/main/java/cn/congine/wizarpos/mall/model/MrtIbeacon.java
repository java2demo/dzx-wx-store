package cn.congine.wizarpos.mall.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mrt_ibeacon")
public class MrtIbeacon extends BaseEntity {
	private static final long serialVersionUID = 1L;

	// 唯一标识
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String id;

	// 慧商户号
	@Column(name = "mid")
	private String mid;

	// 场景，A：单张券发放，B：多张券发放，C：广告投放
	@Column(name = "type")
	private String type;

	// 卡券定义ID
	@Column(name = "content")
	private String content;

	// 0：活跃场景，1：冻结场景
	@Column(name = "enabled")
	private String enabled;

	// 创建时间
	@Column(name = "create_time")
	private long createTime;
	
	// 关注链接
	@Column(name = "attention_url")
	private String attentionUrl;
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(long createTime) {
		this.createTime = createTime;
	}

	public String getEnabled() {
		return enabled;
	}

	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}

	public String getAttentionUrl() {
		return attentionUrl;
	}

	public void setAttentionUrl(String attentionUrl) {
		this.attentionUrl = attentionUrl;
	}

}
