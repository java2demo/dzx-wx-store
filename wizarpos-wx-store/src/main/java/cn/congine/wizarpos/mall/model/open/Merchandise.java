package cn.congine.wizarpos.mall.model.open;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class Merchandise implements Serializable {

	private static final long serialVersionUID = 1L;

	private String productId = null;
	private String barcode = null;
	private String name = null;
	private String spec = null;
	private String unit = null;
	private String price = null;
	private String memberPrice = null;
	private String costPrice = null;
	private String min_stock_id = null;
	private String min_stock_conv = null;
	private String flag = null;
	private String hotSaleFlag = null;
	private String picUrl = null;
	private String descn = null;
	private String code = null;// add xudongdong
	private String mid = null;//add xudongdong
	private int stock;//add xudongdong 
	private String picPage = null;//add xudongdong

	/**
	 * 商品规格
	 */
	private List<Map<String, Object>> productAtts;//add xudongdong	
	
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSpec() {
		return spec;
	}

	public void setSpec(String spec) {
		this.spec = spec;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getMemberPrice() {
		return memberPrice;
	}

	public void setMemberPrice(String memberPrice) {
		this.memberPrice = memberPrice;
	}

	public String getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(String costPrice) {
		this.costPrice = costPrice;
	}

	public String getMin_stock_id() {
		return min_stock_id;
	}

	public void setMin_stock_id(String min_stock_id) {
		this.min_stock_id = min_stock_id;
	}

	public String getMin_stock_conv() {
		return min_stock_conv;
	}

	public void setMin_stock_conv(String min_stock_conv) {
		this.min_stock_conv = min_stock_conv;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getHotSaleFlag() {
		return hotSaleFlag;
	}

	public void setHotSaleFlag(String hotSaleFlag) {
		this.hotSaleFlag = hotSaleFlag;
	}

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	public String getDescn() {
		return descn;
	}

	public void setDescn(String descn) {
		this.descn = descn;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}


	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public String getPicPage() {
		return picPage;
	}

	public void setPicPage(String picPage) {
		this.picPage = picPage;
	}

	public List<Map<String, Object>> getProductAtts() {
		return productAtts;
	}

	public void setProductAtts(List<Map<String, Object>> productAtts) {
		this.productAtts = productAtts;
	}
}
