package cn.congine.wizarpos.mall.service;

import cn.congine.wizarpos.mall.model.SysInTicket;

public interface SysInTicketService {

	public void updateSysInTicket(SysInTicket obj);

	public SysInTicket findByApiSymbolAndMid(String apiSymbol, String mid);

	public SysInTicket saveSysInTicket(SysInTicket sysInTicket);

	public void del(SysInTicket obj);
}
