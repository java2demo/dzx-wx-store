package cn.congine.wizarpos.mall.vo;

/**
 * 电子券
 * 
 */
public class Ticket {

	public Ticket() {
	}

	public Ticket(int id) {
		this.id = id;
	}

	private int id;

	private String balance;

	private String ticketName;

	private String startTime;

	private Boolean validFlag;

	private String expriyTime;

	private String counts;

	private String desc;

	private String wxFlag;

	private String wxCodeId;

	private String ticketNo;

	private String wxAdded;

	public String getTicketNo() {
		return ticketNo;
	}

	public void setTicketNo(String ticketNo) {
		this.ticketNo = ticketNo;
	}

	public String getWxAdded() {
		return wxAdded;
	}

	public void setWxAdded(String wxAdded) {
		this.wxAdded = wxAdded;
	}

	public String getWxCodeId() {
		return wxCodeId;
	}

	public void setWxCodeId(String wxCodeId) {
		this.wxCodeId = wxCodeId;
	}

	public String getWxFlag() {
		return wxFlag;
	}

	public void setWxFlag(String wxFlag) {
		this.wxFlag = wxFlag;
	}

	public Boolean getValidFlag() {
		return validFlag;
	}

	public String getBalance() {
		return balance;
	}

	public String getCounts() {
		return counts;
	}

	public void setCounts(String counts) {
		this.counts = counts;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getTicketName() {
		return ticketName;
	}

	public void setTicketName(String ticketName) {
		this.ticketName = ticketName;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public Boolean isValidFlag() {
		return validFlag;
	}

	public void setValidFlag(Boolean validFlag) {
		this.validFlag = validFlag;
	}

	public String getExpriyTime() {
		return expriyTime;
	}

	public void setExpriyTime(String expriyTime) {
		this.expriyTime = expriyTime;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
