package cn.congine.wizarpos.mall.dao;

import cn.congine.wizarpos.mall.entity.MyOpenidRelation;

public interface MyOpenidRelationDao extends GenericDao<MyOpenidRelation> {
	MyOpenidRelation getByMidAndDzxOpenid(String mid, String dzxOpenid);
}
