package cn.congine.wizarpos.mall.dao.hibernate;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import cn.congine.wizarpos.mall.dao.WeiXinTemplateDao;
import cn.congine.wizarpos.mall.model.WeiXinTemplate;

@Repository("weiXinTemplateDao")
public class WeiXinTemplateDaoHibernate extends
		GenericDaoHibernate<WeiXinTemplate> implements WeiXinTemplateDao {

	public WeiXinTemplateDaoHibernate() {
		super(WeiXinTemplate.class);
	}

	@Override
	public WeiXinTemplate getTemplate(String mid, String templateNo) {

		Query query = getSession()
				.createQuery(
						"from WeiXinTemplate where mid = :mid and templateNo= :templateNo")
				.setParameter("mid", mid)
				.setParameter("templateNo", templateNo);
		return (WeiXinTemplate) query.uniqueResult();
	}
}
