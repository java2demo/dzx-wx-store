package cn.congine.wizarpos.mall.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import cn.congine.wizarpos.mall.model.BaseEntity;

/**
 * 用户openid绑定关系
 * 
 * @author Guzhenjuan
 * @date 2015-05-12
 * 
 */
@Entity
@Table(name = "my_openid_relation")
public class MyOpenidRelation extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id; //主键ID

	private String mid;// 一般商户的慧商户号

	private String dzxOpenid;// 用户在东之星的公众号的openid

	private String gzhOpenid; // 用户在商户公众号的openid

	private Integer status;// 0绑定 , 1东之星公众号取消关注后取消绑定 , 2商户公众号取消关注后取消绑定

	private Date createTime;// 绑定时间
	
	private Date lastTime;// 最后修改时间

	@Id
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@GeneratedValue(generator = "system-uuid")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	@Column(name = "dzx_openid")
	public String getDzxOpenid() {
		return dzxOpenid;
	}


	public void setDzxOpenid(String dzxOpenid) {
		this.dzxOpenid = dzxOpenid;
	}
	
	@Column(name = "gzh_openid")
	public String getGzhOpenid() {
		return gzhOpenid;
	}

	public void setGzhOpenid(String gzhOpenid) {
		this.gzhOpenid = gzhOpenid;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Column(name = "create_time")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Column(name = "last_time")
	public Date getLastTime() {
		return lastTime;
	}

	public void setLastTime(Date lastTime) {
		this.lastTime = lastTime;
	}
}
