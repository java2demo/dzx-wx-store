package cn.congine.wizarpos.mall.dao.hibernate;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import cn.congine.wizarpos.mall.dao.SaleOrderDao;
import cn.congine.wizarpos.mall.model.SaleOrder;

@Repository("saleOrderDao")
public class SaleOrderDaoHibernate extends GenericDaoHibernate<SaleOrder>
		implements SaleOrderDao {

	public SaleOrderDaoHibernate() {
		super(SaleOrder.class);
	}

	@Override
	public SaleOrder getById(String id) {
		Query query = getSession().createQuery("from SaleOrder where id = :id")
				.setParameter("id", id);
		return (SaleOrder) query.uniqueResult();
	}

	@Override
	public SaleOrder getByNo(String orderNo) {
		Query query = getSession().createQuery(
				"from SaleOrder where orderNo = :orderNo").setParameter(
				"orderNo", orderNo);
		return (SaleOrder) query.uniqueResult();
	}

}
