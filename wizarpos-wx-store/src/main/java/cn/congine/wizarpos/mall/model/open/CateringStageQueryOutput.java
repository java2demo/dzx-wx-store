package cn.congine.wizarpos.mall.model.open;

import java.util.List;

public class CateringStageQueryOutput extends CommonOutput {

	private static final long serialVersionUID = 1L;
	private List<Stage> result = null;

	public List<Stage> getResult() {
		return result;
	}

	public void setResult(List<Stage> result) {
		this.result = result;
	}
}
