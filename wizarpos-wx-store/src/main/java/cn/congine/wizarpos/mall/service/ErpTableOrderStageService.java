package cn.congine.wizarpos.mall.service;

import java.util.List;

import cn.congine.wizarpos.mall.model.ErpTableOrderStage;

public interface ErpTableOrderStageService {

	List<ErpTableOrderStage> getAll(String mid);

	Object getCurrentStage(String mid);

	ErpTableOrderStage getByIdMid(String stageId, String mid);

}
