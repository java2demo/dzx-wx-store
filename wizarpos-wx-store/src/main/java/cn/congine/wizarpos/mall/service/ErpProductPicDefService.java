package cn.congine.wizarpos.mall.service;

import cn.congine.wizarpos.mall.model.ErpProductPicDef;

public interface ErpProductPicDefService {

	ErpProductPicDef getErpProductPicDefById(String id);
}
