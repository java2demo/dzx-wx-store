package cn.congine.wizarpos.mall.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.congine.wizarpos.mall.dao.MrtMerchantDefDao;
import cn.congine.wizarpos.mall.entity.MrtMerchantDef;
import cn.congine.wizarpos.mall.service.MrtMerchantDefService;

@Service("MrtMerchantDefService")
public class MrtMerchantDefServiceImpl implements MrtMerchantDefService {

	@Autowired
	private MrtMerchantDefDao mrtMerchantDefDao;

	@Override
	public MrtMerchantDef getSysMerchantDefByMid(String mid) {
		return mrtMerchantDefDao.getSysMerchantDefByMid(mid);
	}

}
