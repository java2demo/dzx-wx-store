package cn.congine.wizarpos.mall.weChat.request;

import java.util.Date;

public abstract class RequestMessage {

	protected String pid;

	protected String openId;

	protected MessageType type;

	protected Date createTime;

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public MessageType getType() {
		return type;
	}

	public void setType(MessageType type) {
		this.type = type;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Override
	public String toString() {
		return "To[" + pid + "] From[" + openId + "] Type[" + type.getLabel()
				+ "] Create[" + createTime + "]";
	}
}