package cn.congine.wizarpos.mall.dao.hibernate;

import java.util.List;

import org.hibernate.FlushMode;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import cn.congine.wizarpos.mall.dao.BankCardBindDao;
import cn.congine.wizarpos.mall.model.BankCardBind;
import cn.congine.wizarpos.mall.utils.Function;

@Repository("bankCardBindDao")
public class BankCardBindDaoHibernate extends GenericDaoHibernate<BankCardBind>
		implements BankCardBindDao {

	public BankCardBindDaoHibernate() {
		super(BankCardBind.class);
	}

	@Override
	public BankCardBind save(BankCardBind obj) {
		String uuid = Function.getUid();
		obj.setId(uuid);
		String sql = "INSERT INTO wx_bank_card_bind(id,card_id,bank_card_no,bind_flag,bind_time,create_time,mid,nick_name,open_id)"
				+ "values(?,?,?,?,?,?,?,?,?)";

		this.getSession().setFlushMode(FlushMode.MANUAL);

		Query q = getSession().createSQLQuery(sql);

		q.setString(0, uuid);
		q.setString(1, obj.getCardId());
		q.setString(2, obj.getBankCardNo());
		q.setBoolean(3, obj.getBindFlag());
		q.setString(4,
				Function.formatDate(obj.getBindTime(), "yyyy-MM-dd HH:mm:ss"));
		q.setString(5,
				Function.formatDate(obj.getCreateTime(), "yyyy-MM-dd HH:mm:ss"));
		q.setString(6, obj.getMid());
		q.setString(7, obj.getNickName());
		q.setString(8, obj.getOpenId());

		q.executeUpdate();
		return obj;
	}

	@Override
	public void update(BankCardBind obj) {
		String now = Function.formatDate("yyyy-MM-dd HH:mm:ss");
		String sql = "UPDATE wx_bank_card_bind SET card_id = ?, bank_card_no = ?,"
				+ " bind_flag = ?, bind_time = ?, nick_name = ?, open_id = ?"
				+ " WHERE id = ? and mid = ?";

		this.getSession().setFlushMode(FlushMode.MANUAL);

		Query q = getSession().createSQLQuery(sql);

		q.setString(0, obj.getCardId());
		q.setString(1, obj.getBankCardNo());
		q.setBoolean(2, obj.getBindFlag());
		q.setString(3, now);
		q.setString(4, obj.getNickName());
		q.setString(5, obj.getOpenId());
		q.setString(6, obj.getId());
		q.setString(7, obj.getMid());
		
		q.executeUpdate();
	}

	@Override
	public BankCardBind find(String id, String mid) {
		return (BankCardBind) getSession()
				.createQuery(
						"from BankCardBind b where b.mid=:mid and b.id=:id")
				.setString("mid", mid).setString("id", id).uniqueResult();
	}

	@Override
	public BankCardBind get(String cardNo, String mid) {
		return (BankCardBind) getSession()
				.createQuery(
						"from BankCardBind b where b.mid=:mid and b.bankCardNo=:bankCardNo")
				.setString("mid", mid).setString("bankCardNo", cardNo)
				.uniqueResult();
	}

	@Override
	public BankCardBind get(Integer cid) {
		return (BankCardBind) getSession()
				.createQuery("from BankCardBind b where b.cid=:cid")
				.setInteger("cid", cid).uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BankCardBind> getList(String mid) {
		return getSession().createQuery("from BankCardBind b where b.mid=:mid")
				.setString("mid", mid).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BankCardBind> getList(String mid, String openId) {
		return getSession()
				.createQuery("from BankCardBind where mid=:mid and openId=:openId and bindFlag='1'")
				.setString("mid", mid).setString("openId", openId).list();
	}
}
