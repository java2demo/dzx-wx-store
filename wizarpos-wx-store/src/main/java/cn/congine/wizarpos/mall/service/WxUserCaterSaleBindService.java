package cn.congine.wizarpos.mall.service;

import java.util.List;

import cn.congine.wizarpos.mall.model.WxUserCaterSaleBind;

public interface WxUserCaterSaleBindService {
	/**
	 * 存储记录
	 * 
	 * @param obj
	 */
	void save(WxUserCaterSaleBind obj);

	/**
	 * 检索用户预订单
	 * 
	 * @param openId
	 * @return
	 */
	List<WxUserCaterSaleBind> getByOpenId(String openId);

	/**
	 * 按页检索用户预订单
	 * 
	 * @param openId
	 * @param pageNo
	 * @return
	 */
	List<WxUserCaterSaleBind> getByOpenId(String openId, int pageNo);

	/**
	 * 检索用户在商户下的预订单
	 * 
	 * @param openId
	 * @param mid
	 * @return
	 */
	List<Object[]> getByOpenIdMid(String openId, String mid);

	/**
	 * 按页检索用户在商户下的预订单
	 * 
	 * @param openId
	 * @param mid
	 * @param pageNo
	 * @return
	 */
	List<WxUserCaterSaleBind> getByOpenIdMid(String openId, String mid,
			int pageNo);
}
