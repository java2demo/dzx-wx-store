package cn.congine.wizarpos.mall.dao;

import java.util.List;

import cn.congine.wizarpos.mall.model.WxUserCaterBind;

public interface WxUserCaterBindDao extends GenericDao<WxUserCaterBind> {

	/**
	 * 检索用户预订单
	 * 
	 * @param openId
	 * @return
	 */
	List<WxUserCaterBind> getByOpenId(String openId);

	/**
	 * 按页检索用户预订单
	 * 
	 * @param openId
	 * @param pageNo
	 * @return
	 */
	List<WxUserCaterBind> getByOpenId(String openId, int pageNo);

	/**
	 * 检索用户在商户下的预订单
	 * 
	 * @param openId
	 * @param mid
	 * @return
	 */
	List<Object[]> getByOpenIdMid(String openId, String mid);

	/**
	 * 按页检索用户在商户下的预订单
	 * 
	 * @param openId
	 * @param mid
	 * @param pageNo
	 * @return
	 */
	List<WxUserCaterBind> getByOpenIdMid(String openId, String mid, int pageNo);

}
