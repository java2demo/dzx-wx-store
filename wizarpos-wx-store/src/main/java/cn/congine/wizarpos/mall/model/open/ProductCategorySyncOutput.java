package cn.congine.wizarpos.mall.model.open;

import java.util.List;

import cn.congine.wizarpos.mall.model.ErpProductCategory;


public class ProductCategorySyncOutput extends CommonOutput {

	private static final long serialVersionUID = 1L;

	private List<ErpProductCategory> result = null;

	public List<ErpProductCategory> getResult() {
		return result;
	}

	public void setResult(List<ErpProductCategory> result) {
		this.result = result;
	}
}
