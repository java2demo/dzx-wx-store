package cn.congine.wizarpos.mall.dao.hibernate;

import java.util.List;

import org.hibernate.FlushMode;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import cn.congine.wizarpos.mall.dao.MrtTicketDefDao;
import cn.congine.wizarpos.mall.model.MrtTicketDef;
import cn.congine.wizarpos.mall.utils.Function;

@Repository("mrtTicketDefDao")
public class MrtTicketDefDaoHibernate extends GenericDaoHibernate<MrtTicketDef>
		implements MrtTicketDefDao {

	public MrtTicketDefDaoHibernate() {
		super(MrtTicketDef.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MrtTicketDef> getMrtTicketDefByMid(String mid) {
		Query query = getSession().createQuery(
				"from MrtTicketDef where mid = :mid and usedFlag=1 and state=0").setParameter("mid", mid);
		return (List<MrtTicketDef>) query.list();
	}

	@Override
	public MrtTicketDef getByWxCardId(String wxCardId) {
		Query query = getSession().createQuery(
				"from MrtTicketDef where wxCodeId = :wxCardId and usedFlag=1 and state=0").setParameter(
				"wxCardId", wxCardId);
		return (MrtTicketDef) query.uniqueResult();
	}

	@Override
	public MrtTicketDef save(MrtTicketDef t) {
		String sql = "insert into mrt_ticket_def (id,ticket_code,mid,ticket_name,balance,used_flag,"
				+ "valid_period,description,create_time,state,wx_flag,wx_code_id,hb_bonus_num,hb_use_num) "
				+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		getSession().setFlushMode(FlushMode.MANUAL);
		Query q = getSession().createSQLQuery(sql);
		String uuid = Function.getUid();
		t.setId(uuid);
		q.setString(0, uuid);
		q.setString(1, t.getTicketCode());
		q.setString(2, t.getMid());
		q.setString(3, t.getTicketName());
		q.setString(4, t.getBalance());
		q.setString(5, t.getUsedFlag());
		q.setString(6, t.getValidPeriod());
		q.setString(7, t.getDescription());
		q.setString(8, Function.formatDate("yyyy-MM-dd HH:mm:ss"));
		q.setCharacter(9, t.getState());
		q.setCharacter(10, t.getWxFlag());
		q.setString(11, t.getWxCodeId() == null ? null : t.getWxCodeId());
		q.setInteger(12, t.getHbBonusNum() == null ? 0 : t.getHbBonusNum());
		q.setInteger(13, t.getHbUseNum() == null ? 0 : t.getHbUseNum());
		q.executeUpdate();
		return t;
	}

	// DRDS
	@Override
	public void update(MrtTicketDef tInfo) {
		String sql = "UPDATE mrt_ticket_def "
				+ "SET ticket_code = ?, ticket_name = ?, balance = ?, used_flag = ?, valid_period = ?, "
				+ "description = ?, create_time = ?, state = ?, wx_flag = ?, wx_code_id = ?, hb_bonus_num = ?,"
				+ " hb_use_num = ? " + "WHERE id = ? and mid = ? ";
		this.getSession().setFlushMode(FlushMode.MANUAL);
		Query q = getSession().createSQLQuery(sql);
		q.setString(0, tInfo.getTicketCode());
		q.setString(1, tInfo.getTicketName());
		q.setString(2, tInfo.getBalance());
		q.setString(3, tInfo.getUsedFlag());
		q.setString(4, tInfo.getValidPeriod());
		q.setString(5, tInfo.getDescription());
		q.setString(6, tInfo.getCreateTime());
		q.setCharacter(7, tInfo.getState());
		q.setCharacter(8, tInfo.getWxFlag());
		q.setString(9, tInfo.getWxCodeId());
		q.setInteger(10, tInfo.getHbBonusNum() == null ? 0 : tInfo.getHbBonusNum());
		q.setInteger(11, tInfo.getHbUseNum() == null ? 0 : tInfo.getHbUseNum());
		q.setString(12, tInfo.getId());
		q.setString(13, tInfo.getMid());
		q.executeUpdate();
	}

	@Override
	public MrtTicketDef find(String id, String mid) {
		return (MrtTicketDef) getSession()
				.createQuery(
						"from MrtTicketDef b where b.mid=:mid and b.id=:id and usedFlag=1 and state=0")
				.setString("mid", mid).setString("id", id).uniqueResult();
	}
	
	@Override
	public void ticketPass(String cardid, String mid) {
		String sql ="update mrt_ticket_def set used_flag='1' where mid=:mid and wx_code_id=:cardid";
		this.getSession().setFlushMode(FlushMode.MANUAL);
		Query q = getSession().createSQLQuery(sql);
		
		q.setString(0, mid);
		q.setString(1, cardid);
		q.executeUpdate();
	}

	@Override
	public void ticketUnpass(String cardid, String mid) {
		String sql ="update mrt_ticket_def set used_flag='2' where mid=:mid and wx_code_id=:cardid";
		this.getSession().setFlushMode(FlushMode.MANUAL);
		Query q = getSession().createSQLQuery(sql);

		q.setString(0, cardid);
		q.setString(1, mid);
		q.executeUpdate();
	}
}
