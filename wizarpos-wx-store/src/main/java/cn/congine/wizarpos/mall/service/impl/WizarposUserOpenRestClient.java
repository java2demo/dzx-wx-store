package cn.congine.wizarpos.mall.service.impl;

import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import cn.congine.wizarpos.mall.model.open.CommonInput;
import cn.congine.wizarpos.mall.model.open.CommonOutput;
import cn.congine.wizarpos.mall.utils.SysConstants;

/**
 * restful client 
 * 
 * @author GuZhenjuan
 *
 */
@Service("WizarposUserOpenRestClient")
public class WizarposUserOpenRestClient <T extends CommonOutput, V extends CommonInput> {
	
	private  final Log log = LogFactory.getLog(getClass());

	@Autowired
    private RestTemplate restTemplate;
	
	@Resource(name = "messageSource")
	private MessageSource messageSource;
	
	private String OPEN_SERVICE = "";
	
	/* process successful */
	private static final String SUCCESS_CODE = "0";
	private static final String SUCCESS_MESSAGE = "处理成功";

	/* the merchant is not a alliance */
	private static final String SYSTEM_ERROR_CODE = "1";
	private static final String SYSTEM_ERROR_MESSAGE = "系统错误";
	
	@PostConstruct
	public void initialize() {
		OPEN_SERVICE = messageSource.getMessage(SysConstants.USER_SERVICE_OPEN, null,
				Locale.ENGLISH);
		log.debug("USER_SERVICE_OPEN: " + OPEN_SERVICE);
	}
	/**
	 * query wizarposOpen_server
	 * 
	 * @param mid
	 * @param cardNo
	 * @return
	 */
	public T post(V input, String method, Class<T> z) {

		String url = OPEN_SERVICE + method;

		T output = null;;
		try {
			output = (T) restTemplate.postForObject(url, input, z);
		} catch (RestClientException e) {
			log.error(e.getMessage());
		}
		
		return output;
	}
	
	@SuppressWarnings("unused")
	private void setSuccess(CommonOutput output) {
		output.setCode(SUCCESS_CODE);
		output.setMessage(SUCCESS_MESSAGE);
	}
	
	@SuppressWarnings("unused")
	private void setSystemError(CommonOutput output) {
		output.setCode(SYSTEM_ERROR_CODE);
		output.setMessage(SYSTEM_ERROR_MESSAGE);
	}

}
