package cn.congine.wizarpos.mall.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.congine.wizarpos.mall.dao.ErpCaterOrderDao;
import cn.congine.wizarpos.mall.model.ErpCarterOrder;
import cn.congine.wizarpos.mall.service.ErpCarterOrderService;

@Service("erpCarterOrderService")
public class ErpCarterOrderServiceImpl implements ErpCarterOrderService {
	@Autowired
	private ErpCaterOrderDao erpCaterOrderDao;

	@Override
	public List<String> getYudingTableIdByDate(String mid, Date date) {
		return erpCaterOrderDao.getYudingTableIdByDate(mid, date);
	}

	@Override
	public List<String> getYudingTableIdByDateStage(String mid, Date date,
			String stageId) {
		return erpCaterOrderDao.getYudingTableIdByDateStage(mid, date, stageId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public ErpCarterOrder save(ErpCarterOrder order) {
		return erpCaterOrderDao.save(order);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void update(ErpCarterOrder order) {
		erpCaterOrderDao.update(order);
	}

	@Override
	public Object getReOrderId(String mid, Date date, String stageId,
			String tableId) {
		return erpCaterOrderDao.getReOrderId(mid, date, stageId, tableId);
	}

	@Override
	public ErpCarterOrder getByIdMid(String orderId, String mid) {
		return erpCaterOrderDao.getByIdMid(orderId, mid);
	}

	@Override
	public List<ErpCarterOrder> getByOpenidMid(String openId, String mid) {
		return erpCaterOrderDao.getByOpenidMid(openId, mid);
	}

}
