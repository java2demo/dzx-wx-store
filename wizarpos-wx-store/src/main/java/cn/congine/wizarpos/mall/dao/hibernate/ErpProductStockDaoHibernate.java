package cn.congine.wizarpos.mall.dao.hibernate;

import org.hibernate.FlushMode;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import cn.congine.wizarpos.mall.dao.ErpProductStockDao;
import cn.congine.wizarpos.mall.model.ErpProductStock;
import cn.congine.wizarpos.mall.utils.Function;

@Repository("erpProductStockDao")
public class ErpProductStockDaoHibernate extends GenericDaoHibernate<ErpProductStock>
		implements ErpProductStockDao {

	public ErpProductStockDaoHibernate() {
		super(ErpProductStock.class);
	}

	@Override
	public Integer getTotalStock(String mid, String productId) {
		String sql = "select sum(qty) from erp_product_stock where mid = ? and product_id = ?";
		this.getSession().setFlushMode(FlushMode.MANUAL);
		Query q = getSession().createSQLQuery(sql);
		q.setString(0, mid);
		q.setString(1, productId);
		Object object = q.uniqueResult();
		Integer result = 0;
		if (object != null) {
			try {
				result = Integer.parseInt(object.toString());
			} catch (NumberFormatException e) {
				// do nothing
			}
			
		} 
		return result ;
	}
	
	
}
