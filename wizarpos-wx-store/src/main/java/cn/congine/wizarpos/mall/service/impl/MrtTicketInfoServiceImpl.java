package cn.congine.wizarpos.mall.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.congine.wizarpos.mall.dao.MrtTicketDefDao;
import cn.congine.wizarpos.mall.dao.MrtTicketInfoDao;
import cn.congine.wizarpos.mall.model.MrtTicketInfo;
import cn.congine.wizarpos.mall.service.MrtTicketInfoService;

@Service("mrtTicketInfoService")
public class MrtTicketInfoServiceImpl implements MrtTicketInfoService {

	@Autowired
	private MrtTicketInfoDao mrtTicketInfoDao;

	@Autowired
	private MrtTicketDefDao mrtTicketDefDao;

	@Override
	public List<Object> getMrtTicketInfoByCardId(String cardId, int pageNo) {
		return mrtTicketInfoDao.getMrtTicketInfoByCardId(cardId, pageNo);
	}

	@Override
	public List<Object> getUsedTicketInfo(String cardId, int pageNo) {
		return mrtTicketInfoDao.getUsedTicketInfo(cardId, pageNo);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public MrtTicketInfo save(MrtTicketInfo ticketInfo) {
		return mrtTicketInfoDao.save(ticketInfo);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void update(MrtTicketInfo ticketInfo) {
		mrtTicketInfoDao.update(ticketInfo);
	}

	@Override
	public String getIdByWxTicketIdAndNoAndOpenid(String cardId, String no,
			String openid) {
		return (String) mrtTicketInfoDao.getIdByWxTicketIdAndNoAndOpenid(
				cardId, no, openid);
	}

	@Override
	public MrtTicketInfo get(String id) {
		return mrtTicketInfoDao.get(id);
	}

	@Override
	public MrtTicketInfo getByNo(String mid, String ticketNo) {
		return mrtTicketInfoDao.getByNo(mid, ticketNo);
	}

	@Override
	public boolean isExist(String ticketDefId, String cardId, String mid) {
		return mrtTicketInfoDao.isExist(ticketDefId, cardId, mid);
	}

	@Override
	public MrtTicketInfo getByTicketDefIdAndCardIdAndMid(String ticketDefId,
			String cardId, String mid) {
		// TODO Auto-generated method stub
		return mrtTicketInfoDao.getByTicketDefIdAndCardIdAndMid(ticketDefId,
				cardId, mid);
	}
}
