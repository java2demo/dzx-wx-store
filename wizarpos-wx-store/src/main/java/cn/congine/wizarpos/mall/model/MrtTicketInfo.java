package cn.congine.wizarpos.mall.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mrt_ticket_info")
public class MrtTicketInfo extends BaseEntity {
	private static final long serialVersionUID = 8673064447636559672L;
	// 唯一标识
	@Id
	@Column(name = "id")
	private String id;

	// 券类型
	@Column(name = "ticket_id")
	private String ticketId;

	// 券号
	@Column(name = "ticket_no")
	private String ticketNo;

	// 会员卡号
	@Column(name = "card_id")
	private String cardId;

	// 启用时间
	@Column(name = "start_time")
	private String startTime;

	// 过期时间
	@Column(name = "expriy_time")
	private String expriyTime;

	// 是否有效
	@Column(name = "valid_flag")
	private String validFlag;

	// 注销时间
	@Column(name = "cancel_time")
	private String cancelTime;

	// 添加时间
	@Column(name = "used_time")
	private String usedTime;

	// 是否添加到了卡包（0未添加，1已添加）
	@Column(name = "wx_added")
	private char wxAdded;

	// 商户号
	@Column(name = "mid")
	private String mid;

	// 红包是否分享（1分享，0未分享）
	@Column(name = "hb_shared")
	private String hbShared;

	// 红包是否分享（1分享，0未分享）
	@Column(name = "remark")
	private String remark;

	@Column(name = "master_tran_log_id")
	private String masterTranLogId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	public String getTicketNo() {
		return ticketNo;
	}

	public void setTicketNo(String ticketNo) {
		this.ticketNo = ticketNo;
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getExpriyTime() {
		return expriyTime;
	}

	public void setExpriyTime(String expriyTime) {
		this.expriyTime = expriyTime;
	}

	public String getValidFlag() {
		return validFlag;
	}

	public void setValidFlag(String validFlag) {
		this.validFlag = validFlag;
	}

	public String getCancelTime() {
		return cancelTime;
	}

	public void setCancelTime(String cancelTime) {
		this.cancelTime = cancelTime;
	}

	public String getUsedTime() {
		return usedTime;
	}

	public void setUsedTime(String usedTime) {
		this.usedTime = usedTime;
	}

	public char getWxAdded() {
		return wxAdded;
	}

	public void setWxAdded(char wxAdded) {
		this.wxAdded = wxAdded;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getHbShared() {
		return hbShared;
	}

	public void setHbShared(String hbShared) {
		this.hbShared = hbShared;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getMasterTranLogId() {
		return masterTranLogId;
	}

	public void setMasterTranLogId(String masterTranLogId) {
		this.masterTranLogId = masterTranLogId;
	}

}
