package cn.congine.wizarpos.mall.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "erp_table_order_stage")
public class ErpTableOrderStage extends BaseEntity {
	private static final long serialVersionUID = 1L;

	// 唯一标识
	@Id
	@Column(name = "id")
	private String id;

	// 慧商户号
	@Column(name = "mid")
	private String mid;

	@Column(name = "stage_name")
	private String stageName;

	@Column(name = "stage_code")
	private String stageCode;

	@Column(name = "stage_range")
	private String stageRange;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getStageName() {
		return stageName;
	}

	public void setStageName(String stageName) {
		this.stageName = stageName;
	}

	public String getStageCode() {
		return stageCode;
	}

	public void setStageCode(String stageCode) {
		this.stageCode = stageCode;
	}

	public String getStageRange() {
		return stageRange;
	}

	public void setStageRange(String stageRange) {
		this.stageRange = stageRange;
	}

}
