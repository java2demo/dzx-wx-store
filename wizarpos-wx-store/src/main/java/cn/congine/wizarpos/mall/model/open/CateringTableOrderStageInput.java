package cn.congine.wizarpos.mall.model.open;

import java.io.Serializable;

public class CateringTableOrderStageInput extends CommonInput {

	private static final long serialVersionUID = 1L;
	private String mid = null;
	private String id = null;
	public String getMid() {
		return mid;
	}
	public void setMid(String mid) {
		this.mid = mid;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
}
