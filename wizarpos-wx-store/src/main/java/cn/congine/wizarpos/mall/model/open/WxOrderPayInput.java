package cn.congine.wizarpos.mall.model.open;

public class WxOrderPayInput extends CommonInput {

	private static final long serialVersionUID = 1L;

	private String openId = null;
	private String mid = null;
	private String orderId = null;
	private String payAmount = null;
	private boolean print = false;
	// 微信消息模板跳转链接
	private String url = null;
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getOpenId() {
		return openId;
	}
	public void setOpenId(String openId) {
		this.openId = openId;
	}
	public String getMid() {
		return mid;
	}
	public void setMid(String mid) {
		this.mid = mid;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getPayAmount() {
		return payAmount;
	}
	public void setPayAmount(String payAmount) {
		this.payAmount = payAmount;
	}
	public boolean isPrint() {
		return print;
	}
	public void setPrint(boolean print) {
		this.print = print;
	}
	
}
