package cn.congine.wizarpos.mall.model.open;

import cn.congine.wizarpos.mall.model.WxAddress;


/**
 * 
 * @author xudongdong
 * Date: 2015.7.13
 *
 */
public class DefaultAddressOutput extends CommonOutput {

	private static final long serialVersionUID = 1L;

	private WxAddress wxAddress = null;

	public WxAddress getWxAddress() {
		return wxAddress;
	}

	public void setWxAddress(WxAddress wxAddress) {
		this.wxAddress = wxAddress;
	}



}
