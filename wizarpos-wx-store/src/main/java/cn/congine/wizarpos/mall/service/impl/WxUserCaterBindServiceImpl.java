package cn.congine.wizarpos.mall.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.congine.wizarpos.mall.dao.WxUserCaterBindDao;
import cn.congine.wizarpos.mall.model.WxUserCaterBind;
import cn.congine.wizarpos.mall.service.WxUserCaterBindService;

@Service("wxUserCaterBindService")
public class WxUserCaterBindServiceImpl implements WxUserCaterBindService {

	@Autowired
	private WxUserCaterBindDao wxUserCaterBindDao;

	@Override
	public void save(WxUserCaterBind obj) {
		wxUserCaterBindDao.save(obj);
	}

	@Override
	public List<WxUserCaterBind> getByOpenId(String openId) {
		return wxUserCaterBindDao.getByOpenId(openId);
	}

	@Override
	public List<WxUserCaterBind> getByOpenId(String openId, int pageNo) {
		return wxUserCaterBindDao.getByOpenId(openId, pageNo);
	}

	@Override
	public List<Object[]> getByOpenIdMid(String openId, String mid) {
		return wxUserCaterBindDao.getByOpenIdMid(openId, mid);
	}

	@Override
	public List<WxUserCaterBind> getByOpenIdMid(String openId, String mid,
			int pageNo) {
		return wxUserCaterBindDao.getByOpenIdMid(openId, mid, pageNo);
	}

}
