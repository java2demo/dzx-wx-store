package cn.congine.wizarpos.mall.dao;

import java.util.List;

import cn.congine.wizarpos.mall.model.SaleOrderItem;

public interface SaleOrderItemDao extends GenericDao<SaleOrderItem> {

	List<SaleOrderItem> getSaleOrderItemByOrderId(String orderId);
}
