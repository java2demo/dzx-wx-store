package cn.congine.wizarpos.mall.dao;

import java.util.List;

import cn.congine.wizarpos.mall.common.Page;
import cn.congine.wizarpos.mall.model.ErpProduct;

public interface ErpProductDao extends GenericDao<ErpProduct> {

	List<ErpProduct> getErpProductListByCategory(String categoryId);

	List<ErpProduct> getErpProductListByMid(String mid);

	ErpProduct getErpProductByCode(String code);

	List<ErpProduct> getErpProductByName(String name, String mid, String id);

	List<ErpProduct> getMoreHotErpProductListByMid(String mid);

	List<ErpProduct> getHotErpProductListByMid(String mid, String hotJudge);

	List<ErpProduct> getErpProductListByMid(String mid, String categoryId);

	List<ErpProduct> getHotErpProductListByMid(String mid);

	List<ErpProduct> getErpProductListByMid(String mid, Page<ErpProduct> page);

	List<ErpProduct> getHotErpProductListByMid(String mid, String hotJudge,
			Page<ErpProduct> page);

	List<ErpProduct> getHotErpProductListByMid(String mid, String hotJudge,
			Page<ErpProduct> page, String productName);

	List<ErpProduct> getHotErpProductListByMid(String mid, String hotJudge,
			String productName);

	public List<ErpProduct> getProductList(String mid, String categoryId,
			String kyeWord);

	List<ErpProduct> getProductList(String mid, String keyWord);

	List<ErpProduct> getProductList(String mid, Page<ErpProduct> page,
			String categoryId, String keyword);

	List<ErpProduct> getHotErpProductListByCategoryId(String mid,
			String hotJudge, String categoryId);

	List<ErpProduct> getErpProductListByPage(String mid, String categoryId,
			int pageIndex);
	
	public List<ErpProduct> getErpProductPageListByMid(String mid,
			String categoryId, Integer pageIndex);

}
