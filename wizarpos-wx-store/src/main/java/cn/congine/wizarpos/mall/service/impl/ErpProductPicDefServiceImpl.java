package cn.congine.wizarpos.mall.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.congine.wizarpos.mall.dao.ErpProductPicDefDao;
import cn.congine.wizarpos.mall.model.ErpProductPicDef;
import cn.congine.wizarpos.mall.service.ErpProductPicDefService;

@Service("erpProductPicDefService")
public class ErpProductPicDefServiceImpl implements ErpProductPicDefService {

	@Autowired
	private ErpProductPicDefDao erpProductPicDefDao;

	@Override
	public ErpProductPicDef getErpProductPicDefById(String id) {
		return erpProductPicDefDao.get(id);
	}

}
