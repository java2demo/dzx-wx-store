package cn.congine.wizarpos.mall.service;

import cn.congine.wizarpos.mall.model.SysParamsDef;

public interface SysParamsDefService {

	void updateSysParamsDef(SysParamsDef obj);

	SysParamsDef getSysParamsDef(String keyName);

}
