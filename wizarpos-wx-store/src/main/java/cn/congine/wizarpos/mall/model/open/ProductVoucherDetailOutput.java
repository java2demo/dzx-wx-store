package cn.congine.wizarpos.mall.model.open;

import cn.congine.wizarpos.mall.vo.ProductVoucher;

/**
 * 
 * @author xudongdong
 * Date: 2015.7.20
 */


public class ProductVoucherDetailOutput extends CommonOutput {

	private static final long serialVersionUID = 1L;
	
	private Result result = null;
		
	public Result getResult() {
		return result;
	}

	public void setResult(Result result) {
		this.result = result;
	}

	public class Result {
		
		private ProductVoucher productVoucher = null;

		public ProductVoucher getProductVoucher() {
			return productVoucher;
		}

		public void setProductVoucher(ProductVoucher productVoucher) {
			this.productVoucher = productVoucher;
		}
	}
	
}
