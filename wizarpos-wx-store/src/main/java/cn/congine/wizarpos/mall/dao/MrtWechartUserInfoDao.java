package cn.congine.wizarpos.mall.dao;

import cn.congine.wizarpos.mall.model.MrtWechartUserInfo;

public interface MrtWechartUserInfoDao extends GenericDao<MrtWechartUserInfo> {

	public MrtWechartUserInfo get(String openId, String mid);

	MrtWechartUserInfo find(String id, String mid);

}
