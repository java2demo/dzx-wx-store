package cn.congine.wizarpos.mall.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 银行卡绑定表
 * 
 * @author Administrator
 */

@Entity
@Table(name = "wx_bank_card_bind")
public class BankCardBind extends BaseEntity {

	private static final long serialVersionUID = -6386816676401989412L;

	@Id
	@GeneratedValue(generator = "uuid")
	// 指定生成器名称
	@GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
	private String id;

	@Column(name = "card_id")
	private String cardId;

	@Column(name = "bank_card_no")
	private String bankCardNo;

	@Column(name = "mid")
	private String mid;

	@Column(name = "bind_flag")
	private Boolean bindFlag;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8", shape = JsonFormat.Shape.STRING)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "bind_time")
	private Date bindTime;

	@Column(name = "c_id")
	private String cid;

	@Column(name = "open_id")
	private String openId;

	@Column(name = "nick_name")
	private String nickName;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8", shape = JsonFormat.Shape.STRING)
	@Column(name = "create_time")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date createTime = new Date();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public String getBankCardNo() {
		return bankCardNo;
	}

	public void setBankCardNo(String bankCardNo) {
		this.bankCardNo = bankCardNo;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public Boolean getBindFlag() {
		return bindFlag;
	}

	public void setBindFlag(Boolean bindFlag) {
		this.bindFlag = bindFlag;
	}

	public Date getBindTime() {
		return bindTime;
	}

	public void setBindTime(Date bindTime) {
		this.bindTime = bindTime;
	}

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

}
