/**
 * Copyright (c) 2011 Congine Information Technology Corporation, All rights reserved.
 * The Software is owned by Congine and is protected by copyright laws and other national laws.
 * You agree that you have no right, title or interest in the Software.
 * This code cannot simply be copied and put under another distribution license.
 * Copyright remains Congine's, and as such any Copyright notices in the code are not to be removed.
 */
package cn.congine.wizarpos.mall.model;

import java.io.Serializable;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class BaseEntity implements Serializable {
	private static final long serialVersionUID = 8544854365241203554L;

	// public static final String SCHEMA = "wizarpos_o2o";

	/**
	 * @return a String representation of this class.
	 */
	@Override
	public String toString() {
		return getClass().toString();
	}

}
