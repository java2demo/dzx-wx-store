package cn.congine.wizarpos.mall.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.congine.wizarpos.mall.dao.SysParamsDefDao;
import cn.congine.wizarpos.mall.model.SysParamsDef;
import cn.congine.wizarpos.mall.service.SysParamsDefService;

@Service("sysParamsDefService")
public class SysParamsDefServiceImpl implements SysParamsDefService {

	@Autowired
	private SysParamsDefDao sysParamsDefDao;

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void updateSysParamsDef(SysParamsDef obj) {
		sysParamsDefDao.update(obj);
	}

	@Override
	public SysParamsDef getSysParamsDef(String keyName) {
		return sysParamsDefDao.get(keyName);
	}

}
