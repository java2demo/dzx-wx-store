package cn.congine.wizarpos.mall.dao.hibernate;

import org.hibernate.FlushMode;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import cn.congine.wizarpos.mall.dao.SysWxPrimaryMerchantTokenDao;
import cn.congine.wizarpos.mall.model.SysWxPrimaryMerchantToken;


@Repository("sysWxPrimaryMerchantTokenDao")
public class SysWxPrimaryMerchantTokenDaoHibernate extends GenericDaoHibernate<SysWxPrimaryMerchantToken> implements SysWxPrimaryMerchantTokenDao{

	public SysWxPrimaryMerchantTokenDaoHibernate() {
		super(SysWxPrimaryMerchantToken.class);
	}
	
	public SysWxPrimaryMerchantToken getByComponentAppid(String componentAppid) {
		Query query = getSession().createQuery(
				"from SysWxPrimaryMerchantToken where componentAppid=:componentAppid").setParameter("componentAppid", componentAppid);
		return (SysWxPrimaryMerchantToken) query.uniqueResult();
	}
	
	public int update(String mid, String component_appid, String component_access_token, long component_access_token_expirey, String component_verify_ticket) {
		this.getSession().setFlushMode(FlushMode.MANUAL);
		Query query = getSession().createSQLQuery("UPDATE sys_wx_primary_merchant_token SET "
				+ "component_access_token=:component_access_token,"
				+ "component_access_token_expirey=:component_access_token_expirey,"
				+ "component_verify_ticket=:component_verify_ticket"
				+ " WHERE mid=:mid and component_appid=:component_appid")
				.setParameter("component_access_token", component_access_token)
				.setParameter("component_access_token_expirey", component_access_token_expirey)
				.setParameter("component_verify_ticket", component_verify_ticket)
				.setParameter("mid", mid)
				.setParameter("component_appid", component_appid);
		return query.executeUpdate();
	}
}
