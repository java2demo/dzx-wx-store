package cn.congine.wizarpos.mall.model.dzx;

import java.util.List;
import java.util.Map;

import cn.congine.wizarpos.mall.model.open.CommonOutput;

public class SendCouponsOutput extends CommonOutput {

	private static final long serialVersionUID = 1L;
	
	private  List<Map<String, Object>> result;


	public List<Map<String, Object>> getResult()
	{
		return result;
	}


	public void setResult(List<Map<String, Object>> result)
	{
		this.result = result;
	}

		
}
