package cn.congine.wizarpos.mall.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.congine.wizarpos.mall.common.Page;
import cn.congine.wizarpos.mall.dao.SysMerchantDefDao;
import cn.congine.wizarpos.mall.model.SysMerchantDef;
import cn.congine.wizarpos.mall.service.SysMerchantDefService;

@Service("sysMerchantDefService")
public class SysMerchantDefServiceImpl implements SysMerchantDefService {

	@Autowired
	private SysMerchantDefDao sysMerchantDefDao;

	@Override
	public SysMerchantDef getSysMerchantDefByMid(String mid) {
		return sysMerchantDefDao.getSysMerchantDefByMid(mid);
	}

	@Override
	public SysMerchantDef getSysMerchantDefByPid(String pid) {
		return sysMerchantDefDao.getSysMerchantDefByPid(pid);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void updateAccess(SysMerchantDef obj) {
		sysMerchantDefDao.updateAccess(obj);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void updateStartNo(String mid, int startNo) {
		sysMerchantDefDao.updateStartNo(mid, startNo);
	}
}
