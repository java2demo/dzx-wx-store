package cn.congine.wizarpos.mall.model.open;

import java.util.List;

public class TicketUseInput extends CommonInput {

	private static final long serialVersionUID = 1L;
	
	private String mid = null;
	private String openId = null;
	private String orderId = null;
	private List<String> ticketInfoId = null;
	public String getOpenId() {
		return openId;
	}
	public void setOpenId(String openId) {
		this.openId = openId;
	}
	public String getMid() {
		return mid;
	}
	public void setMid(String mid) {
		this.mid = mid;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public List<String> getTicketInfoId() {
		return ticketInfoId;
	}
	public void setTicketInfoId(List<String> ticketInfoId) {
		this.ticketInfoId = ticketInfoId;
	}
}
