package cn.congine.wizarpos.mall.dao.hibernate;

import org.hibernate.FlushMode;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import cn.congine.wizarpos.mall.dao.ErpMerchantLoginDao;
import cn.congine.wizarpos.mall.model.ErpMerchantLogin;
import cn.congine.wizarpos.mall.utils.Function;

@Repository("ErpMerchantLoginDao")
public class ErpMerchantLoginDaoHibernate extends GenericDaoHibernate<ErpMerchantLogin>
		implements ErpMerchantLoginDao {

	public ErpMerchantLoginDaoHibernate() {
		super(ErpMerchantLogin.class);
	}

	@Override
	public ErpMerchantLogin save(ErpMerchantLogin obj) {
		String uuid = Function.getUid();
		obj.setId(uuid);
		String sql = "INSERT INTO erp_merchant_login(id,mid,login_name,login_time,system_type)"
				+ "values(?,?,?,?,?)";
		this.getSession().setFlushMode(FlushMode.MANUAL);
		Query q = getSession().createSQLQuery(sql);

		q.setParameter(0, obj.getId());
		q.setParameter(1, obj.getMid());
		q.setParameter(2, obj.getLoginName());
		q.setParameter(3, obj.getLoginTime());
		q.setParameter(4, obj.getSystemType());

		q.executeUpdate();
		return obj;
	}

	@Override
	public Object count(String mid) {
		String sql = "select count(*) from erp_merchant_login where mid=? and system_type='2'";
		Query query = getSession().createSQLQuery(sql);
		query.setParameter(0, mid);
		return query.uniqueResult();
	}
	
}
