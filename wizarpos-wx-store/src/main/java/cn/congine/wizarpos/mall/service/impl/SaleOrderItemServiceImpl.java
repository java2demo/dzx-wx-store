package cn.congine.wizarpos.mall.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.congine.wizarpos.mall.dao.SaleOrderItemDao;
import cn.congine.wizarpos.mall.model.SaleOrderItem;
import cn.congine.wizarpos.mall.service.SaleOrderItemService;

@Service("saleOrderItemService")
public class SaleOrderItemServiceImpl implements SaleOrderItemService {

	@Autowired
	private SaleOrderItemDao saleOrderItemDao;

	@Override
	public List<SaleOrderItem> getSaleOrderItemByOrderId(String orderId) {
		return saleOrderItemDao.getSaleOrderItemByOrderId(orderId);
	}

}
