package cn.congine.wizarpos.mall.model.open;

import java.util.List;

public class WeChartMemberInfoQueryOutput extends CommonOutput {

	private static final long serialVersionUID = 1L;

	private Result result = null;

	public Result getResult() {
		return result;
	}

	public void setResult(Result result) {
		this.result = result;
	}

	public class Result {

		private String id = null;//add xudongdong
		private String cardNo = null;
		private String cardType = null;
		private String userName = null;
		private String mobileNo = null;
		private String nickName = null;
		private String imgUrl = null;
		private String openId = null;
		private String balance = null;
		private String activeTime = null;
		private String expriyTime = null;
		private String canceled = null;
		private String cancelTime = null;
		private String freeze = null;
		private String freezeTime = null;
		private List<Ticket> ticketList = null;

		
		private String points;
		
		
		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getPoints()
		{
			return points;
		}

		public void setPoints(String points)
		{
			this.points = points;
		}

		public String getCardNo() {
			return cardNo;
		}

		public void setCardNo(String cardNo) {
			this.cardNo = cardNo;
		}

		public String getCardType() {
			return cardType;
		}

		public void setCardType(String cardType) {
			this.cardType = cardType;
		}

		public String getUserName() {
			return userName;
		}

		public void setUserName(String userName) {
			this.userName = userName;
		}

		public String getMobileNo() {
			return mobileNo;
		}

		public void setMobileNo(String mobileNo) {
			this.mobileNo = mobileNo;
		}

		public String getNickName() {
			return nickName;
		}

		public void setNickName(String nickName) {
			this.nickName = nickName;
		}

		public String getImgUrl() {
			return imgUrl;
		}

		public void setImgUrl(String imgUrl) {
			this.imgUrl = imgUrl;
		}

		public String getOpenId() {
			return openId;
		}

		public void setOpenId(String openId) {
			this.openId = openId;
		}

		public String getBalance() {
			return balance;
		}

		public void setBalance(String balance) {
			this.balance = balance;
		}

		public String getActiveTime() {
			return activeTime;
		}

		public void setActiveTime(String activeTime) {
			this.activeTime = activeTime;
		}

		public String getExpriyTime() {
			return expriyTime;
		}

		public void setExpriyTime(String expriyTime) {
			this.expriyTime = expriyTime;
		}

		public String getCanceled() {
			return canceled;
		}

		public void setCanceled(String canceled) {
			this.canceled = canceled;
		}

		public String getCancelTime() {
			return cancelTime;
		}

		public void setCancelTime(String cancelTime) {
			this.cancelTime = cancelTime;
		}

		public String getFreeze() {
			return freeze;
		}

		public void setFreeze(String freeze) {
			this.freeze = freeze;
		}

		public String getFreezeTime() {
			return freezeTime;
		}

		public void setFreezeTime(String freezeTime) {
			this.freezeTime = freezeTime;
		}

		public List<Ticket> getTicketList() {
			return ticketList;
		}

		public void setTicketList(List<Ticket> ticketList) {
			this.ticketList = ticketList;
		}
	}
}
