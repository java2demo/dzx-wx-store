package cn.congine.wizarpos.mall.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import cn.congine.wizarpos.mall.model.BaseEntity;

/**
 * 红包流水表
 * 
 * @author Kirk Zhou
 * @date 2015-05-12
 * 
 */
@Entity
@Table(name = "my_red_envelopes")
public class MyRedEnvelopes extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id; // 主键ID

	private String mid;// 商户号

	private Long amount;

	private MyOnlineActivity myOnlineActivity;

	private MyShake myShake;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "my_shake_id")
	public MyShake getMyShake() {
		return myShake;
	}

	public void setMyShake(MyShake myShake) {
		this.myShake = myShake;
	}

	@Id
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@GeneratedValue(generator = "system-uuid")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "online_activity_id")
	public MyOnlineActivity getMyOnlineActivity() {
		return myOnlineActivity;
	}

	public void setMyOnlineActivity(MyOnlineActivity myOnlineActivity) {
		this.myOnlineActivity = myOnlineActivity;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

}
