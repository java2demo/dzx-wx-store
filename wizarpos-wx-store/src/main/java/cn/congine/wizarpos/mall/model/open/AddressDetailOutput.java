package cn.congine.wizarpos.mall.model.open;

import cn.congine.wizarpos.mall.model.WxAddress;

/**
 * 
 * @author xudongdong
 * Date: 2015.7.13
 */


public class AddressDetailOutput extends CommonOutput {

	private static final long serialVersionUID = 1L;
	
	private WxAddress result = null;

	public WxAddress getResult() {
		return result;
	}

	public void setResult(WxAddress result) {
		this.result = result;
	}

}
