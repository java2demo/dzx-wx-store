package cn.congine.wizarpos.mall.service;

import cn.congine.wizarpos.mall.model.SysWxChildMerchantToken;

public interface SysWxChildMerchantTokenService {
	
	public SysWxChildMerchantToken getByWxAppIdAndMidAndPrimaryId(String mid);
	public int updateMrtToken(SysWxChildMerchantToken t);
	public int update(SysWxChildMerchantToken sysWxChildMerchantToken);
	public SysWxChildMerchantToken findByGhCode(String gh);
}
