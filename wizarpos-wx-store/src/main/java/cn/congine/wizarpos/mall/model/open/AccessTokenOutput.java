package cn.congine.wizarpos.mall.model.open;

public class AccessTokenOutput extends CommonOutput {

	private static final long serialVersionUID = 1L;

	private Result result = null;

	public Result getResult() {
		return result;
	}

	public void setResult(Result result) {
		this.result = result;
	}

	public class Result {
		private String token = null;
		private String expires = null;
		public String getToken() {
			return token;
		}
		public void setToken(String token) {
			this.token = token;
		}
		public String getExpires() {
			return expires;
		}
		public void setExpires(String expires) {
			this.expires = expires;
		}
	}
}
