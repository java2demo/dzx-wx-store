package cn.congine.wizarpos.mall.vo;

import java.io.Serializable;

public class CateringDetail implements Serializable {

	private static final long serialVersionUID = 1L;
	private String productId = null;
	private String amount = null;
	private String price = null;
	private String qty = null;

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getQty() {
		return qty;
	}

	public void setQty(String qty) {
		this.qty = qty;
	}

}
