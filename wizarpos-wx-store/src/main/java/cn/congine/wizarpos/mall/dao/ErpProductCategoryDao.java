package cn.congine.wizarpos.mall.dao;

import java.util.List;

import cn.congine.wizarpos.mall.model.ErpProductCategory;

public interface ErpProductCategoryDao extends GenericDao<ErpProductCategory> {
	List<ErpProductCategory> getErpProductCategoryByMid(String mid);

	ErpProductCategory getErpProductCategoryByCode(String mid, String code);

	ErpProductCategory getErpProductCategoryById(String id);

	List<ErpProductCategory> getErpProductCategoryByParentId(String mid);

	List<ErpProductCategory> getErpProductCategoryByParentId(String mid,
			String parentId);
}
