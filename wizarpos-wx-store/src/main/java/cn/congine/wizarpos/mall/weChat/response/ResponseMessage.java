package cn.congine.wizarpos.mall.weChat.response;

import java.util.Date;

import cn.congine.wizarpos.mall.weChat.request.MessageType;

public abstract class ResponseMessage {

	protected String openId;

	protected String mpId;

	protected Date createTime;

	protected MessageType type;

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getMpId() {
		return mpId;
	}

	public void setMpId(String mpId) {
		this.mpId = mpId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public MessageType getType() {
		return type;
	}

	public void setType(MessageType type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "To[" + openId + "] From[" + mpId + "] Type[" + type.getLabel()
				+ "] Create[" + createTime + "]";
	}
}
