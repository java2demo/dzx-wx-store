package cn.congine.wizarpos.mall.model.open;

/**
 * 记录用户访问output
 * @author xudongdong
 * Date: 2015.7.29
 */

public class MemberReadRecordOutput extends CommonOutput {

	private static final long serialVersionUID = 1L;
	
	private String result = null;

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
}


