package cn.congine.wizarpos.mall.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.congine.wizarpos.mall.dao.MrtWechartUserInfoDao;
import cn.congine.wizarpos.mall.model.MrtWechartUserInfo;
import cn.congine.wizarpos.mall.service.MrtWechartUserInfoService;

@Service("mrtWechartUserInfoService")
public class MrtWechartUserInfoServiceImpl implements MrtWechartUserInfoService {
	@Autowired
	private MrtWechartUserInfoDao mrtWechartUserInfoDao;

	@Override
	public MrtWechartUserInfo get(String openId, String mid) {
		return mrtWechartUserInfoDao.get(openId, mid);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void save(MrtWechartUserInfo obj) {
		mrtWechartUserInfoDao.save(obj);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void update(MrtWechartUserInfo obj) {
		mrtWechartUserInfoDao.update(obj);
	}

}
