package cn.congine.wizarpos.mall.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import sun.misc.BASE64Encoder;

public class Utils {

	/**
	 * 金额格式化
	 * 
	 * @param obj
	 * @return
	 */
	public static String formatAmount(Integer obj) {
		if (obj == null) {
			return "0.00";
		}
		return formatAmount("##0.00", obj / 100.00);
	}

	/**
	 * 金额格式化
	 * 
	 * @param obj
	 * @return
	 */
	public static String formatAmount(String obj) {
		if (obj == null) {
			return "0.00";
		}
		return formatAmount("##0.00", Integer.valueOf(obj) / 100.00);
	}

	/**
	 * 金额格式化
	 * 
	 * @param obj
	 * @return
	 */
	public static String formatAmount(Double obj) {
		if (obj == null) {
			return "0.00";
		}
		return formatAmount("##0.00", obj / 100.00);
	}

	/**
	 * 金额格式化
	 * 
	 * @param pattern
	 * @param obj
	 * @return
	 */
	public static String formatAmount(String pattern, Object obj) {
		DecimalFormat myformat = new DecimalFormat();
		myformat.applyPattern(pattern);
		return myformat.format(obj);
	}

	/**
	 * 格式化ID 前补0
	 * 
	 * @param id
	 * @param k
	 *            长度
	 * @return
	 */
	public static String formatId(String id, int k) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < k; i++) {
			sb.append("0");
		}
		String str = sb.toString() + id;
		return str.substring(str.length() - k);
	}

	/**
	 * 将long 转成 日期格式字符串
	 * 
	 * @param time
	 * @return
	 */
	public static String long2DateTimeString(long time) {
		return long2DateString("yyyy-MM-dd HH:mm:ss", time);
	}

	public static String long2DateString(String format, long time) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		Date date = new Date(time);
		return sdf.format(date);
	}

	public static String formatNumber(int number) {
		DecimalFormat myformat = new DecimalFormat("##0.00");
		return myformat.format(number);
	}

	public static String formatNumber(double number) {
		DecimalFormat myformat = new DecimalFormat("##0.00");
		return myformat.format(number);
	}

	public static String formatNumber(float number) {
		DecimalFormat myformat = new DecimalFormat("0.00");
		return myformat.format(number);
	}

	/**
	 * 将分为单位的转换为元 （除100）
	 * 
	 * @param amount
	 * @return
	 */

	public static String changF2Y(int amount) {
		return formatNumber(amount * 1.0 / 100);
	}

	/**
	 * 生成临牌来检验重复提交
	 * 
	 * @return
	 */
	public static String makeToken() { // checkException
		// 7346734837483 834u938493493849384 43434384
		String token = (System.currentTimeMillis() + new Random()
				.nextInt(999999999)) + "";
		// 数据指纹 128位长 16个字节 md5
		try {
			MessageDigest md = MessageDigest.getInstance("md5");
			byte md5[] = md.digest(token.getBytes());
			// base64编码--任意二进制编码明文字符 adfsdfsdfsf
			BASE64Encoder encoder = new BASE64Encoder();
			return encoder.encode(md5);
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 设置重复提交校验token
	 * 
	 * @param request
	 */
	public static void setSessionToken(HttpServletRequest httpRequest) {
		String token = makeToken();
		httpRequest.getSession()
				.setAttribute(SysConstants.SESSION_TOKEN, token);
	}

	/**
	 * 判断客户端提交上来的令牌和服务器端生成的令牌是否一致，一致移除token
	 * 
	 * @param request
	 * @return true 用户重复提交了表单 false 用户没有重复提交表单
	 */
	public static boolean isRepeatSubmit(HttpServletRequest httpRequest) {
		String client_token = httpRequest
				.getParameter(SysConstants.SESSION_TOKEN);
		// 1 如果用户提交的表单数据中没有token，则用户是重复提交了表单
		if (client_token == null) {
			return true;
		}
		// 取出存储在Session中的token
		String server_token = (String) httpRequest.getSession().getAttribute(
				SysConstants.SESSION_TOKEN);
		// 2 如果当前用户的Session中不存在Token(令牌)，则用户是重复提交了表单
		if (server_token == null) {
			return true;
		}
		// 3 存储在Session中的Token(令牌)与表单提交的Token(令牌)不同，则用户是重复提交了表单
		if (!client_token.equals(server_token)) {
			return true;
		}

		// 4 校验成功，移除session中的token
		httpRequest.getSession().removeAttribute("token");
		return false;
	}

	/**
	 * 判断客户端提交上来的令牌和服务器端生成的令牌是否一致，一致移除token
	 * 
	 * @param request
	 * @return true 用户重复提交了表单 false 用户没有重复提交表单
	 */
	public static boolean isRepeatSubmit(HttpServletRequest httpRequest,
			String client_token) {
		// 1 如果用户提交的表单数据中没有token，则用户是重复提交了表单
		if (client_token == null || client_token.isEmpty()) {
			return true;
		}
		// 取出存储在Session中的token
		String server_token = (String) httpRequest.getSession().getAttribute(
				SysConstants.SESSION_TOKEN);
		// 2 如果当前用户的Session中不存在Token(令牌)，则用户是重复提交了表单
		if (server_token == null) {
			return true;
		}
		// 3 存储在Session中的Token(令牌)与表单提交的Token(令牌)不同，则用户是重复提交了表单
		if (!client_token.equals(server_token)) {
			return true;
		}
		return false;
	}
	
	public static void removeSessionToken(HttpServletRequest httpRequest) {
		// 4 校验成功，移除session中的token
		httpRequest.getSession().removeAttribute("token");
	}
	

}
