package cn.congine.wizarpos.mall.dao.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import cn.congine.wizarpos.mall.dao.MyOnlineActivityDetailDao;
import cn.congine.wizarpos.mall.entity.MyOnlineActivityDetail;
import cn.congine.wizarpos.mall.entity.MyRedEnvelopes;
import cn.congine.wizarpos.mall.utils.Function;

@Repository("myOnlineActivityDetailDao")
public class MyOnlineActivityDetailDaoHibernate extends
		GenericDaoHibernate<MyOnlineActivityDetail> implements MyOnlineActivityDetailDao {

	public MyOnlineActivityDetailDaoHibernate() {
		super(MyOnlineActivityDetail.class);
	}

	@Override
	public List<MyOnlineActivityDetail> getMyOnlineActivityDetailList(
			String mid, String onlineActivityId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MyOnlineActivityDetail getMyOnlineActivityDetail(String mid,
			String onlineActivityId, String openId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getMyOnlineActivityDetailTicketMidList(String openId,
			String onlineActivityId) {

		String sql = "select DISTINCT(ticket_mid) FROM my_online_activity_detail WHERE person_open_id = ? and online_activity_id=? ";
		Query query = getSession().createSQLQuery(sql);
		query.setParameter(0, openId);
		query.setParameter(1, onlineActivityId);
		List list = query.list();
		if (list == null || list.size() == 0) {
			return null;
		}
		return query.list();
	}


}
