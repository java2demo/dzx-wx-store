package cn.congine.wizarpos.mall.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.congine.wizarpos.mall.dao.ErpProductDao;
import cn.congine.wizarpos.mall.dao.WxShoppingCartItemDao;
import cn.congine.wizarpos.mall.model.ErpProduct;
import cn.congine.wizarpos.mall.model.WxShoppingCartItem;
import cn.congine.wizarpos.mall.service.WxShoppingCartItemService;

@Service("wxShoppingCartItemService")
public class WxShoppingCartItemServiceImpl implements WxShoppingCartItemService {

	@Autowired
	private WxShoppingCartItemDao wxShoppingCartItemDao;

	@Autowired
	private ErpProductDao erpProductDao;

	@Override
	public List<cn.congine.wizarpos.mall.vo.WxShoppingCartItem> getWxShoppingCartItemListByCartId(
			String cartId) {

		List<cn.congine.wizarpos.mall.vo.WxShoppingCartItem> returnList = new ArrayList<cn.congine.wizarpos.mall.vo.WxShoppingCartItem>();

		List<WxShoppingCartItem> list = wxShoppingCartItemDao
				.getWxShoppingCartItemListByCartId(cartId);
		for (WxShoppingCartItem item : list) {
			cn.congine.wizarpos.mall.vo.WxShoppingCartItem v = new cn.congine.wizarpos.mall.vo.WxShoppingCartItem();
			v.setId(item.getId());
			v.setCartId(item.getCartId());
			v.setProductId(item.getProductId());
			v.setProductNum(item.getProductNum());
			v.setProductPrice(item.getProductPrice());
			ErpProduct pr = erpProductDao.get(item.getProductId());
			if (pr != null) {
				v.setProductName(pr.getName());
				v.setPicUrl(pr.getPicUrl());
			}
			returnList.add(v);
		}

		return returnList;

	}

	@Override
	public WxShoppingCartItem getWxShoppingCartItem(String productId,
			String cartId) {
		return wxShoppingCartItemDao.getWxShoppingCartItem(productId, cartId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void save(WxShoppingCartItem obj) {
		wxShoppingCartItemDao.save(obj);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void update(WxShoppingCartItem obj) {
		wxShoppingCartItemDao.update(obj);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void removeByCartId(String mid, String cartId) {
		wxShoppingCartItemDao.removeByCartId(mid, cartId);
	}

	@Override
	public WxShoppingCartItem getWxShoppingCartItemById(String id) {
		return wxShoppingCartItemDao.get(id);
	}

	@Override
	public WxShoppingCartItem getWxShoppingCartItemByOpenId(String openId) {
		return wxShoppingCartItemDao.getWxShoppingCartItemByOpenId(openId);
	}

	@Override
	public List<WxShoppingCartItem> getCarList(String cartId) {
		return wxShoppingCartItemDao.getCarList(cartId);
	}

	@Override
	public Boolean checkItemsExist(String proudctId, String cartId) {
		return wxShoppingCartItemDao.checkItemsExist(proudctId, cartId);
	}

	@Override
	public Integer getItemsNum(String cartId) {
		return wxShoppingCartItemDao.getItemsNum(cartId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public Boolean updateNum(Integer num, String cartId, String productId, String mid) {
		return wxShoppingCartItemDao.updateNum(num, cartId, productId, mid);
	}

	@Override
	public int getItemNum(String cartId, String productId) {
		return wxShoppingCartItemDao.getItemNum(cartId, productId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void remove(String id, String mid) {
		wxShoppingCartItemDao.remove(id, mid);
	}

}
