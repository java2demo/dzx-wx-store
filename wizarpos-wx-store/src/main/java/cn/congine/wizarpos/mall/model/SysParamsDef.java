package cn.congine.wizarpos.mall.model;

// Generated 2013-12-16 10:43:18 by Hibernate Tools 3.4.0.CR1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import cn.congine.wizarpos.mall.model.BaseEntity;

@Entity
@Table(name = "sys_params_def")
public class SysParamsDef extends BaseEntity {

	private static final long serialVersionUID = 1L;
	private String sysKey;
	private String sysValue;
	private String remark;

	public SysParamsDef() {
	}

	@Id
	@Column(name = "sys_key", unique = true, nullable = false, length = 30)
	public String getSysKey() {
		return sysKey;
	}

	public void setSysKey(String sysKey) {
		this.sysKey = sysKey;
	}

	@Column(name = "sys_value")
	public String getSysValue() {
		return sysValue;
	}

	public void setSysValue(String sysValue) {
		this.sysValue = sysValue;
	}

	@Column(name = "remark")
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
}
