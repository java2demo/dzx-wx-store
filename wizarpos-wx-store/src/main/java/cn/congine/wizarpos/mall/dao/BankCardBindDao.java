package cn.congine.wizarpos.mall.dao;

import java.util.List;

import cn.congine.wizarpos.mall.model.BankCardBind;

public interface BankCardBindDao extends GenericDao<BankCardBind> {
	BankCardBind get(String cardNo, String merchantId);

	BankCardBind get(Integer cid);

	List<BankCardBind> getList(String merchantId);

	List<BankCardBind> getList(String mid, String cardId);

	BankCardBind save(BankCardBind obj);

	BankCardBind find(String id, String mid);
}
