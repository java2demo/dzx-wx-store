package cn.congine.wizarpos.mall.weChat.request;

public enum MessageType {
	text("文本"), image("图片"), voice("语音"), event("事件"), news("图文"), location(
			"地理位置");

	private String label;

	private MessageType(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
}
