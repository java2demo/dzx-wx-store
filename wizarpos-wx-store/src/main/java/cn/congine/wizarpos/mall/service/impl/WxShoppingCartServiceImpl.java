package cn.congine.wizarpos.mall.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.congine.wizarpos.mall.dao.WxShoppingCartDao;
import cn.congine.wizarpos.mall.model.WxShoppingCart;
import cn.congine.wizarpos.mall.service.WxShoppingCartService;

@Service("wxShoppingCartService")
public class WxShoppingCartServiceImpl implements WxShoppingCartService {

	@Autowired
	private WxShoppingCartDao wxShoppingCartDao;

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void save(WxShoppingCart obj) {
		wxShoppingCartDao.save(obj);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void update(WxShoppingCart obj) {
		wxShoppingCartDao.update(obj);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void remove(String id, String mid) {
		wxShoppingCartDao.remove(id, mid);
	}

	@Override
	public List<WxShoppingCart> getWxShoppingCartByOpenId(String openId) {
		return wxShoppingCartDao.getWxShoppingCartByOpenId(openId);
	}

	@Override
	public WxShoppingCart getWxShoppingCart(String openId, String mid) {
		return wxShoppingCartDao.getWxShoppingCart(openId, mid);
	}

	@Override
	public WxShoppingCart getWxShoppingCartByMId(String mid) {
		return wxShoppingCartDao.getWxShoppingCartByMId(mid);
	}

}
