package cn.congine.wizarpos.mall.model;

/**
 * 销售单主表
 */
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

/***
 * The persistent class for the erp_sale_order database table.
 * 
 */
@Entity
@Table(name = "erp_sale_order")
public class SaleOrder extends BaseEntity {

	private static final long serialVersionUID = -7705951781133199238L;
	/**
	 * id
	 */
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
	// 指定生成器名称
	@Column(name = "id", length = 80)
	private String id;
	/**
	 * 应付金额
	 */
	@Column(name = "amount")
	private int amount;

	/**
	 * 最后修改时间
	 */
	@Column(name = "last_time")
	private Long lastTime;
	/**
	 * 日流水号
	 */
	@Column(name = "day_order_seq")
	private Long dayOrderSeq;
	/**
	 * 会员商户号
	 */
	@Column(name = "mid")
	private String mid;

	/**
	 * 操作员标识
	 */
	@Column(name = "operator_id")
	private String operatorId;

	/**
	 * 单据标识（0销售单，1退货单）
	 */
	@Column(name = "order_flag")
	private int orderFlag;

	/**
	 * 引用的销售单id
	 */
	@Column(name = "order_id")
	private String orderId;

	/**
	 * 销售单号
	 */
	@Column(name = "order_no")
	private String orderNo;

	/**
	 * 仓库编号
	 */
	@Column(name = "storage_id")
	private String storageId;
	/**
	 * 交易时间
	 */
	@Column(name = "order_time")
	private Long orderTime;
	/**
	 * 备注
	 */
	@Column(name = "remark")
	private String remark;

	/**
	 * 终端编号
	 */
	@Column(name = "sn")
	private String sn;
	/**
	 * 订单来源('1' pos销售订单 '2' 微信订单 '3' 支付宝订单)
	 */
	@Column(name = "order_source")
	private String orderSource;
	/**
	 * 引用的销售单id
	 */
	@Column(name = "order_catalog_id")
	private String orderCatalogId;
	/**
	 * 支付交易流水号
	 */
	@Column(name = "master_tran_log_id")
	private String masterTranLogId;
	/**
	 * 离线交易流水号
	 */
	@Column(name = "offline_tran_log_id")
	private String offlineTranLogId;
	/**
	 * 操作员名字
	 */
	@Column(name = "operator_name")
	private String operatorName;
	/**
	 * 支付状态
	 */
	@Column(name = "pay_status")
	private String payStatus;

	public SaleOrder() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getAmount() {
		return this.amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(String operatorId) {
		this.operatorId = operatorId;
	}

	public int getOrderFlag() {
		return this.orderFlag;
	}

	public void setOrderFlag(int orderFlag) {
		this.orderFlag = orderFlag;
	}

	public String getOrderNo() {
		return this.orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getLastTime() {
		return lastTime;
	}

	public void setLastTime(Long lastTime) {
		this.lastTime = lastTime;
	}

	public Long getDayOrderSeq() {
		return dayOrderSeq;
	}

	public void setDayOrderSeq(Long dayOrderSeq) {
		this.dayOrderSeq = dayOrderSeq;
	}

	public Long getOrderTime() {
		return orderTime;
	}

	public void setOrderTime(Long orderTime) {
		this.orderTime = orderTime;
	}

	public String getStorageId() {
		return storageId;
	}

	public void setStorageId(String storageId) {
		this.storageId = storageId;
	}

	public String getOrderSource() {
		return orderSource;
	}

	public void setOrderSource(String orderSource) {
		this.orderSource = orderSource;
	}

	@Transient
	public String getOrderId() {
		return orderId;
	}

	@Transient
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getMasterTranLogId() {
		return masterTranLogId;
	}

	public void setMasterTranLogId(String masterTranLogId) {
		this.masterTranLogId = masterTranLogId;
	}

	public String getOfflineTranLogId() {
		return offlineTranLogId;
	}

	public void setOfflineTranLogId(String offlineTranLogId) {
		this.offlineTranLogId = offlineTranLogId;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public String getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(String payStatus) {
		this.payStatus = payStatus;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public String getOrderCatalogId() {
		return orderCatalogId;
	}

	public void setOrderCatalogId(String orderCatalogId) {
		this.orderCatalogId = orderCatalogId;
	}

}