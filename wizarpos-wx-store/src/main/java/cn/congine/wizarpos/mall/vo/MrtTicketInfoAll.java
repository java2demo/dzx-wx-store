package cn.congine.wizarpos.mall.vo;

public class MrtTicketInfoAll {
	// 唯一标识
	private String id;

	// 卡券码
	private String ticketCode;

	// 慧商户ID
	private String mid;

	// 卡券名称
	private String ticketName;

	// 卡券金额
	private String balance;

	// 是否启用 1为启用 0为不启用
	private String usedFlag;

	// 有效期，单位：天。-1表示永久有效
	private String validPeriod;

	// 描述信息
	private String description;

	// 添加时间
	private Integer createTime;

	// state
	private Integer state;

	// 券号
	private String ticketNo;

	// 会员卡号
	private String cardId;

	// 启用时间
	private String startTime;

	// 过期时间
	private String expriyTime;

	// 是否有效
	private String validFlag;

	// 注销时间
	private String cancelTime;

	// 添加时间
	private Integer usedTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTicketCode() {
		return ticketCode;
	}

	public void setTicketCode(String ticketCode) {
		this.ticketCode = ticketCode;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getTicketName() {
		return ticketName;
	}

	public void setTicketName(String ticketName) {
		this.ticketName = ticketName;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getUsedFlag() {
		return usedFlag;
	}

	public void setUsedFlag(String usedFlag) {
		this.usedFlag = usedFlag;
	}

	public String getValidPeriod() {
		return validPeriod;
	}

	public void setValidPeriod(String validPeriod) {
		this.validPeriod = validPeriod;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Integer createTime) {
		this.createTime = createTime;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getTicketNo() {
		return ticketNo;
	}

	public void setTicketNo(String ticketNo) {
		this.ticketNo = ticketNo;
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getExpriyTime() {
		return expriyTime;
	}

	public void setExpriyTime(String expriyTime) {
		this.expriyTime = expriyTime;
	}

	public String getValidFlag() {
		return validFlag;
	}

	public void setValidFlag(String validFlag) {
		this.validFlag = validFlag;
	}

	public String getCancelTime() {
		return cancelTime;
	}

	public void setCancelTime(String cancelTime) {
		this.cancelTime = cancelTime;
	}

	public Integer getUsedTime() {
		return usedTime;
	}

	public void setUsedTime(Integer usedTime) {
		this.usedTime = usedTime;
	}

}
