package cn.congine.wizarpos.mall.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.congine.wizarpos.mall.dao.WxAddressDao;
import cn.congine.wizarpos.mall.model.WxAddress;
import cn.congine.wizarpos.mall.service.WxAddressService;

@Service("wxAddressService")
public class WxAddressServiceImpl implements WxAddressService {

	@Autowired
	private WxAddressDao wxAddressDao;

	@Override
	public List<WxAddress> getWxAddressListByCardId(String cardId) {
		return wxAddressDao.getWxAddressListByCardId(cardId);
	}

	@Override
	public WxAddress getDefaultWxAddress(String cardId) {
		return wxAddressDao.getDefaultWxAddress(cardId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void update(WxAddress obj) {
		wxAddressDao.update(obj);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void save(WxAddress obj) {
		wxAddressDao.save(obj);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void remove(String id, String mid) {
		wxAddressDao.remove(id, mid);
	}

	@Override
	public WxAddress getWxAddressId(String id) {
		return wxAddressDao.getWxAddressId(id);
	}

}
