package cn.congine.wizarpos.mall.utils;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.MessageSource;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;

public class Function {
	
	@Resource(name = "messageSource")
	private MessageSource messageSource;
	
	public static String addOne(String num) {
		if (num == null || "null".equals(num)) {
			num = "0";
		}
		int r = Integer.parseInt(num) + 1;
		if (String.valueOf(r).length() < 2) {
			return "0" + r;
		}
		return r + "";
	}

	public static String getMerchantCardId(String mid, String cardType,
			String cardNo) {
		return StringUtils.leftPad(mid, 6, '0') + cardType + cardNo;
	}

	public static String getUid() {
		return UUID.randomUUID().toString().replace("", "");
	}

	public static String toYuan(long fen) {
		BigDecimal bdFen = new BigDecimal(fen);
		BigDecimal bdYuan = bdFen.divide(new BigDecimal(100), 2,
				BigDecimal.ROUND_UNNECESSARY);
		return bdYuan.toString();
	}

	/**
	 * 加 天
	 * 
	 * @param currentTime
	 * @param day
	 * @return
	 */
	public static Date addDay(Date currentTime, int day) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(currentTime);
		calendar.add(Calendar.DAY_OF_MONTH, day);
		return calendar.getTime();
	}

	/**
	 * 加 分
	 * 
	 * @param currentTime
	 * @param minute
	 * @return
	 */
	public static Date addMinute(Date currentTime, int minute) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(currentTime);
		calendar.add(Calendar.MINUTE, minute);
		return calendar.getTime();
	}

	/**
	 * 天 开始
	 * 
	 * @param currentTime
	 * @return
	 */
	public static Date getDayStart(Date currentTime) {
		if (currentTime == null) {
			currentTime = new Date();
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(currentTime);
		calendar.set(Calendar.HOUR_OF_DAY, 00);
		calendar.set(Calendar.MINUTE, 00);
		calendar.set(Calendar.SECOND, 00);
		return calendar.getTime();
	}

	/**
	 * 天 结束
	 * 
	 * @param currentTime
	 * @return
	 */
	public static Date getDayEnd(Date currentTime) {
		if (currentTime == null) {
			currentTime = new Date();
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(currentTime);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		return calendar.getTime();
	}

	/**
	 * 今天开始
	 * 
	 * @return
	 */
	public static Date getDayStart() {
		return getDayStart(new Date());
	}

	/**
	 * 今天结束
	 * 
	 * @return
	 */
	public static Date getDayEnd() {
		return getDayEnd(new Date());
	}

	public static Date addWeek(Date currentTime, int amount) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(currentTime);
		calendar.add(Calendar.WEEK_OF_YEAR, amount);
		return calendar.getTime();
	}

	/**
	 * 一周中，一天的索引
	 * 
	 * @param dayOfWeek
	 * @return
	 */
	public static int getIndexForDayOfWeek(int dayOfWeek) {
		if (dayOfWeek == 1) {
			return 6;
		} else {
			return dayOfWeek - 2;
		}
	}

	/**
	 * 一周的开始
	 * 
	 * @param currentTime
	 * @return
	 */
	public static Date getWeekStart(Date currentTime) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(currentTime);
		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
		calendar.add(Calendar.DAY_OF_MONTH, -getIndexForDayOfWeek(dayOfWeek));
		calendar.set(Calendar.HOUR_OF_DAY, 00);
		calendar.set(Calendar.MINUTE, 00);
		calendar.set(Calendar.SECOND, 00);
		return calendar.getTime();
	}

	/**
	 * 一周的结束
	 * 
	 * @param currentTime
	 * @return
	 */
	public static Date getWeekEnd(Date currentTime) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(currentTime);
		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
		calendar.add(Calendar.DAY_OF_MONTH, 6 - getIndexForDayOfWeek(dayOfWeek));
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		return calendar.getTime();
	}

	/**
	 * 月 加
	 * 
	 * @param currentTime
	 * @param amount
	 * @return
	 */
	public static Date addMonth(Date currentTime, int amount) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(currentTime);
		calendar.add(Calendar.MONTH, amount);
		return calendar.getTime();
	}

	/**
	 * 月开始
	 * 
	 * @param currentTime
	 * @return
	 */
	public static Date getMonthStart(Date currentTime) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(currentTime);
		int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH) - 1;
		calendar.add(Calendar.DAY_OF_MONTH, -dayOfMonth);
		calendar.set(Calendar.HOUR_OF_DAY, 00);
		calendar.set(Calendar.MINUTE, 00);
		calendar.set(Calendar.SECOND, 00);
		return calendar.getTime();
	}

	/**
	 * 月结束
	 * 
	 * @param currentTime
	 * @return
	 */
	public static Date getMonthEnd(Date currentTime) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(currentTime);
		int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH) - 1;
		int count = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		calendar.add(Calendar.DAY_OF_MONTH, count - dayOfMonth - 1);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		return calendar.getTime();
	}

	public static Date addYear(Date currentTime, int amount) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(currentTime);
		calendar.add(Calendar.YEAR, amount);
		return calendar.getTime();
	}

	public static Date getYearStart(Date currentTime) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(currentTime);
		calendar.set(Calendar.MONTH, 0);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		calendar.set(Calendar.HOUR_OF_DAY, 00);
		calendar.set(Calendar.MINUTE, 00);
		calendar.set(Calendar.SECOND, 00);
		return calendar.getTime();
	}

	public static Date getYearEnd(Date currentTime) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(currentTime);
		calendar.set(Calendar.MONTH, 11);
		calendar.set(Calendar.DAY_OF_MONTH, 31);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		return calendar.getTime();
	}

	public static TimeRange getTimeRange(int p, Date currentTime) {
		TimeRange range = new TimeRange();
		switch (p) {
		case 1: // 今天
			range.setBeginTime(getDayStart());
			range.setEndTime(getDayEnd());
			return range;
		case 2: // 昨天
			Date yesterday = addDay(currentTime, -1);
			range.setBeginTime(getDayStart(yesterday));
			range.setEndTime(getDayEnd(yesterday));
			return range;
		case 3: // 本周
			range.setBeginTime(getWeekStart(currentTime));
			range.setEndTime(getWeekEnd(currentTime));
			return range;
		case 4: // 上周
			Date lastWeek = addWeek(currentTime, -1);
			range.setBeginTime(getWeekStart(lastWeek));
			range.setEndTime(getWeekEnd(lastWeek));
			return range;
		case 5: // 本月
			range.setBeginTime(getMonthStart(currentTime));
			range.setEndTime(getMonthEnd(currentTime));
			return range;
		case 6: // 上月
			Date lastMonth = addMonth(currentTime, -1);
			range.setBeginTime(getMonthStart(lastMonth));
			range.setEndTime(getMonthEnd(lastMonth));
			return range;
		case 7: // 前两个月
			Date twoMonth = addMonth(currentTime, -2);
			range.setBeginTime(getDayStart(twoMonth));
			range.setEndTime(getDayEnd(currentTime));
			return range;
		case 8: // 前三个月
			Date threeMonth = addMonth(currentTime, -3);
			range.setBeginTime(getDayStart(threeMonth));
			range.setEndTime(getDayEnd(currentTime));
		case 9: // 前六个月
			Date sixMonth = addMonth(currentTime, -6);
			range.setBeginTime(getDayStart(sixMonth));
			range.setEndTime(getDayEnd(currentTime));
		case 10: // 前一年
			Date lastYear = addYear(currentTime, -1);
			range.setBeginTime(getDayStart(lastYear));
			range.setEndTime(getDayEnd(currentTime));
			return range;
		case 11: // 前三年
			Date threeYear = addYear(currentTime, -3);
			range.setBeginTime(getDayStart(threeYear));
			range.setEndTime(getDayEnd(currentTime));
			return range;

		default:
			range.setBeginTime(getDayStart());
			range.setEndTime(getDayEnd());
			return range;
		}
	}

	public static class TimeRange {
		public Date beginTime;
		public Date endTime;

		public Date getBeginTime() {
			return beginTime;
		}

		public void setBeginTime(Date beginTime) {
			this.beginTime = beginTime;
		}

		public Date getEndTime() {
			return endTime;
		}

		public void setEndTime(Date endTime) {
			this.endTime = endTime;
		}

	}

	public static String formatDate(Date date, String pattern) {
		SimpleDateFormat format = new SimpleDateFormat(pattern);
		return format.format(date);
	}

	public static String formatDate(String pattern) {
		return formatDate(new Date(), pattern);
	}

	public static boolean isToday(Date date) {
		Date currentTime = new Date();
		Date startDate = getDayStart(currentTime);
		Date endDate = getDayEnd(currentTime);
		if (date.getTime() >= startDate.getTime()
				&& date.getTime() <= endDate.getTime()) {
			return true;
		}
		return false;
	}

	public static <T> T mapToJavaBean(Map<String, Object> map, Class<T> clazz) {
		return JSON.toJavaObject(JSON.parseObject(JSON.toJSONString(map)),
				clazz);
	}

	public static Map<String, Object> javaBeanToMap(Object javaBean) {
		return JSON.parseObject(JSON.toJSONString(javaBean),
				new TypeReference<Map<String, Object>>() {
				});
	}

	public static Map<String, String> httpParamToMap(String httpParam) {
		Map<String, String> m = new TreeMap<String, String>();
		if (httpParam == null) {
			return m;
		}
		String[] ss = httpParam.split("&");
		for (String s : ss) {
			String[] kv = s.split("=");
			if (kv == null || "".equals(kv) || kv.length == 0) {
				continue;
			} else if (kv.length == 1) {
				m.put(kv[0], null);
			} else {
				m.put(kv[0], kv[1]);
			}
		}
		return m;
	}

	public static String byteToHex(byte[] bs) {
		String stmp = "";
		StringBuilder sb = new StringBuilder("");
		for (int n = 0; n < bs.length; n++) {
			stmp = Integer.toHexString(bs[n] & 0xFF);
			sb.append((stmp.length() == 1) ? "0" + stmp : stmp);
		}
		return sb.toString().toUpperCase().trim();
	}

	public static String getStringObject(Object o) {
		if (o == null) {
			return "";
		}
		return o.toString();
	}

	public static long getLongObject(Object o) {
		if (o == null) {
			return 0;
		}
		return Long.valueOf(o.toString());
	}

	public static String dealGridPicUrl(String url, int weight, int height) {
		if (url == null) {
			return "";
		}
		String dex = "";
		if (url.contains("jpg")) {
			dex = ".jpg";
		} else if (url.contains("jpeg")) {
			dex = ".jpeg";
		} else if (url.contains("webp")) {
			dex = ".webp";
		} else if (url.contains("png")) {
			dex = ".png";
		} else if (url.contains("bmp")) {
			dex = ".bmp";
		} else {
			return "";
		}
		return url + "@" + weight + "w_" + height + "h_100Q_1e_1c_1x" + dex + "?" + new Date().getTime();
	}

	public String getBaseURL(HttpServletRequest httpRequest) {
		String baseUrl = "http://"
				+ messageSource.getMessage(SysConstants.SERVICE_DNS, null,
						Locale.ENGLISH);
		baseUrl += httpRequest.getSession().getServletContext().getContextPath();
		return baseUrl;
	}
	
	/**
	 * 隐藏订单号中的mid
	 * 订单号为  2位标志位  + 15位mid + 8位yyyyMMdd日期  + 4位序号
	 * 
	 * @param origin
	 * 
	 * @return
	 */
	public static String getShowWxOrderId(String origin) {
		String id = null;
		
		if (origin== null || origin.isEmpty()) {
			id = "";
		} else if (origin.length() < 18) {
			id = origin;
		} else {
			id = origin.substring(0, 2) + origin.substring(17);
		}
		
		return id;
	}
	
	/**
	 * 隐藏销售单号中的mid
	 * 订单号为  1位标志位  + 15位mid + 8位yyyyMMdd日期  + 4位序号
	 * 
	 * @param origin
	 * 
	 * @return
	 */
	public static String getShowSaleOrderNo(String origin) {
		String id = null;
		
		if (origin== null || origin.isEmpty()) {
			id = "";
		} else if (origin.length() < 17) {
			id = origin;
		} else {
			id = origin.substring(0, 1) + origin.substring(16);
		}
		
		return id;
	}
	
	public static String getShowWxOrderNo(String origin) {
		String id = null;
		
		if (origin== null || origin.isEmpty()) {
			id = "";
		} else if (origin.length() < 17) {
			id = origin;
		} else {
			id = origin.substring(0, 1) + origin.substring(16);
		}
		
		return id;
	}
}
