package cn.congine.wizarpos.mall.model.open;

import java.util.List;
import cn.congine.wizarpos.mall.vo.BankCard;


public class WeChartMemberBankCardListOutput extends CommonOutput {

	private static final long serialVersionUID = 1L;

	private List<BankCard> result = null;

	public List<BankCard> getResult() {
		return result;
	}

	public void setResult(List<BankCard> bankcard) {
		this.result = bankcard;
	}

}
