package cn.congine.wizarpos.mall.dao.hibernate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.FlushMode;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import cn.congine.wizarpos.mall.common.Page;
import cn.congine.wizarpos.mall.dao.WxOrderDao;
import cn.congine.wizarpos.mall.model.WxOrder;

@Repository("wxOrderDao")
public class WxOrderDaoHibernate extends GenericDaoHibernate<WxOrder> implements
		WxOrderDao {

	public WxOrderDaoHibernate() {
		super(WxOrder.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<WxOrder> getWxOrderListByMid(String mid) {
		Query query = getSession().createQuery("from WxOrder where mid = :mid")
				.setParameter("mid", mid);
		return (List<WxOrder>) query.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<WxOrder> getWxOrderListByOpenId(String openId) {
		Query query = getSession().createQuery(
				"from WxOrder where openId = :openId").setParameter("openId",
				openId);
		return (List<WxOrder>) query.list();
	}

	@Override
	public WxOrder getWxOrderByOrderId(String orderId) {
		Query query = getSession().createQuery(
				"from WxOrder where orderId = :orderId").setParameter(
				"orderId", orderId);
		return (WxOrder) query.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<WxOrder> getWeekWxOrderListByOpenId(String openId, String mid,
			int type) {
		long start_time = System.currentTimeMillis();
		long end_time = 0;
		if (type == 1) {
			end_time = (long) (start_time - start_time);
			// end_time = (long) (start_time-(30.0*24*60*60*1000));
		} else if (type == 2) {
			end_time = (long) (start_time - (3 * 30.0 * 24 * 60 * 60 * 1000));
		} else {
			// end_time = (long) (start_time-start_time);
			end_time = (long) (start_time - (30.0 * 24 * 60 * 60 * 1000));
		}
		String sql = "from WxOrder where openId = :openId and mid= :mid and (createTime <=:start_time and createTime >=:end_time) ";
		Map<String, Object> param = new HashMap<String, Object>();
		Query query = getSession().createQuery(sql);
		param.put("openId", openId);
		param.put("mid", mid);
		param.put("start_time", start_time);
		param.put("end_time", end_time);
		query.setProperties(param);

		return (List<WxOrder>) query.list();
	}

	@Override
	public List<WxOrder> getOrderListByType(String openId, String mid,
			int status, Page<WxOrder> page) {
		String hql = "";
		Map<String, Object> param = new HashMap<String, Object>();
		if (status == 2) {
			hql = "from WxOrder where openId=:openId and mid=:mid order by createTime desc";
			param.put("openId", openId);
			param.put("mid", mid);
		} else {
			long start_time = System.currentTimeMillis();
			long end_time = 0;
			if (status == 0) {
				end_time = (long) (start_time - (30.0 * 24 * 60 * 60 * 1000));
			} else if (status == 1) {
				end_time = (long) (start_time - (3 * 30.0 * 24 * 60 * 60 * 1000));
			}
			hql = "from WxOrder where openId=:openId and mid=:mid and createTime <=:start_time and createTime >=:end_time order by createTime desc";
			param.put("openId", openId);
			param.put("mid", mid);
			param.put("start_time", start_time);
			param.put("end_time", end_time);
		}
		page = this.readAll4Page(hql.toString(), "", param, page);
		return page.getList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<WxOrder> getVoWxOrderListByOpenId(String openId, String mid,
			String type) {
		long start_time = System.currentTimeMillis();
		long end_time = (long) (start_time - (30.0 * 24 * 60 * 60 * 1000));
		String sql = null;
		if ("2".equals(type)) {
			sql = "from WxOrder where openId = :openId and mid=:mid and payStatus ='1' and (dispatchType in('3','5')) order by lastTime  desc";
		} else if ("1".equals(type)) {
			sql = "from WxOrder where openId = :openId and mid=:mid and status in('1','2') and payStatus ='1' and (dispatchType in('3','5')) and ( createTime <=:end_time)  order by lastTime  desc";
		} else if ("0".equals(type)) {
			sql = "from WxOrder where openId = :openId and mid=:mid and status in('1','2') and payStatus ='1' and (dispatchType in('3','5')) and (createTime <=:start_time and createTime >=:end_time)  order by lastTime  desc";
		}
		Map<String, Object> param = new HashMap<String, Object>();
		Query query = getSession().createQuery(sql);
		param.put("openId", openId);
		param.put("mid", mid);
		param.put("start_time", start_time);
		param.put("end_time", end_time);
		query.setProperties(param);
		return (List<WxOrder>) query.list();
	}

	@Override
	public List<WxOrder> getVoByPage(String openId, String mid, Integer nextPage) {
		String hql = "from WxOrder where openId=:openId and mid=:mid and payStatus ='1' and (dispatchType in('3','5')) order by lastTime  desc";

		Map<String, Object> param = new HashMap<String, Object>();
		param.put("openId", openId);
		param.put("mid", mid);

		Page<WxOrder> page = new Page<WxOrder>();
		page.setPageNo(nextPage);

		page = this.readAll4Page(hql.toString(), "", param, page);
		return page.getList();
	}

	@Override
	public List<WxOrder> getWeekWxOrderListByOpenId(String openId, String mid,
			int type, Page<WxOrder> page) {
		long start_time = System.currentTimeMillis();
		long end_time = 0;
		if (type == 1) {
			end_time = (long) (start_time - start_time);
			// end_time = (long) (start_time-(30.0*24*60*60*1000));
		} else if (type == 2) {
			end_time = (long) (start_time - (3 * 30.0 * 24 * 60 * 60 * 1000));
		} else {
			// end_time = (long) (start_time-start_time);
			end_time = (long) (start_time - (30.0 * 24 * 60 * 60 * 1000));
		}
		String sql = "from WxOrder where openId = :openId and mid= :mid and (createTime <=:start_time and createTime >=:end_time) ";
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("openId", openId);
		param.put("mid", mid);
		param.put("start_time", start_time);
		param.put("end_time", end_time);
		page = this.readAll4Page(sql, "", param, page);
		return page.getList();
	}

	@Override
	public WxOrder save(WxOrder obj) {
		String sql = "INSERT wx_order (order_id, open_id, mid, status, dispatch_type, amount, pay_status,"
				+ " address_id, pick_up_qr, create_time, last_time, sale_order_id)"
				+ " VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
		this.getSession().setFlushMode(FlushMode.MANUAL);
		Query q = getSession().createSQLQuery(sql);
		q.setString(0, obj.getOrderId());
		q.setString(1, obj.getOpenId());
		q.setString(2, obj.getMid());
		q.setString(3, obj.getStatus());
		q.setString(4, obj.getDispatchType());
		q.setInteger(5, obj.getAmount());
		q.setString(6, obj.getPayStatus());
		q.setString(7, obj.getAddressId());
		q.setString(8, obj.getPickUpQr());
		q.setLong(9, obj.getCreateTime());
		q.setLong(10, obj.getLastTime());
		q.setString(11, obj.getSaleOrderId());
		q.executeUpdate();
		return obj;
	}

	@Override
	public void update(WxOrder obj) {
		String sql = "UPDATE wx_order SET open_id = ?, status = ?, dispatch_type = ?,"
				+ " amount = ?, pay_status = ?, address_id = ?,pick_up_qr = ?, create_time = ?,"
				+ " last_time = ?, sale_order_id = ? WHERE order_id = ? and mid = ?";
		this.getSession().setFlushMode(FlushMode.MANUAL);
		Query q = getSession().createSQLQuery(sql);

		q.setString(0, obj.getOpenId());
		q.setString(1, obj.getStatus());
		q.setString(2, obj.getDispatchType());
		q.setInteger(3, obj.getAmount());
		q.setString(4, obj.getPayStatus());
		q.setString(5, obj.getAddressId());
		q.setString(6, obj.getPickUpQr());
		q.setLong(7, obj.getCreateTime());
		q.setLong(8, obj.getLastTime());
		q.setString(9, obj.getSaleOrderId());
		q.setString(10, obj.getOrderId());
		q.setString(11, obj.getMid());
		q.executeUpdate();
	}
}
