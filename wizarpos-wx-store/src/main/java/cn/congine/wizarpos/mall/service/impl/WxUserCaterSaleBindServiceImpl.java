package cn.congine.wizarpos.mall.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.congine.wizarpos.mall.dao.WxUserCaterSaleBindDao;
import cn.congine.wizarpos.mall.model.WxUserCaterSaleBind;
import cn.congine.wizarpos.mall.service.WxUserCaterSaleBindService;

@Service("wxUserCaterSaleBindService")
public class WxUserCaterSaleBindServiceImpl implements
		WxUserCaterSaleBindService {

	@Autowired
	private WxUserCaterSaleBindDao wxUserCaterSaleBindDao;

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void save(WxUserCaterSaleBind obj) {
		wxUserCaterSaleBindDao.save(obj);
	}

	@Override
	public List<WxUserCaterSaleBind> getByOpenId(String openId) {
		return wxUserCaterSaleBindDao.getByOpenId(openId);
	}

	@Override
	public List<WxUserCaterSaleBind> getByOpenId(String openId, int pageNo) {
		return wxUserCaterSaleBindDao.getByOpenId(openId, pageNo);
	}

	@Override
	public List<Object[]> getByOpenIdMid(String openId, String mid) {
		return wxUserCaterSaleBindDao.getByOpenIdMid(openId, mid);
	}

	@Override
	public List<WxUserCaterSaleBind> getByOpenIdMid(String openId, String mid,
			int pageNo) {
		return wxUserCaterSaleBindDao.getByOpenIdMid(openId, mid, pageNo);
	}

}
