package cn.congine.wizarpos.mall.dao;

import java.util.List;

import cn.congine.wizarpos.mall.model.WxAddress;

public interface WxAddressDao extends GenericDao<WxAddress> {

	List<WxAddress> getWxAddressListByCardId(String cardId);

	WxAddress getDefaultWxAddress(String cardId);

	WxAddress getWxAddressId(String id);
	
	void remove(String id, String mid);
}
