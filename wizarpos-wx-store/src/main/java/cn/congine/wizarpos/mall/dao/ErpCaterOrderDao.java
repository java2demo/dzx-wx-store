package cn.congine.wizarpos.mall.dao;

import java.util.Date;
import java.util.List;

import cn.congine.wizarpos.mall.model.ErpCarterOrder;

public interface ErpCaterOrderDao extends GenericDao<ErpCarterOrder> {

	/**
	 * 根据日期，选择已经被预定的table_id
	 * 
	 * @param mid
	 * @param date
	 * @return
	 */
	List<String> getYudingTableIdByDate(String mid, Date date);

	/**
	 * 根据日期和时间段，选择已经被预定的table_id
	 * 
	 * @param mid
	 * @param date
	 * @param stageId
	 * @return
	 */
	List<String> getYudingTableIdByDateStage(String mid, Date date,
			String stageId);

	/**
	 * 餐桌是否被预订，返回订单号
	 * 
	 * @param mid
	 * @param date
	 * @param stageId
	 * @param tableId
	 * @return
	 */
	Object getReOrderId(String mid, Date date, String stageId, String tableId);

	/**
	 * 唯一订单
	 * 
	 * @param orderId
	 * @param mid
	 * @return
	 */
	ErpCarterOrder getByIdMid(String orderId, String mid);

	/**
	 * 用户的预订列表
	 * 
	 * @param openId
	 * @param mid
	 * @return
	 */
	List<ErpCarterOrder> getByOpenidMid(String openId, String mid);

	/**
	 * ID查找
	 * 
	 * @param id
	 * @param mid
	 * @return
	 */
	ErpCarterOrder find(String id, String mid);
}
