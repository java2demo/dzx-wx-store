package cn.congine.wizarpos.mall.vo;

public class WxShoppingCartItem {

	// 唯一标识
	private String id;

	// 关联的购物车编号
	private String cartId;

	// 产品ID
	private String productId;

	// 商品数量
	private Integer productNum;

	// 商品价格
	private Integer productPrice;

	// 商品名称
	private String productName;

	// 商品图片
	private String picUrl;

	// 商品显示价格
	private String showPrice;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCartId() {
		return cartId;
	}

	public void setCartId(String cartId) {
		this.cartId = cartId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public Integer getProductNum() {
		return productNum;
	}

	public void setProductNum(Integer productNum) {
		this.productNum = productNum;
	}

	public Integer getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(Integer productPrice) {
		this.productPrice = productPrice;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String prcUrl) {
		this.picUrl = prcUrl;
	}

	public String getShowPrice() {
		return showPrice;
	}

	public void setShowPrice(String showPrice) {
		this.showPrice = showPrice;
	}
}
