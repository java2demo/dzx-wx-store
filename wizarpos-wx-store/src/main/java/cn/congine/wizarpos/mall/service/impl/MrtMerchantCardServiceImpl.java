package cn.congine.wizarpos.mall.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.congine.wizarpos.mall.dao.MrtMerchantCardDao;
import cn.congine.wizarpos.mall.dao.SysMerchantDefDao;
import cn.congine.wizarpos.mall.model.MrtMerchantCard;
import cn.congine.wizarpos.mall.service.MrtMerchantCardService;

@Service("mrtMerchantCardService")
public class MrtMerchantCardServiceImpl implements MrtMerchantCardService {

	@Autowired
	private MrtMerchantCardDao mrtMerchantCardDao;

	@Autowired
	private SysMerchantDefDao sysMerchantDefDao;

	@Override
	public MrtMerchantCard getMrtMerchantCardByCardNo(String cardNo) {
		return mrtMerchantCardDao.getMrtMerchantCardByCardNo(cardNo);
	}

	@Override
	public MrtMerchantCard getMrtMerchantCard(String mid, String openId) {
		return mrtMerchantCardDao.getMrtMerchantCard(mid, openId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void update(MrtMerchantCard obj) {
		mrtMerchantCardDao.update(obj);
	}

	@Override
	public MrtMerchantCard getMrtMerchantCardByCardId(String id) {
		return mrtMerchantCardDao.getMrtMerchantCardByCardId(id);
	}
}
