﻿package cn.congine.wizarpos.mall.utils;

public class SysConstants {
	public final static int NOCARD = 5;
	public final static int SUCCESS = 0;
	/* open process successful */
	public static final String OPEN_SUCCESS = "0";
	public static final String OPEN_MESSAGE = "处理成功";

	/* open process exception */
	public static final String OPEN_ERROR = "1";
	public static final String OPEN_ERROR_MESSAGE = "系统错误";
	
	public static final String NO_MERCHANT_CODE = "60001";
	public static final String NO_MEMBER = "60019";
	// session token for duplicate requset
	public static final String SESSION_TOKEN = "token";
	// 暂无图片-商户列表
	public static final String DEFAULT_PICTURE_MRT = "http://image.wizarpos.com/default.png";
	// 暂无图片-产品列表
	public static final String DEFAULT_PICTURE = "http://image.wizarpos.com/noimage.png";
	// 暂无图片-产品明细
	public static final String DEFAULT_PICTURE_DETAIL = "http://image.wizarpos.com/noimage.png@300w_300h_100Q_1e_1c.png";
	// 默认图片-会员卡
	public static final String DEFAULT_VIP_IMAGE = "http://image.wizarpos.com/1003000100000012015081808551223375.png";

	public static final String WIZARPOS_APPID = "wizarpos.appid";
	public static final String WIZARPOS_APPSECRET = "wizarpos.appsecret";
	public static final String OPENID = "OPENID";
	public static final String APPID = "APPID";
	public static final String APPSECRET = "APPSECRET";
	public static final String ACCESS_TOKEN = "ACCESS_TOKEN";

	// pos service URL
	public static final String POS_SERVICE_MEMBER = "pos.service.member";
	public static final String POS_SERVICE_CASHIER = "pos.service.cashier";
	public static final String POS_SERVICE_OPEN = "pos.service.open";
	public static final String USER_SERVICE_OPEN = "user.service.open";
	public static final String SERVICE_DNS = "server.dns";

	// 微信接口基础路径
	public static final String WX_BASE_URL = "https://api.weixin.qq.com";
	// access token method
	public static final String TOKENNAME = "/cgi-bin/token";
	// api ticket method
	public static final String JSNNAME = "/cgi-bin/ticket";
	// card api ticket method
	public static final String CARDNAME = "/cgi-bin/ticket/card";
	// access token
	public static final String JS_API_ACCESS_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";
	// js api ticket
	public static final String JS_API_TICKET_URL = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?type=jsapi&access_token=ACCESS_TOKEN";
	// js api ticket
	public static final String CARD_API_TICKET_URL = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?type=wx_card&access_token=ACCESS_TOKEN";
	// 卡券创建
	public static final String CREATE_CARD_URL = "https://api.weixin.qq.com/card/create?access_token=ACCESS_TOKEN";
	// 卡券二维码
	public static final String CREATE_QR_URL = "https://api.weixin.qq.com/card/qrcode/create?access_token=ACCESS_TOKEN";
	// 卡券核销
	public static final String CONSUME_URL = "https://api.weixin.qq.com/card/code/consume?access_token=ACCESS_TOKEN";
	// 卡券核销
	public static final String CORD_ID_URL = "https://api.weixin.qq.com/card/code/get?access_token=ACCESS_TOKEN";
	// 微信服务器的IP地址列表
	public static final String GET_IP_URL = "https://api.weixin.qq.com/cgi-bin/getcallbackip?access_token=ACCESS_TOKEN";
	// 卡券列表
	public static final String CARD_LIST_URL = "https://api.weixin.qq.com/card/batchget?access_token=ACCESS_TOKEN";
	// 卡券详情
	public static final String CARD_DETAIL_URL = "https://api.weixin.qq.com/card/get?access_token=ACCESS_TOKEN";
	// 获取用户信息
	public static final String USER_INFO_URL = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=ACCESS_TOKEN&openid=OPENID";
	// 获取微信二维码ticket
	public static final String QRCODE_URL = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=ACCESS_TOKEN";

	// 消息模板 -发送模板消息
	public static final String MESSAGE_TEMPLATE = "/cgi-bin/message/template/send?access_token=";
//	// TM00009 会员充值通知
//	public static final String TM00009 = "TM00009";
//	// TM00015 订单支付成功
//	public static final String TM00015 = "TM00015";
//	// TM00017 订单状态更新
//	public static final String TM00017 = "TM00017";
//	// OPENTM200565259 订单发货提醒
//	public static final String OPENTM200565259 = "OPENTM200565259";
//	// TM00251 领取成功通知
//	public static final String TM00251 = "TM00251";

//	// 下单提醒
//	public static final String ORDER_SUBMIT_SUCCESS = "3CBNY913zkEvwuCTy93kxjBFaFmwi6xDUx1cZMZYMko";
//	// 订单支付成功
//	public static final String ORDER_PAY_SUCCESS = "4Weme5qHUj0TSrVKuSRm8vR1TkOSZzwbQ8j_wxClOxc";
//	// 会员充值通知
//	public static final String MEMBER_CHARGE = "DlTvo19tLiUOSSJSvefWJeAmNea0Dp0Fqk1XXzJJPyY";
//	// 订单状态更新
//	public static final String ORDER_STATUS_UPDATE = "P6_GYXv3k3kbZflOKsFjpZKkqPkIgrJg415fP8YQT7M";
//	// 订单发货提醒
//	public static final String PACKAGE_DISPATCH = "Pvjoax_MOTy_Qh8SMLsO9hfM06h19HxEjl-Yt_prfss";
//	// 领取成功通知
//	public static final String PACKAGE_RECEIVE = "oYu_cRfa_ruya8xReL_zcZtjWvu4txuzO4fJ13RCofg ";

	/**
	 * 发红包提醒模版编号
	 */
	public static final String HONG_BAO_PUBLISH_ALARM = "OPENTM200681790";
	/**
	 * 实时交易提醒模板
	 */
	public static final String TRAN_PUBLISH_ALARM = "TM00244";
	
	// 关注公众号的素材地址
	public static final String FOLLOW_URL = "follow.url";

	public static final String NO_MERCHANT = "商户不存在";
	
	public static final String SYSTEM_RRROR = "系统异常，操作失败，请重试";//add xudongdong
	
	// 1号母商户公众号appid
//	public static final String COMPONENT_APP_ID = "wxe03cecf4c8449cd7";
	// 2号母商户公众号appid
	public static final String COMPONENT_APP_ID = "wxf6506ace0f46808e";
	public static final String API_COMPONENT_TOKEN = "API_COMPONENT_TOKEN";
	// 获取第三方平台access_token
	public static final String API_COMPONENT_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/component/api_component_token";
	// 获取（刷新）授权公众号的令牌
	public static final String API_AUTHORIZER_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/component/api_authorizer_token?component_access_token=API_COMPONENT_TOKEN";
}
