package cn.congine.wizarpos.mall.model.open;

/**
 * 
 * @author xudongdong
 * Date: 2015.7.8
 */

import java.util.List;

public class CateringSaleOrderListQueryOutput extends CommonOutput {

	private static final long serialVersionUID = 1L;
	
	private List<CateringSaleOrderDto> saleOrderList = null;

	public List<CateringSaleOrderDto> getSaleOrderList() {
		return saleOrderList;
	}

	public void setSaleOrderList(List<CateringSaleOrderDto> saleOrderList) {
		this.saleOrderList = saleOrderList;
	}
}
