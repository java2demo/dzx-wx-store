package cn.congine.wizarpos.mall.dao.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import cn.congine.wizarpos.mall.dao.MyShakeDao;
import cn.congine.wizarpos.mall.entity.MyShake;

@Repository("myShakeDao")
public class MyShakeDaoHibernate extends GenericDaoHibernate<MyShake>
		implements MyShakeDao {

	public MyShakeDaoHibernate() {
		super(MyShake.class);
	}

	@Override
	public List<MyShake> getMyShakeList(String mid, String onlineActivityId,List<String> ticketMids) {
		
		Query query = getSession().createQuery(
				"from MyShake where mid = :mid and onlineActivityId=:onlineActivityId and (ticketType=:ticketType  or ticketMid not in (:ticketMids))").setParameter("mid",
						mid).setParameter("onlineActivityId",onlineActivityId).setParameter("ticketType", "10").setParameterList("ticketMids", ticketMids);
		return (List<MyShake>) query.list();
	}
	
	@Override
	public List<MyShake> getMyShakeList(String mid, String onlineActivityId) {
		
		Query query = getSession().createQuery(
				"from MyShake where mid = :mid and onlineActivityId=:onlineActivityId").setParameter("mid",
						mid).setParameter("onlineActivityId",onlineActivityId);
		return (List<MyShake>) query.list();
	}

	@Override
	public MyShake getMyShakeByid(String ticketId, String mid, String onlineActivityId) {
		Query query = getSession()
				.createQuery(
						"from MyShake where ticketId = :ticketId and mid= :mid and onlineActivityId= :onlineActivityId")
				.setParameter("ticketId", ticketId).setParameter("mid", mid).setParameter("onlineActivityId", onlineActivityId);
		return (MyShake) query.uniqueResult();
	}

	@Override
	public List<MyShake> getMyShakeListExitHongBao(String mid,
			String onlineActivityId) {
		Query query = getSession().createQuery(
				"from MyShake where  mid = :mid and onlineActivityId=:onlineActivityId").setParameter("mid",
						mid).setParameter("onlineActivityId",onlineActivityId);
		return (List<MyShake>) query.list();
	}
	
}
