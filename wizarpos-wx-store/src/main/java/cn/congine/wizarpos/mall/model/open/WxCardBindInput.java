package cn.congine.wizarpos.mall.model.open;

/**
 * 
 * @author xudongdong
 * Date: 2015.7.15
 */


public class WxCardBindInput extends CommonInput {

	private static final long serialVersionUID = 1L;
	
	private String openId = null;
	
	private String ticketId = null;

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}



}
