package cn.congine.wizarpos.mall.dao;

import java.util.List;

import cn.congine.wizarpos.mall.common.Page;
import cn.congine.wizarpos.mall.model.SysMerchantDef;

public interface SysMerchantDefDao extends GenericDao<SysMerchantDef> {

	public SysMerchantDef getSysMerchantDefByMid(String mid);

	public List<SysMerchantDef> getSysMerchantDefListByName(String merchantName);

	public SysMerchantDef getSysMerchantDefByPid(String pid);

	/**
	 * 分页
	 * 
	 * @param name
	 * @param page
	 * @return
	 */
	public List<SysMerchantDef> getMerchantDefListByName(String name,
			Page<SysMerchantDef> page);

	public void updateAccess(SysMerchantDef obj);

	public void updateStartNo(String mid, int startNo);

}
