package cn.congine.wizarpos.mall.model.dzx;

import cn.congine.wizarpos.mall.model.open.CommonInput;

public class DxChartMemberRegisterInput extends CommonInput {

	private static final long serialVersionUID = 1L;

	private String loginName = null;

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
 
}
