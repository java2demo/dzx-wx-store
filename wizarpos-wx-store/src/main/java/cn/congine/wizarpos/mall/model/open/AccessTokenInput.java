package cn.congine.wizarpos.mall.model.open;

public class AccessTokenInput extends CommonInput {

	private static final long serialVersionUID = 1L;

	private String mid = null;
	
	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}
}
