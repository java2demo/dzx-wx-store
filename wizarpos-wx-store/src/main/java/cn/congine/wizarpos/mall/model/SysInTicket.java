package cn.congine.wizarpos.mall.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 银行卡绑定表
 * 
 * @author Administrator
 */

@Entity
@Table(name = "sys_in_ticket")
public class SysInTicket extends BaseEntity {

	private static final long serialVersionUID = -6386816676401989412L;

	@Id
	@Column(name = "wx_ticket")
	private String wxTicket;

	@Column(name = "mid")
	private String mid;

	@Column(name = "expiry_time")
	private Long expiryTime;

	@Column(name = "api_symbol")
	private String apiSymbol;

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public Long getExpiryTime() {
		return expiryTime;
	}

	public void setExpiryTime(Long expiryTime) {
		this.expiryTime = expiryTime;
	}

	public String getWxTicket() {
		return wxTicket;
	}

	public void setWxTicket(String wxTicket) {
		this.wxTicket = wxTicket;
	}

	public String getApiSymbol() {
		return apiSymbol;
	}

	public void setApiSymbol(String apiSymbol) {
		this.apiSymbol = apiSymbol;
	}

}
