package cn.congine.wizarpos.mall.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "wx_shopping_cart_item")
public class WxShoppingCartItem extends BaseEntity {

	private static final long serialVersionUID = -6358013158565485606L;

	// 唯一标识
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "id", length = 80)
	private String id;

	// 关联的购物车编号
	@Column(name = "mid")
	private String mid;
	
	// 关联的购物车编号
	@Column(name = "cart_id")
	private String cartId;

	// 产品ID
	@Column(name = "product_id")
	private String productId;

	// 商品数量
	@Column(name = "product_num")
	private Integer productNum;

	// 商品价格
	@Column(name = "product_price")
	private Integer productPrice;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getCartId() {
		return cartId;
	}

	public void setCartId(String cartId) {
		this.cartId = cartId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public Integer getProductNum() {
		return productNum;
	}

	public void setProductNum(Integer productNum) {
		this.productNum = productNum;
	}

	public Integer getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(Integer productPrice) {
		this.productPrice = productPrice;
	}

	public WxShoppingCartItem() {
		super();
	}

	public WxShoppingCartItem(String mid, String cartId, String productId,
			Integer productNum, Integer productPrice) {
		super();
		this.mid = mid;
		this.cartId = cartId;
		this.productId = productId;
		this.productNum = productNum;
		this.productPrice = productPrice;
	}

	@Override
	public String toString() {
		return "WxShoppingCartItem [id=" + id + ", cartId=" + cartId
				+ ", productId=" + productId + ", productNum=" + productNum
				+ ", productPrice=" + productPrice + "]";
	}

}
