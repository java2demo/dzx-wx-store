package cn.congine.wizarpos.mall.service;

import java.util.List;

import cn.congine.wizarpos.mall.entity.MyShake;

public interface MyShakeService {
	List<MyShake> getMyShakeList(String mid,String onlineActivityId,List<String> ticketMids);
	
	List<MyShake> getMyShakeList(String mid,String onlineActivityId);
	
	MyShake getMyShakeByid(String ticketId,String mid,String onlineActivityId);
	
	MyShake get(String id);
	
	void update(MyShake object);
}
