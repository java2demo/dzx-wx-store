package cn.congine.wizarpos.mall.service;

import cn.congine.wizarpos.mall.entity.MyOnlineActivity;


public interface MyOnlineActivityService {

   MyOnlineActivity getMyOnlineActivityByMid(String mid);
}
