package cn.congine.wizarpos.mall.model.open;

import java.util.List;


public class MerchandiseSyncOutput extends CommonOutput {

	private static final long serialVersionUID = 1L;
	
	private List<Merchandise> result = null;

	public List<Merchandise> getResult() {
		return result;
	}

	public void setResult(List<Merchandise> result) {
		this.result = result;
	}

	
}
