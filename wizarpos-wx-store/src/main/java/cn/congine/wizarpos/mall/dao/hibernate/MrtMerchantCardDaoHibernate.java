package cn.congine.wizarpos.mall.dao.hibernate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.FlushMode;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import cn.congine.wizarpos.mall.dao.MrtMerchantCardDao;
import cn.congine.wizarpos.mall.model.MrtMerchantCard;
import cn.congine.wizarpos.mall.utils.Function;

@Repository("mrtMerchantCardDao")
public class MrtMerchantCardDaoHibernate extends
		GenericDaoHibernate<MrtMerchantCard> implements MrtMerchantCardDao {

	public MrtMerchantCardDaoHibernate() {
		super(MrtMerchantCard.class);
	}

	@Override
	public MrtMerchantCard getMrtMerchantCardByCardNo(String cardNo) {
		Query query = getSession().createQuery(
				"from MrtMerchantCard where cardNo = :cardNo").setParameter(
				"cardNo", cardNo);
		return (MrtMerchantCard) query.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MrtMerchantCard> getMrtMerchantCardListByOpenId(String openId) {
		Query query = getSession().createQuery(
				"from MrtMerchantCard where openId = :openId").setParameter(
				"openId", openId);
		return (List<MrtMerchantCard>) query.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MrtMerchantCard> getMrtMerchantCardByMid(String mid) {
		Query query = getSession().createQuery(
				"from MrtMerchantCard where mid = :mid").setParameter("mid",
				mid);
		return (List<MrtMerchantCard>) query.list();
	}

	@Override
	public MrtMerchantCard getMrtMerchantCard(String mid, String openId) {
		Query query = getSession()
				.createQuery(
						"from MrtMerchantCard where mid =:mid and openId =:openId and canceled='0'");
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("mid", mid);
		param.put("openId", openId);
		query.setProperties(param);
		return (MrtMerchantCard) query.uniqueResult();
	}

	@Override
	public MrtMerchantCard getMrtMerchantCardByCardId(String id) {
		Query query = getSession().createQuery(
				"from MrtMerchantCard where id = :id").setParameter(
				"id", id);
		return (MrtMerchantCard) query.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MrtMerchantCard> getMrtMerchantCardList(String openId) {

		Query query = getSession().createQuery(
				"from MrtMerchantCard where openId = :openId").setParameter(
				"openId", openId);

		return (List<MrtMerchantCard>) query.list();
	}

	@Override
	public int getMemberNum(String mid) {
		Query query = getSession().createQuery(
				"select count(*) from MrtMerchantCard where mid = :mid")
				.setParameter("mid", mid);
		return ((Long) query.uniqueResult()).intValue();
	}

//	@Override
//	public MrtMerchantCard save(MrtMerchantCard merchantCard) {
//		String uuid = Function.getUid();
//		merchantCard.setId(uuid);
//		String sql = "insert into mrt_merchant_card(card_no,wx_bind_no,card_type,open_id,username,mobile_no,"
//				+ "active_time,balance,expriy_time,canceled,cancel_time,freeze,"
//				+ "freeze_time,create_time,points,total_points,mid,id) "
//				+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
//		this.getSession().setFlushMode(FlushMode.MANUAL);
//		Query q = getSession().createSQLQuery(sql);
//		q.setString(0, merchantCard.getCardNo());
//		q.setString(1, merchantCard.getWxBindNo());
//		q.setString(2, merchantCard.getCardType());
//		q.setString(3, merchantCard.getOpenId());
//		q.setString(4, merchantCard.getUserName());
//		q.setString(5, merchantCard.getMobileNo());
//		q.setString(6, Function.formatDate(merchantCard.getActiveTime(),
//				"yyyy-MM-dd HH:mm:ss"));
//		q.setInteger(7, merchantCard.getBalance());
//		q.setString(
//				8,
//				(merchantCard.getExpriyTime() == null) ? null : (Function
//						.formatDate(merchantCard.getExpriyTime(),
//								"yyyy-MM-dd HH:mm:ss")));
//		q.setBoolean(9, merchantCard.getCanceled());
//		q.setDate(10, merchantCard.getCancelTime());
//		q.setBoolean(11, merchantCard.getFreeze());
//		q.setDate(12, merchantCard.getFreezeTime());
//		q.setDate(13, merchantCard.getCreateTime());
//		q.setInteger(14, merchantCard.getPoints());
//		q.setInteger(15, merchantCard.getTotalPoints() == null ? 0
//				: merchantCard.getTotalPoints());
//		q.setString(16, merchantCard.getMid());
//		q.setString(17, uuid);
//		q.executeUpdate();
//		return merchantCard;
//	}

	@Override
	public void update(MrtMerchantCard merchantCard) {
		String hql = "UPDATE mrt_merchant_card "
				+ "SET card_no = ?, wx_bind_no = ?, card_type = ?, open_id = ?, username = ?, mobile_no = ?,"
				+ "active_time = ?, balance = ?, expriy_time = ?, canceled = ?, cancel_time = ?, freeze = ?,"
				+ "freeze_time = ?, create_time = ?, points = ?, total_points = ? "
				+ "WHERE id = ? and mid = ? ";
		this.getSession().setFlushMode(FlushMode.MANUAL);
		Query q = getSession().createSQLQuery(hql);
		q.setString(0, merchantCard.getCardNo());
		q.setString(1, merchantCard.getWxBindNo());
		q.setString(2, merchantCard.getCardType());
		q.setString(3, merchantCard.getOpenId());
		q.setString(4, merchantCard.getUserName());
		q.setString(5, merchantCard.getMobileNo());
		q.setString(
				6,
				merchantCard.getActiveTime() == null ? null : (Function
						.formatDate(merchantCard.getActiveTime(),
								"yyyy-MM-dd HH:mm:ss")));
		q.setInteger(7, merchantCard.getBalance());
		q.setString(
				8,
				merchantCard.getExpriyTime() == null ? null : (Function
						.formatDate(merchantCard.getExpriyTime(),
								"yyyy-MM-dd HH:mm:ss")));
		q.setBoolean(9, merchantCard.getCanceled());
		q.setString(
				10,
				merchantCard.getCancelTime() == null ? null : (Function
						.formatDate(merchantCard.getCancelTime(),
								"yyyy-MM-dd HH:mm:ss")));
		q.setBoolean(11, merchantCard.getFreeze());
		q.setString(
				12,
				merchantCard.getFreezeTime() == null ? null : (Function
						.formatDate(merchantCard.getFreezeTime(),
								"yyyy-MM-dd HH:mm:ss")));
		q.setString(
				13,
				merchantCard.getCreateTime() == null ? null : (Function
						.formatDate(merchantCard.getCreateTime(),
								"yyyy-MM-dd HH:mm:ss")));
		q.setInteger(14, merchantCard.getPoints());
		q.setInteger(15, merchantCard.getTotalPoints() == null ? 0
				: merchantCard.getTotalPoints());
		q.setString(16, merchantCard.getId());
		q.setString(17, merchantCard.getMid());
		q.executeUpdate();
	}

	@Override
	public MrtMerchantCard find(String id, String mid) {
		return (MrtMerchantCard) getSession()
				.createQuery(
						"from MrtMerchantCard b where b.mid=:mid and b.id=:id")
				.setString("mid", mid).setString("id", id).uniqueResult();
	}
}
