package cn.congine.wizarpos.mall.dao.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import cn.congine.wizarpos.mall.dao.ErpTableOrderStageDao;
import cn.congine.wizarpos.mall.model.ErpTableOrderStage;
import cn.congine.wizarpos.mall.utils.Function;

@Repository("erpTableOrderStageDao")
public class ErpTableOrderStageDaoHibernate extends
		GenericDaoHibernate<ErpTableOrderStage> implements
		ErpTableOrderStageDao {

	public ErpTableOrderStageDaoHibernate() {
		super(ErpTableOrderStage.class);
	}

	@Override
	public List<ErpTableOrderStage> getAll(String mid) {
		String hql = "from ErpTableOrderStage where mid=? order by stageRange";
		Query query = this.getSession().createQuery(hql);
		query.setString(0, mid);
		return (List<ErpTableOrderStage>) query.list();
	}

	@Override
	public Object getCurrentStage(String mid) {
		String sql = "select id FROM erp_table_order_stage WHERE mid = ? and ? between left(stage_range,5) and right(stage_range,5)";
		Query query = getSession().createSQLQuery(sql);
		query.setParameter(0, mid);
		query.setParameter(1, Function.formatDate("HH:mm"));
		List list = query.list();
		if (list == null || list.size() == 0) {
			return null;
		}
		return query.list().get(0);
	}

	@Override
	public ErpTableOrderStage getByIdMid(String stageId, String mid) {
		String hql = "from ErpTableOrderStage where mid=? and id=?";
		Query query = this.getSession().createQuery(hql);
		query.setString(0, mid);
		query.setString(1, stageId);
		return (ErpTableOrderStage) query.uniqueResult();
	}

}
