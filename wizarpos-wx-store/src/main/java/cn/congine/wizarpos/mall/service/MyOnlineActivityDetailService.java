package cn.congine.wizarpos.mall.service;

import java.util.List;

import cn.congine.wizarpos.mall.entity.MyOnlineActivityDetail;



public interface MyOnlineActivityDetailService {

	List<MyOnlineActivityDetail> getMyOnlineActivityDetailList(String mid,String onlineActivityId);
	
	List<String> getMyOnlineActivityDetailTicketMidList(String openId,String onlineActivityId);
	
	
	MyOnlineActivityDetail getMyOnlineActivityDetail(String mid,String onlineActivityId,String openId);
	
	
    void saveMyOnlineActivityDetail(MyOnlineActivityDetail myOnlineActivityDetail);
}
