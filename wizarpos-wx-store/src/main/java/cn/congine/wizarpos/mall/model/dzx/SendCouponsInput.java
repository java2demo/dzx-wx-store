package cn.congine.wizarpos.mall.model.dzx;

import java.util.List;
import java.util.Map;

import cn.congine.wizarpos.mall.model.open.CommonInput;

public class SendCouponsInput extends CommonInput {

	private static final long serialVersionUID = 1L;
	private String mid = null;
	private String openId = null;
	private String mobileNo = null;
	private String cardNo = null;
	List<Map<String, String>> ticketIds = null;

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public List<Map<String, String>> getTicketIds() {
		return ticketIds;
	}

	public void setTicketIds(List<Map<String, String>> ticketIds) {
		this.ticketIds = ticketIds;
	}

	 
	
	
}
