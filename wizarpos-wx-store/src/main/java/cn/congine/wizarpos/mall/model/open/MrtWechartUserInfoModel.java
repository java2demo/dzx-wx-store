package cn.congine.wizarpos.mall.model.open;

import java.io.Serializable;

public class MrtWechartUserInfoModel implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String id  = null;

	// 慧商户号

	private String mid = null;

	//

	private String openId = null;

	// 0：取消关注，1：关注

	private String subscribe = null;

	// 昵称
	
	private String nickname = null;

	// 0：男，1女

	private String sex = null;

	// 城市

	private String city = null;

	// 国家

	private String country = null;

	// 省份

	private String province = null;

	// 语言：zh_CH
	
	private String language = null;

	// 头像地址

	private String headimgurl = null;

	// 关注时间

	private String subscribeTime = null;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getSubscribe() {
		return subscribe;
	}

	public void setSubscribe(String subscribe) {
		this.subscribe = subscribe;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getHeadimgurl() {
		return headimgurl;
	}

	public void setHeadimgurl(String headimgurl) {
		this.headimgurl = headimgurl;
	}

	public String getSubscribeTime() {
		return subscribeTime;
	}

	public void setSubscribeTime(String subscribeTime) {
		this.subscribeTime = subscribeTime;
	}
	
}
