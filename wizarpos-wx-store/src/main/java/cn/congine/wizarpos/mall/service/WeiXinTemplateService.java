package cn.congine.wizarpos.mall.service;

import cn.congine.wizarpos.mall.model.WeiXinTemplate;

public interface WeiXinTemplateService {

	public WeiXinTemplate getByTemplate(String mid, String templateNo);

}
