package cn.congine.wizarpos.mall.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 微信消息模版实体
 * 
 * @author test
 *
 */
@Entity
@Table(name = "mrt_template_info")
public class WeiXinTemplate extends BaseEntity {

	private static final long serialVersionUID = 1L;
	/**
	 * 主键
	 */
	@Id
	@Column(name = "id")
	@GeneratedValue(generator = "uuid")
	// 指定生成器名称
	@GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
	private String id;
	/**
	 * 慧商户号
	 */
	@Column(name = "mid")
	private String mid;
	/**
	 * 行业id
	 */
	@Column(name = "industry_id")
	private String industryId;
	/**
	 * 模版编号
	 */
	@Column(name = "template_no")
	private String templateNo;
	/**
	 * 模版id
	 */
	@Column(name = "template_id")
	private String templateId;
	/**
	 * 创建时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8", shape = JsonFormat.Shape.STRING)
	@Column(name = "create_time")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date createTime = new Date();

	@Column(name = "creater_name")
	private String createrName;

	/**
	 * 最后修改时间
	 */
	@Column(name = "last_time")
	private Long lastTime;
	/**
	 * 是否可用 0 可用 1 不可用
	 */
	@Column(name = "enabled")
	private String enabled;
	/**
	 * 备注
	 */
	@Column(name = "remark")
	private String remark;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getIndustryId() {
		return industryId;
	}

	public void setIndustryId(String industryId) {
		this.industryId = industryId;
	}

	public String getTemplateNo() {
		return templateNo;
	}

	public void setTemplateNo(String templateNo) {
		this.templateNo = templateNo;
	}

	public String getTemplateId() {
		return templateId;
	}

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getLastTime() {
		return lastTime;
	}

	public void setLastTime(Long lastTime) {
		this.lastTime = lastTime;
	}

	public String getEnabled() {
		return enabled;
	}

	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getCreaterName() {
		return createrName;
	}

	public void setCreaterName(String createrName) {
		this.createrName = createrName;
	}

}
