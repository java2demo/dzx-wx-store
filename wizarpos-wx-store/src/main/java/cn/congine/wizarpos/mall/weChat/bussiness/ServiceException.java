package cn.congine.wizarpos.mall.weChat.bussiness;

/**
 * 应用程序异常，在使用api的过程中，应用此异常处理错误或未知异常，以保证错误码及异常消息的正常显示。
 * 
 * @author lizhou
 *
 */
public class ServiceException extends Exception {

	public ServiceException() {
	}

	/**
	 * 设置异常消息。注意此时错误码为0，表示系统处理无异常。
	 * 
	 * @param msg
	 *            异常消息
	 */
	public ServiceException(String msg) {
		super(msg);
		this.code = 99;
	}

	public ServiceException(Throwable cause) {
		super(cause);
		this.code = 99;
	}

	/**
	 * 设置错误码和异常消息。
	 * 
	 * @param code
	 *            错误码
	 * @param msg
	 *            异常消息
	 */
	public ServiceException(int code, String msg) {
		super(msg);
		this.code = code;
	}

	/**
	 * 设置异常消息和异常的堆栈信息。
	 * 
	 * @param msg
	 *            异常消息
	 * @param throwable
	 *            异常堆栈信息
	 */
	public ServiceException(String msg, Throwable throwable) {
		super(msg, throwable);
		this.code = 99;
	}

	/**
	 * 设置错误码、异常消息和异常的堆栈信息。
	 * 
	 * @param code
	 *            错误码
	 * @param msg
	 *            异常消息
	 * @param throwable
	 *            异常堆栈信息
	 */
	public ServiceException(int code, String msg, Throwable throwable) {
		super(msg, throwable);
		this.code = code;
	}

	/**
	 * 获取应用程序异常或错误码。0表示正常，非0表示异常。
	 * 
	 * @return 错误码
	 */
	public int getCode() {
		return code;
	}

	private int code = 99;
	// json为空
	public static int JSON_IS_NULL_EXCEPTION = 4;

	private static final long serialVersionUID = 7714791475417240709L;
}
