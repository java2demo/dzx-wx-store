package cn.congine.wizarpos.mall.dao;

import java.util.List;

import cn.congine.wizarpos.mall.model.MrtTicketDef;

public interface MrtTicketDefDao extends GenericDao<MrtTicketDef> {
	List<MrtTicketDef> getMrtTicketDefByMid(String mid);

	MrtTicketDef getByWxCardId(String wxCardId);

	MrtTicketDef get(String id);

	MrtTicketDef find(String id, String mid);
	
	void ticketPass(String cardid, String mid);

	void ticketUnpass(String cardid, String mid);
}
