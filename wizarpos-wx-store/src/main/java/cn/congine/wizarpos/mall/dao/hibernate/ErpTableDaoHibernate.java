package cn.congine.wizarpos.mall.dao.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import cn.congine.wizarpos.mall.dao.ErpTableDao;
import cn.congine.wizarpos.mall.model.ErpTable;

@Repository("ErpTableDao")
public class ErpTableDaoHibernate extends GenericDaoHibernate<ErpTable>
		implements ErpTableDao {
	public ErpTableDaoHibernate() {
		super(ErpTable.class);
	}

	@Override
	public List<ErpTable> getIdleTables(String mid, String tableIds) {
		String hql = "from ErpTable where mid=?";
		
		if (tableIds != null && !tableIds.trim().isEmpty()) {
			hql += " and tableId not in (?)";
		}
		
		Query query = this.getSession().createQuery(hql);
		query.setString(0, mid);
		
		
		if (tableIds != null && !tableIds.trim().isEmpty()) {
			query.setString(1, tableIds);
		}
		
		return query.list();
	}

	@Override
	public ErpTable getTable(String mid, String tableId) {
		String hql = "from ErpTable where mid=? and tableId=?";
		Query query = this.getSession().createQuery(hql);
		query.setString(0, mid);
		query.setString(1, tableId);
		return (ErpTable) query.uniqueResult();
	}
}
