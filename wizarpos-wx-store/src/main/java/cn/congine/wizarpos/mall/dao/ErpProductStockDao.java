package cn.congine.wizarpos.mall.dao;

public interface ErpProductStockDao {

	/**
	 * 	查询总库存
	 * 
	 * @param mid
	 * @param productId
	 * @return
	 */
	Integer getTotalStock(String mid, String productId);

}
