package cn.congine.wizarpos.mall.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.congine.wizarpos.mall.dao.ErpProductCategoryDao;
import cn.congine.wizarpos.mall.model.ErpProductCategory;
import cn.congine.wizarpos.mall.service.ErpProductCategoryService;

@Service("erpProductCategoryService")
public class ErpProductCategoryServiceImpl implements ErpProductCategoryService {

	@Autowired
	private ErpProductCategoryDao erpProductCategoryDao;

	@Override
	public List<ErpProductCategory> getErpProductCategory(String mid) {
		return (List<ErpProductCategory>) erpProductCategoryDao
				.getErpProductCategoryByMid(mid);
	}

	@Override
	public List<ErpProductCategory> getErpProductCategoryByParentId(String mid,
			String id) {
		return (List<ErpProductCategory>) erpProductCategoryDao
				.getErpProductCategoryByParentId(mid, id);
	}

	@Override
	public ErpProductCategory getErpProductCategoryByCode(String mid, String code) {
		return erpProductCategoryDao.getErpProductCategoryByCode(mid, code);
	}

}
