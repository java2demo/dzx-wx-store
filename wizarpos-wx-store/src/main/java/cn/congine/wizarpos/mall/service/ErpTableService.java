package cn.congine.wizarpos.mall.service;

import java.util.List;

import cn.congine.wizarpos.mall.model.ErpTable;

public interface ErpTableService {
	/**
	 * 获取空闲餐台
	 * 
	 * @param tableIds
	 *            已经被预定的餐台ID
	 * @return
	 */
	List<ErpTable> getIdleTables(String mid, String tableIds);

	/**
	 * 获取唯一餐台信息
	 * 
	 * @param tableId
	 *            要预定的餐台ID
	 * @return
	 */
	ErpTable getTable(String mid, String tableId);

}
