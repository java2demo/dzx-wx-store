package cn.congine.wizarpos.mall.model.open;

/**
 * 记录用户访问input
 * @author xudongdong
 * Date: 2015.7.29
 */


public class MemberReadRecordInput extends CommonInput {

	private static final long serialVersionUID = 1L;
	
	private String pid = null;

	private String openId = null;

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

}
