package cn.congine.wizarpos.mall.model.open;

import java.util.List;

import cn.congine.wizarpos.mall.model.WxOrderDetail;

public class WechartOrder   {

	private static final long serialVersionUID = 1L;

	private String orderId = null;
	private String mid = null;
	private String openId = null;
	private String status = null;
	private String dispatchType = null;
	private String amount = null;
	private String payStatus = null;
	private String addressId = null;
	// 创建时间
	private Long createTime;//add xudongdong		
	// 销售单Id
	private String saleOrderId;//add xudongdong
	// 销售单号
	private String saleOrderNo;//add xudongdong	
	// 发货时间
	private Long dispatchTime;//add xudongdong
	private List<WxOrderDetail> orderDetail = null;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDispatchType() {
		return dispatchType;
	}

	public void setDispatchType(String dispatchType) {
		this.dispatchType = dispatchType;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(String payStatus) {
		this.payStatus = payStatus;
	}

	public String getAddressId() {
		return addressId;
	}

	public void setAddressId(String addressId) {
		this.addressId = addressId;
	}

	public Long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

	public String getSaleOrderId() {
		return saleOrderId;
	}

	public void setSaleOrderId(String saleOrderId) {
		this.saleOrderId = saleOrderId;
	}

	public String getSaleOrderNo() {
		return saleOrderNo;
	}

	public void setSaleOrderNo(String saleOrderNo) {
		this.saleOrderNo = saleOrderNo;
	}

	public Long getDispatchTime() {
		return dispatchTime;
	}

	public void setDispatchTime(Long dispatchTime) {
		this.dispatchTime = dispatchTime;
	}

	public List<WxOrderDetail> getOrderDetail()
	{
		return orderDetail;
	}

	public void setOrderDetail(List<WxOrderDetail> orderDetail)
	{
		this.orderDetail = orderDetail;
	}

	
}
