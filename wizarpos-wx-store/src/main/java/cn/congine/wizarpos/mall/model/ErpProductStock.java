package cn.congine.wizarpos.mall.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "erp_product_stock")
public class ErpProductStock extends BaseEntity {
	private static final long serialVersionUID = -4309994218963920437L;

	// 唯一标识
	@Id
	@Column(name = "id")
	private String id;

	// 慧商户号
	@Column(name = "mid")
	private String mid;

	// 最后修改时间
	@Column(name = "last_time")
	private Long lastTime;
	
	//商品ID
	@Column(name = "product_id")
	private String productId;
	
	//仓库ID
	@Column(name = "storage_id")
	private String storageId;
	
	//库存
	@Column(name = "qty")
	private String qty;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public Long getLastTime() {
		return lastTime;
	}

	public void setLastTime(Long lastTime) {
		this.lastTime = lastTime;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getStorageId() {
		return storageId;
	}

	public void setStorageId(String storageId) {
		this.storageId = storageId;
	}

	public String getQty() {
		return qty;
	}

	public void setQty(String qty) {
		this.qty = qty;
	}
}
