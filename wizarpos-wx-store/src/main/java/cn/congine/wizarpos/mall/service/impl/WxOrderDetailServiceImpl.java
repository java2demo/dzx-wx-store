package cn.congine.wizarpos.mall.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.congine.wizarpos.mall.dao.WxOrderDetailDao;
import cn.congine.wizarpos.mall.model.WxOrderDetail;
import cn.congine.wizarpos.mall.service.WxOrderDetailService;

@Service("wxOrderDetailService")
public class WxOrderDetailServiceImpl implements WxOrderDetailService {

	@Autowired
	private WxOrderDetailDao wxOrderDetailDao;

	@Override
	public List<WxOrderDetail> getWxOrderDetailListByOrderId(String orderId) {
		return wxOrderDetailDao.getWxOrderDetailListByOrderId(orderId);
	}

	@Override
	public WxOrderDetail getWxOrderDetailByProductId(String productId) {
		return wxOrderDetailDao.getWxOrderDetailByProductId(productId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void save(WxOrderDetail obj) {
		wxOrderDetailDao.save(obj);
	}

	@Override
	public List<WxOrderDetail> getWxOrderDetailList(String orderId) {
		return wxOrderDetailDao.getWxOrderDetailListByOrderId(orderId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void removeByOrderId(String mid, String OrderId) {
		wxOrderDetailDao.removeByOrderId(mid, OrderId);
	}

}
