package cn.congine.wizarpos.mall.model.open;

/**
 * 商家信息
 * 
 * @author Gu
 * 
 *         2015-5-13 下午10:06:48
 */
public class MerchantDefQueryOutput extends CommonOutput {

	private static final long serialVersionUID = 1L;

	private MerchantDef result = null;

	public MerchantDef getResult() {
		return result;
	}

	public void setResult(MerchantDef result) {
		this.result = result;
	}

}
