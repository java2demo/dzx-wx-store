package cn.congine.wizarpos.mall.dao;

import java.util.List;

import cn.congine.wizarpos.mall.model.MrtTicketInfo;

public interface MrtTicketInfoDao extends GenericDao<MrtTicketInfo> {
	List<Object> getMrtTicketInfoByCardId(String cardId, int pageNo);

	List<Object> getUsedTicketInfo(String cardId, int pageNo);

	Object getIdByWxTicketIdAndNoAndOpenid(String cardId, String no,
			String openid);

	MrtTicketInfo getByNo(String mid, String ticketNo);

	/**
	 * 用户是否已经拥有此类券
	 * 
	 * @param ticketDefId
	 * @param cardId
	 * @param mid
	 * @return
	 */
	boolean isExist(String ticketDefId, String cardId, String mid);

	MrtTicketInfo find(String id, String mid);

	MrtTicketInfo getByTicketDefIdAndCardIdAndMid(String ticketDefId,
			String cardId, String mid);
}
