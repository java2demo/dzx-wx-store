package cn.congine.wizarpos.mall.model.open;

import java.io.Serializable;

public class AddressDetailInput extends CommonInput {

	private static final long serialVersionUID = 1L;
	String openId = null;
	String mid = null;
	String id = null;
	
	public String getOpenId() {
		return openId;
	}
	public void setOpenId(String openId) {
		this.openId = openId;
	}
	public String getMid() {
		return mid;
	}
	public void setMid(String mid) {
		this.mid = mid;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
}