package cn.congine.wizarpos.mall.model.open;

/**
 * 
 * @author xudongdong
 * Date: 2015.7.7
 */

import java.util.Map;

public class CateringOrderBindingQueryOutput extends CommonOutput {

	private static final long serialVersionUID = 1L;
	
	private Map<String, Object> result = null;

	public Map<String, Object> getResult() {
		return result;
	}

	public void setResult(Map<String, Object> result) {
		this.result = result;
	}
}
