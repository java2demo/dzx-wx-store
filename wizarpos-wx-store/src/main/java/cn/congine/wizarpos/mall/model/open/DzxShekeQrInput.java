package cn.congine.wizarpos.mall.model.open;

public class DzxShekeQrInput extends CommonInput {

	private static final long serialVersionUID = -6386816676401989412L;

	private String info = null;

	private String subInfo = null;

	private String mid = null;

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getSubInfo() {
		return subInfo;
	}

	public void setSubInfo(String subInfo) {
		this.subInfo = subInfo;
	}

}
