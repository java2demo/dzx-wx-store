package cn.congine.wizarpos.mall.model.open;

/**
 * 
 * @author xudongdong
 * Date: 2015.7.9
 */

import java.util.Date;

public class TranLogView implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 交易时间
	 */
	private Date tranTime;
	/**
	 * 交易码
	 */
	private String tranCode;
	/**
	 * 交易金额
	 */
	private Integer tranAmount;
	/**
	 * 余额
	 */
	private Integer balance;
	public Date getTranTime() {
		return tranTime;
	}
	public void setTranTime(Date tranTime) {
		this.tranTime = tranTime;
	}
	public String getTranCode() {
		return tranCode;
	}
	public void setTranCode(String tranCode) {
		this.tranCode = tranCode;
	}
	public Integer getTranAmount() {
		return tranAmount;
	}
	public void setTranAmount(Integer tranAmount) {
		this.tranAmount = tranAmount;
	}
	public Integer getBalance() {
		return balance;
	}
	public void setBalance(Integer balance) {
		this.balance = balance;
	}
}
