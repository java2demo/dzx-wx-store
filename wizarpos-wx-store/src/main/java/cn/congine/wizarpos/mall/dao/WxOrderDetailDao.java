package cn.congine.wizarpos.mall.dao;

import java.util.List;

import cn.congine.wizarpos.mall.model.WxOrderDetail;

public interface WxOrderDetailDao extends GenericDao<WxOrderDetail> {

	List<WxOrderDetail> getWxOrderDetailListByOrderId(String orderId);

	WxOrderDetail getWxOrderDetailByProductId(String productId);

	void removeByOrderId(String mid, String orderId);

}
