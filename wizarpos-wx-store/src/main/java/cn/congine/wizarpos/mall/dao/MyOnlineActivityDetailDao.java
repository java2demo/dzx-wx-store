package cn.congine.wizarpos.mall.dao;

import java.util.List;

import cn.congine.wizarpos.mall.entity.MyOnlineActivityDetail;

public interface MyOnlineActivityDetailDao extends GenericDao<MyOnlineActivityDetail> {

      List<MyOnlineActivityDetail> getMyOnlineActivityDetailList(String mid,String onlineActivityId);
	
      List<String> getMyOnlineActivityDetailTicketMidList(String openId,String onlineActivityId);
      
      
      MyOnlineActivityDetail getMyOnlineActivityDetail(String mid,String onlineActivityId,String openId);
}
