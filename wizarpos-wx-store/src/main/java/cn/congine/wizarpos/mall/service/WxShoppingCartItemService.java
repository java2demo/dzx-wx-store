package cn.congine.wizarpos.mall.service;

import java.util.List;

import cn.congine.wizarpos.mall.model.WxShoppingCartItem;

public interface WxShoppingCartItemService {

	WxShoppingCartItem getWxShoppingCartItemById(String id);

	void save(WxShoppingCartItem obj);

	void update(WxShoppingCartItem obj);

	void remove(String id, String mid);

	List<cn.congine.wizarpos.mall.vo.WxShoppingCartItem> getWxShoppingCartItemListByCartId(
			String cartId);

	WxShoppingCartItem getWxShoppingCartItem(String productId, String cartId);

	WxShoppingCartItem getWxShoppingCartItemByOpenId(String openId);

	/**
	 * 更新购物车中物品数量数量
	 * 
	 * @param num
	 * @return
	 */
	public Boolean updateNum(Integer num, String cartId, String productId, String mid);

	/**
	 * 获取当前购物车中的商品
	 * 
	 * @param cartId
	 * @return
	 */
	public List<WxShoppingCartItem> getCarList(String cartId);

	/**
	 * 检查购物车中是否有相同的商品
	 * 
	 * @param proudctId
	 *            产品id
	 * @param cartId
	 *            购物车Id
	 * @return 是否
	 */
	public Boolean checkItemsExist(String proudctId, String cartId);

	/**
	 * 获取当前购物车商品的数量
	 * 
	 * @param cartId
	 *            购物车Id
	 * @return
	 */
	public Integer getItemsNum(String cartId);

	/**
	 * 获得购物车某商品的数量
	 * 
	 * @param cartId
	 * @param productId
	 * @return
	 */
	public int getItemNum(String cartId, String productId);

	void removeByCartId(String mid, String cartId);

}