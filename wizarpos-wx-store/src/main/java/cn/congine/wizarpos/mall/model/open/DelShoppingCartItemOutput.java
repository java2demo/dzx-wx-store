package cn.congine.wizarpos.mall.model.open;

/**
 * 
 * @author xudongdong
 * Date: 2015.7.6
 */

import java.util.List;

public class DelShoppingCartItemOutput extends CommonOutput {

	private static final long serialVersionUID = 1L;

	private int totalNum;
	
	private int delNum;
	
	private List<String> faildItemId = null;
	
	public int getTotalNum() {
		return totalNum;
	}

	public void setTotalNum(int totalNum) {
		this.totalNum = totalNum;
	}

	public int getDelNum() {
		return delNum;
	}

	public void setDelNum(int delNum) {
		this.delNum = delNum;
	}

	public List<String> getFaildItemId() {
		return faildItemId;
	}

	public void setFaildItemId(List<String> faildItemId) {
		this.faildItemId = faildItemId;
	}

}


