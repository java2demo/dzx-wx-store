package cn.congine.wizarpos.mall.service;

import cn.congine.wizarpos.mall.entity.SysInToken;

public interface SysInTokenService {
	
	SysInToken getSysInTokenByTokenId(String tokenId);

	void save(SysInToken object);

	void delete(SysInToken token);
	
}
