package cn.congine.wizarpos.mall.dao.hibernate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import cn.congine.wizarpos.mall.common.Page;
import cn.congine.wizarpos.mall.dao.ErpProductDao;
import cn.congine.wizarpos.mall.model.ErpProduct;

@Repository("erpProductDao")
public class ErpProductDaoHibernate extends GenericDaoHibernate<ErpProduct>
		implements ErpProductDao {

	public ErpProductDaoHibernate() {
		super(ErpProduct.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ErpProduct> getErpProductListByCategory(String categoryId) {
		Query query = getSession()
				.createQuery(
						"from ErpProduct where categoryId = :categoryId and storageRackFlag ='1' and used_flag='1'")
				.setParameter("categoryId", categoryId);
		return (List<ErpProduct>) query.list();
	}

	@Override
	public ErpProduct getErpProductByCode(String code) {
		Query query = getSession()
				.createQuery(
						"from ErpProduct where code = :code and storageRackFlag ='1' and used_flag='1'")
				.setParameter("code", code);
		return (ErpProduct) query.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ErpProduct> getErpProductListByMid(String mid) {
		Query query = getSession()
				.createQuery(
						"from ErpProduct where mid = :mid and storageRackFlag ='1' and used_flag='1'")
				.setParameter("mid", mid);
		query.setFirstResult(0);
		query.setMaxResults(3);
		return (List<ErpProduct>) query.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ErpProduct> getErpProductListByMid(String mid, String categoryId) {
		Query query = getSession()
				.createQuery(
						"from ErpProduct where mid = :mid and categoryId=:categoryId and storageRackFlag='1' and used_flag='1'");
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("mid", mid);
		param.put("categoryId", categoryId);
		query.setProperties(param);
		return (List<ErpProduct>) query.list();
	}

	@Override
	public List<ErpProduct> getErpProductListByPage(String mid,
			String categoryId, int pageIndex) {
		String hql = "from ErpProduct where mid = :mid and categoryId=:categoryId and storageRackFlag ='1' and used_flag='1'";

		Map<String, Object> param = new HashMap<String, Object>();
		param.put("mid", mid);
		param.put("categoryId", categoryId);

		Page<ErpProduct> page = new Page<ErpProduct>();
		page.setPageNo(pageIndex);

		page = this.readAll4Page(hql.toString(), "", param, page);

		return page.getList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ErpProduct> getHotErpProductListByMid(String mid,
			String hotJudge) {
		Query query = getSession()
				.createQuery(
						"from ErpProduct where mid = :mid and hotSaleFlag ='1' and storageRackFlag ='1' and used_flag='1' order by lastTime DESC")
				.setParameter("mid", mid);
		query.setFirstResult(0);
		query.setMaxResults(7);
		return (List<ErpProduct>) query.list();
	}

	@Override
	public List<ErpProduct> getHotErpProductListByMid(String mid,
			String hotJudge, Page<ErpProduct> page) {
		StringBuilder hql = new StringBuilder(
				"from ErpProduct where mid = :mid and hotSaleFlag ='1' and storageRackFlag ='1' and used_flag='1' order by lastTime DESC");
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("mid", mid);
		page = this.readAll4Page(hql.toString(), "", params, page);
		return page.getList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ErpProduct> getMoreHotErpProductListByMid(String mid) {
		Query query = getSession()
				.createQuery(
						"from ErpProduct where mid = :mid  and hotSaleFlag ='1' and storageRackFlag ='1' and used_flag='1'")
				.setParameter("mid", mid);
		query.setFirstResult(0);
		query.setMaxResults(3);
		return (List<ErpProduct>) query.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ErpProduct> getErpProductByName(String name, String mid,
			String id) {
		Map<String, Object> param = new HashMap<String, Object>();

		String sql = "from ErpProduct where 1=1 and mid=:mid  and categoryId=:categoryId and storageRackFlag ='1' and used_flag='1' ";
		if (!StringUtils.isEmpty(name)) {
			sql += " and name like :name ";
		}

		Query query = getSession().createQuery(sql);
		if (!StringUtils.isEmpty(name)) {
			param.put("name", "%" + name + "%");
		}
		param.put("mid", mid);
		param.put("categoryId", id);
		query.setProperties(param);
		return (List<ErpProduct>) query.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ErpProduct> getHotErpProductListByMid(String mid) {
		Query query = getSession()
				.createQuery(
						"from ErpProduct where mid = :mid and storageRackFlag ='1' and hotSaleFlag ='1' and used_flag='1' ")
				.setParameter("mid", mid);
		query.setFirstResult(0); // 浠庣0鏉″紑濮�
		query.setMaxResults(3);
		return (List<ErpProduct>) query.list();
	}

	@Override
	public List<ErpProduct> getErpProductListByMid(String mid,
			Page<ErpProduct> page) {
		String hql = "from ErpProduct where mid = :mid and storageRackFlag ='1' and used_flag='1'";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("mid", mid);
		page = this.readAll4Page(hql, "", params, page);
		return page.getList();
	}

	@Override
	public List<ErpProduct> getHotErpProductListByMid(String mid,
			String hotJudge, Page<ErpProduct> page, String categoryId) {
		StringBuilder hql = new StringBuilder(
				"from ErpProduct where mid ="
						+ mid
						+ " and storageRackFlag ='1' and hotSaleFlag ='1' and used_flag='1'");
		Map<String, Object> params = new HashMap<String, Object>();
		if (!("").equals(categoryId) && categoryId != null) {
			hql.append(" and categoryId ='" + categoryId + "'");
		}
		page = this.readAll4Page(hql.toString(), "", params, page);
		return page.getList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ErpProduct> getHotErpProductListByMid(String mid,
			String hotJudge, String categoryId) {
		StringBuilder hql = new StringBuilder(
				"from ErpProduct where mid ="
						+ mid
						+ " and storageRackFlag ='1' and hotSaleFlag ='1' and used_flag='1'");
		if (!("").equals(categoryId) && categoryId != null) {
			hql.append(" and categoryId ='" + categoryId + "'");
		}
		Query query = getSession().createQuery(hql.toString());
		query.setFirstResult(0);
		query.setMaxResults(10);
		return (List<ErpProduct>) query.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ErpProduct> getHotErpProductListByCategoryId(String mid,
			String hotJudge, String categoryId) {
		StringBuilder hql = new StringBuilder("from ErpProduct where mid ='"
				+ mid + "' and storageRackFlag ='1' and used_flag='1'");
		if (categoryId != null && !categoryId.isEmpty()) {
			hql.append(" and categoryId ='" + categoryId + "'");
		}
		Query query = getSession().createQuery(hql.toString());
		query.setFirstResult(0);
		query.setMaxResults(10);
		return (List<ErpProduct>) query.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ErpProduct> getProductList(String mid, String categoryId,
			String kyeWord) {
		String hql = " from ErpProduct where mid='"
				+ mid
				+ "' and storageRackFlag ='1' and used_flag='1' and categoryId='"
				+ categoryId + "' and name like '%" + kyeWord + "%'";
		Query query = getSession().createQuery(hql.toString());
		query.setFirstResult(0);
		query.setMaxResults(10);
		return query.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ErpProduct> getProductList(String mid, String keyWord) {
		StringBuilder hql = new StringBuilder(
				"from ErpProduct where mid ='"
						+ mid
						+ "' and storageRackFlag ='1' and used_flag='1' and name like '%"
						+ keyWord + "%'");

		// String
		// sql="select * from erp_product where mid='"+mid+"' and category_id='"+categoryId+"' and name like '%"+kyeWord+"%'";
		Query query = getSession().createQuery(hql.toString());
		query.setFirstResult(0);
		query.setMaxResults(10);
		return query.list();
	}

	@Override
	public List<ErpProduct> getProductList(String mid, Page<ErpProduct> page,
			String categoryId, String keyword) {
		StringBuilder hql = new StringBuilder("from ErpProduct where mid ='"
				+ mid + "' and storageRackFlag ='1' and used_flag='1'");
		Map<String, Object> params = new HashMap<String, Object>();
		if (!("").equals(categoryId) && categoryId != null) {
			hql.append(" and categoryId ='" + categoryId + "'");
		}
		if (!("").equals(keyword) && keyword != null) {
			hql.append(" and name  like '%" + keyword + "%'");
		}
		page = this.readAll4Page(hql.toString(), "", params, page);
		return page.getList();
	}

	@Override
	public List<ErpProduct> getErpProductPageListByMid(String mid,
			String categoryId, Integer pageIndex) {
		Query query = getSession()
				.createQuery(
						"from ErpProduct where mid = :mid and categoryId=:categoryId and storageRackFlag='1' and used_flag='1'");
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("mid", mid);
		param.put("categoryId", categoryId);
		query.setProperties(param);
		query.setMaxResults(10);
		Integer first = (pageIndex - 1) * 10;
		query.setFirstResult(first);
		return (List<ErpProduct>) query.list();
	}

	// @Override
	// public void update(ErpProduct obj) {
	// String sql =
	// "UPDATE erp_product SET pic_url = ?, last_time = ? WHERE id = ? and mid = ?";
	//
	// this.getSession().setFlushMode(FlushMode.MANUAL);
	//
	// Query q = getSession().createSQLQuery(sql);
	//
	// q.setString(0, obj.getPicUrl());
	// q.setString(1, Function.formatDate("yyyy-MM-dd HH:mm:ss"));
	// q.setString(2, obj.getId());
	// q.setString(3, obj.getMid());
	//
	// q.executeUpdate();
	// }

}
