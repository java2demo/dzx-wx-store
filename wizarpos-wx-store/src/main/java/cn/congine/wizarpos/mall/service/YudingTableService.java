package cn.congine.wizarpos.mall.service;

import java.util.Date;
import java.util.List;

import cn.congine.wizarpos.mall.model.ErpTable;

public interface YudingTableService {

	/**
	 * 根据时间选择可供预定(空闲)的餐台
	 * 
	 * @param mid
	 * @param date
	 * @return
	 */
	List<ErpTable> getYudingTableIdByDate(String mid, Date date);

	/**
	 * 根据日期和时间段选择可供预定(空闲)的餐台
	 * 
	 * @param mid
	 * @param date
	 * @return
	 */
	List<ErpTable> getYudingTableIdByDateStage(String mid, Date date,
			String stageId);

}
