package cn.congine.wizarpos.mall.model.open;

/**
 * 
 * @author xudongdong
 * Date: 2015.7.14
 */

import java.util.List;

import cn.congine.wizarpos.mall.model.SaleOrderItem;

public class SaleOrderView implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	private String id;
	/**
	 * 应付金额
	 */
	private int amount;

	/**
	 * 最后修改时间
	 */
	private Long lastTime;
	/**
	 * 会员商户号
	 */
	private String mid;

	/**
	 * 操作员标识
	 */
	private String operatorId;

	/**
	 * 单据标识（0销售单，1退货单）
	 */
	private int orderFlag;

	/**
	 * 销售单号
	 */
	private String orderNo;

	/**
	 * 仓库编号
	 */
	private String storageId;
	/**
	 * 交易时间
	 */
	private Long orderTime;
	/**
	 * 备注
	 */
	private String remark;

/*	*//**
	 * 终端编号
	 *//*
	private Terminal terminal;*/
	/**
	 * 订单来源('1' pos销售订单 '2' 微信订单 '3' 支付宝订单)
	 */
	private String orderSource;
	/**
	 * 引用的销售单id
	 */
	private String orderId;
	/**
	 * 支付交易流水号
	 */
	private String masterTranLogId;
	/**
	 * 离线交易流水号
	 */
	private String offlineTranLogId;
	/**
	 * 叫号
	 */
	private Integer dayOrderSeq;
	/**
	 * 操作员名字
	 */
	private String operatorName;
	/**
	 * 支付状态0,1
	 */
	private String payStatus;
	/**
	 * 订单类型编号
	 */
	private String orderCatalogId;
	
	private List<SaleOrderItemView> saleOrderItems = null;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public Long getLastTime() {
		return lastTime;
	}
	public void setLastTime(Long lastTime) {
		this.lastTime = lastTime;
	}
	public String getMid() {
		return mid;
	}
	public void setMid(String mid) {
		this.mid = mid;
	}
	public String getOperatorId() {
		return operatorId;
	}
	public void setOperatorId(String operatorId) {
		this.operatorId = operatorId;
	}
	public int getOrderFlag() {
		return orderFlag;
	}
	public void setOrderFlag(int orderFlag) {
		this.orderFlag = orderFlag;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public String getStorageId() {
		return storageId;
	}
	public void setStorageId(String storageId) {
		this.storageId = storageId;
	}
	public Long getOrderTime() {
		return orderTime;
	}
	public void setOrderTime(Long orderTime) {
		this.orderTime = orderTime;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
/*	public Terminal getTerminal() {
		return terminal;
	}
	public void setTerminal(Terminal terminal) {
		this.terminal = terminal;
	}*/
	public String getOrderSource() {
		return orderSource;
	}
	public void setOrderSource(String orderSource) {
		this.orderSource = orderSource;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getMasterTranLogId() {
		return masterTranLogId;
	}
	public void setMasterTranLogId(String masterTranLogId) {
		this.masterTranLogId = masterTranLogId;
	}
	public String getOfflineTranLogId() {
		return offlineTranLogId;
	}
	public void setOfflineTranLogId(String offlineTranLogId) {
		this.offlineTranLogId = offlineTranLogId;
	}
	public Integer getDayOrderSeq() {
		return dayOrderSeq;
	}
	public void setDayOrderSeq(Integer dayOrderSeq) {
		this.dayOrderSeq = dayOrderSeq;
	}
	public String getOperatorName() {
		return operatorName;
	}
	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}
	public String getPayStatus() {
		return payStatus;
	}
	public void setPayStatus(String payStatus) {
		this.payStatus = payStatus;
	}
	public String getOrderCatalogId() {
		return orderCatalogId;
	}
	public void setOrderCatalogId(String orderCatalogId) {
		this.orderCatalogId = orderCatalogId;
	}
	public List<SaleOrderItemView> getSaleOrderItems() {
		return saleOrderItems;
	}
	public void setSaleOrderItems(List<SaleOrderItemView> saleOrderItems) {
		this.saleOrderItems = saleOrderItems;
	}
}
