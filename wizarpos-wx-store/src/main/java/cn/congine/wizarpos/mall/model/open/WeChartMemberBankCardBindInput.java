package cn.congine.wizarpos.mall.model.open;

import java.io.Serializable;

public class WeChartMemberBankCardBindInput extends CommonInput {

	private static final long serialVersionUID = 1L;

	private String openId = null;
	private String ticketId = null;
	private String operate  = null;
	private String id  = null;
	public String getOpenId() {
		return openId;
	}
	public void setOpenId(String openId) {
		this.openId = openId;
	}
	public String getTicketId() {
		return ticketId;
	}
	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}
	public String getOperate() {
		return operate;
	}
	public void setOperate(String operate) {
		this.operate = operate;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}	
	
}
