package cn.congine.wizarpos.mall.model.open;

/**
 * 
 * @author xudongdong
 * Date 2015.7.13
 */

import java.util.List;

public class WechartOrderQueryOutput extends CommonOutput {

	private static final long serialVersionUID = 1L;

	List<WxOrderView> wxOrderViewList = null;

	public List<WxOrderView> getWxOrderViewList() {
		return wxOrderViewList;
	}

	public void setWxOrderViewList(List<WxOrderView> wxOrderViewList) {
		this.wxOrderViewList = wxOrderViewList;
	}
}
