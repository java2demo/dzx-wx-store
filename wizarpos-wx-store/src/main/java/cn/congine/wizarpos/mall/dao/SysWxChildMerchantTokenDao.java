package cn.congine.wizarpos.mall.dao;

import cn.congine.wizarpos.mall.model.SysWxChildMerchantToken;

public interface SysWxChildMerchantTokenDao extends GenericDao<SysWxChildMerchantToken>{

	public int update(
			String mid,
			String authorizer_access_token,
			long authorizer_access_token_expiry,
			String authorizer_refresh_token,
			String id);

	public int updateMrtToken(SysWxChildMerchantToken t);
	
	public SysWxChildMerchantToken getByMid(String mid);
	
	public SysWxChildMerchantToken findByGhCode(String gh);
}
