package cn.congine.wizarpos.mall.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "erp_merchant_login")
public class ErpMerchantLogin extends BaseEntity {
	private static final long serialVersionUID = 1L;

	// 唯一标识
	@Id
	@Column(name = "id")
	private String Id;

	// 慧商户号
	@Column(name = "mid")
	private String mid;

	// 操作人
	@Column(name = "login_name")
	private String loginName;

	//操作时间
	@Column(name = "login_time")
	private String loginTime;

	//场景 1 商户门户 2 微店
	@Column(name = "system_type")
	private String systemType;

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(String loginTime) {
		this.loginTime = loginTime;
	}

	public String getSystemType() {
		return systemType;
	}

	public void setSystemType(String systemType) {
		this.systemType = systemType;
	}

}
