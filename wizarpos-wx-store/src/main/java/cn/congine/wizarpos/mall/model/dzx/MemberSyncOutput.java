package cn.congine.wizarpos.mall.model.dzx;

import cn.congine.wizarpos.mall.model.open.CommonOutput;

public class MemberSyncOutput extends CommonOutput {

	private static final long serialVersionUID = 1L;
    
    private Object result=null;

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	
    		
}
