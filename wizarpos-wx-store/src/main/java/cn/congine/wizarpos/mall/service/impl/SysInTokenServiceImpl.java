package cn.congine.wizarpos.mall.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.congine.wizarpos.mall.dao.SysInTokenDao;
import cn.congine.wizarpos.mall.entity.SysInToken;
import cn.congine.wizarpos.mall.service.SysInTokenService;

@Service("sysInTokenService")
public class SysInTokenServiceImpl implements SysInTokenService {
	@Autowired
	private SysInTokenDao sysInTokenDao;

	@Override
	public SysInToken getSysInTokenByTokenId(String tokenId) {
		return sysInTokenDao.getSysInTokenByTokenId(tokenId);
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void save(SysInToken object) {
		sysInTokenDao.save(object);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void delete(SysInToken token) {
		sysInTokenDao.delete(token);
	}

}
