package cn.congine.wizarpos.mall.dao;

import cn.congine.wizarpos.mall.model.SysWxPrimaryMerchantToken;

public interface SysWxPrimaryMerchantTokenDao extends GenericDao<SysWxPrimaryMerchantToken>{

	public SysWxPrimaryMerchantToken getByComponentAppid(String componentAppid);
	
	public int update(String mid, String component_appid, String component_access_token, long component_access_token_expirey, String component_verify_ticket);
}
