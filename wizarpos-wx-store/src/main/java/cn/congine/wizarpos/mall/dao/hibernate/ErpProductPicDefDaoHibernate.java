package cn.congine.wizarpos.mall.dao.hibernate;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import cn.congine.wizarpos.mall.dao.ErpProductPicDefDao;
import cn.congine.wizarpos.mall.model.ErpProductPicDef;

@Repository("erpProductPicDefDao")
public class ErpProductPicDefDaoHibernate extends
		GenericDaoHibernate<ErpProductPicDef> implements ErpProductPicDefDao {

	public ErpProductPicDefDaoHibernate() {
		super(ErpProductPicDef.class);
	}

	@Override
	public ErpProductPicDef getErpProductPicDefById(String id) {
		Query query = getSession().createQuery(
				"from ErpProductPicDef where id = :id").setParameter("id", id);
		return (ErpProductPicDef) query.uniqueResult();
	}

}
