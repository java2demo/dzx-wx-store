package cn.congine.wizarpos.mall.service;

import cn.congine.wizarpos.mall.model.SysWxPrimaryMerchantToken;

public interface SysWxPrimaryMerchantTokenService {
	
	public SysWxPrimaryMerchantToken getByComponentAppid(String componentAppid);

	public int update(SysWxPrimaryMerchantToken wxComponentAccessInfo);
}
