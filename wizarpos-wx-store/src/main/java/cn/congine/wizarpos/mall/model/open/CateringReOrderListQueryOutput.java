package cn.congine.wizarpos.mall.model.open;

/**
 * 
 * @author xudongdong
 * Date: 2015.7.7
 */

import java.util.List;

public class CateringReOrderListQueryOutput extends CommonOutput {

	private static final long serialVersionUID = 1L;
	
	private List<CateringReOrderDto> orderList = null;

	public List<CateringReOrderDto> getOrderList() {
		return orderList;
	}

	public void setOrderList(List<CateringReOrderDto> orderList) {
		this.orderList = orderList;
	}
	
/*	private Map<String, Object> result = null;

	public Map<String, Object> getResult() {
		return result;
	}

	public void setResult(Map<String, Object> result) {
		this.result = result;
	}*/
	
/*	private Result result = null;

	public Result getResult() {
		return result;
	}

	public void setResult(Result result) {
		this.result = result;
	}

	public class Result {
		private List<Object> orderList = null;

		public List<Object> getOrderList() {
			return orderList;
		}

		public void setOrderList(List<Object> orderList) {
			this.orderList = orderList;
		}
	}*/
}
