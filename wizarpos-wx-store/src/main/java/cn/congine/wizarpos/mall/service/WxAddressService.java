package cn.congine.wizarpos.mall.service;

import java.util.List;

import cn.congine.wizarpos.mall.model.WxAddress;

public interface WxAddressService {

	List<WxAddress> getWxAddressListByCardId(String cardId);

	WxAddress getDefaultWxAddress(String cardId);

	WxAddress getWxAddressId(String id);

	void update(WxAddress obj);

	void save(WxAddress obj);

	void remove(String id,String mid);
}
