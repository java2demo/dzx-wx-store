package cn.congine.wizarpos.mall.service;

import cn.congine.wizarpos.mall.model.SaleOrder;

public interface SaleOrderService {
	SaleOrder getById(String id);

	SaleOrder getByNo(String orderNo);
}
