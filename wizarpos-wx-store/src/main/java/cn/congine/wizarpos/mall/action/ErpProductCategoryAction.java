package cn.congine.wizarpos.mall.action;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import cn.congine.wizarpos.mall.model.ErpProduct;
import cn.congine.wizarpos.mall.model.ErpProductCategory;
import cn.congine.wizarpos.mall.model.MrtMerchantCard;
import cn.congine.wizarpos.mall.model.SysMerchantDef;
import cn.congine.wizarpos.mall.model.open.CommonResultOutput;
import cn.congine.wizarpos.mall.model.open.Merchandise;
import cn.congine.wizarpos.mall.model.open.MerchandiseSyncInput;
import cn.congine.wizarpos.mall.model.open.MerchandiseSyncOutput;
import cn.congine.wizarpos.mall.model.open.ProductCategorySyncInput;
import cn.congine.wizarpos.mall.model.open.ProductCategorySyncOutput;
import cn.congine.wizarpos.mall.model.open.WeChartMemberBankCardListInput;
import cn.congine.wizarpos.mall.service.ErpProductCategoryService;
import cn.congine.wizarpos.mall.service.ErpProductPicDefService;
import cn.congine.wizarpos.mall.service.ErpProductService;
import cn.congine.wizarpos.mall.service.MrtMerchantCardService;
import cn.congine.wizarpos.mall.service.SysMerchantDefService;
import cn.congine.wizarpos.mall.service.impl.WizarposOpenRestClient;
import cn.congine.wizarpos.mall.utils.Function;
import cn.congine.wizarpos.mall.utils.SysConstants;
import cn.congine.wizarpos.mall.utils.Utils;

@Controller
@RequestMapping(value = "/erp_product_category")
public class ErpProductCategoryAction {
	private static Logger logger = Logger
			.getLogger(ErpProductCategoryAction.class);

//	@Autowired
//	private SysMerchantDefService sysMerchantDefService;
//
//	@Autowired
//	private MrtMerchantCardService mrtMerchantCardService;
//
//	@Autowired
//	private ErpProductCategoryService erpProductCategoryService;
//	
//	@Autowired
//	private ErpProductService erpProductService;
//
//	@Autowired
//	private ErpProductPicDefService erpProductPicDefService;
	
	@Autowired
	private WizarposOpenRestClient wizarposOpenRestClient;

	/**
	 * 获得所有分类
	 * 
	 * @param openId
	 * @param mid
	 * @return
	 */
	/*@RequestMapping(value = "/categoryList", method = RequestMethod.GET)
	public ModelAndView categoryList(
			@RequestParam(value = "openId") String openId,
			@RequestParam(value = "mid") String mid) {
		Map<String, Object> data = new HashMap<String, Object>();
		List<ErpProductCategory> erp = null;
		Map<String, Object> getMproduct = new HashMap<String, Object>();
		List<ErpProductCategory> list = null;
		erp = erpProductCategoryService.getErpProductCategory(mid);
		for (ErpProductCategory e : erp) {
			list = erpProductCategoryService
					.getErpProductCategoryByParentId(mid, e.getId());
			if (list != null && list.size() > 0) {
				e.setFlag(true);
			} else {
				e.setFlag(false);
			}
		}
		data.put("first", false);
		data.put("getMproduct", getMproduct);
		data.put("list", list);
		data.put("erp", erp);
		data.put("mid", mid);
		data.put("openId", openId);
		
		return new ModelAndView("productcategory/secondclass", data);

	}*/
	
	/**
	 * 获得所有分类
	 * 
	 * @param openId
	 * @param mid
	 * @return
	 */
	@RequestMapping(value = "/categoryList", method = RequestMethod.GET)
	public ModelAndView categoryList(
			@RequestParam(value = "openId") String openId,
			@RequestParam(value = "mid") String mid) {
		ProductCategorySyncInput input = new ProductCategorySyncInput();
		input.setMid(mid);
		input.setParentId("0");
		ProductCategorySyncOutput output = (ProductCategorySyncOutput) wizarposOpenRestClient.post(input,"/product/category",ProductCategorySyncOutput.class);
		Map<String, Object> data = new HashMap<String, Object>();
		List<ErpProductCategory> list = null;
		List<ErpProductCategory> erp = output.getResult();
		for (ErpProductCategory e : erp) {
			input.setParentId(e.getId());
			list = output.getResult();
			if (list != null && list.size() > 0) {// 有子类分类
				e.setFlag(true);
			} else {
				e.setFlag(false);
			}
		}
		data.put("first", false);
		data.put("erp", output.getResult());
		data.put("list", list);
		data.put("mid", mid);
		data.put("openId", openId);
		return new ModelAndView("productcategory/secondclass", data);
	}
	/**
	 * 进入二级分类
	 * 
	 * @param openId
	 * @param mid
	 * @return
	 */
	/*@RequestMapping(value = "/toSecondclass", method = RequestMethod.GET)
	public ModelAndView toSecondClass(
			@RequestParam(value = "openId") String openId,
			@RequestParam(value = "mid") String mid,
			@RequestParam(value = "categoryId") String categoryId) {

		Map<String, Object> data = new HashMap<String, Object>();
		List<ErpProductCategory> erp = null;

		List<ErpProductCategory> list = null;
		erp = erpProductCategoryService.getErpProductCategoryByParentId(
				mid, categoryId);
		for (ErpProductCategory e : erp) {
			list = erpProductCategoryService
					.getErpProductCategoryByParentId(mid, e.getId());
			if (list != null && list.size() > 0) {// 有子类分类
				e.setFlag(true);
			} else {
				e.setFlag(false);
			}
		}
		data.put("first", true);
		data.put("erp", erp);
		data.put("mid", mid);
		data.put("openId", openId);
		return new ModelAndView("productcategory/secondclass", data);

	}*/
	/**
	 * 进入二级分类
	 * 
	 * @param openId
	 * @param mid
	 * @return
	 */
	@RequestMapping(value = "/toSecondclass", method = RequestMethod.GET)
	public ModelAndView toSecondClass(
			@RequestParam(value = "openId") String openId,
			@RequestParam(value = "mid") String mid,
			@RequestParam(value = "categoryId", required = false) String categoryId ) {
		ProductCategorySyncInput input = new ProductCategorySyncInput();
		input.setMid(mid);
		input.setParentId(categoryId);
		ProductCategorySyncOutput output = (ProductCategorySyncOutput) wizarposOpenRestClient.post(input,"/product/category",ProductCategorySyncOutput.class);
		Map<String, Object> data = new HashMap<String, Object>();
		List<ErpProductCategory> list = null;
		List<ErpProductCategory> erp = output.getResult();
/*		for (ErpProductCategory e : erp) {
			
			list = output.getResult();
			if (list != null && list.size() > 0) {// 有子类分类
				e.setFlag(true);
			} else {
				e.setFlag(false);
			}
		}*/
		data.put("first", false);
		data.put("erp", output.getResult());
		data.put("mid", mid);
		data.put("openId", openId);
		return new ModelAndView("productcategory/secondclass", data);
	}
	
	
/*	@RequestMapping(value = "/categoryIndex", method = RequestMethod.GET)
	public ModelAndView categoryIndex(
			@RequestParam(value = "openId") String openId,
			@RequestParam(value = "mid") String mid,
			@RequestParam(value = "categoryId", required = false) String categoryId,
			@RequestParam(value = "hotJudge", required = false) String hotJudge,
			@RequestParam(value = "keyWord", required = false) String keyWord)
			throws UnsupportedEncodingException {
		Map<String, Object> data = new HashMap<String, Object>();
		Map<String, Object> getpricehot = new HashMap<String, Object>();
		Map<String, Object> getmberpricehot = new HashMap<String, Object>();
		Map<String, Object> getpriceMhot = new HashMap<String, Object>();
		Map<String, Object> getmberpriceMhot = new HashMap<String, Object>();

		logger.info("开始进入分类页categoryId:" + categoryId);

		MrtMerchantCard merchantCard = mrtMerchantCardService
				.getMrtMerchantCard(mid, openId);
		SysMerchantDef merchant = sysMerchantDefService
				.getSysMerchantDefByMid(mid);
		List<ErpProductCategory> productCategory = erpProductCategoryService
				.getErpProductCategory(mid);
		List<ErpProduct> producthot = erpProductService
				.getHotErpProductListByCategoryId(mid, hotJudge, categoryId);

		for (ErpProduct erpProduct : producthot) {
			getpricehot.put(erpProduct.getId(),
					Utils.formatAmount(erpProduct.getPrice()));
			getmberpricehot.put(erpProduct.getId(),
					Utils.formatAmount(erpProduct.getMemberPrice()));
			// 商品图片---多张
			if (null != erpProduct.getPicId()
					&& !erpProduct.getPicId().equals("")) {
				String picIds[] = erpProduct.getPicId().split(",");
				if (picIds.length > 0) {
					// 注意id为空的情况
					if (erpProductPicDefService
							.getErpProductPicDefById(picIds[0]) != null)
						erpProduct.setPicUrl(Function.dealGridPicUrl(
								erpProductPicDefService
										.getErpProductPicDefById(picIds[0])
										.getAddress(), 150, 150));
				}
			} else {
				erpProduct.setPicUrl(SysConstants.DEFAULT_PICTURE);
			}

			formatProdName(erpProduct);
		}
		data.put("merchantCard", merchantCard);
		data.put("merchant", merchant);
		data.put("getpricehot", getpricehot);
		data.put("getmberpricehot", getmberpricehot);
		data.put("getpriceMhot", getpriceMhot);
		data.put("getmberpriceMhot", getmberpriceMhot);
		data.put("producthot", producthot);
		data.put("productCategory", productCategory);
		data.put("openId", openId);
		data.put("categoryId", categoryId);
		data.put("mid", mid);
		return new ModelAndView("productcategory/prolist", data);
	}*/

	
	@RequestMapping(value = "/categoryIndex", method = RequestMethod.GET)
	public ModelAndView categoryIndex(
			@RequestParam(value = "openId") String openId,
			@RequestParam(value = "mid") String mid,
			@RequestParam(value = "categoryId", required = false) String categoryId,
			@RequestParam(value = "hotJudge", required = false) String hotJudge,
			@RequestParam(value = "keyWord", required = false) String keyWord)
			throws UnsupportedEncodingException {
		MerchandiseSyncInput input = new MerchandiseSyncInput();
		input.setMid(mid);
		input.setCategoryId(categoryId);
		input.setHot(hotJudge);
		MerchandiseSyncOutput output = (MerchandiseSyncOutput) wizarposOpenRestClient.post(input,"/product/merchandise",MerchandiseSyncOutput.class);
		Map<String, Object> data = new HashMap<String, Object>();
		Map<String, Object> getpricehot = new HashMap<String, Object>();
		Map<String, Object> getmberpricehot = new HashMap<String, Object>();
		List<Merchandise> list = output.getResult();
		List<ErpProduct> producthot = new ArrayList<ErpProduct>();

		for (Merchandise merchandise : list) {
			getpricehot.put(merchandise.getProductId(), merchandise.getPrice());
			getmberpricehot.put(merchandise.getProductId(), merchandise.getMemberPrice());
			
			ErpProduct erpProduct = new ErpProduct();
			erpProduct.setShowMemPrice(merchandise.getMemberPrice());
			erpProduct.setId(merchandise.getProductId());
			erpProduct.setName(merchandise.getName());
			
			// 商品图片---多张			
			if ("".equals(merchandise.getPicUrl())) {
				erpProduct.setPicUrl(SysConstants.DEFAULT_PICTURE);
			} else {
				erpProduct.setPicUrl(Function.dealGridPicUrl(merchandise.getPicUrl(), 150, 150));
			}

			formatProdName(erpProduct);
			producthot.add(erpProduct);
		}

		data.put("getpricehot", getpricehot);
		data.put("getmberpricehot", getmberpricehot);
		data.put("producthot", producthot);
		data.put("openId", openId);
		data.put("categoryId", categoryId);
		data.put("mid", mid);
		return new ModelAndView("productcategory/prolist", data);
	}
	/**
	 * 商品名称过长，做部分截取
	 * 
	 * @param erpProduct
	 */
	private void formatProdName(ErpProduct erpProduct) {
		String name = erpProduct.getName();
		if (name != null && name.length() > 23) {
			erpProduct.setName(name.substring(0, 20) + "...");
		}
	}
}
