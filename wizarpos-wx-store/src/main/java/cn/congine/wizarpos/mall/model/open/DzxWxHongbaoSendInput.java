package cn.congine.wizarpos.mall.model.open;

import java.io.Serializable;

public class DzxWxHongbaoSendInput extends CommonInput {

	private static final long serialVersionUID = 1L;

	String mid = null;
	String amount = null;
	String actName = null;
	String wishing = null;
	String openid = null;
	
	public String getMid() {
		return mid;
	}
	public void setMid(String mid) {
		this.mid = mid;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getActName() {
		return actName;
	}
	public void setActName(String actName) {
		this.actName = actName;
	}
	public String getWishing() {
		return wishing;
	}
	public void setWishing(String wishing) {
		this.wishing = wishing;
	}
	public String getOpenid() {
		return openid;
	}
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	
	
}
