package cn.congine.wizarpos.mall.service;

import java.util.List;

import cn.congine.wizarpos.mall.model.WxOrderDetail;

public interface WxOrderDetailService {

	List<WxOrderDetail> getWxOrderDetailListByOrderId(String orderId);

	void save(WxOrderDetail obj);

	WxOrderDetail getWxOrderDetailByProductId(String productId);

	List<WxOrderDetail> getWxOrderDetailList(String orderId);

	void removeByOrderId(String mid, String OrderId);

}
