package cn.congine.wizarpos.mall.service;

import java.util.Date;
import java.util.List;

import cn.congine.wizarpos.mall.model.ErpCarterOrder;

public interface ErpCarterOrderService {

	/**
	 * 根据日期，选择已经被预定的table_id
	 * 
	 * @param mid
	 * @param date
	 * @return
	 */
	List<String> getYudingTableIdByDate(String mid, Date date);

	/**
	 * 根据日期和时间段，选择已经被预定的table_id
	 * 
	 * @param mid
	 * @param date
	 * @param stageId
	 * @return
	 */
	List<String> getYudingTableIdByDateStage(String mid, Date date,
			String stageId);

	/**
	 * save
	 * 
	 * @param order
	 * @return
	 */
	ErpCarterOrder save(ErpCarterOrder order);

	/**
	 * 餐桌是否被预订,返回订单号
	 * 
	 * @param mid
	 * @param date
	 * @param stageId
	 * @param tableId
	 * @return
	 */
	Object getReOrderId(String mid, Date date, String stageId, String tableId);

	ErpCarterOrder getByIdMid(String orderId, String mid);

	List<ErpCarterOrder> getByOpenidMid(String openId, String mid);

	void update(ErpCarterOrder order);

}
