package cn.congine.wizarpos.mall.dao;

import cn.congine.wizarpos.mall.model.ErpMerchantLogin;

public interface ErpMerchantLoginDao extends GenericDao<ErpMerchantLogin> {

	public ErpMerchantLogin save(ErpMerchantLogin obj);
	
	public Object count(String mid);
	
}
