package cn.congine.wizarpos.mall.model.open;

import cn.congine.wizarpos.mall.model.WxOrder;

/**
 * 
 * @author xudongdong
 * Date: 2015.7.15
 */


public class WxPayUpdateOutput extends CommonOutput {

	private static final long serialVersionUID = 1L;
	
	private WxOrder wxOrder = null;
	
	public WxOrder getWxOrder() {
		return wxOrder;
	}

	public void setWxOrder(WxOrder wxOrder) {
		this.wxOrder = wxOrder;
	}
}


