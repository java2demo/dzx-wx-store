package cn.congine.wizarpos.mall.model.open;

public class AddressEditOutput extends CommonOutput {

	private static final long serialVersionUID = 1L;
	
	private Result result = null;
	
	public Result getResult() {
		return result;
	}

	public void setResult(Result result) {
		this.result = result;
	}

	public class Result {
		// �ջ��ַ���
		private String addressId = null;

		public String getAddressId() {
			return addressId;
		}

		public void setAddressId(String addressId) {
			this.addressId = addressId;
		}
	}
}
