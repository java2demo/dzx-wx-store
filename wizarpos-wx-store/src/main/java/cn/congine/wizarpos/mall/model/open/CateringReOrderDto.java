package cn.congine.wizarpos.mall.model.open;

/**
 * 
 * @author xudongdong
 * Date: 2015.7.7
 */

import java.io.Serializable;

public class CateringReOrderDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private String orderId = null;
	private String showOrderId = null;
	private String amount = null;
	private String tableNo = null;
	private String table = null;	
	private String createTime = null;
	private String StageRange = null;
	//-1 拒绝 0 待接受 1 已接受 2 取消
	private String status = null;
	private String payStatus;//(0未支付 1 已支付)
	private String orderDate;
	
	private String type = null;
	private Object auditFlag = null;
	
	public String getShowOrderId() {
		return showOrderId;
	}

	public void setShowOrderId(String showOrderId) {
		this.showOrderId = showOrderId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getTableNo() {
		return tableNo;
	}

	public void setTableNo(String tableNo) {
		this.tableNo = tableNo;
	}

	public String getTable() {
		return table;
	}

	public void setTable(String table) {
		this.table = table;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getStageRange() {
		return StageRange;
	}

	public void setStageRange(String stageRange) {
		StageRange = stageRange;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(String payStatus) {
		this.payStatus = payStatus;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Object getAuditFlag() {
		return auditFlag;
	}

	public void setAuditFlag(Object auditFlag) {
		this.auditFlag = auditFlag;
	}

	
}
