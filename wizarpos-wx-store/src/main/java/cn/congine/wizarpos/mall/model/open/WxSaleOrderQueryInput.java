package cn.congine.wizarpos.mall.model.open;

/**
 * 
 * @author xudongdong
 * Date: 2015.7.14
 */


public class WxSaleOrderQueryInput extends CommonInput {

	private static final long serialVersionUID = 1L;
	
	private String orderId = null;
	
	private String orderNo = null;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
		
}
