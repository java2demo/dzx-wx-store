package cn.congine.wizarpos.mall.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "wx_merchant_admin_bind")
@NamedQuery(name = "MerchantAdminBind.findAll", query = "SELECT m FROM MerchantAdminBind m")
public class MerchantAdminBind extends BaseEntity implements
		java.io.Serializable {

	private static final long serialVersionUID = 1L;
	/**
	 * 主键
	 */
	private String id;
	/**
	 * 慧商户号
	 */
	private String mid;

	/**
	 * 微信OpenId(商家个人微信)
	 */
	private String openId;
	/**
	 * 创建时间
	 */
	private Long createTime;
	/**
	 * 状态 (0禁用 1启用)
	 */
	private String state;

	/**
	 * 用户名
	 */
	private String userName;
	/**
	 * 昵称
	 */
	private String nickName;

	/**
	 * 收款通知开关
	 */
	private Boolean collectNotifyMark;
	/**
	 * 销售通知开关
	 */
	private Boolean saleNotifyMark;

	public MerchantAdminBind() {
	}

	@Id
	@GeneratedValue(generator = "uuid")
	// 指定生成器名称
	@GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "id", length = 80)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	@Column(name = "open_id")
	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	@Column(name = "create_time")
	public Long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Column(name = "username")
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "nick_name")
	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	@Column(name = "collect_notify_mark")
	public Boolean getCollectNotifyMark() {
		return collectNotifyMark;
	}

	public void setCollectNotifyMark(Boolean collectNotifyMark) {
		this.collectNotifyMark = collectNotifyMark;
	}

	@Column(name = "sale_notify_mark")
	public Boolean getSaleNotifyMark() {
		return saleNotifyMark;
	}

	public void setSaleNotifyMark(Boolean saleNotifyMark) {
		this.saleNotifyMark = saleNotifyMark;
	}

}
