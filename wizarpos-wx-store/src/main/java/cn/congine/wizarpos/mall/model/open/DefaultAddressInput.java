package cn.congine.wizarpos.mall.model.open;

public class DefaultAddressInput extends CommonInput {

	private static final long serialVersionUID = 1L;
	String openId = null;
	String mid = null;

	public String getOpenId() {
		return openId;
	}
	public void setOpenId(String openId) {
		this.openId = openId;
	}
	public String getMid() {
		return mid;
	}
	public void setMid(String mid) {
		this.mid = mid;
	}
	
}