package cn.congine.wizarpos.mall.dao;

import cn.congine.wizarpos.mall.entity.MyOnlineActivityHistory;

public interface MyOnlineActivityHistoryDao extends GenericDao<MyOnlineActivityHistory> {

   MyOnlineActivityHistory getMyOnlineActivityHistoryByOpenId(String openId,String onlineActivityId);
	
}
