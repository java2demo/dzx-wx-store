package cn.congine.wizarpos.mall.dao;

import java.util.List;

import cn.congine.wizarpos.mall.model.ErpTableOrderStage;

public interface ErpTableOrderStageDao extends GenericDao<ErpTableOrderStage> {
	List<ErpTableOrderStage> getAll(String mid);

	Object getCurrentStage(String mid);

	ErpTableOrderStage getByIdMid(String stageId, String mid);
}
