package cn.congine.wizarpos.mall.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import cn.congine.wizarpos.mall.model.BaseEntity;

@Entity
@Table(name = "mrt_merchant_def")
public class MrtMerchantDef extends BaseEntity {
	private static final long serialVersionUID = 7471548394644675615L;

	// 慧商户号
	// 唯一标识
	@Id
	@Column(name = "mid")
	private String mid;

	// 商户名称
	@Column(name = "merchant_name")
	private String merchantName;
	
	// 商户是否启用
	@Column(name = "valid_flag")
	private Integer validFlag;
	
	// 商户LOGO
	@Column(name = "logo_image")
	private String logoImage;

	// 门店相关说明
	@Column(name = "shop_desc")
	private String shopDesc;

	// vip图片
	@Column(name = "vip_image")
	private String vipImage;

	// vip正面图片
	@Column(name = "vip_front_image")
	private String vipFrontImage;

	// 商户公众号openid
	@Column(name = "p_open_id")
	private String pOpenId;

	// 商户地址
	@Column(name = "merchant_addr")
	private String merchantAddr;

	// 商户联系方式(多个联系方式以逗号分隔)
	@Column(name = "merchant_tel")
	private String merchantTel;

	// 商户简介
	@Column(name = "merchant_summary")
	private String merchantSummary;

	// 商户背景图
	@Column(name = "banner_image")
	private String bannerImage;

	public Integer getValidFlag() {
		return validFlag;
	}

	public void setValidFlag(Integer validFlag) {
		this.validFlag = validFlag;
	}

	public String getBannerImage() {
		return bannerImage;
	}

	public void setBannerImage(String bannerImage) {
		this.bannerImage = bannerImage;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getLogoImage() {
		return logoImage;
	}

	public void setLogoImage(String logoImage) {
		this.logoImage = logoImage;
	}

	public String getShopDesc() {
		return shopDesc;
	}

	public void setShopDesc(String shopDesc) {
		this.shopDesc = shopDesc;
	}

	public String getVipImage() {
		return vipImage;
	}

	public void setVipImage(String vipImage) {
		this.vipImage = vipImage;
	}

	public String getVipFrontImage() {
		return vipFrontImage;
	}

	public void setVipFrontImage(String vipFrontImage) {
		this.vipFrontImage = vipFrontImage;
	}

	public String getpOpenId() {
		return pOpenId;
	}

	public void setpOpenId(String pOpenId) {
		this.pOpenId = pOpenId;
	}

	public String getMerchantAddr() {
		return merchantAddr;
	}

	public void setMerchantAddr(String merchantAddr) {
		this.merchantAddr = merchantAddr;
	}

	public String getMerchantTel() {
		return merchantTel;
	}

	public void setMerchantTel(String merchantTel) {
		this.merchantTel = merchantTel;
	}

	public String getMerchantSummary() {
		return merchantSummary;
	}

	public void setMerchantSummary(String merchantSummary) {
		this.merchantSummary = merchantSummary;
	}
}
