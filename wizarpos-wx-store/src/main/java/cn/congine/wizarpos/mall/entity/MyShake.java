package cn.congine.wizarpos.mall.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import cn.congine.wizarpos.mall.model.BaseEntity;

/**
 * 摇一摇券
 * 
 * @author Kirk Zhou
 * @date 2015-05-12
 * 
 */
@Entity
@Table(name = "my_shake")
public class MyShake extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id; // 主键ID

	private String ticketName; // 券名称
 
	private String ticketId; // 券定义ID

	private String mid; // 商户号

	private Long ticketNum; // 券数量

	private String ticketQz; // 权重

	private String ticketContent; // 券明细

	private Date createTime; // 创建时间

	private String ticketType; // 券类型  9红包  10空券

	private Long amount; // 金额

	private String validTime; // 有效时间

	private String onlineActivityId; // 活动ID
	
	private String ticketMid; //券发行商户号

	@Id
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@GeneratedValue(generator = "system-uuid")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "ticket_id")
	public String getTicketId() {
		return ticketId;
	}

	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

	@Column(name = "mid")
	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	@Column(name = "ticket_num")
	public Long getTicketNum() {
		return ticketNum;
	}

	public void setTicketNum(Long ticketNum) {
		this.ticketNum = ticketNum;
	}

	@Column(name = "ticket_qz")
	public String getTicketQz() {
		return ticketQz;
	}

	public void setTicketQz(String ticketQz) {
		this.ticketQz = ticketQz;
	}

	@Column(name = "ticket_content")
	public String getTicketContent() {
		return ticketContent;
	}

	public void setTicketContent(String ticketContent) {
		this.ticketContent = ticketContent;
	}

	@Column(name = "create_time")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Column(name = "ticket_name")
	public String getTicketName() {
		return ticketName;
	}

	public void setTicketName(String ticketName) {
		this.ticketName = ticketName;
	}

	@Column(name = "ticket_type")
	public String getTicketType() {
		return ticketType;
	}

	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}

	@Column(name = "amount")
	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	@Column(name = "valid_time")
	public String getValidTime() {
		return validTime;
	}

	public void setValidTime(String validTime) {
		this.validTime = validTime;
	}

	@Column(name = "online_activity_id")
	public String getOnlineActivityId() {
		return onlineActivityId;
	}

	public void setOnlineActivityId(String onlineActivityId) {
		this.onlineActivityId = onlineActivityId;
	}
	
	@Column(name = "ticket_mid")
	public String getTicketMid() {
		return ticketMid;
	}

	public void setTicketMid(String ticketMid) {
		this.ticketMid = ticketMid;
	}
}
