package cn.congine.wizarpos.mall.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import cn.congine.wizarpos.mall.model.BaseEntity;

/**
 * 领取流水记录
 * 
 * @author Kirk Zhou
 * @date 2015-05-12
 * 
 */
@Entity
@Table(name = "my_online_activity_history")
public class MyOnlineActivityHistory extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id; // 主键ID

	private String mid; // 商户号
	
	private String openId; //东志信公众号用户openid
	
	private Long haveReceivedNum;//已领取次数
	
	private String mobileNo; //手机号码
	
	private String gzhOpenid; //商户公众号用户openid
	
	private String onlineActivityId; // 活动ID

	@Id
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@GeneratedValue(generator = "system-uuid")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	@Column(name = "mid")
	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	@Column(name = "online_activity_id")
	public String getOnlineActivityId() {
		return onlineActivityId;
	}

	public void setOnlineActivityId(String onlineActivityId) {
		this.onlineActivityId = onlineActivityId;
	}
	
	@Column(name = "open_id")
	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}
	
	@Column(name = "have_received_num")
	public Long getHaveReceivedNum() {
		return haveReceivedNum;
	}

	public void setHaveReceivedNum(Long haveReceivedNum) {
		this.haveReceivedNum = haveReceivedNum;
	}
	
	@Column(name = "mobile_no")
	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	@Column(name = "gzh_Openid")
	public String getGzhOpenid() {
		return gzhOpenid;
	}

	public void setGzhOpenid(String gzhOpenid) {
		this.gzhOpenid = gzhOpenid;
	}

}
