package cn.congine.wizarpos.mall.model.dzx;

import cn.congine.wizarpos.mall.model.open.CommonInput;

public class DxChartMemberValidateInput extends CommonInput {

	private static final long serialVersionUID = 1L;

	private String loginName = null;
	private String captcha = null;

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getCaptcha() {
		return captcha;
	}

	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}
 
}
