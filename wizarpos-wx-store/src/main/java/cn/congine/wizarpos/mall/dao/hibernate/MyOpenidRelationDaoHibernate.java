package cn.congine.wizarpos.mall.dao.hibernate;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import cn.congine.wizarpos.mall.dao.MyOpenidRelationDao;
import cn.congine.wizarpos.mall.entity.MyOpenidRelation;

@Repository("MyOpenidRelationDao")
public class MyOpenidRelationDaoHibernate 
	extends GenericDaoHibernate<MyOpenidRelation> 
	implements MyOpenidRelationDao {

	public MyOpenidRelationDaoHibernate() {
		super(MyOpenidRelation.class);
	}

	@Override
	public MyOpenidRelation getByMidAndDzxOpenid(String mid, String dzxOpenid) {
		Query query = getSession()
				.createQuery(
						"from MyOpenidRelation where mid=:mid and dzxOpenid=:dzxOpenid")
				.setParameter("mid", mid).setParameter("dzxOpenid", dzxOpenid);
		return (MyOpenidRelation) query.uniqueResult();
	}

}
