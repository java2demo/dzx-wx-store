package cn.congine.wizarpos.mall.dao.hibernate;

import org.springframework.stereotype.Repository;

import cn.congine.wizarpos.mall.dao.SysParamsDefDao;
import cn.congine.wizarpos.mall.model.SysParamsDef;

@Repository("sysParamsDefDao")
public class SysParamsDefDaoHibernate extends GenericDaoHibernate<SysParamsDef>
		implements SysParamsDefDao {

	public SysParamsDefDaoHibernate() {
		super(SysParamsDef.class);
	}

}
