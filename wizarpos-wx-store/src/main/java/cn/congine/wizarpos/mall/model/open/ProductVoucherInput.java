package cn.congine.wizarpos.mall.model.open;

import java.io.Serializable;

public class ProductVoucherInput extends CommonInput {

	private static final long serialVersionUID = 1L;

	private String mid = null;
	private String OpenId = null;
	private String orderId = null;
	private String pageNo = null;
	public String getMid() {
		return mid;
	}
	public void setMid(String mid) {
		this.mid = mid;
	}
	public String getOpenId() {
		return OpenId;
	}
	public void setOpenId(String openId) {
		OpenId = openId;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getPageNo() {
		return pageNo;
	}
	public void setPageNo(String pageNo) {
		this.pageNo = pageNo;
	}
	
}
