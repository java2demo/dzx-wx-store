package cn.congine.wizarpos.mall.vo;



public class ProductVoucher {
	// 订单编号
	private String orderId;

	// 微信OpenID
	private String openId;

	// 慧商户号
	private String mid;

	// 慧商户号
	private String merchantName;
	// 慧商户号
	private String merchantLogo;  
	
	private String merchantTel;
	
	private String merchantAddr;
	
	private String shopDesc;

	// 订单状态 0 取消  1 待收货 2 已收货 3 完成
	private String status;

	// 配送方式 1 货到付款 2 微信支付送货上门 3 微信支付到店提货 4 会员卡支付送货上门 5 会员卡支付到店提货
	private String dispatchType;

	// 付款金额
	private Integer amount;

	// 支付方式 1 是在线支付 2是货到付款
	private String payStatus;

	// 收货地址关联编号
	private String addressId;

	// 提货券二维码
	private String pickUpQr;

	// 创建时间
	private String createTime;

	// 有效期
	private String lastTime;

	// 提货券状态
	private boolean state;
	
	public String getSaleOrderId() {
		return saleOrderId;
	}

	public void setSaleOrderId(String saleOrderId) {
		this.saleOrderId = saleOrderId;
	}

	private String saleOrderId;
	
	public String getMerchantLogo() {
		return merchantLogo;
	}
	

	public String getMerchantTel() {
		return merchantTel;
	}

	public void setMerchantTel(String merchantTel) {
		this.merchantTel = merchantTel;
	}

	public String getMerchantAddr() {
		return merchantAddr;
	}

	public void setMerchantAddr(String merchantAddr) {
		this.merchantAddr = merchantAddr;
	}

	public boolean isState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getShopDesc() {
		return shopDesc;
	}

	public void setShopDesc(String shopDesc) {
		this.shopDesc = shopDesc;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDispatchType() {
		return dispatchType;
	}

	public void setDispatchType(String dispatchType) {
		this.dispatchType = dispatchType;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public String getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(String payStatus) {
		this.payStatus = payStatus;
	}

	public String getAddressId() {
		return addressId;
	}

	public void setAddressId(String addressId) {
		this.addressId = addressId;
	}

	public String getPickUpQr() {
		return pickUpQr;
	}

	public void setPickUpQr(String pickUpQr) {
		this.pickUpQr = pickUpQr;
	}

	
	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getLastTime() {
		return lastTime;
	}

	public void setLastTime(String lastTime) {
		this.lastTime = lastTime;
	}

	public void setMerchantLogo(String merchantLogo) {
		this.merchantLogo = merchantLogo;
	}

}
