package cn.congine.wizarpos.mall.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.congine.wizarpos.mall.common.Page;
import cn.congine.wizarpos.mall.dao.SysMerchantDefDao;
import cn.congine.wizarpos.mall.dao.WxOrderDao;
import cn.congine.wizarpos.mall.model.SysMerchantDef;
import cn.congine.wizarpos.mall.model.WxOrder;
import cn.congine.wizarpos.mall.service.WxOrderService;
import cn.congine.wizarpos.mall.utils.Utils;
import cn.congine.wizarpos.mall.vo.ProductVoucher;

@Service("wxOrderService")
public class WxOrderServiceImpl implements WxOrderService {

	@Autowired
	private WxOrderDao wxOrderDao;

	@Autowired
	private SysMerchantDefDao sysMerchantDefDao;

	@Override
	public List<WxOrder> getWxOrderListByMid(String mid) {
		return wxOrderDao.getWxOrderListByMid(mid);
	}

	@Override
	public List<WxOrder> getWxOrderListByOpenId(String openId) {
		return wxOrderDao.getWxOrderListByOpenId(openId);
	}

	@Override
	public WxOrder getWxOrderByOrderId(String orderId) {
		return wxOrderDao.getWxOrderByOrderId(orderId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void save(WxOrder obj) {
		wxOrderDao.save(obj);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void update(WxOrder obj) {
		wxOrderDao.update(obj);
	}

	@Override
	public List<WxOrder> getWeekWxOrderListByOpenId(String openId, String mid,
			Integer type) {
		return wxOrderDao.getWeekWxOrderListByOpenId(openId, mid, type);
	}

	@Override
	public List<ProductVoucher> getVoWxOrderListByOpenId(String openId,
			String mid, Integer nextPage) {
		List<ProductVoucher> volist = new ArrayList<ProductVoucher>();
		List<WxOrder> orderlist = wxOrderDao.getVoByPage(openId, mid, nextPage);
		for (WxOrder wxOrder : orderlist) {
			SysMerchantDef m = sysMerchantDefDao.getSysMerchantDefByMid(wxOrder
					.getMid());
			ProductVoucher vo = new ProductVoucher();
			vo.setMid(wxOrder.getMid());
			vo.setOrderId(wxOrder.getOrderId());
			vo.setMerchantName(m.getMerchantName());
			vo.setMerchantLogo(m.getLogoImage());
			vo.setShopDesc(m.getShopDesc());
			vo.setPickUpQr(wxOrder.getPickUpQr());
			vo.setLastTime(Utils.long2DateTimeString(wxOrder.getLastTime()));
			vo.setCreateTime(Utils.long2DateTimeString(wxOrder.getCreateTime()));
			if (wxOrder.getSaleOrderId() != null) {// 有销售单号的微信订单为已提货的，表示此提货券已用，不可再用
				vo.setState(false);
			} else {// 没有销售单号的微信订单表示未提货，表示次提货券还未使用，仍然可用
				vo.setState(true);
			}

			volist.add(vo);
		}

		return volist;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void remove(String id) {
		wxOrderDao.remove(id);
	}

	@Override
	public List<WxOrder> getWeekWxOrderListByOpenId(String openId, String mid,
			int type, Page<WxOrder> page) {
		return wxOrderDao.getWeekWxOrderListByOpenId(openId, mid, type, page);
	}

	@Override
	public List<WxOrder> getOrderListByType(String openId, String mid,
			int type, Page<WxOrder> page) {
		return wxOrderDao.getOrderListByType(openId, mid, type, page);
	}

}
