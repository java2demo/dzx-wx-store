package cn.congine.wizarpos.mall.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "wx_order")
public class WxOrder extends BaseEntity {

	private static final long serialVersionUID = 3236675378755799096L;

	// 订单编号
	@Id
	@Column(name = "order_id")
	private String orderId;

	// 微信OpenID
	@Column(name = "open_id")
	private String openId;

	// 慧商户号
	@Column(name = "mid")
	private String mid;

	// 订单状态 0 待付款  1待发货  2 待收货 3 已收货 （废弃）4 交易成功 5交易取消
	@Column(name = "status")
	private String status;

	// 配送方式1 货到付款 2 微信支付送货上门 3 微信支付到店提货 4 会员卡支付送货上门 5 会员卡支付到店提货
	@Column(name = "dispatch_type")
	private String dispatchType;

	// 付款金额
	@Column(name = "amount")
	private Integer amount;

	// 支付方式 0未支付 1已支付
	@Column(name = "pay_status")
	private String payStatus;

	// 收货地址关联编号
	@Column(name = "address_id")
	private String addressId;

	// 提货券二维码
	@Column(name = "pick_up_qr")
	private String pickUpQr;

	// 创建时间
	@Column(name = "create_time")
	private Long createTime;

	// 最后修改时间
	@Column(name = "last_time")
	private Long lastTime;

	// 销售单Id
	@Column(name = "sale_order_id")
	private String saleOrderId;
	
	// 销售单号
	@Column(name = "sale_order_no")
	private String saleOrderNo;
	
	// 发货时间
	@Column(name = "dispatch_time")
	private Long dispatchTime;

	public Long getDispatchTime() {
		return dispatchTime;
	}

	public void setDispatchTime(Long dispatchTime) {
		this.dispatchTime = dispatchTime;
	}

	public String getSaleOrderId() {
		return saleOrderId;
	}

	public void setSaleOrderId(String saleOrderId) {
		this.saleOrderId = saleOrderId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDispatchType() {
		return dispatchType;
	}

	public void setDispatchType(String dispatchType) {
		this.dispatchType = dispatchType;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public String getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(String payStatus) {
		this.payStatus = payStatus;
	}

	public String getAddressId() {
		return addressId;
	}

	public void setAddressId(String addressId) {
		this.addressId = addressId;
	}

	public String getPickUpQr() {
		return pickUpQr;
	}

	public void setPickUpQr(String pickUpQr) {
		this.pickUpQr = pickUpQr;
	}

	public Long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

	public Long getLastTime() {
		return lastTime;
	}

	public void setLastTime(Long lastTime) {
		this.lastTime = lastTime;
	}

	public String getSaleOrderNo() {
		return saleOrderNo;
	}

	public void setSaleOrderNo(String saleOrderNo) {
		this.saleOrderNo = saleOrderNo;
	}
}
