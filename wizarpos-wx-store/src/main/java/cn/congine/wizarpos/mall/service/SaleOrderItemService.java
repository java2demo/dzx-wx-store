package cn.congine.wizarpos.mall.service;

import java.util.List;

import cn.congine.wizarpos.mall.model.SaleOrderItem;

public interface SaleOrderItemService {

	List<SaleOrderItem> getSaleOrderItemByOrderId(String orderId);

}
