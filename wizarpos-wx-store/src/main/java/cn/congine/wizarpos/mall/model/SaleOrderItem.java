package cn.congine.wizarpos.mall.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * 销售单明细表
 */

/***
 * The persistent class for the erp_sale_order_items database table.
 * 
 */
@Entity
@Table(name = "erp_sale_order_items")
public class SaleOrderItem extends BaseEntity {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
	// 指定生成器名称
	@Column(name = "id", length = 80)
	private String id;
	/**
	 * 金额
	 */
	@Column(name = "amount")
	private Integer amount;

	/**
	 * 最后修改时间
	 */
	@Column(name = "last_time")
	private Long lastTime;
	/**
	 * 销售单号
	 */
	@Column(name = "order_id")
	private String orderId;
	/**
	 * 价格
	 */
	@Column(name = "price")
	private Integer price;
	/**
	 * 商品编号
	 */
	@Column(name = "product_id")
	private String productId;
	/**
	 * 数量
	 */
	@Column(name = "qty")
	private Long qty;

	/**
	 * 实际价格
	 */
	@Column(name = "real_price")
	private Integer realPrice;
	/**
	 * 备注
	 */
	@Column(name = "remark")
	private String remark;

	public SaleOrderItem() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getAmount() {
		return this.amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public Long getLastTime() {
		return lastTime;
	}

	public void setLastTime(Long lastTime) {
		this.lastTime = lastTime;
	}

	public Integer getPrice() {
		return this.price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Long getQty() {
		return qty;
	}

	public void setQty(Long qty) {
		this.qty = qty;
	}

	public Integer getRealPrice() {
		return this.realPrice;
	}

	public void setRealPrice(Integer realPrice) {
		this.realPrice = realPrice;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

}