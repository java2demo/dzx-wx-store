package cn.congine.wizarpos.mall.utils;

import cn.congine.wizarpos.mall.exception.MangoException;
import cn.congine.wizarpos.mall.model.WxOrder;

import com.alibaba.fastjson.JSONObject;

public class WxTemplateMessageUtil {

	/**
	 * 红包分享
	 * 
	 * @return
	 */
	public static String hongbaolinqu(String openId, String templateId,
			String content, String name, String amount, String remark,
			String appId, String ticketId, String dns) {
		JSONObject json = new JSONObject();
		JSONObject data = new JSONObject();
		json.put("touser", openId);
		json.put("template_id", templateId);
		json.put(
				"url",
				"https://open.weixin.qq.com/connect/oauth2/authorize?appid="
						+ appId
						+ "&redirect_uri=http%3a%2f%2f"
						+ dns
						+ "%2fwizarpos-mall-sole%2foauth%2fhongbao_enter&response_type=code&scope=snsapi_base&state="
						+ ticketId + "#wechat_redirect");
		json.put("topcolor", "#FF0000");
		JSONObject item1 = new JSONObject();
		item1.put("value", content);
		item1.put("color", "#173177");
		data.put("first", item1);
		JSONObject item2 = new JSONObject();
		item2.put("value", name);
		item2.put("color", "#173177");
		data.put("keyword1", item2);
		JSONObject item3 = new JSONObject();
		item3.put("value", amount);
		item3.put("color", "#173177");
		data.put("keyword2", item3);
		JSONObject item4 = new JSONObject();
		item4.put("value", remark);
		item4.put("color", "#173177");
		data.put("remark", item4);
		json.put("data", data);
		return json.toJSONString();
	}

	/**
	 * 实时交易提醒 TM00244
	 * 
	 * @param tranTime
	 *            交易时间
	 * @param tranType
	 *            交易类型
	 * @param content
	 *            交易内容
	 * @param amount
	 *            金额
	 * @param remark
	 *            备注
	 * @param openId
	 * @param templateId
	 * @return
	 */
	public static String consume(String tranTime, String tranType,
			String content, int amount, String remark, String openId, String url,
			String templateId) {
		JSONObject json = new JSONObject();
		JSONObject data = new JSONObject();
		json.put("touser", openId);
		json.put("template_id", templateId);
		json.put("url", url);
		json.put("topcolor", "#FF0000");
		JSONObject item1 = new JSONObject();
		item1.put("value", content);
		item1.put("color", "#173177");
		data.put("first", item1);
		JSONObject item2 = new JSONObject();
		item2.put("value", tranTime);
		item2.put("color", "#173177");
		data.put("tradeDateTime", item2);
		JSONObject item3 = new JSONObject();
		item3.put("value", Utils.formatAmount(amount) + "元");
		item3.put("color", "#173177");
		data.put("curAmount", item3);
		JSONObject item4 = new JSONObject();
		item4.put("value", remark);
		item4.put("color", "#173177");
		data.put("remark", item4);
		JSONObject item5 = new JSONObject();
		item5.put("value", tranType);
		item5.put("color", "#173177");
		data.put("tradeType", item5);
		json.put("data", data);
		return json.toJSONString();
	}
	
/*	public static String orderSubmitSuccess(WxOrder wxOrder, String openId,
			String path, String templateId) throws MangoException {
		String url = path
				+ "/wizarpos-mall-sole/wx_order_detail/get_order_detail?orderId="
				+ wxOrder.getOrderId();
		JSONObject json = new JSONObject();
		JSONObject data = new JSONObject();
		json.put("touser", openId);
		json.put("template_id", templateId);
		json.put("url", url);
		json.put("topcolor", "#FF0000");

		JSONObject item01 = new JSONObject();
		item01.put("value", "您的订单已提交成功!");
		item01.put("color", "#173177");
		data.put("first", item01);

		JSONObject item02 = new JSONObject();
		item02.put("value", wxOrder.getOrderId());
		item02.put("color", "#173177");
		data.put("orderID", item02);

		JSONObject item03 = new JSONObject();
		item03.put("value", Utils.formatAmount(wxOrder.getAmount()) + "元");
		item03.put("color", "#173177");
		data.put("orderMoneySum", item03);

		JSONObject item04 = new JSONObject();
		item04.put("value", "下单时间：");
		item04.put("color", "#173177");
		data.put("backupFieldName", item04);

		JSONObject item05 = new JSONObject();
		item05.put("value", Utils.long2DateTimeString(wxOrder.getCreateTime()));
		item05.put("color", "#173177");
		data.put("backupFieldData", item05);

		JSONObject item06 = new JSONObject();
		item06.put("value", "欢迎再次购买！");
		item06.put("color", "#173177");
		json.put("remark", item06);
		json.put("data", data);
		return json.toJSONString();
	}*/

	public static String orderSubmitSuccess(WxOrder wxOrder, String openId,
			String path, String templateId) throws MangoException {
		String url = path
				+ "/wizarpos-mall-sole/wx_order_detail/get_order_detail?mid=" + wxOrder.getMid() + "&orderId="
				+ wxOrder.getOrderId();
		JSONObject json = new JSONObject();
		JSONObject data = new JSONObject();
		json.put("touser", openId);
		json.put("template_id", templateId);
		json.put("url", url);
		json.put("topcolor", "#FF0000");

		JSONObject item01 = new JSONObject();
		item01.put("value", "您的订单已提交成功!");
		item01.put("color", "#173177");
		data.put("first", item01);

		JSONObject item02 = new JSONObject();
		item02.put("value", wxOrder.getOrderId());
		item02.put("color", "#173177");
		data.put("orderID", item02);

		JSONObject item03 = new JSONObject();
		item03.put("value", Utils.formatAmount(wxOrder.getAmount()) + "元");
		item03.put("color", "#173177");
		data.put("orderMoneySum", item03);

		JSONObject item04 = new JSONObject();
		item04.put("value", "下单时间：");
		item04.put("color", "#173177");
		data.put("backupFieldName", item04);

		JSONObject item05 = new JSONObject();
		item05.put("value", Utils.long2DateTimeString(wxOrder.getCreateTime()));
		item05.put("color", "#173177");
		data.put("backupFieldData", item05);

		JSONObject item06 = new JSONObject();
		item06.put("value", "欢迎再次购买！");
		item06.put("color", "#173177");
		json.put("remark", item06);
		json.put("data", data);
		return json.toJSONString();
	}
	
/*	public static String orderPaySuccess(WxOrder wxOrder, String openId,
			String path, String templateId) throws MangoException {
		String url = path
				+ "/wizarpos-mall-sole/wx_order_detail/get_order_detail?orderId="
				+ wxOrder.getOrderId();

		JSONObject json = new JSONObject();
		JSONObject data = new JSONObject();
		json.put("touser", openId);
		json.put("template_id", templateId);
		json.put("url", url);
		json.put("topcolor", "#FF0000");

		JSONObject item01 = new JSONObject();
		item01.put("value", "我们已收到您的货款，开始为您打包商品，请耐心等待。");
		item01.put("color", "#173177");
		data.put("first", item01);

		JSONObject item02 = new JSONObject();
		item02.put("value", Utils.formatAmount(wxOrder.getAmount()) + "元");
		item02.put("color", "#173177");
		data.put("orderMoneySum", item02);

		JSONObject item03 = new JSONObject();
		item03.put("value", wxOrder.getOrderId());
		item03.put("color", "#173177");
		data.put("orderProductName", item03);

		JSONObject item04 = new JSONObject();
		item04.put("value", "欢迎再次购买！");
		item04.put("color", "#173177");
		json.put("remark", item04);
		json.put("data", data);
		return json.toJSONString();
	}*/

	public static String orderPaySuccess(WxOrder wxOrder, String openId,
			String path, String templateId) throws MangoException {
		String url = path
				+ "/wizarpos-mall-sole/wx_order_detail/get_order_detail?mid=" + wxOrder.getMid() + "&orderId="
				+ wxOrder.getOrderId();

		JSONObject json = new JSONObject();
		JSONObject data = new JSONObject();
		json.put("touser", openId);
		json.put("template_id", templateId);
		json.put("url", url);
		json.put("topcolor", "#FF0000");

		JSONObject item01 = new JSONObject();
		item01.put("value", "我们已收到您的货款，开始为您打包商品，请耐心等待。");
		item01.put("color", "#173177");
		data.put("first", item01);

		JSONObject item02 = new JSONObject();
		item02.put("value", Utils.formatAmount(wxOrder.getAmount()) + "元");
		item02.put("color", "#173177");
		data.put("orderMoneySum", item02);

		JSONObject item03 = new JSONObject();
		item03.put("value", wxOrder.getOrderId());
		item03.put("color", "#173177");
		data.put("orderProductName", item03);

		JSONObject item04 = new JSONObject();
		item04.put("value", "欢迎再次购买！");
		item04.put("color", "#173177");
		json.put("remark", item04);
		json.put("data", data);
		return json.toJSONString();
	}
	
	public static String memberChange(String cardNo, String amount,
			String openId, String templateId) throws MangoException {

		JSONObject json = new JSONObject();
		JSONObject data = new JSONObject();
		json.put("touser", openId);
		json.put("template_id", templateId);
		json.put("url", "http://weixin.qq.com/download");
		json.put("topcolor", "#FF0000");

		JSONObject item01 = new JSONObject();
		item01.put("value", "您好，您已成功进行会员卡充值。");
		item01.put("color", "#173177");
		data.put("first", item01);

		JSONObject item02 = new JSONObject();
		item02.put("value", "会员卡号");
		item02.put("color", "#173177");
		data.put("accountType", item02);

		JSONObject item03 = new JSONObject();
		item03.put("value", cardNo);
		item03.put("color", "#173177");
		data.put("account", item03);

		JSONObject item04 = new JSONObject();
		item04.put("value", amount);
		item04.put("color", "#173177");
		data.put("amount", item04);

		JSONObject item05 = new JSONObject();
		item05.put("value", "充值成功");
		item05.put("color", "#173177");
		data.put("result", item05);

		JSONObject item06 = new JSONObject();
		item06.put("value", "欢迎再次充值！");
		item06.put("color", "#173177");
		json.put("remark", item06);
		json.put("data", data);
		return json.toJSONString();
	}

	public static String orderStatusUpdate(WxOrder wxOrder, String openId,
			String templateId) throws MangoException {

		JSONObject json = new JSONObject();
		JSONObject data = new JSONObject();
		json.put("touser", openId);
		json.put("template_id", templateId);
		json.put("url", "http://weixin.qq.com/download");
		json.put("topcolor", "#FF0000");

		JSONObject item01 = new JSONObject();
		item01.put("value", "订单状态更新");
		item01.put("color", "#173177");
		data.put("first", item01);

		JSONObject item02 = new JSONObject();
		item02.put("value", wxOrder.getOrderId());
		item02.put("color", "#173177");
		data.put("OrderSn", item02);

		JSONObject item03 = new JSONObject();
		item03.put("value", wxOrder.getStatus());
		item03.put("color", "#173177");
		data.put("OrderStatus", item03);

		JSONObject item04 = new JSONObject();
		item04.put("value", "慧银小店祝您生活愉快");
		item04.put("color", "#173177");
		json.put("remark", item04);
		json.put("data", data);
		return json.toJSONString();
	}

	public static String packageDispatch(WxOrder wxOrder, String openId,
			String templateId) throws MangoException {

		JSONObject json = new JSONObject();
		JSONObject data = new JSONObject();
		json.put("touser", openId);
		json.put("template_id", templateId);
		json.put("url", "http://weixin.qq.com/download");
		json.put("topcolor", "#FF0000");

		JSONObject item01 = new JSONObject();
		item01.put("value", "亲，宝贝已经启程了，好想快点来到你身边!");
		item01.put("color", "#173177");
		data.put("first", item01);

		JSONObject item02 = new JSONObject();
		item02.put("value", wxOrder.getOrderId());
		item02.put("color", "#173177");
		data.put("keynote1", item02);

		JSONObject item03 = new JSONObject();
		item03.put("value", "未知");
		item03.put("color", "#173177");
		data.put("keynote2", item03);

		JSONObject item04 = new JSONObject();
		item04.put("value", "未知");
		item04.put("color", "#173177");
		data.put("keynote3", item04);

		JSONObject item05 = new JSONObject();
		item05.put("value", "欢迎再次购买！");
		item05.put("color", "#173177");
		json.put("remark", item05);
		json.put("data", data);
		return json.toJSONString();
	}

	public static String packageReceive(String name, String gift, String time,
			String openId, String templateId) throws MangoException {

		JSONObject json = new JSONObject();
		JSONObject data = new JSONObject();
		json.put("touser", openId);
		json.put("template_id", templateId);
		json.put("url", "http://weixin.qq.com/download");
		json.put("topcolor", "#FF0000");

		JSONObject item01 = new JSONObject();
		item01.put("value", "您好，您赠送的商品已经被成功领取");
		item01.put("color", "#173177");
		data.put("first", item01);

		JSONObject item02 = new JSONObject();
		item02.put("value", name);
		item02.put("color", "#173177");
		data.put("toName", item02);

		JSONObject item03 = new JSONObject();
		item03.put("value", gift);
		item03.put("color", "#173177");
		data.put("gift", item03);

		JSONObject item04 = new JSONObject();
		item04.put("value", time);
		item04.put("color", "#173177");
		data.put("time", item04);

		JSONObject item05 = new JSONObject();
		item05.put("value", "欢迎再次购买！");
		item05.put("color", "#173177");
		json.put("remark", item05);
		json.put("data", data);
		return json.toJSONString();
	}

}
