package cn.congine.wizarpos.mall.dao.hibernate;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import cn.congine.wizarpos.mall.dao.MrtMerchantDefDao;
import cn.congine.wizarpos.mall.entity.MrtMerchantDef;

@Repository("MrtMerchantDefDao")
public class MrtMerchantDefDaoHibernate  extends
	GenericDaoHibernate<MrtMerchantDef> implements MrtMerchantDefDao {

	public MrtMerchantDefDaoHibernate() {
		super(MrtMerchantDef.class);
	}

	@Override
	public MrtMerchantDef getSysMerchantDefByMid(String mid) {
		Query query = getSession().createQuery(
				"from MrtMerchantDef where mid = :mid").setParameter(
				"mid", mid);
		return (MrtMerchantDef) query.uniqueResult();
	}

}
