package cn.congine.wizarpos.mall.model.open;

/**
 * 
 * @author xudongdong
 * Date: 2015.7.16
 */


public class SelfTicketAddInput extends CommonInput {

	private static final long serialVersionUID = 1L;
	private String mid = null;
	private String openId = null;
	private String ticketId = null;
	private String ticketNo = null;
	private char wxAdded = '0';//是否添加到了卡包（0未添加，1已添加）
	private String hbShared = null;// 红包是否分享（1分享，0未分享）
	private String remark = null;	
	private String url = null;
	
	public String getMid() {
		return mid;
	}
	public void setMid(String mid) {
		this.mid = mid;
	}
	public String getOpenId() {
		return openId;
	}
	public void setOpenId(String openId) {
		this.openId = openId;
	}
	public String getTicketId() {
		return ticketId;
	}
	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}
	public String getTicketNo() {
		return ticketNo;
	}
	public void setTicketNo(String ticketNo) {
		this.ticketNo = ticketNo;
	}
	public char getWxAdded() {
		return wxAdded;
	}
	public void setWxAdded(char wxAdded) {
		this.wxAdded = wxAdded;
	}
	public String getHbShared() {
		return hbShared;
	}
	public void setHbShared(String hbShared) {
		this.hbShared = hbShared;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	
}
