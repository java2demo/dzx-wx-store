package cn.congine.wizarpos.mall.model.open;

/**
 * 
 * @author xudongdong
 * Date: 2015.7.15
 */


public class UpdateOrderInfoInput extends CommonInput {

	private static final long serialVersionUID = 1L;

	private String mid = null;
	
	private String orderId = null;

	private String status = null;
	
	private String dispatchType = null;	
	
	private String payStatus = null;
	
	private String pickUpQr = null;
	
	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDispatchType() {
		return dispatchType;
	}

	public void setDispatchType(String dispatchType) {
		this.dispatchType = dispatchType;
	}

	public String getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(String payStatus) {
		this.payStatus = payStatus;
	}

	public String getPickUpQr() {
		return pickUpQr;
	}

	public void setPickUpQr(String pickUpQr) {
		this.pickUpQr = pickUpQr;
	}

}
