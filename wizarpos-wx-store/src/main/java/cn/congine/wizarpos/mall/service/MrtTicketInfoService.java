package cn.congine.wizarpos.mall.service;

import java.util.List;

import cn.congine.wizarpos.mall.model.MrtTicketInfo;

public interface MrtTicketInfoService {

	public List<Object> getUsedTicketInfo(String cardId, int pageNo);

	public List<Object> getMrtTicketInfoByCardId(String cardId, int pageNo);

	public MrtTicketInfo save(MrtTicketInfo ticketInfo);

	public String getIdByWxTicketIdAndNoAndOpenid(String cardId, String no,
			String openid);

	public MrtTicketInfo get(String id);

	public MrtTicketInfo getByNo(String mid, String ticketNo);

	public boolean isExist(String ticketDefId, String cardId, String mid);

	public void update(MrtTicketInfo ticketInfo);

	public MrtTicketInfo getByTicketDefIdAndCardIdAndMid(String ticketDefId,
			String cardId, String mid);
}
