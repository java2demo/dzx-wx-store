package cn.congine.wizarpos.mall.dao.hibernate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.FlushMode;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import cn.congine.wizarpos.mall.common.Page;
import cn.congine.wizarpos.mall.dao.PayTranLogDao;
import cn.congine.wizarpos.mall.model.PayTranLog;
import cn.congine.wizarpos.mall.utils.Function;

@Repository("payTranLogDao")
public class PayTranLogDaoHibernate extends GenericDaoHibernate<PayTranLog>
		implements PayTranLogDao {

	public PayTranLogDaoHibernate() {
		super(PayTranLog.class);
	}

	@Override
	public List<PayTranLog> getCardTranLogByCardNo(String mid, String opendId,
			Page<PayTranLog> page) {
		StringBuilder hql = new StringBuilder(
				"select t1 from PayTranLog t1, MrtMerchantCard t2 "
				+ "where t1.cardNo=t2.cardNo and t1.mid=t2.mid and t2.mid=:mid "
				+ "and t2.openId=:openId and t1.tranCode in ('302','310','304') "
				+ "and t1.ticketInfoId is null "
				+ "order by t1.tranTime DESC");
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("mid", mid);
		params.put("openId", opendId);
		page = this.readAll4Page(hql.toString(), "", params, page);
		return page.getList();
	}

	@Override
	public PayTranLog save(PayTranLog log) {
		String sql = "insert into pay_tran_log (id,sn,mid,pid,tran_time,tran_code,cash_type,tran_amount,"
				+ "extra_amount,input_amount,card_no,order_no,ticket_info_id,relate_tran_log_id,canceled_flag,operator_id,"
				+ "tran_mark,tran_desc,offline_tran_log_id,ticket_tran_log_id,master_tran_log_id,ticket_no,balance,"
				+ "dis_count,cash_service_id,mixed_flag,mixed_tran_flag) "
				+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
		this.getSession().setFlushMode(FlushMode.MANUAL);
		Query q = getSession().createSQLQuery(sql);
		q.setString(0, log.getId());
		q.setString(1, log.getSn());
		q.setString(2, log.getMid());
		q.setString(3, log.getPid());
		q.setString(4,
				Function.formatDate(log.getTranTime(), "yyyy-MM-dd HH:mm:ss"));
		q.setString(5, log.getTranCode());
		q.setString(6, log.getCashType());
		q.setInteger(7, log.getTranAmount() == null ? 0 : log.getTranAmount());
		q.setInteger(8, log.getExtraAmount() == null ? 0 : log.getExtraAmount());
		q.setInteger(9, log.getInputAmount() == null ? 0 : log.getInputAmount());
		q.setString(10, log.getCardNo());
		q.setString(11, log.getOrderNo());
		q.setString(12, log.getTicketInfoId());
		q.setString(13, log.getRelateTranLogId());
		q.setBoolean(14, log.isCanceledFlag());
		q.setString(15, log.getOperatorId());
		q.setString(16, log.getTranMark());
		q.setString(17, log.getTranDesc());
		q.setString(18, log.getOfflineTranLogId());
		q.setString(19, log.getTicketTranLogId());
		q.setString(20, log.getMasterTranLogId());
		q.setString(21, log.getTicketNo());
		q.setInteger(22, log.getBalance() == null ? 0 : log.getBalance());
		q.setInteger(23, log.getDisCount() == null ? 100 : log.getDisCount());
		q.setString(24, log.getCashServiceId());
		q.setString(25, log.getMixedFlag());
		q.setString(26, log.getMixedTranFlag());
		q.executeUpdate();
		return log;
	}
}
