package cn.congine.wizarpos.mall.model.open;

import java.util.List;

public class CateringOrderQueryOutput extends CommonOutput {

	private static final long serialVersionUID = 1L;
	private List<TableOrder> result = null;
	public List<TableOrder> getResult() {
		return result;
	}
	public void setResult(List<TableOrder> result) {
		this.result = result;
	}

}
