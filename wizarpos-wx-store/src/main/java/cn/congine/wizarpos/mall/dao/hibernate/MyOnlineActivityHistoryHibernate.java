package cn.congine.wizarpos.mall.dao.hibernate;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import cn.congine.wizarpos.mall.dao.MyOnlineActivityHistoryDao;
import cn.congine.wizarpos.mall.entity.MyOnlineActivityHistory;

@Repository("myOnlineActivityHistoryDao")
public class MyOnlineActivityHistoryHibernate extends GenericDaoHibernate<MyOnlineActivityHistory>
		implements MyOnlineActivityHistoryDao {

	public MyOnlineActivityHistoryHibernate() {
		super(MyOnlineActivityHistory.class);
	}

	@Override
	public MyOnlineActivityHistory getMyOnlineActivityHistoryByOpenId(String openId,
			String onlineActivityId) {
		Query query = getSession()
				.createQuery(
						"from MyOnlineActivityHistory where openId = :openId and onlineActivityId= :onlineActivityId")
				.setParameter("openId", openId).setParameter("onlineActivityId", onlineActivityId);
		return (MyOnlineActivityHistory) query.uniqueResult();
	}


	

}
