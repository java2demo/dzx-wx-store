package cn.congine.wizarpos.mall.dao;

import java.util.List;

import cn.congine.wizarpos.mall.common.Page;
import cn.congine.wizarpos.mall.model.WxOrder;

public interface WxOrderDao extends GenericDao<WxOrder> {

	List<WxOrder> getWxOrderListByMid(String mid);

	List<WxOrder> getWxOrderListByOpenId(String openId);

	WxOrder getWxOrderByOrderId(String orderId);

	List<WxOrder> getWeekWxOrderListByOpenId(String openId, String mid, int type);

	List<WxOrder> getVoWxOrderListByOpenId(String openId, String mid,
			String type);

	List<WxOrder> getWeekWxOrderListByOpenId(String openId, String mid,
			int type, Page<WxOrder> page);

	List<WxOrder> getOrderListByType(String openId, String mid, int type,
			Page<WxOrder> page);

	List<WxOrder> getVoByPage(String openId, String mid, Integer nextPage);

}
