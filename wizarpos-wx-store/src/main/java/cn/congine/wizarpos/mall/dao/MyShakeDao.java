package cn.congine.wizarpos.mall.dao;

import java.util.List;

import cn.congine.wizarpos.mall.entity.MyShake;

public interface MyShakeDao extends GenericDao<MyShake> {

	List<MyShake> getMyShakeList(String mid, String onlineActivityId, List<String> ticketMids);
	
	List<MyShake> getMyShakeList(String mid, String onlineActivityId);
	
	MyShake getMyShakeByid(String id, String mid, String onlineActivityId);
	
	List<MyShake> getMyShakeListExitHongBao(String mid,String onlineActivityId);
}
