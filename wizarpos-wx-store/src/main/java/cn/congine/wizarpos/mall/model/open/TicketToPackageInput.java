package cn.congine.wizarpos.mall.model.open;

/**
 * 
 * @author xudongdong
 * Date: 2015.7.3
 */


public class TicketToPackageInput extends CommonInput {

	private static final long serialVersionUID = 1L;
	
	private String mid = null;
	
	private String openId = null;

	private String ticketNo = null;
	
	private String wxCardId = null;
	//mrt_ticket_info表主键，卡券编号
	private String id = null;//add 2015.7.17
	
	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getTicketNo() {
		return ticketNo;
	}

	public void setTicketNo(String ticketNo) {
		this.ticketNo = ticketNo;
	}

	public String getWxCardId() {
		return wxCardId;
	}

	public void setWxCardId(String wxCardId) {
		this.wxCardId = wxCardId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}	
}
