package cn.congine.wizarpos.mall.service;

public interface ErpProductStockService {
	public Integer getTotalStock(String mid, String productId);
}
