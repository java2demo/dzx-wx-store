package cn.congine.wizarpos.mall.model.open;

import java.util.List;

public class ShopTranLogOutput extends CommonOutput {

	private static final long serialVersionUID = 1L;

	private List<TranLogView> result = null;

	public List<TranLogView> getResult() {
		return result;
	}

	public void setResult(List<TranLogView> result) {
		this.result = result;
	}
	
}
