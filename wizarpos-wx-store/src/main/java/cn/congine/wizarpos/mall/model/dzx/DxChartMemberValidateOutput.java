package cn.congine.wizarpos.mall.model.dzx;

import cn.congine.wizarpos.mall.model.open.CommonOutput;

public class DxChartMemberValidateOutput extends CommonOutput {

	private static final long serialVersionUID = 1L;
    
    private String result=null;

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
    		
}
