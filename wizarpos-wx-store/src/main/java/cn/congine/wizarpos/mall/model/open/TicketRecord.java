package cn.congine.wizarpos.mall.model.open;

/**
 * 
 * @author xudongdong
 * Date: 2015.7.3
 */

import java.io.Serializable;

public class TicketRecord implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id = null;
	
	private String ticket_no = null;
	
	private String ticket_code = null;
	
	private String ticket_name = null;
	/**
	 * 元为单位
	 */
	private String balance = null;	
	
	private String valid_period = null;
	
	private String create_time = null;
	
	private String description = null;
	
	private String cancel_time = null;
	
	private String used_time = null;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTicket_no() {
		return ticket_no;
	}

	public void setTicket_no(String ticket_no) {
		this.ticket_no = ticket_no;
	}

	public String getTicket_code() {
		return ticket_code;
	}

	public void setTicket_code(String ticket_code) {
		this.ticket_code = ticket_code;
	}

	public String getTicket_name() {
		return ticket_name;
	}

	public void setTicket_name(String ticket_name) {
		this.ticket_name = ticket_name;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getValid_period() {
		return valid_period;
	}

	public void setValid_period(String valid_period) {
		this.valid_period = valid_period;
	}

	public String getCreate_time() {
		return create_time;
	}

	public void setCreate_time(String create_time) {
		this.create_time = create_time;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCancel_time() {
		return cancel_time;
	}

	public void setCancel_time(String cancel_time) {
		this.cancel_time = cancel_time;
	}

	public String getUsed_time() {
		return used_time;
	}

	public void setUsed_time(String used_time) {
		this.used_time = used_time;
	}


}
