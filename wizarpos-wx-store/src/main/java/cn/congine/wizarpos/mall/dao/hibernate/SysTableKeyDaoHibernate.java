package cn.congine.wizarpos.mall.dao.hibernate;

import org.springframework.stereotype.Repository;

import cn.congine.wizarpos.mall.dao.SysTableKeyDao;
import cn.congine.wizarpos.mall.model.SysTableKey;

@Repository("sysTableKeyDao")
public class SysTableKeyDaoHibernate extends GenericDaoHibernate<SysTableKey>
		implements SysTableKeyDao {

	public SysTableKeyDaoHibernate() {
		super(SysTableKey.class);
	}

}
