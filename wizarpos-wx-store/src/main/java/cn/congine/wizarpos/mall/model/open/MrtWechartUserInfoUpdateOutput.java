package cn.congine.wizarpos.mall.model.open;

/**
 * 
 * @author xudongdong
 * Date: 2015.7.14
 */
public class MrtWechartUserInfoUpdateOutput extends CommonOutput {

	private static final long serialVersionUID = 1L;

	private Result result =null;//add xudongdong

	public Result getResult() {
		return result;
	}

	public void setResult(Result result) {
		this.result = result;
	}	
	
	public class Result {
		
		//0  非首次关注 1 首次关注 
		private String firstSubscribe = null;
		
		private String mid = null;

		public String getFirstSubscribe() {
			return firstSubscribe;
		}

		public void setFirstSubscribe(String firstSubscribe) {
			this.firstSubscribe = firstSubscribe;
		}

		public String getMid() {
			return mid;
		}

		public void setMid(String mid) {
			this.mid = mid;
		}
	}
}