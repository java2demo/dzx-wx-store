package cn.congine.wizarpos.mall.model.open;


public class MerchantdefSyncInput extends CommonInput {

	private static final long serialVersionUID = 1L;

	String mid = null;

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}
	
}
