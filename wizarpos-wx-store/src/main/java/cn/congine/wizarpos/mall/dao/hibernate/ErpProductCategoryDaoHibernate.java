package cn.congine.wizarpos.mall.dao.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import cn.congine.wizarpos.mall.dao.ErpProductCategoryDao;
import cn.congine.wizarpos.mall.model.ErpProductCategory;

@Repository("erpProductCategoryDao")
public class ErpProductCategoryDaoHibernate extends
		GenericDaoHibernate<ErpProductCategory> implements
		ErpProductCategoryDao {

	public ErpProductCategoryDaoHibernate() {
		super(ErpProductCategory.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ErpProductCategory> getErpProductCategoryByMid(String mid) {
		Query query = getSession()
				.createQuery(
						"from ErpProductCategory where mid = :mid and parentId ='0' and storageRackFlag ='1'")
				.setParameter("mid", mid);
		return (List<ErpProductCategory>) query.list();

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ErpProductCategory> getErpProductCategoryByParentId(String mid,
			String id) {
		Query query = getSession()
				.createQuery(
						"from ErpProductCategory where parentId=:id and mid = :mid and storageRackFlag ='1'")
				.setParameter("id", id).setParameter("mid", mid);
		return (List<ErpProductCategory>) query.list();

	}

	@Override
	public ErpProductCategory getErpProductCategoryByCode(String mid, String code) {
		Query query = getSession().createQuery(
				"from ErpProductCategory where mid =:mid and code = :code and storageRackFlag ='1'")
				.setParameter("mid", mid).setParameter("code", code);
		return (ErpProductCategory) query.uniqueResult();
	}

	@Override
	public ErpProductCategory getErpProductCategoryById(String id) {
		Query query = getSession().createQuery(
				"from ErpProductCategory where id = :id")
				.setParameter("id", id);
		return (ErpProductCategory) query.uniqueResult();
	}

	@Override
	public List<ErpProductCategory> getErpProductCategoryByParentId(String mid) {
		// TODO Auto-generated method stub
		return null;
	}

}
