package cn.congine.wizarpos.mall.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.congine.wizarpos.mall.dao.BankCardBindDao;
import cn.congine.wizarpos.mall.model.BankCardBind;
import cn.congine.wizarpos.mall.service.BankCardBindService;

@Service("bankCardBindService")
public class BankCardBindServiceImpl implements BankCardBindService {
	@Autowired
	private BankCardBindDao bankCardBindDao;

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public BankCardBind save(BankCardBind bind) {
		return bankCardBindDao.save(bind);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void update(BankCardBind obj) {
		bankCardBindDao.update(obj);
	}

	@Override
	public BankCardBind find(String id, String mid) {
		return bankCardBindDao.find(id, mid);
	}

	@Override
	public BankCardBind get(String cardNo, String merchantId) {
		return bankCardBindDao.get(cardNo, merchantId);
	}

	@Override
	public BankCardBind get(Integer cid) {
		return bankCardBindDao.get(cid);
	}

	@Override
	public List<BankCardBind> getList(String mid) {
		return bankCardBindDao.getList(mid);
	}

	@Override
	public List<BankCardBind> getList(String mid, String openid) {
		return bankCardBindDao.getList(mid, openid);
	}

	@Override
	public BankCardBind get(String id) {
		return bankCardBindDao.get(id);
	}
}
