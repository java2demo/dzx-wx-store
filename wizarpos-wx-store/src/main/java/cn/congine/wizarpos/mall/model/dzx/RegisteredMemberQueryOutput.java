package cn.congine.wizarpos.mall.model.dzx;

import cn.congine.wizarpos.mall.model.open.CommonOutput;

public class RegisteredMemberQueryOutput extends CommonOutput {

	private static final long serialVersionUID = 1L;
	private Result result = null;

	public Result getResult() {
		return result;
	}

	public void setResult(Result result) {
		this.result = result;
	}

	public class Result {

		private String cardNo = null;
		private String mobileNo = null;

		public String getCardNo() {
			return cardNo;
		}

		public void setCardNo(String cardNo) {
			this.cardNo = cardNo;
		}

		public String getMobileNo() {
			return mobileNo;
		}

		public void setMobileNo(String mobileNo) {
			this.mobileNo = mobileNo;
		}

	}
}
