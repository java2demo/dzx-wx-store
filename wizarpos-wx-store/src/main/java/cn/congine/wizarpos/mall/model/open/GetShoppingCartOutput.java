package cn.congine.wizarpos.mall.model.open;

/**
 * 
 * @author xudongdong
 * Date: 2015.7.6
 */

import java.util.List;
import cn.congine.wizarpos.mall.vo.WxShoppingCartItem;

public class GetShoppingCartOutput extends CommonOutput {

	private static final long serialVersionUID = 1L;

	private String cartId = null;

	private List<WxShoppingCartItem> wxShoppingCartItemList = null;

	public String getCartId() {
		return cartId;
	}

	public void setCartId(String cartId) {
		this.cartId = cartId;
	}

	public List<WxShoppingCartItem> getWxShoppingCartItemList() {
		return wxShoppingCartItemList;
	}

	public void setWxShoppingCartItemList(
			List<WxShoppingCartItem> wxShoppingCartItemList) {
		this.wxShoppingCartItemList = wxShoppingCartItemList;
	}
}
