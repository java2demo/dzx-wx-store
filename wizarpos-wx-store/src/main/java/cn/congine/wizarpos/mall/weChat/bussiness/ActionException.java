package cn.congine.wizarpos.mall.weChat.bussiness;

/**
 * @author 史正烨
 * @date 2011-6-18
 */
public class ActionException extends Exception {

	private static final long serialVersionUID = 2575385211323401373L;

	/** 动作执行成功 */
	public static final int OK = 0;

	/** MSERVER 获取列表数据错误 */
	public static final int MSERVER_GET_LIST_DATA_ERROR = 1010;

	/** MSERVER 获取表单数据错误 */
	public static final int MSERVER_GET_FORM_DATA_ERROR = 1020;

	/** MSERVER 获取树数据错误 */
	public static final int MSERVER_GET_TREE_DATA_ERROR = 1030;

	/** MSERVER 执行商业服务错误 */
	public static final int MSERVER_DO_SERVICE_ERROR = 1040;

	/** MSERVER 执行表单保存错误 */
	public static final int MSERVER_SAVE_FORM_DATA_ERROR = 1050;

	/** MSERVER 执行登陆错误 */
	public static final int MSERVER_LOGIN_ERROR = 1050;

	/** MADAPTOR 下载本地文件错误 */
	public static final int MADAPTOR_REMOTE_DOWNLOAD_ERROR = 1110;

	/** MADAPTOR 下载HTTP文件错误 */
	public static final int MADAPTOR_HTTP_DOWNLOAD_ERROR = 1120;

	private int code = 0;

	public ActionException(int code, String msg) {
		super(msg);
		this.code = code;
	}

	public ActionException(int code, String msg, Throwable t) {
		super(msg, t);
		this.code = code;
	}

	public int getCode() {
		return this.code;
	}
}
