package cn.congine.wizarpos.mall.utils;

public class Constants {

	public static final String PROJECT_DIRECTORY = "__project_directory";

	public static final String SESSION_ID = "__session_id";

	// 没有匹配的action name
	public static final int NO_METCH_ACTION_NAME = 10;
	// 不能找到config目录
	public static final int NO_FIND_CONFIG = 3;

	// 方法非法访问
	public static final int ILLEGAL_ACCESS = 11;

	// 用户处理错误
	public static final int USER_PROCESS_ERROR = 12;

	// 服务器处理异常
	public static final int SERVER_PROCESS_ERROR = 500;

	// 处理成功
	public static final int OK = 0;

	/** 预览缓存处理 */
	public static final String PREVIEW_CACHE = "__preview_cache_";

	public static final String MSERVER_CLIENT = "__mserver_client";

	public static final String MSERVER_PARTY_ID = "__mserver_party_id";

	private Constants() {
	}
}
