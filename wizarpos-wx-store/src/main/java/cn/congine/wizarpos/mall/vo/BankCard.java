package cn.congine.wizarpos.mall.vo;

import java.io.Serializable;

public class BankCard implements Serializable {

	private static final long serialVersionUID = 1L;
	private String id = null;
	private String maskNo = null;
	private String bankName = null;
	private String bankLogo = null;
	private boolean flag = false;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMaskNo() {
		return maskNo;
	}

	public void setMaskNo(String maskNo) {
		this.maskNo = maskNo;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankLogo() {
		return bankLogo;
	}

	public void setBankLogo(String bankLogo) {
		this.bankLogo = bankLogo;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

}
