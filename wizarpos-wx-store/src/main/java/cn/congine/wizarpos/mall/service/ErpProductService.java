package cn.congine.wizarpos.mall.service;

import java.util.List;

import cn.congine.wizarpos.mall.common.Page;
import cn.congine.wizarpos.mall.model.ErpProduct;

public interface ErpProductService {

	List<ErpProduct> getHotErpProductListByMid(String mid, String hotJudge);

	ErpProduct getErpProductById(String id);

	List<ErpProduct> getErpProductListByMid(String mid, String categoryId);
	
	List<ErpProduct> getErpProductPageListByMid(String mid, String categoryId,Integer pageIndex);

	List<ErpProduct> getHotErpProductListByMid(String mid, String hotJudge,
			String productName);

	List<ErpProduct> getHotErpProductListByCategoryId(String mid,
			String hotJudge, String categoryId);

	/**
	 * 在分类中根据关键字查询
	 * 
	 * @param mid
	 *            慧商户号
	 * @param categoryId
	 *            分类id
	 * @param kyeWord
	 *            查询的关键字
	 * @return 产品集合
	 */
	public List<ErpProduct> getProductList(String mid, String categoryId,
			String kyeWord);

	/**
	 * 在当前商户下进行模糊查询商品
	 * 
	 * @param mid
	 *            商户号
	 * @param keyWord
	 *            查询关键字
	 * @return 产品集合
	 */
	public List<ErpProduct> getProductList(String mid, String keyWord);

	/**
	 * 分页查询商品，支持模糊查询
	 * 
	 * @param mid
	 *            慧商户号
	 * @param page
	 *            分页
	 * @param categoryId
	 *            分类号
	 * @param keyword
	 *            关键字
	 * @return 商品集合
	 */
	public List<ErpProduct> getProudctLit(String mid, Page<ErpProduct> page,
			String categoryId, String keyword);

	List<ErpProduct> getErpProductListByPage(String mid, String categoryId,
			int pageIndex);
}
