package cn.congine.wizarpos.mall.model.open;

import java.io.Serializable;

public class WechartOrderQueryInput extends CommonInput{

	private static final long serialVersionUID = 1L;
	private String openId ;
	private String mid ;
	private int pageNo ;
	private int flag ;
	public String getOpenId() {
		return openId;
	}
	public void setOpenId(String openId) {
		this.openId = openId;
	}
	public String getMid() {
		return mid;
	}
	public void setMid(String mid) {
		this.mid = mid;
	}
	public int getPageNo() {
		return pageNo;
	}
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}
	public int getFlag() {
		return flag;
	}
	public void setFlag(int flag) {
		this.flag = flag;
	}
	
}
