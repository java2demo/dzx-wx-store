package cn.congine.wizarpos.mall.vo;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

public class MerchantConcern {
	// 唯一标识
	private String id;
	// 主键
	private String cardNo;

	// 卡类型
	private String cardType;

	// 慧商户号
	private String mid;

	// 微信ID
	private String openId;

	// 用户姓名
	private String userName;

	// 手机号
	private String mobileNo;

	// 激活时间
	private Date activeTime;

	// 余额
	private Integer balance;

	// 过期时间
	private Date expriyTime;

	// 注销标识
	private String canceled;

	// 注销时间
	private Date cancelTime;

	// 冻结
	private String freeze;

	// 冻结时间
	private Date freezeTime;

	// 添加时间
	private Date cardCreateTime;

	// 收单商户号
	private String merchantId;

	// 收单商户号
	private String mgtLoct;

	// 收单商户号
	private String cityLoct;

	// 支付通道
	private Integer payId;

	// 商户名称
	private String merchantName;

	// 微信会员卡开始编号
	private Integer startNo;

	// 使用慧银的微信配置参数
	private String useWizarposWeixinPayConfig;

	// 使用慧银的支付宝配置参数
	private String useWizarposAlipayConfig;

	// 收款通知开关
	private String collectNotifyMark;

	// 销售通知开关
	private String saleNotifyMark;

	// 商户编号，从1开始顺序增加,每次加1。与自发行卡的卡号前6位对应
	private Integer sequenceNo;

	// 入驻时间
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createTime;

	// 商户LOGO
	private String logoImage;

	// 商铺模板:1货架式模板 2全部商铺式模板
	private Integer storesTemplates;

	// 门店相关说明
	private String shopDesc;

	// vip图片
	private String vipImage;

	// vip正面图片
	private String vipFrontImage;

	// 商户公众号openid
	private String pOpenId;

	// 慧商户公众号openid
	private String mOpenId;

	// 经度
	private String lon;

	// 纬度
	private String lat;

	// 支付宝账户号
	private String alipayAccount;

	// 商户地址
	private String merchantAddr;

	// 商户联系方式(多个联系方式以逗号分隔)
	private String merchantTel;

	// 商户简介
	private String merchantSummary;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public Date getActiveTime() {
		return activeTime;
	}

	public void setActiveTime(Date activeTime) {
		this.activeTime = activeTime;
	}

	public Integer getBalance() {
		return balance;
	}

	public void setBalance(Integer balance) {
		this.balance = balance;
	}

	public Date getExpriyTime() {
		return expriyTime;
	}

	public void setExpriyTime(Date expriyTime) {
		this.expriyTime = expriyTime;
	}

	public String getCanceled() {
		return canceled;
	}

	public void setCanceled(String canceled) {
		this.canceled = canceled;
	}

	public Date getCancelTime() {
		return cancelTime;
	}

	public void setCancelTime(Date cancelTime) {
		this.cancelTime = cancelTime;
	}

	public String getFreeze() {
		return freeze;
	}

	public void setFreeze(String freeze) {
		this.freeze = freeze;
	}

	public Date getFreezeTime() {
		return freezeTime;
	}

	public void setFreezeTime(Date freezeTime) {
		this.freezeTime = freezeTime;
	}

	public Date getCardCreateTime() {
		return cardCreateTime;
	}

	public void setCardCreateTime(Date cardCreateTime) {
		this.cardCreateTime = cardCreateTime;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getMgtLoct() {
		return mgtLoct;
	}

	public void setMgtLoct(String mgtLoct) {
		this.mgtLoct = mgtLoct;
	}

	public String getCityLoct() {
		return cityLoct;
	}

	public void setCityLoct(String cityLoct) {
		this.cityLoct = cityLoct;
	}

	public Integer getPayId() {
		return payId;
	}

	public void setPayId(Integer payId) {
		this.payId = payId;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public Integer getStartNo() {
		return startNo;
	}

	public void setStartNo(Integer startNo) {
		this.startNo = startNo;
	}

	public String getUseWizarposWeixinPayConfig() {
		return useWizarposWeixinPayConfig;
	}

	public void setUseWizarposWeixinPayConfig(String useWizarposWeixinPayConfig) {
		this.useWizarposWeixinPayConfig = useWizarposWeixinPayConfig;
	}

	public String getUseWizarposAlipayConfig() {
		return useWizarposAlipayConfig;
	}

	public void setUseWizarposAlipayConfig(String useWizarposAlipayConfig) {
		this.useWizarposAlipayConfig = useWizarposAlipayConfig;
	}

	public String getCollectNotifyMark() {
		return collectNotifyMark;
	}

	public void setCollectNotifyMark(String collectNotifyMark) {
		this.collectNotifyMark = collectNotifyMark;
	}

	public String getSaleNotifyMark() {
		return saleNotifyMark;
	}

	public void setSaleNotifyMark(String saleNotifyMark) {
		this.saleNotifyMark = saleNotifyMark;
	}

	public Integer getSequenceNo() {
		return sequenceNo;
	}

	public void setSequenceNo(Integer sequenceNo) {
		this.sequenceNo = sequenceNo;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getLogoImage() {
		return logoImage;
	}

	public void setLogoImage(String logoImage) {
		this.logoImage = logoImage;
	}

	public Integer getStoresTemplates() {
		return storesTemplates;
	}

	public void setStoresTemplates(Integer storesTemplates) {
		this.storesTemplates = storesTemplates;
	}

	public String getShopDesc() {
		return shopDesc;
	}

	public void setShopDesc(String shopDesc) {
		this.shopDesc = shopDesc;
	}

	public String getVipImage() {
		return vipImage;
	}

	public void setVipImage(String vipImage) {
		this.vipImage = vipImage;
	}

	public String getVipFrontImage() {
		return vipFrontImage;
	}

	public void setVipFrontImage(String vipFrontImage) {
		this.vipFrontImage = vipFrontImage;
	}

	public String getpOpenId() {
		return pOpenId;
	}

	public void setpOpenId(String pOpenId) {
		this.pOpenId = pOpenId;
	}

	public String getmOpenId() {
		return mOpenId;
	}

	public void setmOpenId(String mOpenId) {
		this.mOpenId = mOpenId;
	}

	public String getLon() {
		return lon;
	}

	public void setLon(String lon) {
		this.lon = lon;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getAlipayAccount() {
		return alipayAccount;
	}

	public void setAlipayAccount(String alipayAccount) {
		this.alipayAccount = alipayAccount;
	}

	public String getMerchantAddr() {
		return merchantAddr;
	}

	public void setMerchantAddr(String merchantAddr) {
		this.merchantAddr = merchantAddr;
	}

	public String getMerchantTel() {
		return merchantTel;
	}

	public void setMerchantTel(String merchantTel) {
		this.merchantTel = merchantTel;
	}

	public String getMerchantSummary() {
		return merchantSummary;
	}

	public void setMerchantSummary(String merchantSummary) {
		this.merchantSummary = merchantSummary;
	}

}
