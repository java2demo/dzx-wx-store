package cn.congine.wizarpos.mall.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "pay_tran_log")
public class PayTranLog extends BaseEntity {
	private static final long serialVersionUID = 8673064447636559672L;

	// 唯一标识
	@Id
	@Column(name = "id")
	private String id;

	@Column(name = "sn")
	private String sn;
	// 商户号
	@Column(name = "mid")
	private String mid;

	// 发行商户号
	@Column(name = "pid")
	private String pid;

	// 交易时间
	@Column(name = "tran_time")
	private Date tranTime;

	// 交易码
	@Column(name = "tran_code")
	private String tranCode;

	// 其他收银类型码
	@Column(name = "cash_type")
	private String cashType;

	// 交易金额
	@Column(name = "tran_amount")
	private Integer tranAmount;

	@Column(name = "balance")
	private Integer balance;

	// 附加金额
	@Column(name = "extra_amount")
	private Integer extraAmount;
	// 输入金额
	@Column(name = "input_amount")
	private Integer inputAmount;

	// 卡号
	@Column(name = "card_no")
	private String cardNo;

	// 订单号
	@Column(name = "order_no")
	private String orderNo;

	// 卡包卡券实例id
	@Column(name = "ticket_info_id")
	private String ticketInfoId;

	// 关联交易撤销流水号
	@Column(name = "relate_tran_log_id")
	private String relateTranLogId;

	// 是否有效
	@Column(name = "canceled_flag")
	private boolean canceledFlag;

	// 操作员号
	@Column(name = "operator_id")
	private String operatorId;

	// 简单标记
	@Column(name = "tran_mark")
	private String tranMark;

	// 交易描述
	@Column(name = "tran_desc")
	private String tranDesc;

	// 卡券关联会员卡交易流水
	@Column(name = "ticket_tran_log_id")
	private String ticketTranLogId;

	// 查询关联交易流水号
	@Column(name = "master_tran_log_id")
	private String masterTranLogId;
	// 券号
	@Column(name = "ticket_no")
	private String ticketNo;

	//
	@Column(name = "offline_tran_log_id")
	private String offlineTranLogId;
	@Column(name = "dis_count")
	private Integer disCount;
	@Column(name = "cash_service_id")
	private String cashServiceId;
	@Column(name = "mixed_flag")
	private String mixedFlag;
	@Column(name = "mixed_tran_flag")
	private String mixedTranFlag;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getBalance() {
		return balance;
	}

	public void setBalance(Integer balance) {
		this.balance = balance;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public Date getTranTime() {
		return tranTime;
	}

	public void setTranTime(Date tranTime) {
		this.tranTime = tranTime;
	}

	public String getTranCode() {
		return tranCode;
	}

	public void setTranCode(String tranCode) {
		this.tranCode = tranCode;
	}

	public String getCashType() {
		return cashType;
	}

	public void setCashType(String cashType) {
		this.cashType = cashType;
	}

	public Integer getTranAmount() {
		return tranAmount;
	}

	public void setTranAmount(Integer tranAmount) {
		this.tranAmount = tranAmount;
	}

	public Integer getExtraAmount() {
		return extraAmount;
	}

	public void setExtraAmount(Integer extraAmount) {
		this.extraAmount = extraAmount;
	}

	public Integer getInputAmount() {
		return inputAmount;
	}

	public void setInputAmount(Integer inputAmount) {
		this.inputAmount = inputAmount;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getTicketInfoId() {
		return ticketInfoId;
	}

	public void setTicketInfoId(String ticketInfoId) {
		this.ticketInfoId = ticketInfoId;
	}

	public String getRelateTranLogId() {
		return relateTranLogId;
	}

	public void setRelateTranLogId(String relateTranLogId) {
		this.relateTranLogId = relateTranLogId;
	}

	public boolean isCanceledFlag() {
		return canceledFlag;
	}

	public void setCanceledFlag(boolean canceledFlag) {
		this.canceledFlag = canceledFlag;
	}

	public String getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(String operatorId) {
		this.operatorId = operatorId;
	}

	public String getTranMark() {
		return tranMark;
	}

	public void setTranMark(String tranMark) {
		this.tranMark = tranMark;
	}

	public String getTranDesc() {
		return tranDesc;
	}

	public void setTranDesc(String tranDesc) {
		this.tranDesc = tranDesc;
	}

	public String getTicketTranLogId() {
		return ticketTranLogId;
	}

	public void setTicketTranLogId(String ticketTranLogId) {
		this.ticketTranLogId = ticketTranLogId;
	}

	public String getMasterTranLogId() {
		return masterTranLogId;
	}

	public void setMasterTranLogId(String masterTranLogId) {
		this.masterTranLogId = masterTranLogId;
	}

	public String getTicketNo() {
		return ticketNo;
	}

	public void setTicketNo(String ticketNo) {
		this.ticketNo = ticketNo;
	}

	public String getOfflineTranLogId() {
		return offlineTranLogId;
	}

	public void setOfflineTranLogId(String offlineTranLogId) {
		this.offlineTranLogId = offlineTranLogId;
	}

	public Integer getDisCount() {
		return disCount;
	}

	public void setDisCount(Integer disCount) {
		this.disCount = disCount;
	}

	public String getCashServiceId() {
		return cashServiceId;
	}

	public void setCashServiceId(String cashServiceId) {
		this.cashServiceId = cashServiceId;
	}

	public String getMixedFlag() {
		return mixedFlag;
	}

	public void setMixedFlag(String mixedFlag) {
		this.mixedFlag = mixedFlag;
	}

	public String getMixedTranFlag() {
		return mixedTranFlag;
	}

	public void setMixedTranFlag(String mixedTranFlag) {
		this.mixedTranFlag = mixedTranFlag;
	}
}
