package cn.congine.wizarpos.mall.model.dzx;

import java.io.Serializable;

public class WeChartMemberRegisterInput implements Serializable {

	private static final long serialVersionUID = 1L;

	private String mobileNo = null;
	private String mid = null;
	private String userName = null;
	private String openId = null;

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}
}
