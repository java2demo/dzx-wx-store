package cn.congine.wizarpos.mall.dao;

import cn.congine.wizarpos.mall.model.SysTableKey;

public interface SysTableKeyDao extends GenericDao<SysTableKey> {

}
