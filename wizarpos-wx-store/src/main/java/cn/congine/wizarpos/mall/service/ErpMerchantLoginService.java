package cn.congine.wizarpos.mall.service;

import cn.congine.wizarpos.mall.model.ErpMerchantLogin;

public interface ErpMerchantLoginService {

	public ErpMerchantLogin save(ErpMerchantLogin obj);
	
	/**
	 * 统计 商户微店的访问人数
	 * 
	 * @param mid
	 * @return
	 */
	public Object count(String mid);
	

}
