package cn.congine.wizarpos.mall.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "erp_carter_order")
public class ErpCarterOrder extends BaseEntity {

	private static final long serialVersionUID = 1L;

	// 唯一标识
	@Id
	@Column(name = "id")
	private String id;

	// 慧商户号
	@Column(name = "mid")
	private String mid;

	@Column(name = "carter_sale_order_id")
	private String carterSaleOrderId;

	@Column(name = "order_id")
	private String orderId;

	@Transient
	private String showOrderId;
	
	@Transient
	private String showTable;
	
	public String getShowCreateTime() {
		return showCreateTime;
	}

	public void setShowCreateTime(String showCreateTime) {
		this.showCreateTime = showCreateTime;
	}

	@Transient
	private String showCreateTime;
	
	@Column(name = "table_id")
	private String tableId;

	@Column(name = "stage_id")
	private String stageId;

	@Column(name = "order_linker")
	private String orderLinker;

	@Column(name = "order_tel")
	private String orderTel;

	@Column(name = "create_time")
	private Date createTime;

	@Column(name = "status")
	private String status;

	@Column(name = "order_mans_num")
	private Integer orderMansNum;

	@Column(name = "order_date")
	private String orderDate;

	@Column(name = "order_amount")
	private String orderAmount;

	@Column(name = "audit_flag")
	private Integer auditFlag;

	@Column(name = "reject_reason")
	private String rejectReason;

	public String getShowTable() {
		return showTable;
	}

	public void setShowTable(String showTable) {
		this.showTable = showTable;
	}

	public String getShowOrderId() {
		return showOrderId;
	}

	public void setShowOrderId(String showOrderId) {
		this.showOrderId = showOrderId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getCarterSaleOrderId() {
		return carterSaleOrderId;
	}

	public void setCarterSaleOrderId(String carterSaleOrderId) {
		this.carterSaleOrderId = carterSaleOrderId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getTableId() {
		return tableId;
	}

	public void setTableId(String tableId) {
		this.tableId = tableId;
	}

	public String getStageId() {
		return stageId;
	}

	public void setStageId(String stageId) {
		this.stageId = stageId;
	}

	public String getOrderLinker() {
		return orderLinker;
	}

	public void setOrderLinker(String orderLinker) {
		this.orderLinker = orderLinker;
	}

	public String getOrderTel() {
		return orderTel;
	}

	public void setOrderTel(String orderTel) {
		this.orderTel = orderTel;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getOrderMansNum() {
		return orderMansNum;
	}

	public void setOrderMansNum(Integer orderMansNum) {
		this.orderMansNum = orderMansNum;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	public String getOrderAmount() {
		return orderAmount;
	}

	public void setOrderAmount(String orderAmount) {
		this.orderAmount = orderAmount;
	}

	public Integer getAuditFlag() {
		return auditFlag;
	}

	public void setAuditFlag(Integer auditFlag) {
		this.auditFlag = auditFlag;
	}

	public String getRejectReason() {
		return rejectReason;
	}

	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}

}
