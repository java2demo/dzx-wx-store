package cn.congine.wizarpos.mall.model.open;

import java.util.List;

public class AddressGainOutput extends CommonOutput {

	private static final long serialVersionUID = 1L;
	
	private List<Address> result = null;

	public List<Address> getResult() {
		return result;
	}

	public void setResult(List<Address> result) {
		this.result = result;
	}

}
