package cn.congine.wizarpos.mall.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "wx_shopping_cart")
public class WxShoppingCart extends BaseEntity {

	private static final long serialVersionUID = -8420734230394726289L;

	// 唯一标识
	@Id
	@Column(name = "id")
	private String id;

	// 微信OpenId
	@Column(name = "open_id")
	private String openId;

	// 慧商户号
	@Column(name = "mid")
	private String mid;

	// 入驻时间
	// @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Column(name = "create_time")
	private Long createTime;

	// 购物车类型
	@Column(name = "cart_type")
	private String cartType;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public Long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

	public String getCartType() {
		return cartType;
	}

	public void setCartType(String cartType) {
		this.cartType = cartType;
	}

}
