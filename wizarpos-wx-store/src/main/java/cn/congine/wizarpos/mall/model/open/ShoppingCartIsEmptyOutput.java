package cn.congine.wizarpos.mall.model.open;

/**
 * 
 * @author xudongdong
 * Date: 2015.7.4
 */


import java.util.List;

import cn.congine.wizarpos.mall.vo.WxShoppingCartItem;

public class ShoppingCartIsEmptyOutput extends CommonOutput {

	private static final long serialVersionUID = 1L;
	
	private String outCode = null;
	
	private String cartId = null;
	
	private List<WxShoppingCartItem> wxShoppingCartItemList = null;

	public String getOutCode() {
		return outCode;
	}

	public void setOutCode(String outCode) {
		this.outCode = outCode;
	}

	public String getCartId() {
		return cartId;
	}

	public void setCartId(String cartId) {
		this.cartId = cartId;
	}

	public List<WxShoppingCartItem> getWxShoppingCartItemList() {
		return wxShoppingCartItemList;
	}

	public void setWxShoppingCartItemList(
			List<WxShoppingCartItem> wxShoppingCartItemList) {
		this.wxShoppingCartItemList = wxShoppingCartItemList;
	}
}


