package cn.congine.wizarpos.mall.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "erp_product")
public class ErpProduct extends BaseEntity {
	private static final long serialVersionUID = -4309994218963920437L;

	// 唯一标识
	@Id
	@Column(name = "id")
	private String id;

	// 慧商户号
	@Column(name = "mid")
	private String mid;

	// 商品代码
	@Column(name = "code")
	private String code;

	// 商品品类
	@Column(name = "category_id")
	private String categoryId;

	// 条码
	@Column(name = "barcode")
	private String barcode;

	// 商品名称
	@Column(name = "name")
	private String name;

	// 商品规格
	@Column(name = "spec")
	private String spec;

	// 计量单位
	@Column(name = "unit")
	private String unit;

	// 商品价格
	@Column(name = "price")
	private String price;

	// 会员价格
	@Column(name = "member_price")
	private Integer memberPrice;

	// 进货价格
	@Column(name = "cost_price")
	private Integer costPrice;

	// 最小库存单位数量
	@Column(name = "min_stock_id")
	private String minStockId;

	// 最小库存换算数量
	@Column(name = "min_stock_conv")
	private Integer minStockConv;

	// 标识服务类项目
	@Column(name = "flag")
	private Integer flag;

	// 最后修改时间
	@Column(name = "last_time")
	private Long lastTime;

	// 上架标识
	@Column(name = "storage_rack_flag")
	private String storageRackFlag;

	// 是否热卖
	@Column(name = "hot_sale_flag")
	private String hotSaleFlag;

	// 盘点优先级 1 高 2中 3低
	@Column(name = "stock_take_level")
	private String stockTakeLevel;

	// 商品图片URL
	@Column(name = "pic_url")
	private String picUrl;

	@Column(name = "pic_page")
	private String picPage;
	// 商品图片Ids
	@Column(name = "pic_id")
	private String picId;

	// descn
	@Column(name = "descn")
	private String descn;

	// 今日新单 1为新单 0为不是新单
	@Column(name = "new_arrival")
	private String newArrival;

	//会员数
	@Transient
	private Integer stock = 0;
	
	//显示会员价
	@Transient
	private String showMemPrice;
	
	public String getShowMemPrice() {
		return showMemPrice;
	}

	public void setShowMemPrice(String showMemPrice) {
		this.showMemPrice = showMemPrice;
	}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

	public String getPicPage() {
		return picPage;
	}

	public void setPicPage(String picPage) {
		this.picPage = picPage;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSpec() {
		return spec;
	}

	public void setSpec(String spec) {
		this.spec = spec;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public Integer getMemberPrice() {
		return memberPrice;
	}

	public void setMemberPrice(Integer memberPrice) {
		this.memberPrice = memberPrice;
	}

	public Integer getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(Integer costPrice) {
		this.costPrice = costPrice;
	}

	public String getMinStockId() {
		return minStockId;
	}

	public void setMinStockId(String minStockId) {
		this.minStockId = minStockId;
	}

	public Integer getMinStockConv() {
		return minStockConv;
	}

	public void setMinStockConv(Integer minStockConv) {
		this.minStockConv = minStockConv;
	}

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}

	public Long getLastTime() {
		return lastTime;
	}

	public void setLastTime(Long lastTime) {
		this.lastTime = lastTime;
	}

	public String getStorageRackFlag() {
		return storageRackFlag;
	}

	public void setStorageRackFlag(String storageRackFlag) {
		this.storageRackFlag = storageRackFlag;
	}

	public String getHotSaleFlag() {
		return hotSaleFlag;
	}

	public void setHotSaleFlag(String hotSaleFlag) {
		this.hotSaleFlag = hotSaleFlag;
	}

	public String getStockTakeLevel() {
		return stockTakeLevel;
	}

	public void setStockTakeLevel(String stockTakeLevel) {
		this.stockTakeLevel = stockTakeLevel;
	}

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	public String getPicId() {
		return picId;
	}

	public void setPicId(String picId) {
		this.picId = picId;
	}

	public String getDescn() {
		return descn;
	}

	public void setDescn(String descn) {
		this.descn = descn;
	}

	public String getNewArrival() {
		return newArrival;
	}

	public void setNewArrival(String newArrival) {
		this.newArrival = newArrival;
	}

}
