package cn.congine.wizarpos.mall.dao;

import java.util.List;

import cn.congine.wizarpos.mall.common.Page;
import cn.congine.wizarpos.mall.model.PayTranLog;

public interface PayTranLogDao extends GenericDao<PayTranLog> {

	List<PayTranLog> getCardTranLogByCardNo(String mid, String openId,
			Page<PayTranLog> page);

}
