package cn.congine.wizarpos.mall.model.open;

/**
 * 
 * @author xudongdong
 * Date: 2015.7.6
 */


import java.util.List;

public class TicketListOutput extends CommonOutput {

	private static final long serialVersionUID = 1L;

	private int num;
	
	private String appId = null;
  
	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	private List<TicketInfoView> ticketList = null;
	
	public List<TicketInfoView> getTicketList() {
		return ticketList;
	}

	public void setTicketList(List<TicketInfoView> ticketList) {
		this.ticketList = ticketList;
	}

/*	private  List<Map<String, Object>> ticketList = null; 
	
	public List<Map<String, Object>> getTicketList() {
		return ticketList;
	}

	public void setTicketList(List<Map<String, Object>> ticketList) {
		this.ticketList = ticketList;
	}*/
}


