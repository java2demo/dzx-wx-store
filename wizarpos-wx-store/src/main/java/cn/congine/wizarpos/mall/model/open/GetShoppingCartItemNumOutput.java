package cn.congine.wizarpos.mall.model.open;

/**
 * 
 * @author xudongdong
 * Date: 2015.7.6
 */

import java.util.Map;

public class GetShoppingCartItemNumOutput extends CommonOutput {

	private static final long serialVersionUID = 1L;
	
	private String cartId = null;
	
	private int num;
	
	public String getCartId() {
		return cartId;
	}

	public void setCartId(String cartId) {
		this.cartId = cartId;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}
}


