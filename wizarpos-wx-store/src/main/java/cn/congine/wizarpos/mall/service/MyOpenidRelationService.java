package cn.congine.wizarpos.mall.service;

import cn.congine.wizarpos.mall.entity.MyOpenidRelation;

public interface MyOpenidRelationService {
	MyOpenidRelation getByMidAndDzxOpenid(String mid, String dzxOpenid);
}
