package cn.congine.wizarpos.mall.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.congine.wizarpos.mall.dao.SysWxPrimaryMerchantTokenDao;
import cn.congine.wizarpos.mall.model.SysWxPrimaryMerchantToken;
import cn.congine.wizarpos.mall.service.SysWxPrimaryMerchantTokenService;

@Service("sysWxPrimaryMerchantTokenService")
public class SysWxPrimaryMerchantTokenServiceImpl implements SysWxPrimaryMerchantTokenService {
	@Autowired
	private SysWxPrimaryMerchantTokenDao sysWxPrimaryMerchantTokenDao;

	@Override
	public SysWxPrimaryMerchantToken getByComponentAppid(String componentAppid) {
		return sysWxPrimaryMerchantTokenDao.getByComponentAppid(componentAppid);
	}

	@Override
	public int update(SysWxPrimaryMerchantToken wxComponentAccessInfo) {
		// TODO Auto-generated method stub
		return sysWxPrimaryMerchantTokenDao.update(wxComponentAccessInfo.getMid(),wxComponentAccessInfo.getComponentAppid(),wxComponentAccessInfo.getComponentAccessToken(),wxComponentAccessInfo.getComponentAccessTokenExpirey(),wxComponentAccessInfo.getComponentVerifyTicket());
	}
}
