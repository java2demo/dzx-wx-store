package cn.congine.wizarpos.mall.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.congine.wizarpos.mall.dao.MyRedEnvelopesDao;
import cn.congine.wizarpos.mall.entity.MyRedEnvelopes;
import cn.congine.wizarpos.mall.service.MyRedEnvelopesService;

@Service("myRedEnvelopesService")
public class MyRedEnvelopesServiceImpl implements MyRedEnvelopesService {

	@Autowired
	private MyRedEnvelopesDao myRedEnvelopesDao;

	@Override
	public MyRedEnvelopes get(String id) {
		// TODO Auto-generated method stub
		return myRedEnvelopesDao.get(id);
	}
	
	@Override
	public List<MyRedEnvelopes> getMyRedEnvelopesList(String mid, String onlineActivityId) {
		// TODO Auto-generated method stub
		return myRedEnvelopesDao.getMyRedEnvelopesList(mid, onlineActivityId);
	}

	@Override
	public MyRedEnvelopes getMyRedEnvelopes(String mid, String onlineActivityId) {
		// TODO Auto-generated method stub
		return myRedEnvelopesDao.getMyRedEnvelopes(mid, onlineActivityId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void update(MyRedEnvelopes object) {
		myRedEnvelopesDao.update(object);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void delete(MyRedEnvelopes object) {
		myRedEnvelopesDao.delete(object);
	}

	@Override
	public MyRedEnvelopes getByMidAndMyShakeId(String mid, String myShakeId) {
		return myRedEnvelopesDao.getByMidAndMyShakeId(mid, myShakeId);
	}

}
