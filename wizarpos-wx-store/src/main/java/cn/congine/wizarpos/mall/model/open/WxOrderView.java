package cn.congine.wizarpos.mall.model.open;

import java.io.Serializable;
import java.util.List;

import cn.congine.wizarpos.mall.model.WxOrderDetail;


public class WxOrderView implements Serializable {

	private static final long serialVersionUID = 1L;
	private String showOrderId;
	private String orderId;
	private String openId;
	private String mid;
	private String status;
	private String dispatchType;
	private int amount;//edit String to int xudondong
	private Long createTime;//edit String to int xudondong
	private String payStatus;
	//private List<ProductView> products;
	private List<WxOrderDetail> wxOrderDetail = null;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getShowOrderId() {
		return showOrderId;
	}

	public void setShowOrderId(String showOrderId) {
		this.showOrderId = showOrderId;
	}

	public String getDispatchType() {
		return dispatchType;
	}

	public void setDispatchType(String dispatchType) {
		this.dispatchType = dispatchType;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public Long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

	public String getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(String payStatus) {
		this.payStatus = payStatus;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<WxOrderDetail> getWxOrderDetail() {
		return wxOrderDetail;
	}

	public void setWxOrderDetail(List<WxOrderDetail> wxOrderDetail) {
		this.wxOrderDetail = wxOrderDetail;
	}

}
