package cn.congine.wizarpos.mall.service;

import java.util.List;

import cn.congine.wizarpos.mall.common.Page;
import cn.congine.wizarpos.mall.model.PayTranLog;

public interface PayTranLogService {
	void save(PayTranLog obj);

	List<PayTranLog> getCardTranLogByCardNo(String mid, String openId,
			Page<PayTranLog> page);

}
