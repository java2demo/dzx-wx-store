package cn.congine.wizarpos.mall.weChat.bussiness;

public class ActionResponse {

	private int code = 0;

	private String message = null;

	private Object data = null;

	public ActionResponse() {
	}

	public ActionResponse(int code, String message) {
		this.code = code;
		this.message = message;
	}

	public ActionResponse(int code, String message, Object data) {
		this.code = code;
		this.message = message;
		this.data = data;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

}
