package cn.congine.wizarpos.mall.model.open;

import cn.congine.wizarpos.mall.model.MrtIbeacon;

public class MrtIbeaconOutput extends CommonOutput {

	private static final long serialVersionUID = 1L;

	private MrtIbeacon result = null;

	public MrtIbeacon getResult() {
		return result;
	}

	public void setResult(MrtIbeacon mrtIbeacon) {
		this.result = mrtIbeacon;
	}
	
}
