package cn.congine.wizarpos.mall.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.congine.wizarpos.mall.dao.MyOpenidRelationDao;
import cn.congine.wizarpos.mall.entity.MyOpenidRelation;
import cn.congine.wizarpos.mall.service.MyOpenidRelationService;

@Service("MyOpenidRelationService")
public class MyOpenidRelationServiceImpl implements MyOpenidRelationService {

	@Autowired
	private MyOpenidRelationDao myOpenidRelationDao;
	
	@Override
	public MyOpenidRelation getByMidAndDzxOpenid(String mid, String dzxOpenid) {
		return myOpenidRelationDao.getByMidAndDzxOpenid(mid, dzxOpenid);
	}

}
