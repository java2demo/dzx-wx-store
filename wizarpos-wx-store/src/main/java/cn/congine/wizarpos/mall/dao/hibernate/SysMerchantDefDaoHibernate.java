package cn.congine.wizarpos.mall.dao.hibernate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.FlushMode;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import cn.congine.wizarpos.mall.common.Page;
import cn.congine.wizarpos.mall.dao.SysMerchantDefDao;
import cn.congine.wizarpos.mall.model.SysMerchantDef;

@Repository("sysMerchantDefDao")
public class SysMerchantDefDaoHibernate extends
		GenericDaoHibernate<SysMerchantDef> implements SysMerchantDefDao {

	public SysMerchantDefDaoHibernate() {
		super(SysMerchantDef.class);
	}

	@Override
	public SysMerchantDef getSysMerchantDefByMid(String mid) {
		Query query = getSession().createQuery(
				"from SysMerchantDef where mid = :mid and validFlag='0'").setParameter("mid",
				mid);
		return (SysMerchantDef) query.uniqueResult();
	}

	@Override
	public SysMerchantDef getSysMerchantDefByPid(String pid) {
		Query query = getSession().createQuery(
				"from SysMerchantDef where pOpenId = :pid and validFlag='0'").setParameter(
				"pid", pid);
		return (SysMerchantDef) query.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SysMerchantDef> getSysMerchantDefListByName(String merchantName) {
		String sql = "from SysMerchantDef where validFlag='0' order by startNo desc ";
		if (!StringUtils.isEmpty(merchantName))
			sql = " from SysMerchantDef where validFlag='0' and merchantName like :merchantName ";
		Query query = getSession().createQuery(sql);
		if (!StringUtils.isEmpty(merchantName))
			query.setParameter("merchantName", "%" + merchantName + "%");
		query.setFirstResult(0);
		query.setMaxResults(10);
		return (List<SysMerchantDef>) query.list();
	}

	@Override
	public List<SysMerchantDef> getMerchantDefListByName(String name,
			Page<SysMerchantDef> page) {
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = "from SysMerchantDef where validFlag='0' order by startNo desc ";
		if (!StringUtils.isEmpty(name)) {
			hql = " from SysMerchantDef where validFlag='0' and merchantName like '%" + name
					+ "%'";
		}
		page = this.readAll4Page(hql.toString(), "", params, page);
		return page.getList();
	}

	@Override
	public void updateAccess(SysMerchantDef m) {
		String sql = "Update sys_merchant_def set wx_access_token = ?, wx_access_token_timestamp = ? "
				+ "where mid = ? ";
		this.getSession().setFlushMode(FlushMode.MANUAL);
		Query q = getSession().createSQLQuery(sql);
		q.setString(0, m.getWxAccessToken());
		q.setLong(1, m.getWxAccessTokenTimestamp());
		q.setString(2, m.getMid());
		q.executeUpdate();
	}

	@Override
	public void updateStartNo(String mid, int startNo) {
		String hql = "update sys_merchant_def set startNo = ? where mid = ?";
		this.getSession().setFlushMode(FlushMode.MANUAL);
		Query query = this.getSession().createQuery(hql);
		query.setInteger(0, startNo);
		query.setString(1, mid);
		query.executeUpdate();
	}
}
