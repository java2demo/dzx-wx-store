package cn.congine.wizarpos.mall.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sys_wx_primary_merchant_token")
public class SysWxPrimaryMerchantToken extends BaseEntity {
	private static final long serialVersionUID = -4309994218963920437L;

	// 唯一标识
	@Id
	@Column(name = "id")
	private String id;

	// 慧商户号
	@Column(name = "mid")
	private String mid;
	
	// 第三方平台access_token
	@Column(name = "component_access_token")
	private String componentAccessToken;

	// 第三方平台推送票据
	@Column(name = "component_verify_ticket")
	private String componentVerifyTicket;

	// 第三方平台access_token有效期
	@Column(name = "component_access_token_expirey")
	private Long componentAccessTokenExpirey;

	// 预授权码
	@Column(name = "pre_auth_code")
	private String preAuthCode;

	// 预授权码有效期
	@Column(name = "pre_auth_code_expirey")
	private Long preAuthCodeExpirey;

	// 母商户app_id
	@Column(name = "component_appid")
	private String componentAppid;

	// 母商户app_secret
	@Column(name = "component_appsecret")
	private String componentAppsecret;

	// 公众号消息校验Token
	@Column(name = "check_token")
	private String checkToken;

	// 公众号消息加解密Key
	@Column(name = "encoding_aes_key")
	private String encodingAesKey;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getComponentAccessToken() {
		return componentAccessToken;
	}

	public void setComponentAccessToken(String componentAccessToken) {
		this.componentAccessToken = componentAccessToken;
	}

	public String getComponentVerifyTicket() {
		return componentVerifyTicket;
	}

	public void setComponentVerifyTicket(String componentVerifyTicket) {
		this.componentVerifyTicket = componentVerifyTicket;
	}

	public Long getComponentAccessTokenExpirey() {
		return componentAccessTokenExpirey;
	}

	public void setComponentAccessTokenExpirey(Long componentAccessTokenExpirey) {
		this.componentAccessTokenExpirey = componentAccessTokenExpirey;
	}

	public String getPreAuthCode() {
		return preAuthCode;
	}

	public void setPreAuthCode(String preAuthCode) {
		this.preAuthCode = preAuthCode;
	}

	public Long getPreAuthCodeExpirey() {
		return preAuthCodeExpirey;
	}

	public void setPreAuthCodeExpirey(Long preAuthCodeExpirey) {
		this.preAuthCodeExpirey = preAuthCodeExpirey;
	}

	public String getComponentAppid() {
		return componentAppid;
	}

	public void setComponentAppid(String componentAppid) {
		this.componentAppid = componentAppid;
	}

	public String getComponentAppsecret() {
		return componentAppsecret;
	}

	public void setComponentAppsecret(String componentAppsecret) {
		this.componentAppsecret = componentAppsecret;
	}

	public String getCheckToken() {
		return checkToken;
	}

	public void setCheckToken(String checkToken) {
		this.checkToken = checkToken;
	}

	public String getEncodingAesKey() {
		return encodingAesKey;
	}

	public void setEncodingAesKey(String encodingAesKey) {
		this.encodingAesKey = encodingAesKey;
	}

}
