package cn.congine.wizarpos.mall.vo;

import java.io.Serializable;

public class CateringReOrderView implements Serializable {

	private static final long serialVersionUID = 1L;
	private String orderId = null;
	private String showOrderId = null;
	private String amount = null;
	private String table = null;
	private String createTime = null;
	private String StageRange = null;
	private String status = null;
	private String type; //1:预订单及餐桌订单，2：外卖
	private String address; //外卖地址
	private String phone; //外卖电话
	private String name;//外卖姓名
	private String payStatus;//(0未支付 1 已支付)
	private String orderDate;
	private String carterType;

	public String getShowOrderId() {
		return showOrderId;
	}

	public void setShowOrderId(String showOrderId) {
		this.showOrderId = showOrderId;
	}

	public String getCarterType() {
		return carterType;
	}

	public void setCarterType(String carterType) {
		this.carterType = carterType;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getTable() {
		return table;
	}

	public void setTable(String table) {
		this.table = table;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getStageRange() {
		return StageRange;
	}

	public void setStageRange(String stageRange) {
		StageRange = stageRange;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(String payStatus) {
		this.payStatus = payStatus;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	
}
