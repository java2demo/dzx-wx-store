package cn.congine.wizarpos.mall.model.open;

/**
 * 
 * @author xudongdong
 * Date: 2015.7.8
 */

import java.io.Serializable;

public class CateringSaleOrderBindingInput extends CommonInput {

	private static final long serialVersionUID = 1L;
	
	private String mid = null;
	
	private String openId = null;
	
	private String saleOrderId = null;	 

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getSaleOrderId() {
		return saleOrderId;
	}

	public void setSaleOrderId(String saleOrderId) {
		this.saleOrderId = saleOrderId;
	}
	
}
