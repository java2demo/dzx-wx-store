package cn.congine.wizarpos.mall.dao;

import cn.congine.wizarpos.mall.model.SaleOrder;

public interface SaleOrderDao extends GenericDao<SaleOrder> {

	SaleOrder getById(String id);

	SaleOrder getByNo(String orderNo);

}
