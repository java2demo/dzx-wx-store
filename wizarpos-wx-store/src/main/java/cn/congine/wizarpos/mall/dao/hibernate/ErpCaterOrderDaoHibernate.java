package cn.congine.wizarpos.mall.dao.hibernate;

import java.util.Date;
import java.util.List;

import org.hibernate.FlushMode;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import cn.congine.wizarpos.mall.dao.ErpCaterOrderDao;
import cn.congine.wizarpos.mall.model.ErpCarterOrder;
import cn.congine.wizarpos.mall.utils.Function;

@Repository("erpCaterOrderDao")
public class ErpCaterOrderDaoHibernate extends
		GenericDaoHibernate<ErpCarterOrder> implements ErpCaterOrderDao {
	public ErpCaterOrderDaoHibernate() {
		super(ErpCarterOrder.class);
	}

	@Override
	public List<String> getYudingTableIdByDate(String mid, Date date) {
		String sql = "SELECT DISTINCT(table_id) FROM erp_carter_order WHERE order_date=? AND mid=?";
		Query query = getSession().createSQLQuery(sql);
		query.setDate(0, date);
		query.setString(1, mid);
		return query.list();
	}

	@Override
	public List<String> getYudingTableIdByDateStage(String mid, Date date,
			String stageId) {
		String sql = "SELECT DISTINCT(table_id) FROM erp_carter_order WHERE order_date=? AND stage_id=? AND mid=?";
		Query query = getSession().createSQLQuery(sql);
		query.setDate(0, date);
		query.setString(1, stageId);
		query.setString(2, mid);
		return query.list();
	}

	@Override
	public Object getReOrderId(String mid, Date date, String stageId,
			String tableId) {
		String sql = "SELECT id FROM erp_carter_order WHERE order_date=? AND stage_id=? AND mid=? and table_id=? and status='0' and audit_flag='1'";
		Query query = getSession().createSQLQuery(sql);
		query.setDate(0, date);
		query.setString(1, stageId);
		query.setString(2, mid);
		query.setString(3, tableId);
		List list = query.list();
		
		Object id = null;
		if (list != null && !list.isEmpty()) {
			id = list.get(0);
		}
		
		return id;
	}

	@Override
	public ErpCarterOrder getByIdMid(String orderId, String mid) {
		String sql = "FROM ErpCarterOrder WHERE id=? AND mid=?";
		Query query = getSession().createQuery(sql);
		query.setString(0, orderId);
		query.setString(1, mid);
		return (ErpCarterOrder) query.uniqueResult();
	}

	@Override
	public List<ErpCarterOrder> getByOpenidMid(String openId, String mid) {
		String sql = "FROM ErpCarterOrder WHERE openId=? AND mid=?";
		Query query = getSession().createQuery(sql);
		query.setString(0, openId);
		query.setString(1, mid);
		return (List<ErpCarterOrder>) query.list();
	}

	@Override
	public ErpCarterOrder save(ErpCarterOrder obj) {
		String uuid = Function.getUid();
		obj.setId(uuid);
		String now = Function.formatDate("yyyy-MM-dd HH:mm:ss");
		String sql = "INSERT INTO erp_carter_order(id,mid,carter_sale_order_id,order_id,table_id,stage_id,order_linker,"
				+ "order_tel,create_time,status,order_mans_num,order_date,order_amount,audit_flag,reject_reason)"
				+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		this.getSession().setFlushMode(FlushMode.MANUAL);

		Query q = getSession().createSQLQuery(sql);

		q.setString(0, uuid);
		q.setString(1, obj.getMid());
		q.setString(2, obj.getCarterSaleOrderId());
		q.setString(3, obj.getOrderId());
		q.setString(4, obj.getTableId());
		q.setString(5, obj.getStageId());
		q.setString(6, obj.getOrderLinker());
		q.setString(7, obj.getOrderTel());
		q.setString(8, now);
		q.setString(9, obj.getStatus());
		q.setInteger(10, obj.getOrderMansNum());
		q.setString(11, obj.getOrderDate());
		q.setString(12, obj.getOrderAmount());
		q.setInteger(13, obj.getAuditFlag());
		q.setString(14, obj.getRejectReason());

		q.executeUpdate();
		return obj;
	}

	@Override
	public void update(ErpCarterOrder obj) {
		String sql = "UPDATE erp_carter_order SET carter_sale_order_id = ?, order_id = ?, table_id = ?,"
				+ " stage_id = ?, order_linker = ?, order_tel = ?, status = ?, order_mans_num = ?,"
				+ " order_date = ?, order_amount = ?, audit_flag = ?, reject_reason = ?"
				+ " WHERE id = ? and mid = ?";

		this.getSession().setFlushMode(FlushMode.MANUAL);

		Query q = getSession().createSQLQuery(sql);

		q.setString(0, obj.getCarterSaleOrderId());
		q.setString(1, obj.getOrderId());
		q.setString(2, obj.getTableId());
		q.setString(3, obj.getStageId());
		q.setString(4, obj.getOrderLinker());
		q.setString(5, obj.getOrderTel());
		q.setString(6, obj.getStatus());
		q.setInteger(7, obj.getOrderMansNum());
		q.setString(8, obj.getOrderDate());
		q.setString(9, obj.getOrderAmount());
		q.setInteger(10, obj.getAuditFlag());
		q.setString(11, obj.getRejectReason());
		q.setString(12, obj.getId());
		q.setString(13, obj.getMid());
		
		q.executeUpdate();
	}

	@Override
	public ErpCarterOrder find(String id, String mid) {
		return (ErpCarterOrder) getSession()
				.createQuery(
						"from ErpCarterOrder b where b.mid=:mid and b.id=:id")
				.setString("mid", mid).setString("id", id).uniqueResult();
	}
}
