package cn.congine.wizarpos.mall.dao.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import cn.congine.wizarpos.mall.dao.SaleOrderItemDao;
import cn.congine.wizarpos.mall.model.SaleOrderItem;

@Repository("saleOrderItemDao")
public class SaleOrderItemDaoHibernate extends
		GenericDaoHibernate<SaleOrderItem> implements SaleOrderItemDao {

	public SaleOrderItemDaoHibernate() {
		super(SaleOrderItem.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SaleOrderItem> getSaleOrderItemByOrderId(String orderId) {
		Query query = getSession().createQuery(
				"from SaleOrderItem where orderId = :orderId").setParameter(
				"orderId", orderId);
		return (List<SaleOrderItem>) query.list();
	}

}
