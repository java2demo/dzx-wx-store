package cn.congine.wizarpos.mall.model.open;

/**
 * 
 * @author xudongdong
 * Date: 2015.7.8
 */

import java.io.Serializable;

public class CateringSaleOrderDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private String orderId = null;
	private String showOrderId = null;
	private String amount = null;
	private String tableNo = null;
	private String table = null;
	private String createTime = null;
	private String status = null;
	private String address; //外卖地址
	private String phone; //外卖电话
	private String name;//外卖姓名
	private String payStatus;//(0未支付 1 已支付)
	private String carterType;

	public String getShowOrderId() {
		return showOrderId;
	}

	public void setShowOrderId(String showOrderId) {
		this.showOrderId = showOrderId;
	}

	public String getCarterType() {
		return carterType;
	}

	public void setCarterType(String carterType) {
		this.carterType = carterType;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	public String getTableNo() {
		return tableNo;
	}

	public void setTableNo(String tableNo) {
		this.tableNo = tableNo;
	}
	
	public String getTable() {
		return table;
	}

	public void setTable(String table) {
		this.table = table;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(String payStatus) {
		this.payStatus = payStatus;
	}	
}
