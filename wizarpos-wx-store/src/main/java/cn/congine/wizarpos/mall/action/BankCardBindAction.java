package cn.congine.wizarpos.mall.action;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.congine.wizarpos.mall.model.open.WeChartMemberBankCardBindInput;
import cn.congine.wizarpos.mall.model.open.WeChartMemberBankCardBindOutput;
import cn.congine.wizarpos.mall.model.open.WeChartMemberBankCardListInput;
import cn.congine.wizarpos.mall.model.open.WeChartMemberBankCardListOutput;
import cn.congine.wizarpos.mall.service.impl.WizarposOpenRestClient;

@Controller
@RequestMapping(value = "/bankCardBind")
public class BankCardBindAction {
	private final Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private WizarposOpenRestClient wizarposOpenRestClient;

	/**
	 * 会员银行卡列表
	 * 
	 * @param openId
	 * @param mid
	 * @return
	 */
	@RequestMapping(value = "/list")
	public ModelAndView bankCardList(
			@RequestParam(value = "openId", required = true) String openId,
			@RequestParam(value = "mid", required = true) String mid) {
		WeChartMemberBankCardListInput input = new WeChartMemberBankCardListInput();
		input.setMid(mid);
		input.setOpenId(openId);
		WeChartMemberBankCardListOutput output = (WeChartMemberBankCardListOutput) wizarposOpenRestClient
				.post(input, "/wechart/bankcard/list",
						WeChartMemberBankCardListOutput.class);

		return new ModelAndView("card/bankcardlist").addObject("mid", mid)
				.addObject("openId", openId)
				.addObject("list", output.getResult());
	}
	
	/**
	 * 解绑已绑定的银行卡
	 * 
	 * @param openId
	 * @param mid
	 * @return
	 */
	@RequestMapping(value = "/unBind")
	public @ResponseBody String unBind(
			@RequestParam(value = "openId", required = true) String openId,
			@RequestParam(value = "mid", required = true) String mid,
			@RequestParam(value = "id", required = true) String id) {

		WeChartMemberBankCardBindInput input = new WeChartMemberBankCardBindInput();
		input.setOpenId(openId);		
		input.setId(id);
		input.setOperate("1");
		WeChartMemberBankCardBindOutput output = (WeChartMemberBankCardBindOutput) wizarposOpenRestClient.post(input, "/wechart/bankcard/update",WeChartMemberBankCardBindOutput.class);		
		if (output != null && "0".equals(output.getCode())) {//操作成功
			return "0";
		} else if (output != null && "60048".equals(output.getCode())) {//银行卡未找到，解绑失败
			return "1";
		} else {//系统错误
			return "2";
		}
	}
}
