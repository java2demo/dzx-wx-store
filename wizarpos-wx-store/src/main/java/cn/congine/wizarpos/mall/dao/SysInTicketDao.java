package cn.congine.wizarpos.mall.dao;

import cn.congine.wizarpos.mall.model.SysInTicket;

public interface SysInTicketDao extends GenericDao<SysInTicket> {
	
	SysInTicket getSysInTicket(String apiSymbol, String mid);

	SysInTicket find(String id, String mid);

	void del(SysInTicket obj);
}
