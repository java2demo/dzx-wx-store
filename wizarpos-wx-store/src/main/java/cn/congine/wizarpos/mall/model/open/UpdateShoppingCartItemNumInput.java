package cn.congine.wizarpos.mall.model.open;

/**
 * 
 * @author xudongdong
 * Date: 2015.7.4
 */

public class UpdateShoppingCartItemNumInput extends CommonInput {

	private static final long serialVersionUID = 1L;
	
	private String itemId = null;
	
	private String num = null;

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}	
}
