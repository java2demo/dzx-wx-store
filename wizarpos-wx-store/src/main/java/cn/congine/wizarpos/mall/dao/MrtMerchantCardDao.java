package cn.congine.wizarpos.mall.dao;

import java.util.List;

import cn.congine.wizarpos.mall.model.MrtMerchantCard;

public interface MrtMerchantCardDao extends GenericDao<MrtMerchantCard> {
	MrtMerchantCard getMrtMerchantCardByCardNo(String cardNo);

	List<MrtMerchantCard> getMrtMerchantCardListByOpenId(String openId);

	List<MrtMerchantCard> getMrtMerchantCardList(String openId);

	List<MrtMerchantCard> getMrtMerchantCardByMid(String mid);

	MrtMerchantCard getMrtMerchantCard(String mid, String openId);

	MrtMerchantCard getMrtMerchantCardByCardId(String id);

	int getMemberNum(String mid);

	MrtMerchantCard find(String id, String mid);

}
