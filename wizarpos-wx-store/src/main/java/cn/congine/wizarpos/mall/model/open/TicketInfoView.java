package cn.congine.wizarpos.mall.model.open;

/**
 * 
 * @author xudongdong
 * Date: 2015.7.9
 */

import java.io.Serializable;

public class TicketInfoView implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String id = null;
	private String ticketId = null;
	private String ticketNo = null;
	private String ticketName = null;
	private int num;	
	private String balance = null;
	private String expriyTime = null;
	private String descn = null;
	private String wxFlag = null;
	private String validFlag = null;
	private String wxCodeId = null;
	private String wxAdded = null;
	
	public String getTicketId() {
		return ticketId;
	}
	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}
	public String getTicketNo() {
		return ticketNo;
	}
	public void setTicketNo(String ticketNo) {
		this.ticketNo = ticketNo;
	}
	public String getTicketName() {
		return ticketName;
	}
	public void setTicketName(String ticketName) {
		this.ticketName = ticketName;
	}

	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public String getBalance() {
		return balance;
	}
	public void setBalance(String balance) {
		this.balance = balance;
	}
	public String getExpriyTime() {
		return expriyTime;
	}
	public void setExpriyTime(String expriyTime) {
		this.expriyTime = expriyTime;
	}
	public String getDescn() {
		return descn;
	}
	public void setDescn(String descn) {
		this.descn = descn;
	}
	public String getWxFlag() {
		return wxFlag;
	}
	public void setWxFlag(String wxFlag) {
		this.wxFlag = wxFlag;
	}
	public String getValidFlag() {
		return validFlag;
	}
	public void setValidFlag(String validFlag) {
		this.validFlag = validFlag;
	}
	public String getWxCodeId() {
		return wxCodeId;
	}
	public void setWxCodeId(String wxCodeId) {
		this.wxCodeId = wxCodeId;
	}
	public String getWxAdded() {
		return wxAdded;
	}
	public void setWxAdded(String wxAdded) {
		this.wxAdded = wxAdded;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	
}