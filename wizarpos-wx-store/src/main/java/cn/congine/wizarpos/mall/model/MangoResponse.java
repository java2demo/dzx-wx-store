package cn.congine.wizarpos.mall.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

/**
 * Represent the XML response message that responded from external web service.
 * 
 * @author Rime
 * 
 */
@XStreamAlias("reponse")
public class MangoResponse implements Serializable {
	private static final long serialVersionUID = -1447084908508402125L;

	/**
	 * Content type of response message. <br/>
	 * Supported types: Text(文本), News(图文), URL(链接), Image(图片), Voice(语音),
	 * Music(音乐), Video(视频)
	 * 
	 * @author Rime
	 * 
	 */
	public enum Type {
		Text("文本"), News("图文"), URL("链接"), Image("图片"), Voice("语音"), Music("音乐"), Video(
				"视频");
		private String label;

		private Type(String label) {
			this.label = label;
		}

		public String getLabel() {
			return label;
		}
	}

	@XStreamAsAttribute
	private String openID;

	@XStreamAsAttribute
	private Type type = Type.Text;

	@XStreamAlias("content")
	private String content;

	@XStreamImplicit(itemFieldName = "article")
	private List<Article> articles = new ArrayList<Article>();

	protected MangoResponse() {
	}

	public MangoResponse(String openID) {
		this.openID = openID;
	}

	/**
	 * Build a simple text message
	 * 
	 * @param openID
	 *            Open ID of the receiver
	 * @param message
	 *            Message content.
	 */
	public MangoResponse(String openID, String message) {
		this(openID, Type.Text, message);
	}

	/**
	 * Build a response message.
	 * 
	 * @param openID
	 *            openID Open ID of the receiver
	 * @param type
	 *            Indicate the type of message
	 * @param content
	 *            Response content
	 */
	public MangoResponse(String openID, Type type, String content) {
		this.openID = openID;
		this.type = type;
		this.content = content;
		if (!articles.isEmpty()) {
			articles.clear();
		}
	}

	/**
	 * Add article to a news message
	 * 
	 * @param title
	 *            Title of article
	 * @param imageURL
	 *            Image URL of article
	 * @param contentURL
	 *            Content URL of article
	 */
	public void addArticle(String title, String imageURL, String contentURL) {
		this.type = Type.News;
		articles.add(new Article(title, imageURL, contentURL));
		content = null;
	}

	public void addArticle(String title, String description, String imageURL,
			String contentURL) {
		this.type = Type.News;
		articles.add(new Article(title, description, imageURL, contentURL));
		content = null;
	}

	/**
	 * Represent who will receive this message.
	 * 
	 * @return
	 */
	public String getOpenID() {
		return openID;
	}

	public void setOpenID(String openID) {
		this.openID = openID;
	}

	/**
	 * Content type
	 * 
	 * @return
	 */
	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	/**
	 * Content
	 * 
	 * @return
	 */
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * Valid when content type is news. Represents the news articles.
	 * 
	 * @return
	 */
	public List<Article> getArticles() {
		return articles;
	}

	@XStreamAlias("item")
	public static class Article {
		@XStreamAlias("title")
		private String title;

		@XStreamAlias("desc")
		private String description;

		@XStreamAlias("image")
		private String imageURL;

		@XStreamAlias("url")
		private String contentURL;

		public Article() {
		}

		public Article(String title, String imageURL, String contentURL) {
			this();
			this.title = title;
			this.imageURL = imageURL;
			this.contentURL = contentURL;
		}

		public Article(String title, String description, String imageURL,
				String contentURL) {
			this(title, imageURL, contentURL);
			this.description = description;
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getImageURL() {
			return imageURL;
		}

		public void setImageURL(String imageURL) {
			this.imageURL = imageURL;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public String getContentURL() {
			return contentURL;
		}

		public void setContentURL(String contentURL) {
			this.contentURL = contentURL;
		}
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("MangoResponse [openID=" + openID + ", type=" + type + "]");
		if (content != null) {
			sb.append(" Content: " + content);
		} else {
			sb.append(" Articles: " + articles.size());
		}
		return sb.toString();
	}
}
