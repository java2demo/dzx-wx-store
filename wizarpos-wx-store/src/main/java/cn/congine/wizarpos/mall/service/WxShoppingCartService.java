package cn.congine.wizarpos.mall.service;

import java.util.List;

import cn.congine.wizarpos.mall.model.WxShoppingCart;

public interface WxShoppingCartService {

	List<WxShoppingCart> getWxShoppingCartByOpenId(String openId);

	void save(WxShoppingCart obj);

	void update(WxShoppingCart obj);

	WxShoppingCart getWxShoppingCart(String openId, String mid);

	WxShoppingCart getWxShoppingCartByMId(String mid);

	void remove(String id, String mid);
}