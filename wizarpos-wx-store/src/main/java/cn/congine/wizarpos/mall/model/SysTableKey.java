package cn.congine.wizarpos.mall.model;

// Generated 2013-12-16 10:43:18 by Hibernate Tools 3.4.0.CR1

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import cn.congine.wizarpos.mall.model.BaseEntity;

/**
 * TicketDef generated by hbm2java
 */
@Entity
@Table(name = "sys_table_key")
public class SysTableKey extends BaseEntity {

	private static final long serialVersionUID = 1L;
	private String keyName;
	private Integer curNo;
	private Date updateTime;
	private Date createTime;

	public SysTableKey() {
	}

	@Id
	@Column(name = "key_name", unique = true, nullable = false, length = 100)
	public String getKeyName() {
		return this.keyName;
	}

	public void setKeyName(String keyName) {
		this.keyName = keyName;
	}

	@Column(name = "cur_no")
	public Integer getCurNo() {
		return curNo;
	}

	public void setCurNo(Integer curNo) {
		this.curNo = curNo;
	}

	@Column(name = "update_time")
	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	@Column(name = "create_time")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
}
