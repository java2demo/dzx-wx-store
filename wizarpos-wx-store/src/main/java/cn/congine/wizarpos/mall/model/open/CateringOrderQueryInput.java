package cn.congine.wizarpos.mall.model.open;

import java.io.Serializable;

public class CateringOrderQueryInput extends CommonInput {

	private static final long serialVersionUID = 1L;
	private String mid = null;
	private String reOrderId = null;
	private String phone = null;
	private String date = null;
	
	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getReOrderId() {
		return reOrderId;
	}

	public void setReOrderId(String reOrderId) {
		this.reOrderId = reOrderId;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
}
