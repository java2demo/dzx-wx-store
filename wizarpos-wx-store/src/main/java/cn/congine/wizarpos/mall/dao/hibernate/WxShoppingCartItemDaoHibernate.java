package cn.congine.wizarpos.mall.dao.hibernate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.FlushMode;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import cn.congine.wizarpos.mall.dao.WxShoppingCartItemDao;
import cn.congine.wizarpos.mall.model.WxShoppingCartItem;
import cn.congine.wizarpos.mall.utils.Function;

@Repository
public class WxShoppingCartItemDaoHibernate extends
		GenericDaoHibernate<WxShoppingCartItem> implements
		WxShoppingCartItemDao {

	public WxShoppingCartItemDaoHibernate() {
		super(WxShoppingCartItem.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<WxShoppingCartItem> getWxShoppingCartItemListByCartId(
			String cartId) {
		Query query = getSession().createQuery(
				"from WxShoppingCartItem where cartId=:cartId ").setParameter(
				"cartId", cartId);
		return (List<WxShoppingCartItem>) query.list();
	}

	@Override
	public WxShoppingCartItem getWxShoppingCartItem(String productId,
			String cartId) {
		Query query = getSession()
				.createQuery(
						"from WxShoppingCartItem where productId=:productId and cartId=:cartId");
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("productId", productId);
		param.put("cartId", cartId);
		query.setProperties(param);
		return (WxShoppingCartItem) query.uniqueResult();
	}

	@Override
	public void removeByCartId(String mid, String cartId) {
		String hql = "delete From WxShoppingCartItem where mid = ? and cartId = ?";
		this.getSession().setFlushMode(FlushMode.MANUAL);
		Query q = getSession().createQuery(hql);
		q.setString(0, mid);
		q.setString(1, cartId);
		q.executeUpdate();
	}

	@Override
	public WxShoppingCartItem getWxShoppingCartItemByOpenId(String openId) {
		Query query = getSession().createQuery(
				"from WxShoppingCartItem where openId=:openId ").setParameter(
				"openId", openId);
		return (WxShoppingCartItem) query.uniqueResult();
	}

	@Override
	public void addShoppingCartItem(WxShoppingCartItem items) {
		save(items);
	}

	@Override
	public Boolean updateNum(Integer num, String cartId, String productId, String mid) {
		String sql = "update wx_shopping_cart_item set product_num= ? where cart_id= ? and product_id = ? and mid = ?";
		
		this.getSession().setFlushMode(FlushMode.MANUAL);
		Query query = this.getSession().createSQLQuery(sql);
		
		query.setInteger(0, num);
		query.setString(1, cartId);
		query.setString(2, productId);
		query.setString(3, mid);
		
		return query.executeUpdate() > 0;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<WxShoppingCartItem> getCarList(String cartId) {
		String sql = "select * from wx_shopping_cart_items where cart_id='"
				+ cartId + "";
		Query query = this.getSession().createSQLQuery(sql);
		return query.list();
	}

	@Override
	public Boolean checkItemsExist(String productId, String cartId) {
		String sql = "select id from wx_shopping_cart_item  where product_id='"
				+ productId + "' and cart_id='" + cartId + "'";
		Query query = this.getSession().createSQLQuery(sql);
		if (query.list().size() > 0) {
			return true;
		}
		return false;
	}

	@Override
	public Integer getItemsNum(String cartId) {
		String sql = "select * from wx_shopping_cart_item where cart_id='"
				+ cartId + "'";
		Query query = this.getSession().createSQLQuery(sql);
		Integer res = 0;
		if (query.list().size() > 0) {
			res = query.list().size();
		}

		return res;
	}

	@Override
	public int getItemNum(String cartId, String productId) {
		int res = 0;
		String sql = "select product_num from wx_shopping_cart_item where cart_id='"
				+ cartId + "' and product_id='" + productId + "'";
		Query query = this.getSession().createSQLQuery(sql);
		if ((query.uniqueResult()) != null) {
			res = (int) query.uniqueResult();
		}
		return res;
	}

	@Override
	public WxShoppingCartItem save(WxShoppingCartItem obj) {
		String uuid = Function.getUid();
		obj.setId(uuid);
		String sql = "INSERT INTO wx_shopping_cart_item (id,mid,cart_id,product_id,product_num,product_price)"
				+ " VALUES (?,?,?,?,?,?)";
		this.getSession().setFlushMode(FlushMode.MANUAL);
		Query query = this.getSession().createSQLQuery(sql);
		query.setString(0, uuid);
		query.setString(1, obj.getMid());
		query.setString(2, obj.getCartId());
		query.setString(3, obj.getProductId());
		query.setInteger(4, obj.getProductNum());
		query.setInteger(5, obj.getProductPrice());
		query.executeUpdate();
		return obj;
	}

	@Override
	public void update(WxShoppingCartItem obj) {
		String sql = "UPDATE wx_shopping_cart_item SET cart_id = ?, product_id = ?,"
				+ " product_num = ?, product_price = ? WHERE id = ? and mid = ?";
		this.getSession().setFlushMode(FlushMode.MANUAL);
		Query query = this.getSession().createSQLQuery(sql);
		
		query.setString(0, obj.getCartId());
		query.setString(1, obj.getProductId());
		query.setInteger(2, obj.getProductNum());
		query.setInteger(3, obj.getProductPrice());
		query.setString(4, obj.getId());
		query.setString(5, obj.getMid());
		
		query.executeUpdate();
	}

	@Override
	public void remove(String id, String mid) {
		String hql = "delete From WxShoppingCartItem where id = ? and mid = ?";
		this.getSession().setFlushMode(FlushMode.MANUAL);
		Query q = getSession().createQuery(hql);
		q.setString(0, id);
		q.setString(1, mid);
		q.executeUpdate();
	}
}
