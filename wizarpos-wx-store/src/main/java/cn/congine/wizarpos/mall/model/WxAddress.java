package cn.congine.wizarpos.mall.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "wx_address")
public class WxAddress extends BaseEntity {
	private static final long serialVersionUID = -2806445711598802536L;

	// 唯一标识
	@Id
	@Column(name = "id")
	private String id;
	
	@Column(name = "mid")
	private String mid;
	
	// 会员卡关联编号
	@Column(name = "card_id")
	private String cardId;

	// 地址
	@Column(name = "address")
	private String address;

	// 姓名(收货人)
	@Column(name = "username")
	private String username;

	// 联系方式
	@Column(name = "phone")
	private String phone;

	// 默认收货地址 0 为不是 1为是
	@Column(name = "default_state")
	private String defaultState;
	// 邮政编码
	@Column(name = "zip_code")
	private String zipCode;

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String userName) {
		this.username = userName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getDefaultState() {
		return defaultState;
	}

	public void setDefaultState(String defaultState) {
		this.defaultState = defaultState;
	}

}
