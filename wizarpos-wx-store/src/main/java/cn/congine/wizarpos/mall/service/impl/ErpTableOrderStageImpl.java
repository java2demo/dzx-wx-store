package cn.congine.wizarpos.mall.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.congine.wizarpos.mall.dao.ErpTableOrderStageDao;
import cn.congine.wizarpos.mall.model.ErpTableOrderStage;
import cn.congine.wizarpos.mall.service.ErpTableOrderStageService;

@Service("erpTableOrderStageService")
public class ErpTableOrderStageImpl implements ErpTableOrderStageService {

	@Autowired
	private ErpTableOrderStageDao erpTableOrderStageDao;

	@Override
	public List<ErpTableOrderStage> getAll(String mid) {
		return erpTableOrderStageDao.getAll(mid);
	}

	@Override
	public Object getCurrentStage(String mid) {
		return erpTableOrderStageDao.getCurrentStage(mid);
	}

	@Override
	public ErpTableOrderStage getByIdMid(String stageId, String mid) {
		return erpTableOrderStageDao.getByIdMid(stageId, mid);
	}

}
