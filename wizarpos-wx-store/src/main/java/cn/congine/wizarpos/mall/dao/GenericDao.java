/**
 * Copyright (c) 2011 Congine Information Technology Corporation, All rights reserved.
 * The Software is owned by Congine and is protected by copyright laws and other national laws.
 * You agree that you have no right, title or interest in the Software.
 * This code cannot simply be copied and put under another distribution license.
 * Copyright remains Congine's, and as such any Copyright notices in the code are not to be removed.
 */
package cn.congine.wizarpos.mall.dao;

import java.util.List;

import cn.congine.wizarpos.mall.model.BaseEntity;

/**
 * Generic DAO (Data Access Object) with common methods to CRUD POJOs.
 * 
 * Extend this interface if you want typesafe (no casting necessary) DAO's for
 * your domain objects.
 * 
 * @param <T>
 *            a type variable
 * @author <a href="mailto:lizhen@congine.cn">Rime Lee</a>
 */
public interface GenericDao<T extends BaseEntity> {

	/**
	 * Generic method used to get all objects of a particular type. This is the
	 * same as lookup up all rows in a table.
	 * 
	 * @return List of populated objects
	 */
	List<T> getAll();

	/**
	 * Gets all records without duplicates.
	 * <p>
	 * Note that if you use this method, it is imperative that your model
	 * classes correctly implement the hashcode/equals methods
	 * </p>
	 * 
	 * @return List of populated objects
	 */
	List<T> getAllDistinct();

	/**
	 * Generic method to refresh object.
	 * 
	 * @param object
	 * @return
	 */
	T refresh(T object);

	/**
	 * Generic method to get an object based on class and identifier. Null is
	 * returned if nothing is found.
	 * 
	 * @param id
	 *            the identifier (primary key) of the object to get
	 * @return a populated object
	 */
	T get(String id);

	/**
	 * Checks for existence of an object of type T using the id arg.
	 * 
	 * @param id
	 *            the id of the entity
	 * @return - true if it exists, false if it doesn't
	 */
	boolean exists(String id);

	/**
	 * Generic method to save an object - handles both update and insert.
	 * 
	 * @param object
	 *            the object to save
	 * @return the persisted object
	 */
	T save(T object);

	/**
	 * Generic method to delete an object based on class and id
	 * 
	 * @param id
	 *            the identifier (primary key) of the object to remove
	 */
	void remove(String id);

	/**
	 * Generic method to delete an object
	 * 
	 * @param object
	 *            the object to remove
	 */
	void remove(T object);

	void removeAll(List<T> list);

	void update(T object);
}