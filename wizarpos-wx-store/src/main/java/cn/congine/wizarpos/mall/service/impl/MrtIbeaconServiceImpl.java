package cn.congine.wizarpos.mall.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.congine.wizarpos.mall.dao.MrtIbeaconDao;
import cn.congine.wizarpos.mall.model.MrtIbeacon;
import cn.congine.wizarpos.mall.service.MrtIbeaconService;

@Service("mrtIbeaconService")
public class MrtIbeaconServiceImpl implements MrtIbeaconService {

	@Autowired
	private MrtIbeaconDao mrtIbeaconDao;

	@Override
	public MrtIbeacon getActive(String mid) {
		return mrtIbeaconDao.getActive(mid);
	}

}
