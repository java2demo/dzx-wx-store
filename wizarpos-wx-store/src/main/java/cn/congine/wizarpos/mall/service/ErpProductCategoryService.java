package cn.congine.wizarpos.mall.service;

import java.util.List;

import cn.congine.wizarpos.mall.model.ErpProductCategory;

public interface ErpProductCategoryService {

	List<ErpProductCategory> getErpProductCategory(String mid);

	ErpProductCategory getErpProductCategoryByCode(String mid, String categoryCode);

	List<ErpProductCategory> getErpProductCategoryByParentId(String mid,
			String parentId);

}
