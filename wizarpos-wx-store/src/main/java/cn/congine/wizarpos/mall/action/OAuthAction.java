package cn.congine.wizarpos.mall.action;

import java.util.Arrays;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import cn.congine.wizarpos.mall.model.MrtIbeacon;
import cn.congine.wizarpos.mall.model.MrtTicketDef;
import cn.congine.wizarpos.mall.model.MrtWechartUserInfo;
import cn.congine.wizarpos.mall.model.open.AccessTokenInput;
import cn.congine.wizarpos.mall.model.open.AccessTokenOutput;
import cn.congine.wizarpos.mall.model.open.AccessTokenOutput.Result;
import cn.congine.wizarpos.mall.model.open.MrtIbeaconInput;
import cn.congine.wizarpos.mall.model.open.MrtIbeaconOutput;
import cn.congine.wizarpos.mall.model.open.MrtWechartUserInfoInput;
import cn.congine.wizarpos.mall.model.open.MrtWechartUserInfoOutput;
import cn.congine.wizarpos.mall.model.open.MrtWechartUserInfoUpdateInput;
import cn.congine.wizarpos.mall.model.open.MrtWechartUserInfoUpdateOutput;
import cn.congine.wizarpos.mall.model.open.TicketDefInfoInput;
import cn.congine.wizarpos.mall.model.open.TicketDefInfoOutput;
import cn.congine.wizarpos.mall.service.impl.WPosHttpClient;
import cn.congine.wizarpos.mall.service.impl.WizarposOpenRestClient;
import cn.congine.wizarpos.mall.service.impl.WxHttpClient;
import cn.congine.wizarpos.mall.utils.SysConstants;

import com.alibaba.fastjson.JSONObject;

@Controller
@RequestMapping(value = "/oauth")
public class OAuthAction {

	private final Log log = LogFactory.getLog(getClass());

	@Resource(name = "messageSource")
	private MessageSource messageSource;

	@Autowired
	private WxHttpClient wxHttpClient;

	@Autowired
	private WPosHttpClient wposHttpClient;
	
	@Autowired
	private WizarposOpenRestClient wizarposOpenRestClient;

	// 单张券
	public static final String SIGLE = "A";

	// 多张券
	public static final String MULTIPLE = "B";

	// 广告投放
	public static final String ADVERTISEMENT = "C";

	// 未关注
	public static final String UNSUBSCRIBE = "0";

	/**
	 * 货架
	 * 
	 * @param code
	 * @param state
	 * @return
	 */
	@RequestMapping(value = "/shelf")
	public String index(@RequestParam(value = "code") String code,
			@RequestParam(value = "state", required = false) String state) {
		log.info("code=" + code + ",state=" + state);
		String openid = null;
		try {
			openid = wxHttpClient.getOpenId(code, state);
		} catch (Exception e) {
			return "redirect:/oauth/errmsg?msg=" + e.getMessage();
		}
		log.info("openId is " + openid);
		if (openid == null) {
			return "redirect:/oauth/error?type=1";
		}
		return "redirect:/index/index?openId=" + openid + "&mid=" + state;
	}

	/**
	 * 微信订单
	 * 
	 * @param code
	 * @param state
	 * @return
	 */
	@RequestMapping(value = "/orders")
	public String getOrders(@RequestParam(value = "code") String code,
			@RequestParam(value = "state", required = false) String state) {
		String openid = null;
		try {
			openid = wxHttpClient.getOpenId(code, state);
		} catch (Exception e) {
			return "redirect:/oauth/errmsg?msg=" + e.getMessage();
		}
		log.info("openId is " + openid);
		if (openid == null) {
			return "redirect:/oauth/error?type=1";
		}
		return "redirect:/wx_order_detail/my_order_detail?openId=" + openid
				+ "&mid=" + state;
	}

	/**
	 * 购物车
	 * 
	 * @param code
	 * @param state
	 * @return
	 */
	@RequestMapping(value = "/shopping_cart")
	public String shoppingCart(@RequestParam(value = "code") String code,
			@RequestParam(value = "state", required = false) String state) {
		String openid = null;
		try {
			openid = wxHttpClient.getOpenId(code, state);
		} catch (Exception e) {
			return "redirect:/oauth/errmsg?msg=" + e.getMessage();
		}
		log.info("openId is " + openid);
		if (openid == null) {
			return "redirect:/oauth/error?type=1";
		}
		return "redirect:/wx_shopping_cart/get_cart_list?openId=" + openid
				+ "&mid=" + state;
	}

	/**
	 * 提货券
	 * 
	 * @param code
	 * @param state
	 * @return
	 */
	@RequestMapping(value = "/vouchers")
	public String vouchers(@RequestParam(value = "code") String code,
			@RequestParam(value = "state", required = false) String state) {
		String openid = null;
		try {
			openid = wxHttpClient.getOpenId(code, state);
		} catch (Exception e) {
			return "redirect:/oauth/errmsg?msg=" + e.getMessage();
		}
		log.info("openId is " + openid);
		if (openid == null) {
			return "redirect:/oauth/error?type=1";
		}
		return "redirect:/product_voucher/product_voucher_list?openId="
				+ openid + "&mid=" + state;
	}

	/**
	 * 
	 * @param code
	 * @param state
	 * @return
	 */
	@RequestMapping(value = "/cards")
	public String cards(@RequestParam(value = "code") String code,
			@RequestParam(value = "state", required = false) String state,
			HttpServletRequest request) {
		String openid = null;
		try {
			openid = wxHttpClient.getOpenId(code, state);
		} catch (Exception e) {
			return "redirect:/oauth/errmsg?msg=" + e.getMessage();
		}
		log.info("openId is " + openid);
		if (openid == null) {
			return "redirect:/oauth/error?type=1";
		}
			
		return "redirect:/vip/vip_card?openId=" + openid + "&mid=" + state;
	}

	/**
	 * 门店信息
	 * 
	 * @param code
	 * @param state
	 * @return
	 */
	@RequestMapping(value = "/merchant_info")
	public String merchantInfo(@RequestParam(value = "code") String code,
			@RequestParam(value = "state", required = false) String state) {
		String openid = null;
		try {
			openid = wxHttpClient.getOpenId(code, state);
		} catch (Exception e) {
			return "redirect:/oauth/errmsg?msg=" + e.getMessage();
		}
		log.info("openId is " + openid);
		if (openid == null) {
			return "redirect:/oauth/error?type=1";
		} else {
			return "redirect:/merchant/info?mid=" + state + "&openId=" + openid;
		}
		
	}

	/**
	 * 红包入口, state是ticketDef的id
	 */
	@RequestMapping(value = "/hongbao_enter")
	public String hongbaoEnter(@RequestParam(value = "code") String code,
			@RequestParam(value = "state", required = false) String state) {
		log.info("code=" + code + ",state=" + state);

		TicketDefInfoInput input = new TicketDefInfoInput();
		input.setTicketDefIds(Arrays.asList(state));
		TicketDefInfoOutput output = (TicketDefInfoOutput) wizarposOpenRestClient.post(input, "/wechart/ticketdef/info", TicketDefInfoOutput.class);		
		MrtTicketDef ticketDef = null;
		if (output == null || SysConstants.OPEN_ERROR.equals(output.getCode())) {
			return "redirect:/oauth/error?type=0";
		} else {
			ticketDef  = output.getResult().get(0);
		}
		
		if (ticketDef == null) {
			return "redirect:/hongbao/enter?mid=&openId=";
		}
		String mid = ticketDef.getMid();
		String openid = null;
		try {
			openid = wxHttpClient.getOpenId(code, mid);
		} catch (Exception e) {
			return "redirect:/oauth/errmsg?msg=" + e.getMessage();
		}

		log.info("openId is " + openid);
		if (openid == null || openid.isEmpty()) {
			return "redirect:/oauth/error?type=1";
		}
		
		JSONObject result = wposHttpClient.getMerchantCardByOpenId(openid, mid);
		if (result.getIntValue("code") == SysConstants.NOCARD) {
			return "redirect:/hongbao/register?mid=" + mid + "&openId="
					+ openid;
		}

		if (ticketDef.getHbBonusNum() > ticketDef.getHbUseNum()) {
			return "redirect:/hongbao/detail?mid=" + mid + "&openId=" + openid
					+ "&ticketId=" + ticketDef.getId();
		} else {
			return "redirect:/hongbao/no_hongbao?mid=" + mid + "&openId="
					+ openid;
		}

	}

	/**
	 * 摇一摇入口, state是mid
	 */
	@RequestMapping(value = "/ibeacon")
	public String ibeaconEnter(@RequestParam(value = "code") String code,
			@RequestParam(value = "state", required = false) String state) {
		log.info("code=" + code + ",state=" + state);

		String openid = null;
		try {
			openid = wxHttpClient.getOpenId(code, state);
		} catch (Exception e) {
			return "redirect:/oauth/errmsg?msg=" + e.getMessage();
		}
		if (openid == null) {
			return "redirect:/oauth/error?type=1";
		}

		MrtIbeaconInput input = new MrtIbeaconInput();
		input.setMid(state);
		MrtIbeaconOutput output = (MrtIbeaconOutput) wizarposOpenRestClient.post(input, "/wxshop/ibeacon/info", MrtIbeaconOutput.class);		
		MrtIbeacon scene = null;
		if (output == null || SysConstants.OPEN_ERROR.equals(output.getCode())) {
			return "redirect:/oauth/error?type=0";
		} else if (SysConstants.NO_MERCHANT_CODE.equals(output.getCode())) {
			return "redirect:/oauth/error?type=7";
		} else {
			scene = output.getResult();
		}
		if (scene == null) {
			return "redirect:/oauth/error?type=2";
		}
		
		//获取对应商户的关注我页面
		String followUrl = scene.getAttentionUrl();
		
		String content = scene.getContent();// 场景内容
		String type = scene.getType(); // 场景类型
		if (content == null || content.isEmpty() || type == null
				|| type.isEmpty()) {
			return "redirect:/oauth/error?type=2";
		}

		if (ADVERTISEMENT.equals(type)) {// 广告投放
			return "redirect:" + content;
		}
		
		MrtWechartUserInfoInput userInput = new MrtWechartUserInfoInput();
		userInput.setMid(state);
		userInput.setOpenId(openid);
		MrtWechartUserInfoOutput userOutput = (MrtWechartUserInfoOutput) wizarposOpenRestClient.post(userInput, "/wxshop/userinfo", MrtWechartUserInfoOutput.class);	
		MrtWechartUserInfo user = null;
		if (userOutput == null || SysConstants.OPEN_ERROR.equals(userOutput.getCode())) {
			return "redirect:/oauth/error?type=0";
		} else if (SysConstants.NO_MERCHANT_CODE.equals(userOutput.getCode())) {
			return "redirect:/oauth/error?type=7";
		} else {
			user = userOutput.getResult();
		}
		
		if (SIGLE.equals(type)) {
			TicketDefInfoInput ticketInput = new TicketDefInfoInput();
			ticketInput.setTicketDefIds(Arrays.asList(content));
			TicketDefInfoOutput ticketOutput = (TicketDefInfoOutput) wizarposOpenRestClient.post(ticketInput, "/wechart/ticketdef/info", TicketDefInfoOutput.class);		
			MrtTicketDef ticketDef = null;
			if (ticketOutput == null || SysConstants.OPEN_ERROR.equals(ticketOutput.getCode())) {
				return "redirect:/oauth/error?type=0";
			} else if (SysConstants.NO_MERCHANT_CODE.equals(ticketOutput.getCode())) {
				return "redirect:/oauth/error?type=7";
			} else {
				ticketDef  = ticketOutput.getResult().get(0);
			}
			if (ticketDef == null) { // 券无效
				return "redirect:/oauth/error?type=3";
			}

			if (ticketDef.getWxFlag() == '1' && ticketDef.getWxCodeId() != null && user == null) {// 单张微信券，
				return "redirect:/ibeacon/toOneWx?wechat_card_js=1&mid="
						+ state + "&openId=" + openid + "&ticketDefId="
						+ content;
			}
		}

		if (user == null) {
			JSONObject userInfo = null;
			try {
				AccessTokenInput jsInput = new AccessTokenInput();
				jsInput.setMid(state);
				AccessTokenOutput jsOutput = (AccessTokenOutput) wizarposOpenRestClient.post(jsInput, "/weixin/accesstoken", AccessTokenOutput.class);		
				String assessToken = null;
				if (jsOutput == null || SysConstants.OPEN_ERROR.equals(jsOutput.getCode())) {
					return "redirect:/oauth/error?type=0";
				} else if (SysConstants.NO_MERCHANT_CODE.equals(jsOutput.getCode())) {
					return "redirect:/oauth/error?type=7";
				} else {
					Result apiTicket = jsOutput.getResult();
					if (apiTicket == null) {
						return "redirect:/oauth/error?type=0";
					}
					assessToken = apiTicket.getToken();
				}
				
				String userInfoUrl = SysConstants.USER_INFO_URL
						.replace(SysConstants.ACCESS_TOKEN,assessToken)
						.replace(SysConstants.OPENID, openid);
				userInfo = wposHttpClient.get(userInfoUrl);
			} catch (Exception e) {
				log.error(e.getMessage());
				return "redirect:/oauth/error?type=4";
			}

			if (userInfo.containsKey("errcode")) {
				log.error(userInfo.getString("errmsg"));
				return "redirect:/oauth/error?type=4";
			}

			if (UNSUBSCRIBE.equals(userInfo.getString("subscribe"))) {
				if (followUrl == null) {
					return "redirect:/oauth/error?type=6";
				}
				return "redirect:" + followUrl;
			}
			MrtWechartUserInfoUpdateInput useInput = new MrtWechartUserInfoUpdateInput();
			useInput.setPid(state);
			useInput.setOpenId(openid);
			useInput.setSubscribe("1");
			MrtWechartUserInfoUpdateOutput useOutput = (MrtWechartUserInfoUpdateOutput) wizarposOpenRestClient.post(useInput, "/wxshop/userinfoupdate", MrtWechartUserInfoUpdateOutput.class);				
			
		} else if (UNSUBSCRIBE.equals(user.getSubscribe())) {
			if (followUrl == null) {
				return "redirect:/oauth/error?type=6";
			}
			return "redirect:" + followUrl;
		}
		
		// 查询会员卡
		JSONObject result = wposHttpClient.getMerchantCardByOpenId(openid, state);
		int errcode = result.getIntValue("code");
		if (errcode == SysConstants.NOCARD) {// 快速注册
			try {
				result = wposHttpClient.registerVipCard(openid, state, "","");
			} catch (Exception e) {
				log.error(e.getMessage());
				return "redirect:/oauth/error?type=5";
			}
			int resultCode = result.getIntValue("code");
			if (resultCode != 0) {
				log.error("快速注册失败");
				return "redirect:/oauth/error?type=5";
			}
		}

		if (SIGLE.equals(type)) {// 单张券
			return "redirect:/ibeacon/toOne?wechat_card_js=1&mid=" + state
					+ "&openId=" + openid + "&ticketDefId=" + content;
		} else if (MULTIPLE.equals(type)) {// 多张券
			return "redirect:/ibeacon/toList?&mid=" + state + "&openId="
					+ openid + "&ticketDefId=" + content;
		}

		return "redirect:/oauth/error?type=5";
	}
	
	/**
	 * 第三方授权摇一摇入口, state是mid
	 */
	@RequestMapping(value = "/ibeacon4auth")
	public String ibeacon4auth(@RequestParam(value = "code") String code,
			@RequestParam(value = "state", required = false) String state) {
		log.info("code=" + code + ",state=" + state);

		String openid = null;
		try {
			openid = wxHttpClient.getOpenId(code, state);
		} catch (Exception e) {
			return "redirect:/oauth/errmsg?msg=" + e.getMessage();
		}
		if (openid == null) {
			return "redirect:/oauth/error?type=1";
		}

		MrtIbeaconInput input = new MrtIbeaconInput();
		input.setMid(state);
		MrtIbeaconOutput output = (MrtIbeaconOutput) wizarposOpenRestClient.post(input, "/wxshop/ibeacon/info", MrtIbeaconOutput.class);		
		MrtIbeacon scene = null;
		if (output == null || SysConstants.OPEN_ERROR.equals(output.getCode())) {
			return "redirect:/oauth/error?type=0";
		} else if (SysConstants.NO_MERCHANT_CODE.equals(output.getCode())) {
			return "redirect:/oauth/error?type=7";
		} else {
			scene = output.getResult();
		}
		if (scene == null) {
			return "redirect:/oauth/error?type=2";
		}
		
		String content = scene.getContent();// 场景内容
		String type = scene.getType(); // 场景类型
		if (content == null || content.isEmpty() || type == null
				|| type.isEmpty()) {
			return "redirect:/oauth/error?type=2";
		}

		if (ADVERTISEMENT.equals(type)) {// 广告投放
			return "redirect:" + content;
		}
		
		JSONObject result = wposHttpClient.getMerchantCardByOpenId(openid, state);
		if (result.getIntValue("code") == SysConstants.NOCARD) {// 快速注册
			try {
				result = wposHttpClient.registerVipCard(openid, state, "", "");
				if (result.getIntValue("code") != SysConstants.SUCCESS) {
					log.error(result.getString("msg"));
					return "redirect:/oauth/error?type=5";
				}
			} catch (Exception e) {
				log.error(e.getMessage());
				return "redirect:/oauth/error?type=5";
			}
			int resultCode = result.getIntValue("code");
			if (resultCode != 0) {
				log.error("快速注册会员失败");
				return "redirect:/oauth/error?type=5";
			}
		}
		
		if (SIGLE.equals(type)) {
			TicketDefInfoInput ticketInput = new TicketDefInfoInput();
			ticketInput.setTicketDefIds(Arrays.asList(content));
			TicketDefInfoOutput ticketOutput = (TicketDefInfoOutput) wizarposOpenRestClient.post(ticketInput, "/wechart/ticketdef/info", TicketDefInfoOutput.class);		
			MrtTicketDef ticketDef = null;
			if (output == null || SysConstants.OPEN_ERROR.equals(output.getCode())) {
				return "redirect:/oauth/error?type=0";
			} else {
				ticketDef = ticketOutput.getResult().get(0);
			}
			if (ticketDef == null || ticketDef.getWxFlag() != '1' || ticketDef.getWxCodeId() == null) {
				// 无效的微信券
				return "redirect:/oauth/error?type=3";
			}

			return "redirect:/ibeacon/onewx4auth?wechat_card_js=1&mid="
						+ state + "&openId=" + openid + "&ticketDefId="
						+ content;
		} else if (MULTIPLE.equals(type)) {// 多张券
			return "redirect:/ibeacon/toList?&mid=" + state + "&openId="
					+ openid + "&ticketDefId=" + content;
		}

		return "redirect:/oauth/error?type=5";
	}

	@RequestMapping(value = "/error")
	public ModelAndView error(int type) {
		if (1==type) {
			log.error("请求用户openid失败");
			return new ModelAndView("error").addObject("err_message",
					"请求用户openid失败,请重新操作,多次出现请联系商家");
		} else if (2==type) {
			log.error("ibeacon，场景未设置");
			return new ModelAndView("error").addObject("err_message",
					"ibeacon，场景未设置");
		} else if (3==type) {
			log.error("ibeacon，场景设置的券已失效");
			return new ModelAndView("error").addObject("err_message",
					"ibeacon，场景设置的券已失效");
		} else if (4==type) {
			log.error("ibeacon，请求用户信息失败");
			return new ModelAndView("error").addObject("err_message",
					"网络异常，请求用户信息失败");
		} else if (5==type) {
			log.error("快速注册失败");
			return new ModelAndView("error").addObject("err_message",
					"系统异常，快速注册用户失败");
		} else if (6==type) {
			log.error("ibeacon，关注我页面未设置");
			return new ModelAndView("error").addObject("err_message",
					"ibeacon，关注我页面未设置");
		} else if (7==type) {
			log.error("未知商户");
			return new ModelAndView("error").addObject("err_message",
					"未知商户");
		}
		
		log.error("ibeacon，系统错误");
		return new ModelAndView("error").addObject("err_message",
				"系统错误");
		
	}
	
	@RequestMapping(value = "/errmsg")
	public ModelAndView errmsg(String msg) {
		return new ModelAndView("error").addObject("err_message",
				msg);
	}
	
	@RequestMapping(value = "/adDemo")
	public ModelAndView ibeaconAdDemo() {
		return new ModelAndView("demo/test-page");
	}
	
	/**
	 * 餐饮点单入口
	 * 
	 * @param code
	 * @param state
	 * @return
	 */
	@RequestMapping(value = "/catering")
	public String cateringEnter(@RequestParam(value = "code") String code,
			@RequestParam(value = "state", required = false) String state) {
		log.info("code=" + code + ",state=" + state);
		String openid = null;
		try {
			openid = wxHttpClient.getOpenId(code, state);
		} catch (Exception e) {
			return "redirect:/oauth/errmsg?msg=" + e.getMessage();
		}
		
		log.info("openId is " + openid);
		if (openid == null) {
			return "redirect:/oauth/error?type=1";
		}
		return "redirect:/catering/tomenu?openId=" + openid + "&mid=" + state;
//		return "redirect:/index/ys?openId=" + openid + "&mid=" + state;
	}	
	
	/**
	 * 扫码后进入餐饮点单页
	 * 
	 * @param code
	 * @param state
	 * @return
	 */
	@RequestMapping(value = "/scan2menu")
	public String scan2menu(@RequestParam(value = "code") String code,
			@RequestParam(value = "state", required = false) String state) {
		log.info("code=" + code + ",state=" + state);
		String openid = null;
		try {
			openid = wxHttpClient.getOpenId(code, state);
		} catch (Exception e) {
			return "redirect:/oauth/errmsg?msg=" + e.getMessage();
		}
		log.info("openId is " + openid);
		if (openid == null) {
			return "redirect:/oauth/error?type=1";
		}
		return "redirect:/catering/scan2menu?openId=" + openid + "&mid=" + state;
	}
	
	/**
	 * The enter of yuding for WeChart menu setting
	 * 
	 * @param code
	 * @param state
	 * @return
	 */
	@RequestMapping(value = "/yuding")
	public String yuding(@RequestParam(value = "code") String code,
			@RequestParam(value = "state", required = false) String state) {
		log.info("code=" + code + ",state=" + state);
		String openid = null;
		try {
			openid = wxHttpClient.getOpenId(code, state);
		} catch (Exception e) {
			return "redirect:/oauth/errmsg?msg=" + e.getMessage();
		}
		log.info("openId is " + openid);
		if (openid == null) {
			return "redirect:/oauth/error?type=1";
		}
		return "redirect:/catering/yudingTable?mid=" + state + "&openId=" + openid;
	}
	
	/**
	 * The enter of yuding for WeChart menu setting
	 * 
	 * @param code
	 * @param state
	 * @return
	 */
	@RequestMapping(value = "/takeout")
	public String takeout(@RequestParam(value = "code") String code,
			@RequestParam(value = "state", required = false) String state) {
		log.info("code=" + code + ",state=" + state);
		String openid = null;
		try {
			openid = wxHttpClient.getOpenId(code, state);
		} catch (Exception e) {
			return "redirect:/oauth/errmsg?msg=" + e.getMessage();
		}
		log.info("openId is " + openid);
		if (openid == null) {
			return "redirect:/oauth/error?type=1";
		}
		return "redirect:/takeout/tomenu?mid=" + state + "&openId=" + openid;
	}
	
	/**
	 * The enter of service calling for WeChart menu setting
	 * 
	 * @param code
	 * @param state
	 * @return
	 */
	@RequestMapping(value = "/hujiao")
	public String hujiao(@RequestParam(value = "code") String code,
			@RequestParam(value = "state", required = false) String state) {
		log.info("code=" + code + ",state=" + state);
		String openid = null;
		try {
			openid = wxHttpClient.getOpenId(code, state);
		} catch (Exception e) {
			return "redirect:/oauth/errmsg?msg=" + e.getMessage();
		}
		log.info("openId is " + openid);
		if (openid == null) {
			return "redirect:/oauth/error?type=1";
		}
		return "redirect:/catering/tohujiao?mid=" + state + "&openId=" + openid;
	}

	/**
	 * catering order list
	 * 
	 * @param code
	 * @param state
	 * @return
	 */
	@RequestMapping(value = "/catering_list")
	public String cateringList(@RequestParam(value = "code") String code,
			@RequestParam(value = "state", required = false) String state) {
		log.info("code=" + code + ",state=" + state);
		String openid = null;
		try {
			openid = wxHttpClient.getOpenId(code, state);
		} catch (Exception e) {
			return "redirect:/oauth/errmsg?msg=" + e.getMessage();
		}
		log.info("openId is " + openid);
		if (openid == null) {
			return "redirect:/oauth/error?type=1";
		}
		return "redirect:/catering/myorders?openId=" + openid + "&mid=" + state;
	}
	
	/**
	 * catering reorder list.
	 * 餐饮预订单list查询
	 * 
	 * @param code
	 * @param state
	 * @return
	 */
	@RequestMapping(value = "/catering_re_list")
	public String cateringReList(@RequestParam(value = "code") String code,
			@RequestParam(value = "state", required = false) String state) {
		log.info("code=" + code + ",state=" + state);
		String openid = null;
		try {
			openid = wxHttpClient.getOpenId(code, state);
		} catch (Exception e) {
			return "redirect:/oauth/errmsg?msg=" + e.getMessage();
		}
		log.info("openId is " + openid);
		if (openid == null) {
			return "redirect:/oauth/error?type=1";
		}
		return "redirect:/catering/myreorders?openId=" + openid + "&mid=" + state;
	}
	
	/**
	 * dongzhixing H5页面摇一摇
	 * 
	 * @param code
	 * @param state
	 * @return
	 */
//	@RequestMapping(value = "/shake")
//	public String shake(@RequestParam(value = "code") String code,
//			@RequestParam(value = "state", required = false) String state) {
//		log.info("code=" + code + ",state=" + state);
//		String openid = null;
//		try {
//			openid = wxHttpClient.getOpenId(code, state);
//		} catch (Exception e) {
//			return "redirect:/oauth/errmsg?msg=" + e.getMessage();
//		}
//		log.info("openId is " + openid);
//		if (openid == null) {
//			return "redirect:/oauth/error?type=1";
//		}
//		return "redirect:/ibeacon/shake?openId=" + openid + "&mid=" + state;
//	}
	
	/**
	 * dongzhixing H5页面摇一摇周边
	 * 
	 * @param code
	 * @param state
	 * @return
	 */
	@RequestMapping(value = "/shake")
	public String shake(@RequestParam(value = "ticket") String ticket,
			@RequestParam(value = "mid", required = true) String mid) {
		log.info("ticket=" + ticket + ",mid=" + mid);
		String openid = null;
		try {
			openid = wxHttpClient.getShakeOpenId(ticket, mid);
		} catch (Exception e) {
			return "redirect:/oauth/errmsg?msg=" + e.getMessage();
		}
		log.info("openId is " + openid);
		if (openid == null) {
			return "redirect:/oauth/error?type=1";
		}
		return "redirect:/ibeacon/shake?openId=" + openid + "&mid=" + mid;
	}
	
	/**
	 * 东志信  H5页面摇一摇周边二维码版
	 * 
	 * @param code
	 * @param state
	 * @return
	 */
	@RequestMapping(value = "/qr_shake_zb")
	public String qrShakeZb(@RequestParam(value = "ticket") String ticket,
			@RequestParam(value = "mid", required = true) String mid) {
		log.info("ticket=" + ticket + ",mid=" + mid);
		String openid = null;
		try {
			openid = wxHttpClient.getShakeOpenId(ticket, mid);
		} catch (Exception e) {
			return "redirect:/oauth/errmsg?msg=" + e.getMessage();
		}
		log.info("openId is " + openid);
		if (openid == null) {
			return "redirect:/oauth/error?type=1";
		}
		return "redirect:/ibeacon/qr_shake?openId=" + openid + "&mid=" + mid;
	}
	
	/**
	 * 东志信  H5页面摇一摇二维码版
	 * 
	 * @param code
	 * @param state
	 * @return
	 */
	@RequestMapping(value = "/qr_shake")
	public String qrShake(@RequestParam(value = "code") String code,
			@RequestParam(value = "state", required = false) String state) {
		log.info("code=" + code + ",state=" + state);
		String openid = null;
		try {
			openid = wxHttpClient.getOpenId(code, state);
		} catch (Exception e) {
			return "redirect:/oauth/errmsg?msg=" + e.getMessage();
		}
		log.info("openId is " + openid);
		if (openid == null) {
			return "redirect:/oauth/error?type=1";
		}
		return "redirect:/ibeacon/qr_shake?openId=" + openid + "&mid=" + state;
	}
}
