package cn.congine.wizarpos.mall.model.open;

/**
 * @author GuZhenJuan
 *
 */

import java.util.List;

public class TicketUseLogOutput extends CommonOutput {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	List<TicketUseLog> result = null;

	public List<TicketUseLog> getResult() {
		return result;
	}

	public void setResult(List<TicketUseLog> result) {
		this.result = result;
	}
}
