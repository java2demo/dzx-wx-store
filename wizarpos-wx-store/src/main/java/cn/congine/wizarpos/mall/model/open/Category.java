package cn.congine.wizarpos.mall.model.open;

import java.io.Serializable;

public class Category implements Serializable {

	private static final long serialVersionUID = 1L;
	private String id = null;
	private String code = null;
	private String name = null;
	private String parentId = null;
	private String lastTime = null;
	private String storageRackFlag = null;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getLastTime() {
		return lastTime;
	}

	public void setLastTime(String lastTime) {
		this.lastTime = lastTime;
	}

	public String getStorageRackFlag() {
		return storageRackFlag;
	}

	public void setStorageRackFlag(String storageRackFlag) {
		this.storageRackFlag = storageRackFlag;
	}
}