package cn.congine.wizarpos.mall.service;

import cn.congine.wizarpos.mall.entity.MrtMerchantDef;

public interface MrtMerchantDefService {
	public MrtMerchantDef getSysMerchantDefByMid(String mid);
}
