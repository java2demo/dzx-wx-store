package cn.congine.wizarpos.mall.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import cn.congine.wizarpos.mall.model.BaseEntity;

/**
 * 活动领取记录流水表
 * 
 * @author Kirk Zhou
 * @date 2015-05-12
 * 
 */
@Entity
@Table(name = "my_online_activity_detail")
public class MyOnlineActivityDetail extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id; // 主键ID

	private String ticketMid;// 发券方商户号

	private Long amount;// 红包金额

	private String onlineActivityId;

	private String ticketContent;// 券明细

	private Date receiveTime;// 创建时间

	private String ticketType;// 券类型

	private String personOpenId;// 用户openId

	@Id
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@GeneratedValue(generator = "system-uuid")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "ticket_mid")
	public String getTicketMid() {
		return ticketMid;
	}

	public void setTicketMid(String ticketMid) {
		this.ticketMid = ticketMid;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	@Column(name = "online_activity_id")
	public String getOnlineActivityId() {
		return onlineActivityId;
	}

	public void setOnlineActivityId(String onlineActivityId) {
		this.onlineActivityId = onlineActivityId;
	}

	@Column(name = "ticket_content")
	public String getTicketContent() {
		return ticketContent;
	}

	public void setTicketContent(String ticketContent) {
		this.ticketContent = ticketContent;
	}

	@Column(name = "receive_time")
	public Date getReceiveTime() {
		return receiveTime;
	}

	public void setReceiveTime(Date receiveTime) {
		this.receiveTime = receiveTime;
	}

	@Column(name = "ticket_type")
	public String getTicketType() {
		return ticketType;
	}

	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}

	@Column(name = "person_open_id")
	public String getPersonOpenId() {
		return personOpenId;
	}

	public void setPersonOpenId(String personOpenId) {
		this.personOpenId = personOpenId;
	}

}
