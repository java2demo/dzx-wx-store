package cn.congine.wizarpos.mall.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.congine.wizarpos.mall.dao.WeiXinTemplateDao;
import cn.congine.wizarpos.mall.model.WeiXinTemplate;
import cn.congine.wizarpos.mall.service.WeiXinTemplateService;

@Service("weiXinTemplateService")
public class WeiXinTemplateServiceImpl implements WeiXinTemplateService {
	@Autowired
	private WeiXinTemplateDao templateDao;
	@Override
	public WeiXinTemplate getByTemplate(String mid, String templateNo) {
		return templateDao.getTemplate(mid, templateNo);
	}

}
