package cn.congine.wizarpos.mall.model.open;

import java.io.Serializable;

public class Table implements Serializable {

	private static final long serialVersionUID = 1L;
	private String tableId = null;
	private String tableCode = null;
	private String tableName = null;
	private String tableRange = null;
	private String tableDescn = null;

	public String getTableId() {
		return tableId;
	}

	public void setTableId(String tableId) {
		this.tableId = tableId;
	}

	public String getTableCode() {
		return tableCode;
	}

	public void setTableCode(String tableCode) {
		this.tableCode = tableCode;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getTableRange() {
		return tableRange;
	}

	public void setTableRange(String tableRange) {
		this.tableRange = tableRange;
	}

	public String getTableDescn() {
		return tableDescn;
	}

	public void setTableDescn(String tableDescn) {
		this.tableDescn = tableDescn;
	}
}
