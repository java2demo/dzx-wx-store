package cn.congine.wizarpos.mall.model.open;


public class ProductCategorySyncInput extends CommonInput {

	private static final long serialVersionUID = 1L;

	private String mid = null;
	private String parentId = null;
	private String lastReceivedTime = null;

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getLastReceivedTime() {
		return lastReceivedTime;
	}

	public void setLastReceivedTime(String lastReceivedTime) {
		this.lastReceivedTime = lastReceivedTime;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

}
