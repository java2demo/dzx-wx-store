package cn.congine.wizarpos.mall.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "mrt_ticket_def")
public class MrtTicketDef extends BaseEntity {
	private static final long serialVersionUID = -2423779636705430461L;

	// 唯一标识
	@Id
	@Column(name = "id")
	private String id;

	// 卡券码
	@Column(name = "ticket_code")
	private String ticketCode;

	// 慧商户ID
	@Column(name = "mid")
	private String mid;

	// 卡券名称
	@Column(name = "ticket_name")
	private String ticketName;

	// 卡券金额
	@Column(name = "balance")
	private String balance;

	// 是否启用 1为启用 ,审核通过0为不启用 2审核未通过
	@Column(name = "used_flag")
	private String usedFlag;

	// 有效期，单位：天。-1表示永久有效
	@Column(name = "valid_period")
	private String validPeriod;

	private int vp;//add xudongdong
	
	// 描述信息
	@Column(name = "description")
	private String description;

	// 添加时间
	@Column(name = "create_time")
	private String createTime;

	// state
	@Column(name = "state")
	private char state;

	// 微信券标识（1:微信卡券，0:非微信卡券）
	@Column(name = "wx_flag")
	private char wxFlag;

	// 微信卡券的code_id
	@Column(name = "wx_code_id")
	private String wxCodeId;

	// 红包发放数量
	@Column(name = "hb_bonus_num")
	private Integer hbBonusNum;

	// 红包领用数量
	@Column(name = "hb_use_num")
	private Integer hbUseNum;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTicketCode() {
		return ticketCode;
	}

	public void setTicketCode(String ticketCode) {
		this.ticketCode = ticketCode;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getTicketName() {
		return ticketName;
	}

	public void setTicketName(String ticketName) {
		this.ticketName = ticketName;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getUsedFlag() {
		return usedFlag;
	}

	public void setUsedFlag(String usedFlag) {
		this.usedFlag = usedFlag;
	}

	public String getValidPeriod() {
		return validPeriod;
	}

	public void setValidPeriod(String validPeriod) {
		this.validPeriod = validPeriod;
	}

	@Transient
	public int getVp() {
		return vp;
	}

	public void setVp(int vp) {
		this.vp = vp;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public char getState() {
		return state;
	}

	public void setState(char state) {
		this.state = state;
	}

	public char getWxFlag() {
		return wxFlag;
	}

	public void setWxFlag(char wxFlag) {
		this.wxFlag = wxFlag;
	}

	public String getWxCodeId() {
		return wxCodeId;
	}

	public void setWxCodeId(String wxCodeId) {
		this.wxCodeId = wxCodeId;
	}

	public Integer getHbBonusNum() {
		return hbBonusNum;
	}

	public void setHbBonusNum(Integer hbBonusNum) {
		this.hbBonusNum = hbBonusNum;
	}

	public Integer getHbUseNum() {
		return hbUseNum;
	}

	public void setHbUseNum(Integer hbUseNum) {
		this.hbUseNum = hbUseNum;
	}
}
