package cn.congine.wizarpos.mall.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.congine.wizarpos.mall.dao.ErpMerchantLoginDao;
import cn.congine.wizarpos.mall.model.ErpMerchantLogin;
import cn.congine.wizarpos.mall.service.ErpMerchantLoginService;

@Service("erpMerchantLoginService")
public class ErpMerchantLoginServiceImpl implements ErpMerchantLoginService {
	@Autowired
	private ErpMerchantLoginDao epMerchantLoginDao;

	@Override
	public ErpMerchantLogin save(ErpMerchantLogin obj) {
		return epMerchantLoginDao.save(obj);
	}

	@Override
	public Object count(String mid) {
		return epMerchantLoginDao.count(mid);
	}

}
