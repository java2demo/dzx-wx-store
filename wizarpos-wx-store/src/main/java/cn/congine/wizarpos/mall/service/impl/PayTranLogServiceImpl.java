package cn.congine.wizarpos.mall.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import cn.congine.wizarpos.mall.common.Page;
import cn.congine.wizarpos.mall.dao.PayTranLogDao;
import cn.congine.wizarpos.mall.model.PayTranLog;
import cn.congine.wizarpos.mall.service.PayTranLogService;

@Service("payTranLogService")
public class PayTranLogServiceImpl implements PayTranLogService {

	@Autowired
	private PayTranLogDao payTranLogDao;

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void save(PayTranLog obj) {
		payTranLogDao.save(obj);
	}

	@Override
	public List<PayTranLog> getCardTranLogByCardNo(String mid, String openId,
			Page<PayTranLog> page) {
		return payTranLogDao.getCardTranLogByCardNo(mid, openId, page);
	}

}
