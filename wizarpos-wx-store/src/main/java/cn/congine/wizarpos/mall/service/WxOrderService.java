package cn.congine.wizarpos.mall.service;

import java.util.List;

import cn.congine.wizarpos.mall.common.Page;
import cn.congine.wizarpos.mall.model.WxOrder;
import cn.congine.wizarpos.mall.vo.ProductVoucher;

public interface WxOrderService {
	void save(WxOrder obj);

	void update(WxOrder obj);

	void remove(String id);

	List<WxOrder> getWxOrderListByMid(String mid);

	List<WxOrder> getWxOrderListByOpenId(String openId);

	WxOrder getWxOrderByOrderId(String orderId);

	List<WxOrder> getWeekWxOrderListByOpenId(String openId, String mid,
			Integer type);

	// 根据页数获取提货券
	List<ProductVoucher> getVoWxOrderListByOpenId(String openId, String mid,
			Integer type);

	List<WxOrder> getWeekWxOrderListByOpenId(String openId, String mid,
			int type, Page<WxOrder> page);

	// 获取1月，3月，全部 订单
	List<WxOrder> getOrderListByType(String openId, String mid, int type,
			Page<WxOrder> page);

}
