package cn.congine.wizarpos.mall.dao.hibernate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.FlushMode;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import cn.congine.wizarpos.mall.common.Page;
import cn.congine.wizarpos.mall.dao.WxUserCaterSaleBindDao;
import cn.congine.wizarpos.mall.model.WxUserCaterSaleBind;
import cn.congine.wizarpos.mall.utils.Function;

@Repository("wxUserCaterSaleBindDao")
public class WxUserCaterSaleBindDaoHibernate extends
		GenericDaoHibernate<WxUserCaterSaleBind> implements
		WxUserCaterSaleBindDao {

	public WxUserCaterSaleBindDaoHibernate() {
		super(WxUserCaterSaleBind.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<WxUserCaterSaleBind> getByOpenId(String openId) {
		Query query = getSession()
				.createQuery(
						"from WxUserCaterBind where openId = :openId order by lastTime  desc")
				.setParameter("openId", openId);
		return (List<WxUserCaterSaleBind>) query.list();
	}

	@Override
	public List<WxUserCaterSaleBind> getByOpenId(String openId, int pageNo) {
		String hql = "from WxUserCaterBind where openId = :openId ";
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("openId", openId);

		Page<WxUserCaterSaleBind> page = new Page<WxUserCaterSaleBind>();
		page.setPageNo(pageNo);

		page = this.readAll4Page(hql.toString(), "", param, page);
		return page.getList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> getByOpenIdMid(String openId, String mid) {
		Query query = getSession()
				.createSQLQuery(
						"select e.order_no,o.status,o.pay_status,o.create_time,t.table_name,o.takeout_custom,o.takeout_tel,o.takeout_addr,o.order_id from erp_carter_sale_order o"
						+" LEFT JOIN erp_table t on o.table_id = t.table_id "
						+" LEFT JOIN wx_user_carter_sale_bind s  on o.order_id = s.carter_sale_order_id "
						+" LEFT JOIN erp_sale_order e on o.order_id = e.id"
						+" where carter_sale_order_id is not null and o.mid=:mid and s.open_id=:openId order by s.create_time DESC")
				.setParameter("mid", mid).setParameter("openId", openId);
		return (List<Object[]>) query.list();
	}

	@Override
	public List<WxUserCaterSaleBind> getByOpenIdMid(String openId, String mid,
			int pageNo) {

		String hql = "from WxUserCaterBind where mid=:mid and openId=:openId ";
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("openId", openId);
		param.put("mid", mid);

		Page<WxUserCaterSaleBind> page = new Page<WxUserCaterSaleBind>();
		page.setPageNo(pageNo);

		page = this.readAll4Page(hql.toString(), "", param, page);
		return page.getList();
	}

	@Override
	public WxUserCaterSaleBind save(WxUserCaterSaleBind obj) {
		String uuid = Function.getUid();
		obj.setId(uuid);
		String sql = "INSERT INTO wx_user_carter_sale_bind (id, mid, open_id, carter_sale_order_id, create_time) VALUES (?,?,?,?,?)";
		this.getSession().setFlushMode(FlushMode.MANUAL);
		Query query = this.getSession().createSQLQuery(sql);
		query.setString(0, uuid);
		query.setString(1, obj.getMid());
		query.setString(2, obj.getOpenId());
		query.setString(3, obj.getCarterSaleOrderId());
		query.setString(4,
				Function.formatDate(obj.getCreateTime(), "yyyy-MM-dd HH:mm:ss"));
		query.executeUpdate();
		return obj;
	}
}
