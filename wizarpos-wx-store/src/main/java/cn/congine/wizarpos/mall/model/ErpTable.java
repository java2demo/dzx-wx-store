package cn.congine.wizarpos.mall.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "erp_table")
public class ErpTable extends BaseEntity {
	private static final long serialVersionUID = 1L;

	// 唯一标识
	@Id
	@Column(name = "table_id")
	private String tableId;

	// 慧商户号
	@Column(name = "mid")
	private String mid;

	@Column(name = "table_no")
	private String tableNo;

	@Column(name = "table_mans_num")
	private String tableMansNum;

	@Column(name = "table_desc")
	private String tableDesc;

	@Column(name = "table_name")
	private String tableName;

	@Column(name = "delete_flag")
	private String deleteFlag;

	public String getTableId() {
		return tableId;
	}

	public void setTableId(String tableId) {
		this.tableId = tableId;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getTableNo() {
		return tableNo;
	}

	public void setTableNo(String tableNo) {
		this.tableNo = tableNo;
	}

	public String getTableMansNum() {
		return tableMansNum;
	}

	public void setTableMansNum(String tableMansNum) {
		this.tableMansNum = tableMansNum;
	}

	public String getTableDesc() {
		return tableDesc;
	}

	public void setTableDesc(String tableDesc) {
		this.tableDesc = tableDesc;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

}
