package cn.congine.wizarpos.mall.model.open;

import java.util.List;

import cn.congine.wizarpos.mall.model.MrtTicketDef;


public class TicketDefInfoOutput extends CommonOutput {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<MrtTicketDef> result = null;

	public List<MrtTicketDef> getResult() {
		return result;
	}

	public void setResult(List<MrtTicketDef> result) {
		this.result = result;
	}
	
}
