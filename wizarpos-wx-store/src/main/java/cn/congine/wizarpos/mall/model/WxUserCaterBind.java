package cn.congine.wizarpos.mall.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "wx_user_carter_bind")
public class WxUserCaterBind extends BaseEntity {

	private static final long serialVersionUID = 1L;

	// 唯一标识
	@Id
	@GeneratedValue(generator = "uuid")
	// 指定生成器名称
	@GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
	private String id;

	// 慧商户号
	@Column(name = "mid")
	private String mid;

	@Column(name = "carter_order_id")
	private String carterOrderId;

	@Column(name = "open_id")
	private String openId;

	@Column(name = "create_time")
	private Date createTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getCarterOrderId() {
		return carterOrderId;
	}

	public void setCarterOrderId(String carterOrderId) {
		this.carterOrderId = carterOrderId;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

}
