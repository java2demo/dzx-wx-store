package cn.congine.wizarpos.mall.model.open;

/**
 * 
 * @author xudongdong
 * Date: 2015.7.12
 */


public class AddToShoppingCartInput extends CommonInput {

	private static final long serialVersionUID = 1L;	
	
	private String openId = null;
	
	private String productId = null;

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}


}
