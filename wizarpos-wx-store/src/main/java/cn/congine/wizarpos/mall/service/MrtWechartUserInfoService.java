package cn.congine.wizarpos.mall.service;

import cn.congine.wizarpos.mall.model.MrtWechartUserInfo;

public interface MrtWechartUserInfoService {

	public MrtWechartUserInfo get(String openId, String mid);

	public void save(MrtWechartUserInfo obj);

	public void update(MrtWechartUserInfo obj);
}
