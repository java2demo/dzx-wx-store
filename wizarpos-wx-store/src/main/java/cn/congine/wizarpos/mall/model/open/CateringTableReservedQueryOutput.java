package cn.congine.wizarpos.mall.model.open;

import java.util.List;

public class CateringTableReservedQueryOutput extends CommonOutput{

	private static final long serialVersionUID = 1L;

	private List<Table> result = null;

	public List<Table> getResult() {
		return result;
	}

	public void setResult(List<Table> result) {
		this.result = result;
	}
}
