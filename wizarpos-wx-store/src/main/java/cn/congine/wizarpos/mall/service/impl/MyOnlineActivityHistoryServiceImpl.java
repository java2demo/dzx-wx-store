package cn.congine.wizarpos.mall.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.congine.wizarpos.mall.dao.MyOnlineActivityHistoryDao;
import cn.congine.wizarpos.mall.entity.MyOnlineActivityHistory;
import cn.congine.wizarpos.mall.service.MyOnlineActivityHistoryService;

@Service("myOnlineActivityHistoryService")
public class MyOnlineActivityHistoryServiceImpl implements MyOnlineActivityHistoryService {
	
	@Autowired
	private MyOnlineActivityHistoryDao myOnlineActivityHistoryDao;

	@Override
	public MyOnlineActivityHistory getMyOnlineActivityHistoryByOpenId(String openId,
			String onlineActivityId) {
		// TODO Auto-generated method stub
		return myOnlineActivityHistoryDao.getMyOnlineActivityHistoryByOpenId(openId, onlineActivityId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void update(MyOnlineActivityHistory object) {
		myOnlineActivityHistoryDao.update(object);
		
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void save(MyOnlineActivityHistory object) {
		myOnlineActivityHistoryDao.save(object);
	}

	

}
