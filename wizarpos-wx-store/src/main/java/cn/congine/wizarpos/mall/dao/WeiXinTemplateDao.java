package cn.congine.wizarpos.mall.dao;

import cn.congine.wizarpos.mall.model.WeiXinTemplate;

public interface WeiXinTemplateDao extends GenericDao<WeiXinTemplate> {
	public WeiXinTemplate getTemplate(String mid, String templateNo);
}
