package cn.congine.wizarpos.mall.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.congine.wizarpos.mall.dao.ErpTableDao;
import cn.congine.wizarpos.mall.model.ErpTable;
import cn.congine.wizarpos.mall.service.ErpTableService;

@Service("erpTableService")
public class ErpTableServiceImpl implements ErpTableService {
	@Autowired
	private ErpTableDao erpTableDao;

	@Override
	public List<ErpTable> getIdleTables(String mid, String tableIds) {
		return erpTableDao.getIdleTables(mid, tableIds);
	}

	@Override
	public ErpTable getTable(String mid, String tableId) {
		return erpTableDao.getTable(mid, tableId);
	}
}
