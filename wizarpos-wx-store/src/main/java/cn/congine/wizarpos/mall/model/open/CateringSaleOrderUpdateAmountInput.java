package cn.congine.wizarpos.mall.model.open;

public class CateringSaleOrderUpdateAmountInput extends CommonInput {

	private static final long serialVersionUID = 1L;
	private String orderId = null;
	private String mid = null;
	private String amount = null;
	
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getMid() {
		return mid;
	}
	public void setMid(String mid) {
		this.mid = mid;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	
}
