package cn.congine.wizarpos.mall.model.open;

import java.io.Serializable;
import java.util.List;

public class TableOrder implements Serializable {

	private static final long serialVersionUID = 1L;
	private String reOrderDate = null;
	private String reOrderName = null;
	private String reOrderPhone = null;
	private String reOrderTimeRange = null;
	private String reAmount = null;
	private String createTime = null;
	private String auditFlag= null;
	private String rejectReason= null;
	private String status = null;//add xudongdong	
	private String orderId = null;//add xudongdong
	private int orderMansNum;//add xudongdong
	private List<Table> tableList = null;

	public String getReOrderDate() {
		return reOrderDate;
	}

	public void setReOrderDate(String reOrderDate) {
		this.reOrderDate = reOrderDate;
	}

	public String getReOrderName() {
		return reOrderName;
	}

	public void setReOrderName(String reOrderName) {
		this.reOrderName = reOrderName;
	}

	public String getReOrderPhone() {
		return reOrderPhone;
	}

	public void setReOrderPhone(String reOrderPhone) {
		this.reOrderPhone = reOrderPhone;
	}

	public String getReOrderTimeRange() {
		return reOrderTimeRange;
	}

	public String getReAmount() {
		return reAmount;
	}

	public void setReAmount(String reAmount) {
		this.reAmount = reAmount;
	}

	public void setReOrderTimeRange(String reOrderTimeRange) {
		this.reOrderTimeRange = reOrderTimeRange;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getAuditFlag() {
		return auditFlag;
	}

	public void setAuditFlag(String auditFlag) {
		this.auditFlag = auditFlag;
	}

	public String getRejectReason() {
		return rejectReason;
	}

	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public int getOrderMansNum() {
		return orderMansNum;
	}

	public void setOrderMansNum(int orderMansNum) {
		this.orderMansNum = orderMansNum;
	}

	public List<Table> getTableList() {
		return tableList;
	}

	public void setTableList(List<Table> tableList) {
		this.tableList = tableList;
	}
}
