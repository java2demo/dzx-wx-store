package cn.congine.wizarpos.mall.model.open;

import java.util.List;
import java.util.Map;

import cn.congine.wizarpos.mall.vo.ProductVoucher;


public class ProductVoucherOutput extends CommonOutput {

	private static final long serialVersionUID = 1L;

	private List<ProductVoucher> result = null;

	public List<ProductVoucher> getResult() {
		return result;
	}

	public void setResult(List<ProductVoucher> result) {
		this.result = result;
	}
	
}
