package cn.congine.wizarpos.mall.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.congine.wizarpos.mall.dao.SysWxChildMerchantTokenDao;
import cn.congine.wizarpos.mall.model.SysWxChildMerchantToken;
import cn.congine.wizarpos.mall.service.SysWxChildMerchantTokenService;

@Service("sysWxChildMerchantTokenService")
public class SysWxChildMerchantTokenServiceImpl implements SysWxChildMerchantTokenService {
	@Autowired
	private SysWxChildMerchantTokenDao sysWxChildMerchantTokenDao;
	
	@Override
	public SysWxChildMerchantToken getByWxAppIdAndMidAndPrimaryId(String mid) {
		return sysWxChildMerchantTokenDao.getByMid(mid);
	}
	@Override
	public int updateMrtToken(SysWxChildMerchantToken t){
		return sysWxChildMerchantTokenDao.updateMrtToken(t);
	}
	@Override
	public int update(SysWxChildMerchantToken sysWxChildMerchantToken) {
		return sysWxChildMerchantTokenDao.update(sysWxChildMerchantToken.getMid(), sysWxChildMerchantToken.getAuthorizerAccessToken(), sysWxChildMerchantToken.getAuthorizerAccessTokenExpiry(), sysWxChildMerchantToken.getAuthorizerRefreshToken(), sysWxChildMerchantToken.getId());
	}

	@Override
	public SysWxChildMerchantToken findByGhCode(String gh) {
		return sysWxChildMerchantTokenDao.findByGhCode(gh);

	}
}
