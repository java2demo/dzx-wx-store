package cn.congine.wizarpos.mall.model.open;

import java.io.Serializable;

public class TicketUseLog implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String ticketName = null;
	private String num = null;
	private String balance = null;
	private String description = null;
	private String usedTime = null;
	public String getTicketName() {
		return ticketName;
	}
	public void setTicketName(String ticketName) {
		this.ticketName = ticketName;
	}
	public String getNum() {
		return num;
	}
	public void setNum(String num) {
		this.num = num;
	}
	public String getBalance() {
		return balance;
	}
	public void setBalance(String balance) {
		this.balance = balance;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUsedTime() {
		return usedTime;
	}
	public void setUsedTime(String usedTime) {
		this.usedTime = usedTime;
	}
	
}
