package cn.congine.wizarpos.mall.model.open;

import java.io.Serializable;

public class WechartOrderDetailInput extends CommonInput {

	private static final long serialVersionUID = 1L;

	private String mid = null;
	private String orderId = null;

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
}
