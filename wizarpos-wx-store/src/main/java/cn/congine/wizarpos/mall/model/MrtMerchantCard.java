package cn.congine.wizarpos.mall.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mrt_merchant_card")
public class MrtMerchantCard extends BaseEntity {
	private static final long serialVersionUID = -830909022031210611L;
	// 唯一标识
	@Id
	@Column(name = "id")
	private String id;
	// 主键
	@Column(name = "card_no")
	private String cardNo;

	// 卡类型
	@Column(name = "card_type")
	private String cardType;

	// 慧商户号
	@Column(name = "mid")
	private String mid;

	// 微信ID
	@Column(name = "open_id")
	private String openId;

	// 用户姓名
	@Column(name = "username")
	private String userName;

	// 手机号
	@Column(name = "mobile_no")
	private String mobileNo;

	// 激活时间
	@Column(name = "active_time")
	private Date activeTime;

	// 余额
	@Column(name = "balance")
	private Integer balance;

	// 过期时间
	@Column(name = "expriy_time")
	private Date expriyTime;

	// 注销标识 1：已注销，0：未注销
	@Column(name = "canceled")
	private boolean canceled;

	// 注销时间
	@Column(name = "cancel_time")
	private Date cancelTime;

	// 1：已冻结，0：未冻结
	@Column(name = "freeze")
	private boolean freeze;

	// 冻结时间
	@Column(name = "freeze_time")
	private Date freezeTime;

	// 添加时间
	@Column(name = "create_time")
	private Date createTime;

	@Column(name = "wx_bind_no")
	private String wxBindNo;

	@Column(name = "points")
	private Integer points;

	@Column(name = "total_points")
	private Integer totalPoints;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String username) {
		this.userName = username;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public Date getActiveTime() {
		return activeTime;
	}

	public void setActiveTime(Date activeTime) {
		this.activeTime = activeTime;
	}

	public Integer getBalance() {
		return balance;
	}

	public void setBalance(Integer balance) {
		this.balance = balance;
	}

	public Date getExpriyTime() {
		return expriyTime;
	}

	public void setExpriyTime(Date expriyTime) {
		this.expriyTime = expriyTime;
	}

	public boolean getCanceled() {
		return canceled;
	}

	public void setCanceled(boolean canceled) {
		this.canceled = canceled;
	}

	public Date getCancelTime() {
		return cancelTime;
	}

	public void setCancelTime(Date cancelTime) {
		this.cancelTime = cancelTime;
	}

	public boolean getFreeze() {
		return freeze;
	}

	public void setFreeze(boolean freeze) {
		this.freeze = freeze;
	}

	public Date getFreezeTime() {
		return freezeTime;
	}

	public void setFreezeTime(Date freezeTime) {
		this.freezeTime = freezeTime;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getWxBindNo() {
		return wxBindNo;
	}

	public void setWxBindNo(String wxBindNo) {
		this.wxBindNo = wxBindNo;
	}

	public Integer getPoints() {
		return points;
	}

	public void setPoints(Integer points) {
		this.points = points;
	}

	public Integer getTotalPoints() {
		return totalPoints;
	}

	public void setTotalPoints(Integer totalPoints) {
		this.totalPoints = totalPoints;
	}

}
