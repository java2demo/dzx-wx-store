package cn.congine.wizarpos.mall.dao;

import cn.congine.wizarpos.mall.model.SysParamsDef;

public interface SysParamsDefDao extends GenericDao<SysParamsDef> {

}
