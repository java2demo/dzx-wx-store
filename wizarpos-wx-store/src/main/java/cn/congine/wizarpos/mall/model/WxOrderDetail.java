package cn.congine.wizarpos.mall.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "wx_order_detail")
public class WxOrderDetail extends BaseEntity {
	private static final long serialVersionUID = -5323523394263268387L;
	// 编号
	@Id
	@Column(name = "id")
	private String id;

	// 商户号
	@Column(name = "mid")
	private String mid;

	// 订单编号
	@Column(name = "order_id")
	private String orderId;

	// 商品编号
	@Column(name = "product_id")
	private String productId;

	// 商品代码
	@Column(name = "code")
	private String code;

	// 商品名称
	@Column(name = "product_name")
	private String productName;

	// 付款金额
	@Column(name = "amount")
	private Integer amount;

	// 单价
	@Column(name = "price")
	private Integer price;

	// 数量
	@Column(name = "qty")
	private Integer qty;

	// 最后修改时间
	@Column(name = "last_time")
	private Long lastTime;
	//显示会员价
	@Transient
	private String picUrl;
	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Integer getQty() {
		return qty;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}

	public Long getLastTime() {
		return lastTime;
	}

	public void setLastTime(Long lastTime) {
		this.lastTime = lastTime;
	}

}
