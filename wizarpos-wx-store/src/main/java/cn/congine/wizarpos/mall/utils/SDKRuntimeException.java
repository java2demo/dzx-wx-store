package cn.congine.wizarpos.mall.utils;

public class SDKRuntimeException extends Exception {

	private static final long serialVersionUID = 1L;

	public SDKRuntimeException(String str) {
		super(str);
	}
}
