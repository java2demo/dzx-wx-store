package cn.congine.wizarpos.mall.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.congine.wizarpos.mall.common.Page;
import cn.congine.wizarpos.mall.dao.ErpProductDao;
import cn.congine.wizarpos.mall.model.ErpProduct;
import cn.congine.wizarpos.mall.service.ErpProductService;

@Service("erpProductService")
public class ErpProductServiceImpl implements ErpProductService {

	@Autowired
	private ErpProductDao erpProductDao;

	@Override
	public ErpProduct getErpProductById(String id) {
		return erpProductDao.get(id);
	}

	@Override
	public List<ErpProduct> getErpProductListByMid(String mid, String categoryId) {
		return erpProductDao.getErpProductListByMid(mid, categoryId);
	}
	
	@Override
	public List<ErpProduct> getErpProductListByPage(String mid, String categoryId, int pageIndex) {
		return erpProductDao.getErpProductListByPage(mid, categoryId, pageIndex);
	}
	
	@Override
	public List<ErpProduct> getHotErpProductListByMid(String mid,
			String hotJudge) {
		return erpProductDao.getHotErpProductListByMid(mid, hotJudge);
	}

	@Override
	public List<ErpProduct> getHotErpProductListByCategoryId(String mid,
			String hotJudge, String categoryId) {
		return erpProductDao.getHotErpProductListByCategoryId(mid, hotJudge,
				categoryId);
	}

	@Override
	public List<ErpProduct> getProductList(String mid, String categoryId,
			String kyeWord) {
		return erpProductDao.getProductList(mid, categoryId, kyeWord);
	}

	@Override
	public List<ErpProduct> getHotErpProductListByMid(String mid,
			String hotJudge, String categoryId) {
		return erpProductDao.getHotErpProductListByCategoryId(mid, hotJudge,
				categoryId);
	}

	@Override
	public List<ErpProduct> getProductList(String mid, String keyWord) {
		return erpProductDao.getProductList(mid, keyWord);
	}

	@Override
	public List<ErpProduct> getProudctLit(String mid, Page<ErpProduct> page,
			String categoryId, String keyword) {
		return erpProductDao.getProductList(mid, page, categoryId, keyword);
	}

	@Override
	public List<ErpProduct> getErpProductPageListByMid(String mid,
			String categoryId, Integer pageIndex) {
		return erpProductDao.getErpProductPageListByMid(mid, categoryId, pageIndex);
	}
}
