package cn.congine.wizarpos.mall.model.open;

import java.io.Serializable;

public class MerchantDef implements Serializable {

	private static final long serialVersionUID = 1L;

    private String mid = null;
	
	private String agentId = null;
	
	/**
	 * 收款通知开关
	 */
	private int collectNotifyMark ;

	/**
	 * 创建时间
	 */
	private String createTime = null;

	/**
	 * 商户LOGO
	 */
	private String logoImage = null;

	/**
	 * 收单商户号
	 */
	private String merchantId = null;

	/**
	 * 商户名称
	 */
	private String merchantName = null;

	/**
	 * 支付通道
	 */
	private int payId;

	/**
	 * 销售通知开关
	 */
	private int saleNotifyMark ;

	/**
	 * 商户是否可用标记 0 正常 1 不可用
	 */
	private String validFlag = null;
	/**
	 * 
	 * 慧商户编号，从1开始顺序增加,每次加1。与自发行卡的卡号9-15位
	 */
	private int sequenceNo ;

	/**
	 * 门店相关说明
	 */
	private String shopDesc = null;

	/**
	 * 微信会员卡开始编号
	 */
	private int startNo ;

	/**
	 * 商铺模板:1货架式模板 2全部商铺式模板
	 */
	private int storesTemplates ;

	/**
	 * 使用慧银的支付宝配置参数
	 */
	private int useWizarposAlipayConfig;

	/**
	 * 使用慧银的微信配置参数
	 */
	private int useWizarposWeixinPayConfig;

	/**
	 * vip正面图片
	 */
	private String vipFrontImage = null;

	/**
	 * vip图片
	 */
	private String vipImage = null;

	/**
	 * 微信号
	 */
	private String pOpenId = null;

	/**
	 * 慧商户公众号openid
	 */
	private String mOpenId = null;

	private String weixinAppId = null;


	private String weixinAppSecret = null;


	private String weixinMchId = null;

	// 微信商户平台支付密钥
	private String weixinPartnerKey = null;


	private String weixinAppKey = null;

	// 公众号的全局唯一票据，调用各接口时都需使用
	private String wxAccessToken = null;

	// 唯一票据的申请时间戳
	private Long wxAccessTokenTimestamp = null;

	private Integer dayOrderSeq = null;

	/**
	 * 商户地址
	 */
	private String merchantAddr = null;
	/**
	 * 商户电话
	 */
	private String merchantTel = null;
	
	private Long lastTime = null;

	/**
	 * 商户背景图
	 */
	private String bannerImage;//add xudongdong
	
	// 经度
	private String lon;//add xudongdong

	// 纬度
	private String lat;//add xudongdong
	
	// 商户类型 1零售2餐饮3服务
	private String merchantType;//add xudongdong	
	
	/*
	 * 序号每天重置时间
	 */
	private String orderSeqResetTime;
	
	private String weixinCertFilePath;
	
	public String getOrderSeqResetTime()
	{
		return orderSeqResetTime;
	}

	public void setOrderSeqResetTime(String orderSeqResetTime)
	{
		this.orderSeqResetTime = orderSeqResetTime;
	}
	
	public String getWeixinAppId() {
		return weixinAppId;
	}

	public void setWeixinAppId(String weixinAppId) {
		this.weixinAppId = weixinAppId;
	}

	public String getWeixinAppSecret() {
		return weixinAppSecret;
	}

	public void setWeixinAppSecret(String weixinAppSecret) {
		this.weixinAppSecret = weixinAppSecret;
	}

	public String getWeixinMchId() {
		return weixinMchId;
	}

	public void setWeixinMchId(String weixinMchId) {
		this.weixinMchId = weixinMchId;
	}

	public String getWeixinPartnerKey() {
		return weixinPartnerKey;
	}

	public void setWeixinPartnerKey(String weixinPartnerKey) {
		this.weixinPartnerKey = weixinPartnerKey;
	}

	public String getWeixinAppKey() {
		return weixinAppKey;
	}

	public void setWeixinAppKey(String weixinAppKey) {
		this.weixinAppKey = weixinAppKey;
	}

	public String getWxAccessToken() {
		return wxAccessToken;
	}

	public void setWxAccessToken(String wxAccessToken) {
		this.wxAccessToken = wxAccessToken;
	}

	public Long getWxAccessTokenTimestamp() {
		return wxAccessTokenTimestamp;
	}

	public void setWxAccessTokenTimestamp(Long wxAccessTokenTimestamp) {
		this.wxAccessTokenTimestamp = wxAccessTokenTimestamp;
	}

	public Integer getDayOrderSeq() {
		return dayOrderSeq;
	}

	public void setDayOrderSeq(Integer dayOrderSeq) {
		this.dayOrderSeq = dayOrderSeq;
	}

	public String getMerchantAddr() {
		return merchantAddr;
	}

	public void setMerchantAddr(String merchantAddr) {
		this.merchantAddr = merchantAddr;
	}

	public String getMerchantTel() {
		return merchantTel;
	}

	public void setMerchantTel(String merchantTel) {
		this.merchantTel = merchantTel;
	}

	public Long getLastTime() {
		return lastTime;
	}

	public void setLastTime(Long lastTime) {
		this.lastTime = lastTime;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public int getCollectNotifyMark() {
		return collectNotifyMark;
	}

	public void setCollectNotifyMark(int collectNotifyMark) {
		this.collectNotifyMark = collectNotifyMark;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getLogoImage() {
		return logoImage;
	}

	public void setLogoImage(String logoImage) {
		this.logoImage = logoImage;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public int getPayId() {
		return payId;
	}

	public void setPayId(int payId) {
		this.payId = payId;
	}

	public int getSaleNotifyMark() {
		return saleNotifyMark;
	}

	public void setSaleNotifyMark(int saleNotifyMark) {
		this.saleNotifyMark = saleNotifyMark;
	}

	public String getValidFlag() {
		return validFlag;
	}

	public void setValidFlag(String validFlag) {
		this.validFlag = validFlag;
	}

	public int getSequenceNo() {
		return sequenceNo;
	}

	public void setSequenceNo(int sequenceNo) {
		this.sequenceNo = sequenceNo;
	}

	public String getShopDesc() {
		return shopDesc;
	}

	public void setShopDesc(String shopDesc) {
		this.shopDesc = shopDesc;
	}

	public int getStartNo() {
		return startNo;
	}

	public void setStartNo(int startNo) {
		this.startNo = startNo;
	}

	public int getStoresTemplates() {
		return storesTemplates;
	}

	public void setStoresTemplates(int storesTemplates) {
		this.storesTemplates = storesTemplates;
	}

	public int getUseWizarposAlipayConfig() {
		return useWizarposAlipayConfig;
	}

	public void setUseWizarposAlipayConfig(int useWizarposAlipayConfig) {
		this.useWizarposAlipayConfig = useWizarposAlipayConfig;
	}

	public int getUseWizarposWeixinPayConfig() {
		return useWizarposWeixinPayConfig;
	}

	public void setUseWizarposWeixinPayConfig(int useWizarposWeixinPayConfig) {
		this.useWizarposWeixinPayConfig = useWizarposWeixinPayConfig;
	}

	public String getVipFrontImage() {
		return vipFrontImage;
	}

	public void setVipFrontImage(String vipFrontImage) {
		this.vipFrontImage = vipFrontImage;
	}

	public String getVipImage() {
		return vipImage;
	}

	public void setVipImage(String vipImage) {
		this.vipImage = vipImage;
	}

	public String getpOpenId() {
		return pOpenId;
	}

	public void setpOpenId(String pOpenId) {
		this.pOpenId = pOpenId;
	}

	public String getmOpenId() {
		return mOpenId;
	}

	public void setmOpenId(String mOpenId) {
		this.mOpenId = mOpenId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getBannerImage() {
		return bannerImage;
	}

	public void setBannerImage(String bannerImage) {
		this.bannerImage = bannerImage;
	}

	public String getLon() {
		return lon;
	}

	public void setLon(String lon) {
		this.lon = lon;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getWeixinCertFilePath() {
		return weixinCertFilePath;
	}

	public void setWeixinCertFilePath(String weixinCertFilePath) {
		this.weixinCertFilePath = weixinCertFilePath;
	}

	public String getMerchantType() {
		return merchantType;
	}

	public void setMerchantType(String merchantType) {
		this.merchantType = merchantType;
	}

}
