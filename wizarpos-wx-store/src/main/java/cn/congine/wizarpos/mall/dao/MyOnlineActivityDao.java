package cn.congine.wizarpos.mall.dao;

import cn.congine.wizarpos.mall.entity.MyOnlineActivity;

public interface MyOnlineActivityDao extends GenericDao<MyOnlineActivity> {

	   MyOnlineActivity getMyOnlineActivityByMid(String mid);

}
