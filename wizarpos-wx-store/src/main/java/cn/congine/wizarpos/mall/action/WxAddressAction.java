package cn.congine.wizarpos.mall.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.congine.wizarpos.mall.common.RespMessage;
import cn.congine.wizarpos.mall.model.WxAddress;
import cn.congine.wizarpos.mall.model.open.Address;
import cn.congine.wizarpos.mall.model.open.AddressDetailInput;
import cn.congine.wizarpos.mall.model.open.AddressDetailOutput;
import cn.congine.wizarpos.mall.model.open.AddressEditInput;
import cn.congine.wizarpos.mall.model.open.AddressEditOutput;
import cn.congine.wizarpos.mall.model.open.AddressGainInput;
import cn.congine.wizarpos.mall.model.open.AddressGainOutput;
import cn.congine.wizarpos.mall.service.impl.WizarposOpenRestClient;
import cn.congine.wizarpos.mall.utils.SysConstants;
import cn.congine.wizarpos.mall.utils.Utils;

@Controller
@RequestMapping(value = "/wx_address")
public class WxAddressAction {
	private Logger logger = Logger.getLogger(WxAddressAction.class);

	@Autowired
	private WizarposOpenRestClient wizarposOpenRestClient;
	
	@RequestMapping(value = "/select")
	public ModelAndView myaddress(
			@RequestParam(value = "cardId") String cardId,
			@RequestParam(value = "openId") String openId,
			@RequestParam(value = "mid") String mid) {
		Map<String, Object> data = new HashMap<String, Object>();
		// 需要验证会员卡是否存在
		if (StringUtils.isEmpty(cardId)) {
			data.put("err_message", "会员卡编号不存在");
			return new ModelAndView("error", data);
		}
		AddressGainInput addressGainInput = new AddressGainInput();
		addressGainInput.setMid(mid);
		addressGainInput.setOpenId(openId);
		AddressGainOutput addressGainOutput = (AddressGainOutput) wizarposOpenRestClient.post(addressGainInput, "/address/gain", AddressGainOutput.class);		
		List<Address> list = addressGainOutput.getResult();		
		
		data.put("list", list);
		data.put("cardId", cardId);
		data.put("openId", openId);
		data.put("mid", mid);
		return new ModelAndView("address/myaddress", data);
	}
		
	@RequestMapping(value = "/selectAdd")
	public ModelAndView selectAddress(@RequestParam(value = "cardId") String cardId,
			@RequestParam(value = "openId") String openId,
			@RequestParam(value = "mid") String mid,
			@RequestParam(required = false) String productId,
			@RequestParam(required = false) String cartId,
			@RequestParam(required = false) String orderId) {
		Map<String, Object> data = new HashMap<String, Object>();
		// 需要验证会员卡是否存在
		if (StringUtils.isEmpty(cardId)) {
			data.put("err_message", "会员卡编号不存在");
			return new ModelAndView("error", data);
		}
		AddressGainInput addressGainInput = new AddressGainInput();
		addressGainInput.setMid(mid);
		addressGainInput.setOpenId(openId);
		AddressGainOutput addressGainOutput = (AddressGainOutput) wizarposOpenRestClient.post(addressGainInput, "/address/gain", AddressGainOutput.class);		
		List<Address> list = addressGainOutput.getResult();	
		
		data.put("list", list);
		data.put("cardId", cardId);
		data.put("openId", openId);
		data.put("mid", mid);
		data.put("productId", productId);
		data.put("cartId", cartId);
		data.put("orderId", orderId);
		return new ModelAndView("address/selectAddress", data);
	}


	/**
	 * 收货地址删除
	 * @param id
	 * @param cardId
	 * @param openId
	 * @param mid
	 * @param orderId
	 * @param productId
	 * @param cartId
	 * @return
	 * @author xudongdong
	 */
	@RequestMapping(value = "/del")
	public ModelAndView delAddress(
			@RequestParam(value = "id") String id,
			@RequestParam(value = "cardId") String cardId,
			@RequestParam(value = "openId") String openId,
			@RequestParam(value = "mid") String mid,
			@RequestParam(value = "orderId", required = false) String orderId,
			@RequestParam(value = "productId", required = false) String productId,
			@RequestParam(value = "cartId", required = false) String cartId) {
		AddressEditInput addressEditInput = new AddressEditInput();
		addressEditInput.setId(id);
		addressEditInput.setMid(mid);
		addressEditInput.setOpenId(openId);
		addressEditInput.setOperateType("D");
		AddressEditOutput addressEditOutput = (AddressEditOutput) wizarposOpenRestClient.post(addressEditInput, "/address/edit", AddressEditOutput.class);
		
		Map<String, Object> data = new HashMap<String, Object>();
		AddressGainInput addressGainInput = new AddressGainInput();
		addressGainInput.setMid(mid);
		addressGainInput.setOpenId(openId);
		AddressGainOutput addressGainOutput = (AddressGainOutput) wizarposOpenRestClient.post(addressGainInput, "/address/gain", AddressGainOutput.class);		
		List<Address> list = addressGainOutput.getResult();		
		data.put("list", list);
		data.put("cardId", cardId);
		data.put("openId", openId);
		data.put("mid", mid);
		if ((orderId != null && !orderId.isEmpty())
				|| (productId != null && !productId.isEmpty())
				|| (cartId != null && !cartId.isEmpty())) {
			data.put("orderId", orderId);
			data.put("productId", productId);
			data.put("cartId", cartId);
			return new ModelAndView("address/selectAddress", data);
		}
		return new ModelAndView("address/myaddress", data);
	}
	
	//新增或修改
	@RequestMapping(value = "/to_add")
	public ModelAndView toAddress(
			@RequestParam(value = "cardId") String cardId,
			@RequestParam(value = "openId") String openId,
			@RequestParam(value = "orderId", required = false) String orderId,
			@RequestParam(value = "mid") String mid,
			@RequestParam(value = "cartId", required = false) String cartId,
			@RequestParam(value = "productId", required = false) String productId,
			@RequestParam(value = "id", required = false) String id,
			HttpServletRequest httpRequest) {
		Map<String, Object> data = new HashMap<String, Object>();
		logger.debug("进入添加地址页面  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		// 需要验证会员卡是否存在
		if (StringUtils.isEmpty(cardId)) {
			data.put("err_message", "会员卡编号不存在");
			return new ModelAndView("error", data);
		}
		boolean flag = false;//标记是否为老地址修改
		AddressDetailInput in = new AddressDetailInput();
		in.setMid(mid);
		in.setId(id);
		AddressDetailOutput out = (AddressDetailOutput) wizarposOpenRestClient.post(in, "/address/detail", AddressDetailOutput.class);								
		WxAddress address = null;
		if (SysConstants.OPEN_SUCCESS.equals(out.getCode())) {
			address = (WxAddress) out.getResult();
		}
		
		if (address != null && !"".equals(address)) {
			flag = true;//原有地址，标记为修改
			data.put("id", id);
			data.put("wx", address);
			String addr = address.getAddress();
			String[] addressArr = (addr != null) ? addr.split(" ") : new String[0];
			data.put("province", addressArr.length > 0 ? addressArr[0] : "");
			data.put("city", addressArr.length > 1 ? addressArr[1] : "");
			data.put("county", addressArr.length > 2 ? addressArr[2] : "");
			StringBuffer sb = new StringBuffer();
			for (int i=3; i < addressArr.length; i++) {
				sb.append(addressArr[i]);
				sb.append(" ");
			}
			data.put("address", sb.toString());
		}
		data.put("orderId", orderId);
		data.put("cardId", cardId);
		data.put("openId", openId);
		data.put("mid", mid);
		data.put("flag", flag);
		data.put("productId", productId);

		Utils.setSessionToken(httpRequest);

		return new ModelAndView("address/addaddress", data);
	}
	
	@RequestMapping("/addAddressView")
	public ModelAndView addAddressView(@ModelAttribute("mid") String mid)
	{
		if (!StringUtils.isEmpty(mid)) {
			return new ModelAndView("address/addaddress");
		}
		return new ModelAndView("index/index");
	}

	@RequestMapping(value = "/add")
	@ResponseBody
	public RespMessage addAddress(
			@RequestParam(value = "cardId", required = false) String cardId,
			@RequestParam(value = "mid") String mid,
			@RequestParam(value = "openId") String openId,
			@RequestParam(value = "userName", required = false) String userName,
			@RequestParam(value = "mPhone", required = false) String mPhone,
			@RequestParam(value = "defaultState", required = false) String defaultState,
			@RequestParam(value = "address" , required = false) String address,
			@RequestParam(value = "mZip", required = false) String mZip,
			@RequestParam(value = "id", required = false) String id,
			@RequestParam(value = "cartId", required = false) String cartId,
			@RequestParam(value = "productId", required = false) String productId,
			@RequestParam(value = "orderId", required = false) String orderId,
			HttpServletRequest httpRequest) {
		RespMessage res = new RespMessage();
		
		if (Utils.isRepeatSubmit(httpRequest)) {
			res.setCode(4);
			res.setMessage(" 请勿重复提交");
			return res;
		}
		Map<String, Object> data = new HashMap<String, Object>();
		
		AddressEditInput addressEditInput = new AddressEditInput();
		addressEditInput.setId(id);
		addressEditInput.setMid(mid);
		addressEditInput.setOpenId(openId);
		addressEditInput.setAddress(address);
		if ("1".equals(defaultState)) {
			addressEditInput.setDefaultState("1");
		} else {
			addressEditInput.setDefaultState("0");
		}		
		addressEditInput.setPhone(mPhone);
		addressEditInput.setUserName(userName);
		addressEditInput.setZipCode(mZip);
		if (id == null || id.isEmpty()) {//新增地址
			addressEditInput.setOperateType("I");			
		} else {//修改原有地址
			addressEditInput.setOperateType("U");
		}
		AddressEditOutput addressEditOutput = (AddressEditOutput) wizarposOpenRestClient.post(addressEditInput, "/address/edit", AddressEditOutput.class);
		
		AddressGainInput addressGainInput = new AddressGainInput();
		addressGainInput.setMid(mid);
		addressGainInput.setOpenId(openId);
		AddressGainOutput addressGainOutput = (AddressGainOutput) wizarposOpenRestClient.post(addressGainInput, "/address/gain", AddressGainOutput.class);		
		List<Address> list = addressGainOutput.getResult();				
		
		data.put("list", list);
		data.put("cardId", cardId);
		data.put("openId", openId);
		data.put("mid", mid);
		
		if ((orderId != null && !orderId.isEmpty())
				|| (productId != null && !productId.isEmpty())
				|| (cartId != null && !cartId.isEmpty())) {
			data.put("orderId", orderId);
			data.put("cartId", cartId);
			data.put("productId", productId);			
			//return new ModelAndView("address/selectAddress", data);
			res.setCode(1);
			res.setMessage("");
			res.setObj(data);
			return res;			
		}
		res.setCode(0);
		res.setMessage("");
		res.setObj(data);
		return res;		
	}
	
	@RequestMapping(value = "/error")
	@ResponseBody
	public ModelAndView error(String msg) {
		return new ModelAndView("error").addObject("err_message", msg);
	}
}
