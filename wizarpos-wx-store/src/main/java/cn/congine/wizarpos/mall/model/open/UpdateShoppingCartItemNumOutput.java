package cn.congine.wizarpos.mall.model.open;

/**
 * 
 * @author xudongdong
 * Date: 2015.7.4
 */

import java.util.List;
import cn.congine.wizarpos.mall.vo.WxShoppingCartItem;

public class UpdateShoppingCartItemNumOutput extends CommonOutput {

	private static final long serialVersionUID = 1L;

/*	private Result result = null;

	public Result getResult() {
		return result;
	}

	public void setResult(Result result) {
		this.result = result;
	}

	public class Result {
		
		private String cartId = null;
		
		private List<WxShoppingCartItem> wxShoppingCartItemList = null;

		public String getCartId() {
			return cartId;
		}

		public void setCartId(String cartId) {
			this.cartId = cartId;
		}

		public List<WxShoppingCartItem> getWxShoppingCartItemList() {
			return wxShoppingCartItemList;
		}

		public void setWxShoppingCartItemList(
				List<WxShoppingCartItem> wxShoppingCartItemList) {
			this.wxShoppingCartItemList = wxShoppingCartItemList;
		}
	
	}*/
	
	private String cartId = null;
	
	private Object obj = null;
	
	private List<WxShoppingCartItem> wxShoppingCartItemList = null;

	public String getCartId() {
		return cartId;
	}

	public void setCartId(String cartId) {
		this.cartId = cartId;
	}

	public Object getObj() {
		return obj;
	}

	public void setObj(Object obj) {
		this.obj = obj;
	}

	public List<WxShoppingCartItem> getWxShoppingCartItemList() {
		return wxShoppingCartItemList;
	}

	public void setWxShoppingCartItemList(
			List<WxShoppingCartItem> wxShoppingCartItemList) {
		this.wxShoppingCartItemList = wxShoppingCartItemList;
	}
}


