package cn.congine.wizarpos.mall.dao.hibernate;

import org.hibernate.FlushMode;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import cn.congine.wizarpos.mall.dao.SysInTicketDao;
import cn.congine.wizarpos.mall.model.SysInTicket;

@Repository("sysInTicketDao")
public class SysInTicketDaoHibernate extends GenericDaoHibernate<SysInTicket>
		implements SysInTicketDao {

	public SysInTicketDaoHibernate() {
		super(SysInTicket.class);
	}
	
	@Override
	public SysInTicket getSysInTicket(String apiSymbol, String mid) {
		Query query = getSession()
				.createQuery(
						"from SysInTicket where apiSymbol=:apiSymbol and mid=:mid")
				.setParameter("apiSymbol", apiSymbol)
				.setParameter("mid", mid);
		return (SysInTicket) query.uniqueResult();
	}

	@Override
	public void update(SysInTicket t) {
		String sql = "update sys_in_ticket set wx_ticket = ?, expiry_time = ?, api_symbol = ? "
				+ "where  mid = ?";
		getSession().setFlushMode(FlushMode.MANUAL);
		Query q = getSession().createSQLQuery(sql);
		q.setString(0, t.getWxTicket());
		q.setLong(1, t.getExpiryTime());
		q.setString(2, t.getApiSymbol());
		q.setString(3, t.getMid());
		q.executeUpdate();
	}

	@Override
	public SysInTicket save(SysInTicket t) {
		String sql = "INSERT INTO sys_in_ticket (wx_ticket, mid, expiry_time, api_symbol)"
				+ "VALUES (?, ?, ?, ?);";
		getSession().setFlushMode(FlushMode.MANUAL);
		Query q = getSession().createSQLQuery(sql);

		q.setString(0, t.getWxTicket());
		q.setString(1, t.getMid());
		q.setLong(2, t.getExpiryTime());
		q.setString(3, t.getApiSymbol());

		q.executeUpdate();

		return t;
	}

	@Override
	public SysInTicket find(String wxTicket, String mid) {
		return (SysInTicket) getSession()
				.createQuery("from SysInTicket where mid=:mid and wxTicket=:wxTicket")
				.setString("mid", mid).setString("wxTicket", wxTicket).uniqueResult();
	}

	@Override
	public void del(SysInTicket obj) {
		String hql = "delete From SysInTicket where wxTicket = ? and mid=?";
		this.getSession().setFlushMode(FlushMode.MANUAL);
		Query q = getSession().createQuery(hql);
		q.setString(0, obj.getWxTicket());
		q.setString(1, obj.getMid());
		q.executeUpdate();
	}

}
