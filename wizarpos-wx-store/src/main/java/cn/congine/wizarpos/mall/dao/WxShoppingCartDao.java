package cn.congine.wizarpos.mall.dao;

import java.util.List;

import cn.congine.wizarpos.mall.model.WxShoppingCart;

public interface WxShoppingCartDao extends GenericDao<WxShoppingCart> {

	List<WxShoppingCart> getWxShoppingCartByOpenId(String openId);

	WxShoppingCart getWxShoppingCart(String openId, String mid);

	WxShoppingCart getWxShoppingCartByMId(String mid);

	void remove(String id, String mid);
}
