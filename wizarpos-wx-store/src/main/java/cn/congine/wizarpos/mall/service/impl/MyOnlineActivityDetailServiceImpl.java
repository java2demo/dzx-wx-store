package cn.congine.wizarpos.mall.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.congine.wizarpos.mall.dao.MyOnlineActivityDetailDao;
import cn.congine.wizarpos.mall.entity.MyOnlineActivityDetail;
import cn.congine.wizarpos.mall.service.MyOnlineActivityDetailService;

@Service("myOnlineActivityDetailService")
public class MyOnlineActivityDetailServiceImpl implements MyOnlineActivityDetailService {
	
	@Autowired
	private MyOnlineActivityDetailDao myOnlineActivityDetailDao;
	
	@Override
	public List<MyOnlineActivityDetail> getMyOnlineActivityDetailList(
			String mid, String onlineActivityId) {

		return myOnlineActivityDetailDao.getMyOnlineActivityDetailList(mid, onlineActivityId);
	}

	@Override
	public MyOnlineActivityDetail getMyOnlineActivityDetail(String mid,
			String onlineActivityId, String openId) {

		return myOnlineActivityDetailDao.getMyOnlineActivityDetail(mid, onlineActivityId,openId);
	}

	@Override
	public List<String> getMyOnlineActivityDetailTicketMidList(String openId,
			String onlineActivityId) {
		// TODO Auto-generated method stub
		return myOnlineActivityDetailDao.getMyOnlineActivityDetailTicketMidList(openId, onlineActivityId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void saveMyOnlineActivityDetail(
			MyOnlineActivityDetail myOnlineActivityDetail) {
		
		myOnlineActivityDetailDao.save(myOnlineActivityDetail);
		
	}


	
}
