package cn.congine.wizarpos.mall.model.open;

import java.util.List;

public class TicketDefInfoInput extends CommonInput {
	private static final long serialVersionUID = 1L;
	
	private List<String> ticketDefIds = null;

	public List<String> getTicketDefIds() {
		return ticketDefIds;
	}

	public void setTicketDefIds(List<String> ticketDefIds) {
		this.ticketDefIds = ticketDefIds;
	}
	
}
