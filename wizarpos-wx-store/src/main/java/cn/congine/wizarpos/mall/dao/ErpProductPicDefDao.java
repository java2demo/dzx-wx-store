package cn.congine.wizarpos.mall.dao;

import cn.congine.wizarpos.mall.model.ErpProductPicDef;

public interface ErpProductPicDefDao extends GenericDao<ErpProductPicDef> {

	ErpProductPicDef getErpProductPicDefById(String id);

}
