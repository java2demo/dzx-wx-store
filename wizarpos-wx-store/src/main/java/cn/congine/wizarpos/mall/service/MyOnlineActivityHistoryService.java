package cn.congine.wizarpos.mall.service;

import cn.congine.wizarpos.mall.entity.MyOnlineActivityHistory;



public interface MyOnlineActivityHistoryService {
   
	MyOnlineActivityHistory getMyOnlineActivityHistoryByOpenId(String openId,String onlineActivityId);
	
	void update(MyOnlineActivityHistory object);
	
	void save(MyOnlineActivityHistory object);
}
