package cn.congine.wizarpos.mall.model.open;

/**
 * 
 * @author xudongdong
 * Date: 2015.7.29
 */


public class TicketDefUpdateInput extends CommonInput {

	private static final long serialVersionUID = 1L;
	
	// 公众号的gh号
	private String ghCode = null;
	
	//微信卡券ID
	private String wxCodeId = null;

	//是否启用 0:无效 1:有效，审核通过 2:审核未通过
	private String usedFlag = null;
	
	public String getGhCode() {
		return ghCode;
	}

	public void setGhCode(String ghCode) {
		this.ghCode = ghCode;
	}

	public String getWxCodeId() {
		return wxCodeId;
	}

	public void setWxCodeId(String wxCodeId) {
		this.wxCodeId = wxCodeId;
	}

	public String getUsedFlag() {
		return usedFlag;
	}

	public void setUsedFlag(String usedFlag) {
		this.usedFlag = usedFlag;
	}


}
