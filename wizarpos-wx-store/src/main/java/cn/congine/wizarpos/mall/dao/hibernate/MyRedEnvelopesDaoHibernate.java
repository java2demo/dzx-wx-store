package cn.congine.wizarpos.mall.dao.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import cn.congine.wizarpos.mall.dao.MyRedEnvelopesDao;
import cn.congine.wizarpos.mall.entity.MyRedEnvelopes;

@Repository("myRedEnvelopesDao")
public class MyRedEnvelopesDaoHibernate extends
		GenericDaoHibernate<MyRedEnvelopes> implements MyRedEnvelopesDao {

	public MyRedEnvelopesDaoHibernate() {
		super(MyRedEnvelopes.class);
	}

	@Override
	public List<MyRedEnvelopes> getMyRedEnvelopesList(String mid,
			String onlineActivityId) {

		Query query = getSession()
				.createQuery(
						"from MyRedEnvelopes where mid = :mid and myOnlineActivity.id=:onlineActivityId")
				.setParameter("mid", mid)
				.setParameter("onlineActivityId", onlineActivityId);
		return (List<MyRedEnvelopes>) query.list();

	}

	@Override
	public MyRedEnvelopes getMyRedEnvelopes(String mid,
			String onlineActivityId) {
		List<MyRedEnvelopes> list =getMyRedEnvelopesList(mid, onlineActivityId);
		if (list != null && !list.isEmpty()) {
			return list.get(0);
		}
		return null;
	}
	
	@Override
	public void delete(MyRedEnvelopes object) {
		Query query = getSession().createSQLQuery(
						"delete from my_red_envelopes where id=:id")
				.setParameter("id", object.getId());
		query.executeUpdate();
	}

	@Override
	public MyRedEnvelopes getByMidAndMyShakeId(String mid, String myShakeId) {
		Query query = getSession()
				.createQuery(
						"from MyRedEnvelopes where mid =:mid and myShake.id=:myShakeId")
				.setParameter("mid", mid)
				.setParameter("myShakeId", myShakeId);
		List<MyRedEnvelopes> list = (List<MyRedEnvelopes>) query.list();
		
		if (list != null && !list.isEmpty()) {
			return list.get(0);
		}
		
		return null;
	}
	
}
