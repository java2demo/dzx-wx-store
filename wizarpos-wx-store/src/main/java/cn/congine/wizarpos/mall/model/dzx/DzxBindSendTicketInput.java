package cn.congine.wizarpos.mall.model.dzx;

import cn.congine.wizarpos.mall.model.open.CommonInput;

/**
 * 东志信联盟商户二维码摇一摇关注送券事件, 送券input
 * 
 * @author xudongdong
 *         2015年8月24日 上午9:33:19
 */
public class DzxBindSendTicketInput extends CommonInput {

	private static final long serialVersionUID = 1L;
	// 微信号
	private String openId = null;
	// 小票的二维码
	private String ticketId = null;
	
	public String getOpenId() {
		return openId;
	}
	public void setOpenId(String openId) {
		this.openId = openId;
	}
	public String getTicketId() {
		return ticketId;
	}
	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}

}
