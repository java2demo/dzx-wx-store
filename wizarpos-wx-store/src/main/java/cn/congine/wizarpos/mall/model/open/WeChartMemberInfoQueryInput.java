package cn.congine.wizarpos.mall.model.open;

public class WeChartMemberInfoQueryInput extends CommonInput {

	private static final long serialVersionUID = 1L;

	private String openId = null;
	private String mid = null;
	private String cardNo = null;

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getCardNo() {
		return cardNo;
	}

	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}

}
