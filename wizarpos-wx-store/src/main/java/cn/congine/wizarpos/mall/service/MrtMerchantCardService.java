package cn.congine.wizarpos.mall.service;

import cn.congine.wizarpos.mall.model.MrtMerchantCard;

public interface MrtMerchantCardService {
	MrtMerchantCard getMrtMerchantCardByCardNo(String cardNo);

	MrtMerchantCard getMrtMerchantCardByCardId(String id);

	MrtMerchantCard getMrtMerchantCard(String mid, String openId);

	void update(MrtMerchantCard obj);
}
